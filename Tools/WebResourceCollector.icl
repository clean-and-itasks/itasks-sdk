module WebResourceCollector

/**
* This program collects and creates all the necessary public web resources that an iTasks program
* needs to run. These are for example, static javascript files, images and the html launch page.
*
* To find bundled public resources it looks in a project file which modules are
* used by a program and then checks for each module if a directory called "<modulename>-webpublic" exists
* if it exists, the full contents of that directory are copied to the collection directory called "<applicationname>-webpublic"
*
* It also creates an aggregated css file with additional style rules that are needed by Clean modules.
* If for an imported Clean module a file exists with the same name, but a .css extension it is included in the aggregation.
* The total collected css is written to "<applicationname>-webpublic/css/itasks-modules.css"
*
* To always bundle the right versions of web resources, this program must be run after every build in the link phase.
*/

import StdEnv
import StdMaybe

import Control.Monad
import Data.Error
import Data.List
import Data.Tuple
import System.CommandLine
import System.Directory
import System.File
import System.FilePath
import System.OS
import System.OSError
import System.SysCall
from Text import class Text(split), instance Text String, concat3, concat5
import Text.GenJSON
import iTasks.Internal.Util

FILE_EXISTS :== IF_WINDOWS 183 17

:: Options =
	{ output :: !FilePath
	, inputs :: ![FilePath]
	}

:: NodeModulesToCopy =
	{ copy :: ![(FilePath, FilePath)]
	}

derive JSONDecode NodeModulesToCopy

Start :: *World -> *World
Start w
	// Parse command-line options and options file
	# (argv,w) = getCommandLine w
	# mbOptions = parseOptions (tl argv)
	| isError mbOptions = abort (fromError mbOptions +++ "\n")
	# options = fromOk mbOptions
	  outDir = exePathToOutputDir options.output
	  inDirs = objectPathsToInputDirs options.inputs
	  nodeModuleListFiles = objectPathsToNodeModulesListPaths options.inputs
	  (nodeModuleDirectories,w) = objectPathsToNodeModulesDirectories options.inputs w
	  cssParts = objectPathsToCSSFiles options.inputs
	  cssFile = outDir </> "css" </> "itasks-modules.css"
	# w = log ("Output css file " +++ cssFile) w
	// Create output dir and 'css' dir in it
	# (mbErr,w) = createDirectory outDir w
	| not (mbErr =: (Ok _) || mbErr =: (Error (FILE_EXISTS,_))) // Ignore 'File exists' errors
		= abort ("Error creating directory " +++ outDir +++ ": " +++ toString (fromError mbErr) +++ "\n") w
	# (mbErr,w) = createDirectory (outDir </> "css") w
	| not (mbErr =: (Ok _) || mbErr =: (Error (FILE_EXISTS,_))) // Ignore 'File exists' errors
		= abort ("Error creating directory " +++ outDir </> "css" +++ ": " +++ toString (fromError mbErr) +++ "\n") w
	// Create the aggregated css file
	# (mbErr,w) = writeFile cssFile "" w
	# w = foldr (\f w -> aggregateCSS f cssFile w) w cssParts
	// Copy the contents of the input dirs if they exist
	# w = foldr (\d w -> copyWebResources d outDir w) w inDirs
	// Copy node modules
	# w = foldr (\f w -> copyNodeModules f nodeModuleDirectories outDir w) w nodeModuleListFiles
	= w
where
	parseOptions :: ![String] -> MaybeError String Options
	parseOptions opts =
		parse opts ?None [] >>= \(mbOutput, inputs) ->
		case mbOutput of
			?None -> Error "No output file given"
			?Just output -> Ok {output=output, inputs=reverse inputs}
	where
		parse [] output inputs = Ok (output, inputs)
		parse ["-o"] _ _ = Error "-o without argument"
		parse ["-o",output:rest] _ inputs = parse rest (?Just output) inputs
		parse [input:rest] output inputs = parse rest output [input:inputs]

	// Determine the output folder where the web resources should be copied to
	exePathToOutputDir :: FilePath-> FilePath
	exePathToOutputDir path = takeDirectory path </> appNameFor path +++ "-www"

	// Determine the potential input folders and css fragments
	objectPathsToInputDirs :: [FilePath] -> [FilePath]
	objectPathsToInputDirs paths = removeDup (concatMap rewrite paths)
	where
		rewrite path =
			[ dropExtension (dropCleanSystemFiles path) +++ "-WebPublic"
			// Transitional location of WebPublic files, they should eventually be linked directory to specific modules dash
			, dropCleanSystemFiles (takeDirectory path) </> "WebPublic"
			]

	objectPathsToNodeModulesListPaths :: ![FilePath] -> [FilePath]
	objectPathsToNodeModulesListPaths paths = removeDup (map rewrite paths)
	where
		rewrite path = dropExtension (dropCleanSystemFiles path) +++ "-WebPublic-node_modules.json"

	// Find node_modules directories. We expect these in root directories, but accept them also in directories of submodules.
	// Because the linkopts file does not contain root directories, we have to traverse up the tree as long as the directory
	// name is a valid Clean module name. This may yield some false positives: node_modules directories in a directory higher
	// than the Clean project.
	objectPathsToNodeModulesDirectories :: ![FilePath] !*World -> (![FilePath], !*World)
	objectPathsToNodeModulesDirectories paths w
		# paths = removeDup (concatMap rewrite paths)
		= foldr
			(\d (found,w) -> let (e,w`) = fileExists d w in (if e [d:found] found,w`))
			([],w)
			paths
	where
		rewrite = map (flip (</>) "node_modules") o takeWhile (isValidModuleName 0 o dropDirectory) o iterate takeDirectory

		isValidModuleName i s
			| i >= size s
				= i > 0
			# c = s.[i]
			| i == 0
				= isAlpha c || c == '_' && isValidModuleName 1 s
				= isAlphanum c || c == '_' || c == '`' && isValidModuleName (i+1) s

	objectPathsToCSSFiles :: [FilePath] -> [FilePath]
	objectPathsToCSSFiles paths = map rewrite paths
	where
		rewrite path = addExtension (dropExtension (dropCleanSystemFiles path)) "css"

	dropCleanSystemFiles :: !FilePath -> FilePath
	dropCleanSystemFiles path = concatPaths (filter ((<>) "Clean System Files") (split {pathSeparator} path))

// Print a debug message
log :: !String !*World -> *World
log msg w
	# (io,w) = stdio w
	# io = io <<< "iTasks Resource Collector: " <<< msg <<< "\n"
	# (_,w) = fclose io w
	= w

// Copy the web resources if the input directory exists
copyWebResources :: !FilePath !FilePath !*World -> *World
copyWebResources indir outdir w
	# (dir,w) = isDirectory indir w
	| dir
		# w = log ("Copying resources from "+++indir) w
		= copyDirectoryContent indir outdir w
	| otherwise = w

copyNodeModules :: !FilePath ![FilePath] !FilePath !*World -> *World
copyNodeModules listFile nodeModulesDirs outDir w
	# (exists,w) = fileExists listFile w
	| not exists
		= w
	# w = log ("Copying node modules from "+++listFile) w
	# (mbContents,w) = readFile listFile w
	| isError mbContents
		= abort (concat5 "Error reading " listFile ": " (toString (fromError mbContents)) "\n")
	# mbContents = fromJSON (fromString (fromOk mbContents))
	| isNone mbContents
		= abort (concat3 "Corrupt node modules list file: " listFile "\n")
		= foldr (copy nodeModulesDirs) w (fixFilePaths (fromJust mbContents)).copy
where
	fixFilePaths :: !NodeModulesToCopy -> NodeModulesToCopy
	fixFilePaths options = {options & copy=map (\(s,d) -> (fix s, fix d)) options.copy}
	where
		fix = concatPaths o split "/" // we use /, even on Windows

	copy :: ![FilePath] !(!FilePath, !FilePath) !*World -> *World
	copy [] (src,_) w = abort (concat3 "File referenced in node modules list could not be found: " src "\n")
	copy [node_modules:other_node_modules] origSrcDst=:(src,dst) w
		# this_src = node_modules </> src
		  dst = outDir </> dst
		# (dir,w) = isDirectory this_src w
		| dir
			# (mbErr,w) = ensureDirectoryExists dst w
			| isError mbErr && not (mbErr=:(Error (FILE_EXISTS, _)))
				= abort (concat5 "Error creating directory " dst ": " (toString (fromError mbErr)) "\n")
				= copyDirectoryContent this_src dst w
		# (exists,w) = fileExists this_src w
		| not exists
			= copy other_node_modules origSrcDst w
		# (mbErr,w) = ensureDirectoryExists (takeDirectory dst) w
		| isError mbErr && not (mbErr=:(Error (FILE_EXISTS, _)))
			= abort (concat5 "Error creating directory " dst ": " (toString (fromError mbErr)) "\n")
			= copyFile this_src dst False w

aggregateCSS :: !FilePath !FilePath !*World -> *World
aggregateCSS inFile outFile w
	# (exists,w) = fileExists inFile w
	| exists
		# w = log ("Adding css file "+++inFile) w
		= copyFile inFile outFile True w
	| otherwise = w

//GENERAL UTIL, SHOULD BE PART OF PLATFORM
isDirectory :: !FilePath !*World -> (!Bool, !*World)
isDirectory path w = case getFileInfo path w of
	(Ok {FileInfo|directory},w) = (directory,w)
	(_,w)                       = (False,w)

copyDirectoryContent :: !FilePath !FilePath !*World -> *World
copyDirectoryContent indir outdir w
	= case readDirectory indir w of
		(Ok items,w) = foldr (copyItem indir outdir) w items
		(_,w) = w
where
	copyItem indir outdir item w
		| item == "." || item == ".." = w
		# (dir,w) = isDirectory (indir </> item) w
		| dir //Create the target directory and recursively copy content
			# (mbErr,w) = createDirectory (outdir </> item) w
			| isError mbErr && not (mbErr =: (Error (FILE_EXISTS, _)))
				= abort ("Error creating directory " +++ outdir </> item +++ ": " +++ toString (fromError mbErr) +++ "\n")
			= copyDirectoryContent (indir </> item) (outdir </> item) w
		| otherwise //Copy the file
			= copyFile (indir </> item) (outdir </> item) False w

copyFile :: !FilePath !FilePath !Bool !*World -> *World
copyFile inf outf append w
	# (ok,inh,w)    = fopen inf FReadData w
	| not ok
		# (e, w) = getLastOSError w
		= abort ("Error opening " +++ outf +++ ": " +++ toString (fromError e) +++ "\n")
	# (ok,outh,w)   = fopen outf (if append FAppendData FWriteData) w
	| not ok
		# (e, w) = getLastOSError w
		= abort ("Error opening " +++ outf +++ ": " +++ toString (fromError e) +++ "\n")
	# (inh,outh) = copy inh outh
	# (_,w) = fclose inh w
	# (_,w) = fclose outh w
	= w
where
	copy inh outh
		# (string, inh) = freads inh 1000
		| string == "" = (inh, outh)
		| otherwise
			# outh = fwrites string outh
			= copy inh outh
