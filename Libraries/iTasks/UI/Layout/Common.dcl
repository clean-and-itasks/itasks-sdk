definition module iTasks.UI.Layout.Common
/**
* This module provides common alternative layout annotations
* that you can apply at strategic points in your task specifications
* to optimize the user experience for specific tasks
*/
import iTasks.UI.Layout
from iTasks.UI.Definition import :: UISide(..), :: UIDirection(..), :: UIWindowType(..), :: UIHAlign(..), :: UIVAlign(..)
from iTasks.WF.Definition import :: Task
from iTasks.UI.Tune import class tune

/**
* Add a CSS class to customize styling
*/
addCSSClass :: String -> LayoutRule

/**
* Remove a CSS class (to prevent standard styling)
*/
removeCSSClass :: String -> LayoutRule

/**
* Create a tabset with all child items as separate tabs
* The flag denotes whether close buttons should be lifted to the tabs
*/
arrangeWithTabs :: Bool -> LayoutRule

/**
* Extract one child item and put it in a separate 'header' panel on top of the screen
*
* @param Index of the task in the set that will be used as header
*/
arrangeWithHeader :: !Int -> LayoutRule

/**
* Extract one child item and put it in a separate panel at the side of the screen
*
* @param Index of the task in the set that should be put in the sidebar
* @param Location of the sidebar
* @param Enable resize?
*/
arrangeWithSideBar :: !Int !UISide !Bool -> LayoutRule

:: Menu =: Menu String

/**
 * Arrange task continuations as a menu which contains the task continuations.
 *
 * Example:
 * (viewInformation [] "Pick a menu option"
 * 		>>* [OnAction (Action "/File/New") (always $ return ()), OnAction (Action "/Edit/Cut") (always $ return ())]
 * ) <<@ ArrangeAsMenu [!Menu "/File", Menu "/Edit"]
 *
 * Creates two top-level menus (File, Edit) and adds the action /File/New to the File menu.
 * Similarly, the action /Edit/Cut is added to the Edit menu. This is done based on the name of the action.
 * and the menu's that were provided as an argument to arrangeAsMenu.
 *
 * When you want to add an action to a menu, specify the menu as an argument to arrangeAsMenu and make the
 * action name start with the name of the top level menu as is done in the example. The directory / symbols are
 * important as they specify that the action is a menu item instead of a regular item.
 * The names of the top-level menus should start with a / as well.
 *
 * arrangeAsMenu does not support nested menus.

 * @param The top level menus to add (e.g: [!Menu "/File", Menu "/Edit"])
 */
arrangeAsMenu :: ![!Menu] -> LayoutRule

/**
* Divide the available screen space
*
* @param Direction to split the available space in
* @param Enable resize?
*/
arrangeSplit :: !UIDirection !Bool -> LayoutRule

/**
*  Turn current UI into a panel and set direction to vertical.
*/
arrangeVertical :: LayoutRule

/**
*  Turn current UI into a panel and set direction to vertical.
*/
arrangeHorizontal :: LayoutRule

/**
* Turn the UI into a wrapping framed container inside a general container
*
* Use this is if you don't want to use the entire viewport
*/
frameCompact :: LayoutRule

/**
* Adds a CSS class that indicates that content of a panel should scroll.
*/
scrollContent :: LayoutRule

//* Wraps a task in a panel, if needed, and makes it collapsible by clicking on the panel header.
collapsible :: LayoutRule

/**
* Add a tool bar and move selected actions to it
*/
insertToolBar :: [String] -> LayoutRule

/**
* Add a button bar and move selected actions to it.
* Useful for when actions have to be moved from the toolbar to the buttonbar.
*/
insertButtonBar :: [String] -> LayoutRule

/**
 * Add a UI element and move selected actions to it
 */
insertUIElement :: UIType [String] -> LayoutRule

//Convenient annotatation types
:: ArrangeWithTabs = ArrangeWithTabs Bool
instance tune ArrangeWithTabs (Task a)

:: ArrangeWithSideBar = ArrangeWithSideBar !Int !UISide !Bool
instance tune ArrangeWithSideBar (Task a)

:: ArrangeWithHeader = ArrangeWithHeader !Int
instance tune ArrangeWithHeader (Task a)

:: ArrangeAsMenu =: ArrangeAsMenu [!Menu]
instance tune ArrangeAsMenu (Task a)

:: ArrangeSplit = ArrangeSplit !UIDirection !Bool
instance tune ArrangeSplit (Task a)

:: ArrangeVertical = ArrangeVertical
instance tune ArrangeVertical (Task a)

:: ArrangeHorizontal = ArrangeHorizontal
instance tune ArrangeHorizontal (Task a)

:: ScrollContent = ScrollContent
instance tune ScrollContent (Task a)

/**
 * Tuning a task with `Collapsible` wraps it in a panel (if needed), of which the content can be collapsed by clicking
 * on the header.
 */
:: Collapsible = Collapsible
instance tune Collapsible (Task a)

:: AddCSSClass = AddCSSClass !String
instance tune AddCSSClass (Task a)

:: CSSStyle = CSSStyle !String
instance tune CSSStyle (Task a)

//Changing container types

toContainer ::                                   LayoutRule
toPanel     :: Bool ->                           LayoutRule
toWindow    :: UIWindowType UIVAlign UIHAlign -> LayoutRule
toEmpty     ::                                   LayoutRule

:: ToWindow = ToWindow UIWindowType UIVAlign UIHAlign
InWindow                :== InFloatingWindow
InFloatingWindow        :== ToWindow FloatingWindow AlignMiddle AlignCenter
InNotificationBubble    :== ToWindow NotificationBubble AlignTop AlignRight
instance tune ToWindow (Task a)

:: InPanel          = InPanel Bool      //Indicate that a task should be wrapped in a panel
instance tune InPanel (Task a)

:: InContainer      = InContainer       //Indicate that a task should be wrapped in a panel
instance tune InContainer (Task a)

:: NoUserInterface  = NoUserInterface   //Replace the UI by an empty UI
instance tune NoUserInterface (Task a)

actionToButton :: LayoutRule

setActionIcon :: (Map String String) -> LayoutRule

/*
 * Format a basic editor as if it was a generic labelled iconized edtior
 */
toFormItem :: LayoutRule
