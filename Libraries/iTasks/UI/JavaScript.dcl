definition module iTasks.UI.JavaScript

/**
 * This module provides functionality to enrich editors and tasks with client-side code, interacting with JavaScript.
 */

from ABC.Interpreter.JavaScript import :: JSVal, :: JSWorld

from iTasks.UI.Editor import :: Editor, :: LeafEditor
from iTasks.UI.Tune import class tune
from iTasks.WF.Definition import :: Task

//* A subset of `EngineOptions` accessible to functions executed on the frontend with `JavaScriptInitFunction`.
:: FrontendEngineOptions =
	{ serverDirectory :: !String
	}

/**
 * Adds a client-side initialization function, which is executed when a new UI is generated or replaced. See the
 * modules ABC.Interpreter.JavaScript and ABC.Interpreter.JavaScript.Monad for functions that can be used in the
 * client-side function.
 *
 * The `JSVal` argument is a reference to the iTasks component the function is linked to. This element can be used to
 * store references to the Clean heap in JavaScript using `jsMakeCleanReference` or `jsWrapFun`.
 *
 * All client-side functions are currently shared in a single instance of an ABC interpreter in WebAssembly. See
 * itasks-core.js for the implementation and the default settings.
 *
 * When using a client-side function that requires more memory than the default, you can create a custom index.html
 * that sets a large heap/stack size before loading itasks-core.js:
 *
 * ```html
 * <script>
 *     var itasks = {
 *         settings: {
 *             heap_size: 64 << 20, // 64MB
 *             stack_size: 1 << 20, // 1MB
 *         }
 *     };
 * </script>
 * ```
 */
:: JavaScriptInitFunction =: JavaScriptInit (FrontendEngineOptions JSVal *JSWorld -> *JSWorld)

/**
 * Executes a JavaScript function when the task UI is first generated. The function will only execute if the task has
 * a UI.
 */
instance tune JavaScriptInitFunction (Task a)

//* Executes a JavaScript function when the editor UI is first generated.
instance tune JavaScriptInitFunction (Editor r w)

//* Executes a JavaScript function when the editor UI is first generated.
instance tune JavaScriptInitFunction (LeafEditor edit st r w)
