implementation module iTasks.UI.Editor.Modifiers

from StdFunc import o, const, flip, id
import StdBool, StdString, StdList
import iTasks.UI.Editor, iTasks.UI.Definition, iTasks.UI.Tune
import Data.Error, Text.GenJSON, Data.Tuple, Data.Functor
import Data.GenEq, Data.Func, Data.List
import qualified Data.Map as DM
import Data.Maybe
import Text

withExtraAttributes :: ((?r) Bool -> UIAttributes) (w Bool -> UIAttributes) (Editor r w) -> Editor r w
withExtraAttributes onRead onWrite editor
	= editorModifierWithStateToEditor {EditorModifierWithState|onReset=onReset,onEdit=onEdit,onRefresh=onRefresh,writeValue=writeValue}
where
	onReset attr mbr vst=:{VSt|optional} = case editor.Editor.onReset attr mbr vst of
		(Ok (UI t downstreamUi i,st, mbw),vst)
			# extrar = onRead mbr optional
			# extraw = maybe 'DM'.newMap (\w -> onWrite w optional) mbw
			# extra = 'DM'.union extraw extrar
			// The arguments to ``DM'.union extra downstreamUi` are provided in that order
			// on purpose, so upstream changes are prioritized.
			= (Ok (UI t ('DM'.union extra downstreamUi) i, extra, st, mbw),vst)
		(Error e,vst) = (Error e,vst)

	onEdit event downstream st vst=:{VSt|optional} = case editor.Editor.onEdit event st vst of
		(Ok (?Just (change,st,mbw)),vst)
			= case mbw of
				?None = (Ok (?Just (change,downstream,st,mbw)),vst)
				?Just w
					# extra = onWrite w optional
					# extrChange = ChangeUI (diffUIAttributes downstream extra) []
					# change = mergeUIChanges change extrChange
					= (Ok (?Just (change,extra,st,mbw)),vst)
		(Ok ?None,vst) = (Ok ?None,vst)
		(Error e,vst) = (Error e,vst)

	onRefresh mbr (downstream) st vst=:{VSt|optional} = case editor.Editor.onRefresh mbr st vst of
		(Ok (change,st,mbw),vst)
			# extrar = onRead mbr optional
			# extraw = maybe 'DM'.newMap (\w -> onWrite w optional) mbw
			# extra = 'DM'.union extraw extrar
			# extrChange = ChangeUI (diffUIAttributes downstream extra) []
			# change = mergeUIChanges change extrChange
			= (Ok (change,extra,st,mbw),vst)
		(Error e,vst) = (Error e,vst)
	writeValue _ st = editor.Editor.writeValue st

withDynamicHintAttributes :: !String !(Editor r (EditorReport w)) -> Editor r (EditorReport w)
withDynamicHintAttributes typeDesc editor
	= withExtraAttributes onReadAttrs onWriteAttrs editor
where
	onReadAttrs ?None optional = hintAttr optional
	onReadAttrs (?Just _) _ = validAttr

	onWriteAttrs (EmptyEditor) optional = hintAttr optional
	onWriteAttrs (ValidEditor _) _ = validAttr
	onWriteAttrs (InvalidEditor e) _ = invalidAttr (join ", " e)

	hintAttr optional = 'DM'.fromList
		[ (TOOLTIP_TYPE_ATTRIBUTE, JSONString TOOLTIP_TYPE_INFO)
		, ( TOOLTIP_ATTRIBUTE, JSONString ("Please enter a " +++ typeDesc +++ if optional "" " (this value is required)"))
		]
	validAttr = 'DM'.fromList
		[ (TOOLTIP_TYPE_ATTRIBUTE, JSONString TOOLTIP_TYPE_VALID)
		, (TOOLTIP_ATTRIBUTE, JSONString ("You have correctly entered a " +++ typeDesc))
		]
	invalidAttr error = 'DM'.fromList
		[ (TOOLTIP_TYPE_ATTRIBUTE, JSONString TOOLTIP_TYPE_INVALID)
		, (TOOLTIP_ATTRIBUTE, JSONString error)
		]

viewConstantValue :: !r !(Editor r w) -> Editor () w
viewConstantValue val e =
	mapEditorInitialValue (?Just o fromMaybe ()) $
	mapEditorRead (const val) e

ignoreEditorReads :: !(Editor rb wa) -> Editor ra wa
ignoreEditorReads editor=:{Editor|onReset=editorOnReset, onRefresh=editorOnRefresh}
	= {Editor|editor & onReset=onReset, onRefresh=onRefresh}
where
	onReset attr _ vst = editorOnReset attr ?None vst
	onRefresh new st vst = (Ok (NoChange,st,?None),vst)

mapEditorReads :: !((?r) -> ?rb) !(w (?r) -> ?rb) !(Editor rb w) -> Editor r w
mapEditorReads mapReset mapRefresh editor=:{Editor|onReset=editorOnReset,onRefresh=editorOnRefresh,writeValue=editorWriteValue}
	= {Editor| editor & onReset=onReset,onRefresh=onRefresh}
where
	onReset attr mbval vst = editorOnReset attr (mapReset mbval) vst
	onRefresh mbnew st vst = case editorWriteValue st of
		(Ok w) = editorOnRefresh (mapRefresh w mbnew) st vst
		(Error e) = (Error e,vst)

mapEditorRead :: !(r -> rb) !(Editor rb w) -> Editor r w
mapEditorRead mapRead editor = mapEditorReads (fmap mapRead) (const $ fmap mapRead) editor

mapEditorWriteError :: !(wb -> MaybeErrorString w) !(Editor r wb) -> Editor r (EditorReport w)
mapEditorWriteError fromf editor
	= withDynamicHintAttributes "value"
	$ mapEditorWrite (\w -> case fromf w of (Ok x) = ValidEditor x ; (Error e) = InvalidEditor [e]) editor

mapValidEditorWriteError :: !(wb -> MaybeErrorString w) !(Editor r (EditorReport wb)) -> Editor r (EditorReport w)
mapValidEditorWriteError fromf editor =
	withDynamicHintAttributes "value" $
	mapEditorWrite
		(\w -> case w of
			ValidEditor w   = case fromf w of (Ok x) = ValidEditor x ; (Error e) = InvalidEditor [e]
			EmptyEditor     = EmptyEditor
			InvalidEditor e = InvalidEditor e
		)
		editor

mapEditorWrite :: !(wb -> w) !(Editor r wb) -> Editor r w
mapEditorWrite fromf editor=:{Editor|onReset=editorOnReset,onEdit=editorOnEdit,onRefresh=editorOnRefresh,writeValue=editorWriteValue}
	= {Editor| editor & onReset=onReset, onEdit=onEdit,onRefresh=onRefresh,writeValue = writeValue}
where
	onReset attr mbval vst = case editorOnReset attr mbval vst of
		(Ok (ui,st,mbw),vst) = (Ok (ui,st,fmap fromf mbw),vst)
		(Error e,vst) = (Error e,vst)

	onEdit e st vst = case editorOnEdit e st vst of
		(Ok (?Just (ui,st,mbw)),vst) = (Ok (?Just (ui,st,fmap fromf mbw)),vst)
		(Ok ?None,vst) = (Ok ?None, vst)
		(Error e,vst) = (Error e,vst)

	onRefresh mbnew st vst = case editorOnRefresh mbnew st vst of
		(Ok (ui,st,mbw),vst) = (Ok (ui,st,fmap fromf mbw),vst)
		(Error e,vst) = (Error e,vst)

	writeValue st = case editorWriteValue st of
		Ok w = Ok (fromf w)
		Error e = Error e

loopbackEditorWrite :: !(w -> ?r) !(Editor r w) -> Editor r w
loopbackEditorWrite loopback editor=:{Editor|onReset=editorOnReset,onEdit=editorOnEdit,onRefresh=editorOnRefresh,writeValue=editorWriteValue}
	= {Editor| editor & onReset=onReset, onEdit=onEdit,onRefresh=onRefresh,writeValue = writeValue}
where
	onReset attr mbval vst = case editorOnReset attr mbval vst of
		(Ok (ui,st,?None),vst) = (Ok (ui,st,?None),vst)
		(Ok (ui,st,?Just w),vst) = case loopback w of
			?Just r = case onRefresh (?Just r) st vst of
				(Ok (change,st,mbw),vst) = (Ok (applyUIChange change ui,st,mbw),vst)
				(Error e,vst) = (Error e,vst)
			?None = (Ok (ui,st,?Just w),vst)
		(Error e,vst) = (Error e,vst)

	onEdit e st vst = case editorOnEdit e st vst of
		(Ok ?None,vst) = (Ok ?None,vst)
		(Ok (?Just (ui,st,?None)),vst) = (Ok (?Just (ui,st,?None)),vst)
		(Ok (?Just (change1,st,?Just w)),vst) = case loopback w of
			?Just r = case onRefresh (?Just r) st vst of
				(Ok (change2,st,mbw),vst)
					= case maybe (writeValue st) Ok mbw of
						(Ok w) = (Ok (?Just (mergeUIChanges change1 change2,st,?Just w)),vst)
						(Error e) = (Error e,vst)
				(Error e,vst) = (Error e,vst)
			?None = (Ok (?Just (change1,st,?Just w)),vst)
		(Error e,vst) = (Error e,vst)

	onRefresh mbnew st vst = case editorOnRefresh mbnew st vst of
		(Ok (ui,st,?None),vst) = (Ok (ui,st,?None),vst)
		(Ok (change1,st,?Just w),vst) = case loopback w of
			?Just r = case onRefresh (?Just r) st vst of
				(Ok (change2,st,mbw),vst)
					= case maybe (writeValue st) Ok mbw of
						(Ok w) = (Ok (mergeUIChanges change1 change2,st,?Just w),vst)
						(Error e) = (Error e,vst)
				(Error e,vst) = (Error e,vst)
			?None = (Ok (change1,st,?Just w),vst)
		(Error e,vst) = (Error e,vst)

	writeValue st = case editorWriteValue st of
		Ok w = Ok w
		Error e = Error e

mapEditorWithState :: !s !(r s -> (?rb,s)) !(wb s -> (w,s)) !(Editor rb wb) -> Editor r w | TC s
mapEditorWithState init onread onwrite {Editor|onReset=editorOnReset,onEdit=editorOnEdit,onRefresh=editorOnRefresh,writeValue=editorWriteValue}
	= editorModifierWithStateToEditor
		{EditorModifierWithState|onReset=onReset,onEdit=onEdit,onRefresh=onRefresh,writeValue=writeValue}
where
	onReset attr mbval vst
		# (mbval,s) = case mbval of
			?None = (?None, init)
			?Just r = onread r init
		= appFst (fmap (\(ui, st, mbw) ->
			let (mbw`,s`) = maybe (?None,s) (\w -> appFst ?Just $ onwrite w s) mbw in
				(ui, s`, st, mbw`)))
		$ editorOnReset attr mbval vst

	onEdit event s st vst
		= appFst (fmap $ fmap (\(ui, st, mbw) ->
			let (mbw`,s`) = maybe (?None,s) (\w -> appFst ?Just $ onwrite w s) mbw in
				(ui, s`, st, mbw`)))
		$ editorOnEdit event st vst

	onRefresh mbr s st vst
		= case maybe (?Just ?None,s) (\r -> appFst (fmap ?Just) $ onread r s) mbr of
			(?None,s) = (Ok (NoChange, s, st,?None),vst)
			(?Just mbr,s)
				= appFst (fmap (\(ui, st, mbw) ->
					let (mbw`,s`) = maybe (?None,s) (\w -> appFst ?Just $ onwrite w s) mbw in
						(ui, s`, st, mbw`)))
				$ editorOnRefresh mbr st vst

	writeValue s st = case editorWriteValue st of
		Ok w = let (w`,s`) = onwrite w s in Ok w` //FIXME: We can't store s`
		Error e = Error e

mapEditorInitialValue :: !((?r) -> ?r) !(Editor r w) -> Editor r w
mapEditorInitialValue mapReset editor = mapEditorReads mapReset (const id) editor

mapEditorReadWithValue :: !(r -> rb) !(r w -> rb) !(Editor rb w) -> Editor r w
mapEditorReadWithValue mapReset mapRefresh editor
	= mapEditorReads (fmap mapReset) (\w mbr -> fmap (\r -> mapRefresh r w) mbr) editor
