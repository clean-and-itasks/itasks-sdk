definition module iTasks.UI.Editor.Modifiers
/**
* This module provides combinator functions for combining editors
*/
import iTasks.UI.Editor, iTasks.UI.Definition, iTasks.UI.Tune
import Data.Error

//### Modifying atributes of editors ### 

/**
 * Dynamically adjusts an editor's attributes based on the read and write values.
 *
 * When during a refresh/reset both a read and write value are available, the attributes based on the write value take precedence.
 *
 * @param A function that computes the editor's additional attributes during a read.
 *        It gets the read value and a flag whether the editor is optional as parameters.
 *        This enables more specific attributes such as "this value is required" hints.
 * @param A function that computes the editor's additional attributes during a write
 *        It gets the write value and a flag whether the editor is optional as parameters.
 * @param The editor to apply the attributes to.
 */
withExtraAttributes :: ((?r) Bool -> UIAttributes) (w Bool -> UIAttributes) (Editor r w) -> Editor r w

/**
 * Adds a tooltip and tooltip-type attribute based on the editor's state.
 * Layouts (such as the automatic default layout) can use these attributes to
 * create icons and tooltips.
 *
 * @param Name of the type being edited e.g. "foo" -> "you have correctly entered a foo"
 * @param The editor to apply the attributes to.
 */
withDynamicHintAttributes :: !String !(Editor r (EditorReport w)) -> Editor r (EditorReport w)

/**
* Uses the given editor to view a constant value.
*
* @param The constant value to view.
* @param The editor used to view the value.
* @result An editor viewing the constant value.
*/
viewConstantValue :: !r !(Editor r w) -> Editor () w

/**
* Ignore all data reads
*/
ignoreEditorReads :: !(Editor rb wa) -> Editor ra wa

// ### Mapping editors to different domains ###

/**
* Maps the editor's initial (reset) and refresh values to another domain.
* The function's optional values allow you to also clear an editor, or provide default values for empty editors.
*
* @param A mapping when the editor is reset
* @param A mapping when the editor is refreshed, the current write value is also available in this case.
* @param An editor in the orginal domain
* @result An editor in the mapped domain value.
*/
mapEditorReads :: !((?r) -> ?rb) !(w (?r) -> ?rb) !(Editor rb w) -> Editor r w

/**
* Common case for mapping editor reads to a different domain.
* Function is used in resets and refreshes and empty editors are left unchanged and the current value is ignored.
*/
mapEditorRead :: !(r -> rb) !(Editor rb w) -> Editor r w

/**
* Map editor writes to a different domain
*/
mapEditorWrite :: !(wb -> w) !(Editor r wb) -> Editor r w

/**
 * Map editor writes to a different domain with potential errors.
 * If the mapped editor returns an error, the editor writes `InvalidEditor` and a
 * tooltip-type error and tooltip attribute are set on the editor.
 */
mapEditorWriteError :: !(wb -> MaybeErrorString w) !(Editor r wb) -> Editor r (EditorReport w)

/**
 * Map editor writes to a different domain with potential errors.
 * Empty and invalid results are unmodified.
 * This can add an additional validation (e.g. `\v -> if (v < 0) (Error "enter a positive number") (Ok v)`).
 */
mapValidEditorWriteError :: !(wb -> MaybeErrorString w) !(Editor r (EditorReport wb)) -> Editor r (EditorReport w)

/**
* Add a state to an editor which is available to map reads and writes to the editor.
*/
mapEditorWithState :: !s !(r s -> (?rb,s)) !(wb s -> (w,s)) !(Editor rb wb) -> Editor r w | TC s

/**
* Loop back an editor write to an editor read
*/
loopbackEditorWrite :: !(w -> ?r) !(Editor r w) -> Editor r w


//Deprecated versions, can more clearly be expressed with mapEditorReads
mapEditorReadWithValue :: !(r -> rb) !(r w -> rb) !(Editor rb w) -> Editor r w

mapEditorInitialValue :: !((?r) -> ?r) !(Editor r w) -> Editor r w

