implementation module iTasks.UI.Editor.Controls

import ABC.Interpreter.JavaScript
import StdEnv
import iTasks.UI.Definition, iTasks.UI.Editor
import Control.Applicative
import Data.GenEq, Data.Error, Text.GenJSON, Text.HTML, Data.Func, Data.Functor, Data.Tuple, Data.List
import qualified Data.Map as DM
from Data.Map import derive gEq Map
import Data.Maybe
import Data.Integer

import iTasks.WF.Derives
import iTasks.UI.Definition
import iTasks.UI.Editor.Modifiers
import iTasks.UI.JavaScript
import Text, Text.HTML.GenJSON

textField :: Editor String (EditorReport String)
textField = fieldComponent UITextField (?Just "") isValidString

textFieldNoValueByDefault :: Editor String (EditorReport String)
textFieldNoValueByDefault =: fieldComponent UITextField ?None isValidString

textArea :: Editor String (EditorReport String)
textArea = fieldComponent UITextArea (?Just "") isValidString

passwordField :: Editor String (EditorReport String)
passwordField = fieldComponent UIPasswordField (?Just "") isValidString

isValidString :: !UIAttributes !String -> Bool
isValidString attrs str
	= lStr >= getLengthAttr 0 "minlength" && lStr <= getLengthAttr lStr "maxlength"
where
	getLengthAttr :: !Int !String -> Int
	getLengthAttr default attr = case 'DM'.get attr attrs of
		?Just (JSONInt l) = l
		_                 = default

	lStr = size str

integerField :: Editor Int (EditorReport Int)
integerField = fieldComponent UIIntegerField ?None valid
where
	valid :: UIAttributes Int -> Bool
	valid _ _ = True

bigIntegerField :: Editor Integer (EditorReport Integer)
bigIntegerField
	= mapEditorWrite (fmap toInteger) $ mapEditorRead toString
	$ fieldComponent UITextField ?None \_ s
		| size s == 0  = False
		| s.[0] == '-' = size s > 1 && allDigits 1 s
		| otherwise    = allDigits 0 s
where
	allDigits i s
		| size s == i = True
		| otherwise = isDigit s.[i] && allDigits (i+1) s

decimalField :: Editor Real (EditorReport Real)
decimalField = fieldComponent UIDecimalField ?None (\_ _ -> True)

documentField :: Editor (String,String,String,String,Int) (EditorReport (String,String,String,String,Int))
documentField = fieldComponent UIDocumentField ?None (\_ _ -> True)

checkBox :: Editor Bool Bool
checkBox
	= mapEditorWrite (\v -> case v of (ValidEditor x) = x; _ = False)
	$ fieldComponent UICheckbox (?Just False) (\_ _ -> True)

slider :: Editor Int Int
slider
	= mapEditorWrite (\v -> case v of (ValidEditor x) = x; _ = 0)
	$ fieldComponent UISlider (?Just 50) (\_ _ -> True)

button :: Editor Bool Bool
button
	= mapEditorWrite (\v -> case v of (ValidEditor x) = x; _ = False)
	$ fieldComponent UIButton (?Just False) (\_ _ -> True)

label :: Editor String ()
label = viewComponent textAttr () UILabel

icon :: Editor (String,?String) ()
icon = viewComponent (\(iconCls,tooltip) -> 'DM'.unions [iconClsAttr iconCls,maybe 'DM'.newMap tooltipAttr tooltip])
                     () UIIcon

textView :: Editor String ()
textView = viewComponent (valueAttr o JSONString o escapeForAttribute) () UITextView

htmlView :: Editor HtmlTag ()
htmlView = viewComponent (valueAttr o JSONString o toString) () UIHtmlView

toHtmlEventCall :: !w -> String | JSONEncode{|*|} w
toHtmlEventCall event = concat3 "itasks.htmlEvent(event,'" (replaceSubString "'" "\\'" $ toString $ toJSON event) "')"

htmlViewWithCustomEditEvents :: Editor HtmlTag (?w) | JSONDecode{|*|} w
htmlViewWithCustomEditEvents =
	JavaScriptInit (\_ = initOnHtmlEvent) @>>
	{Editor | onReset = onReset, onEdit = onEdit, onRefresh = onRefresh, writeValue = writeValue}
where
	onReset :: !UIAttributes !(?HtmlTag) !*VSt -> (!*MaybeErrorString (UI, EditState, ?(?w)), *VSt)
	onReset uiAttrs mbInitialReadValue vSt
		# (res, vSt) = htmlView.Editor.onReset uiAttrs mbInitialReadValue vSt
		= case res of
			Ok (ui, editSt, _) = (Ok (ui, editSt, ?None), vSt)
			Error e = (Error e, vSt)

	onEdit ::
		!(!EditorId, !JSONNode) !EditState !*VSt -> (!*MaybeErrorString (?(UIChange, EditState, ?(?w))), *VSt)
		| JSONDecode{|*|} w
	onEdit (editorId, (JSONString edit)) editSt vSt
		# mbEvent = fromJSON $ fromString edit
		= case mbEvent of
			?Just event = (Ok (?Just (NoChange, editSt, ?Just (?Just event))), vSt)
			?None = (Error "Invalid event", vSt)

	onRefresh  :: !(?HtmlTag) !EditState !*VSt -> (!*MaybeErrorString (UIChange, EditState, ?(?w)), *VSt)
	onRefresh mbReadValue editSt vSt
		# (res, vSt) = htmlView.Editor.onRefresh mbReadValue editSt vSt
		= case res of
			Ok (uiChange, editSt, _) = (Ok (uiChange, editSt, ?None), vSt)
			Error e = (Error e, vSt)

	writeValue :: EditState -> MaybeErrorString (?w)
	writeValue editSt = Ok ?None

	initOnHtmlEvent :: !JSVal !*JSWorld -> *JSWorld
	initOnHtmlEvent me w
		# (cb, w) = jsWrapFun (onHtmlEvent me) me w
		// This is connected to elements within the HTML view that use `toHtmlEventCall` for a JS event HTML attribute
		// (E.g OnclickAttr).
		// When the JS event (e.g Onclick) occurs, this callback is evaluated and the event is sent to the server.
		# w = (me .# "onHtmlEvent" .= cb) w
		= w
	where
		onHtmlEvent :: !JSVal {!JSVal} !*JSWorld -> *JSWorld
		onHtmlEvent me args w
			# (taskId,w)    = me .# "attributes.taskId" .? w
			# (editorId,w)  = me .# "attributes.editorId" .? w
			= case (jsValToString args.[0]) of
				?Just edit
					= (me .# "doEditEvent" .$! (taskId,editorId,edit)) w
				_	= w

progressBar :: Editor (?Int, ?String) ()
progressBar = viewComponent combine () UIProgressBar
where
	combine (amount,text) =
		'DM'.unions ((maybe [] (\t -> [textAttr t]) text) ++ (maybe [] (\v -> [valueAttr (JSONInt v)]) amount))

loader :: Editor () ()
loader = viewComponent (const emptyAttr) () UILoader

dropdown :: Editor ([ChoiceText], [ChoiceID]) [ChoiceID]
dropdown = choiceComponent (const 'DM'.newMap) id toOptionText checkBoundsText UIDropdown

dropdownWithGroups :: Editor ([(ChoiceText, ?String)], [ChoiceID]) [ChoiceID]
dropdownWithGroups = choiceComponent
	(const 'DM'.newMap)
	id
	toOptionText
	(checkBoundsText o map fst)
	UIDropdown
where
	toOptionText :: !(!ChoiceText, !?String) -> JSONNode
	toOptionText ({ChoiceText|id,text}, groupLabel) =
		JSONObject
			[ ("id",JSONInt id),("text",JSONString text)
			: maybe [] (\label -> [("grouplabel", JSONString label)]) groupLabel
			]

checkGroup :: Editor ([ChoiceText], [ChoiceID]) [ChoiceID]
checkGroup = choiceComponent (const 'DM'.newMap) id toOptionText checkBoundsText UICheckGroup

choiceList :: Editor ([ChoiceText], [ChoiceID]) [ChoiceID]
choiceList = choiceComponent (const 'DM'.newMap) id toOptionText checkBoundsText UIChoiceList

tabBar :: Editor ([ChoiceText], [ChoiceID]) [ChoiceID]
tabBar = choiceComponent (const 'DM'.newMap) id toOptionText checkBoundsText UITabBar

toOptionText {ChoiceText|id,text}= JSONObject [("id",JSONInt id),("text",JSONString text)]
checkBoundsText options idx = or [id == idx \\ {ChoiceText|id} <- options]

getValueText :: ![ChoiceText] ![ChoiceText] -> [Int]
getValueText value options = [i \\ i <- [0..] & {ChoiceText|text} <- options | isMember text value_texts]
where
	value_texts = [text \\ {ChoiceText|text} <- value]

derive JSONEncode ChoiceText
derive JSONDecode ChoiceText

grid :: Editor (ChoiceGrid, [ChoiceID]) [ChoiceID]
grid = choiceComponent (\{ChoiceGrid|header} -> columnsAttr header) (\{ChoiceGrid|rows} -> rows) toOption checkBounds UIGrid
where
	toOption {ChoiceRow|id,cells}= JSONObject [("id",JSONInt id),("cells",JSONArray (map (JSONString o toString) cells))]
	checkBounds options idx = or [id == idx \\ {ChoiceRow|id} <- options]

	getValueRow :: ![ChoiceRow] ![ChoiceRow] -> [Int]
	getValueRow value options = [i \\ i <- [0..] & {ChoiceRow|cells} <- options | any ((===) cells) value_cells]
	where
		value_cells = [cells \\ {ChoiceRow|cells} <- value]

derive JSONEncode ChoiceGrid, ChoiceRow
derive JSONDecode ChoiceGrid, ChoiceRow

tree :: Editor ([ChoiceNode], [ChoiceID]) [ChoiceID]
tree = choiceComponent (const 'DM'.newMap) id toOption checkBounds UITree
where
	toOption {ChoiceNode|id,label,icon,expanded,children}
		= JSONObject [("text",JSONString label)
					 ,("iconCls",maybe JSONNull (\i -> JSONString ("icon-"+++i)) icon)
					 ,("id",JSONInt id)
					 ,("expanded",JSONBool expanded)
					 ,("children",JSONArray (map toOption children))
					]

	checkBounds options idx
		= isJust (fromIndex options idx)
	fromIndex options idx
		= foldl (\r node -> r <|> checkNode idx node) empty options
	checkNode idx node=:{ChoiceNode|id,children}
		| idx == id = ?Just node
		| otherwise = fromIndex children idx

derive JSONEncode ChoiceNode
derive JSONDecode ChoiceNode

withConstantChoices :: !choices !(Editor (!choices, ![ChoiceID]) [ChoiceID]) -> Editor [ChoiceID] [ChoiceID]
withConstantChoices choices editor
	= mapEditorReads (\mbsel -> ?Just (choices,fromMaybe [] mbsel)) (\_ mbsel -> ?Just (choices, fromMaybe [] mbsel)) editor

//Field like components for which simply knowing the UI type is sufficient
fieldComponent
	:: !UIType !(?a) !(UIAttributes a -> Bool) -> Editor a (EditorReport a)
	| JSONDecode{|*|}, JSONEncode{|*|}, gEq{|*|}, TC a
fieldComponent type mbEditModeInitValue isValid = editorWithJSONEncode (leafEditorToEditor o leafEditor)
where
	leafEditor toJSON
		= {LeafEditor|onReset=onReset toJSON,onEdit=onEdit,onRefresh=onRefresh toJSON,writeValue=writeValue}

	onReset toJSON attr mbval vst=:{VSt|optional}
		# noValueIsProvided = isNone mbval
		# mbVal = maybe mbEditModeInitValue ?Just mbval
		# mbVal` =
			maybe
				EmptyEditor
				(\val -> if (isValid attr val) (ValidEditor val) (if noValueIsProvided EmptyEditor (InvalidEditor [])))
				mbVal
		# mbVal = maybe ?None (\val -> if (isValid attr val) (?Just val) ?None) mbVal
		# attr  = 'DM'.unions [ optionalAttr optional, valueAttr $ maybe JSONNull toJSON mbVal, attr]
		= (Ok (uia type attr, (mbVal, attr), ?Just mbVal`), vst)

	onEdit mbVal (_, attrs) vst
		= (Ok (NoChange, (mbVal, attrs), ?Just mbVal`), vst)
	where
		mbVal` = maybe EmptyEditor (\val -> if (isValid attrs val) (ValidEditor val) (InvalidEditor [])) mbVal

	onRefresh toJSON mbNew (mbOld, attrs) vst
		| mbOld === mbNew = (Ok (NoChange, (mbOld, attrs), ?None), vst)
		| otherwise       = (Ok (ChangeUI [SetAttribute "value" (maybe JSONNull toJSON mbNew)] [], (mbNew, attrs), ?None), vst)

	writeValue (mbVal, attrs) =
		Ok $ maybe EmptyEditor (\val -> if (isValid attrs val) (ValidEditor val) (InvalidEditor [])) mbVal

	editorWithJSONEncode :: !((a -> JSONNode) -> Editor a (EditorReport a)) -> Editor a (EditorReport a) | JSONEncode{|*|} a
	editorWithJSONEncode genFunc = genFunc toJSON

//Components which cannot be edited
viewComponent :: !(a -> UIAttributes) !b !UIType -> Editor a b | TC a
viewComponent toAttributes value type = leafEditorToEditor leafEditor
where
	leafEditor = {LeafEditor|onReset=onReset,onEdit=onEdit,onRefresh=onRefresh,writeValue=writeValue}

	onReset attr mbval vst = case mbval of
		?Just val = (Ok (uia type ('DM'.union attr $ toAttributes val), mbval, ?Just value), vst)
		?None     = (Ok (uia type attr, mbval, ?Just value), vst)

	onEdit () val vst = (Ok (NoChange, val, ?None), vst)

	onRefresh mbnew mbval vst = (Ok (changes, mbnew, ?None), vst)
	where
        changes = case setChanges ++ delChanges of
			[]      = NoChange
			changes = ChangeUI changes []

		setChanges = [ SetAttribute key val
		             \\ (key, val) <- 'DM'.toList $ newAttrs
		             | 'DM'.get key oldAttrs <> ?Just val
		             ]
		delChanges = [DelAttribute key \\ (key, _) <- 'DM'.toList $ 'DM'.difference oldAttrs newAttrs]

		oldAttrs = maybe 'DM'.newMap toAttributes mbval
		newAttrs = maybe 'DM'.newMap toAttributes mbnew

	writeValue state = Ok value

/**
 * Choice components that have a set of options. `a` is the exposed type of the
 * option set. `[o]` is something like `ChoiceText`, which the `UIType` knows
 * how to handle.
 */
choiceComponent ::
	!(a -> UIAttributes)      // determines the initial attributes
	!(a -> [o])               // get the low-level options given the high-level option set
	!(o -> JSONNode)          // encode a low-level option
	!([o] ChoiceID -> Bool)   // checks whether the `id` lies inside the bounds of the given option list
	!UIType                   // the type of the editor
	-> Editor (!a, ![Int]) [Int]
	| TC a
choiceComponent attr getOptions encodeOption checkBounds type =
	leafEditorToEditor {LeafEditor|onReset=onReset,onEdit=onEdit,onRefresh=onRefresh,writeValue=writeValue}
where
	onReset attrs mbval vst=:{VSt|taskId}
		# (mbVal, sel) = maybe (?None, []) (appFst ?Just) mbval
		# attr = 'DM'.unions
			[ attrs
			, maybe 'DM'.newMap attr mbVal
			, choiceAttrs taskId ((\(?Just (JSONString i)) -> fromString i) ('DM'.get "editorId" attrs)) sel $ mbValToOptions mbVal
			]
		# multiple = maybe False (\(JSONBool b) -> b) ('DM'.get "multiple" attr)
		= (Ok (uia type attr, (mbVal, sel, multiple), ?None), vst)

	onEdit selection (mbVal, sel, multiple) vst=:{VSt|optional}
		# options = maybe [] getOptions mbVal
		| all (checkBounds options) selection
			= (Ok (NoChange, (mbVal, selection, multiple), ?Just selection),vst)
		| otherwise
			= (Error ("Choice event out of bounds: " +++ toString (toJSON selection)), vst)

	onRefresh mbNew (mbOldVal, oldSel, multiple) vst
		# (mbNewVal,newSel) = maybe (?None, []) (appFst ?Just) mbNew
		//Check options
		# oldOptsJson        = mbValToOptions mbOldVal
		# newOpts            = maybe [] getOptions mbNewVal
		# newOptsJson        = encodeOption <$> newOpts
		# cOptions           = if (newOptsJson =!= oldOptsJson)
		                          (ChangeUI [SetAttribute "options" (JSONArray newOptsJson)] [])
		                          NoChange
		//Update selection
		# cSel               = if (newSel =!= oldSel) (ChangeUI [SetAttribute "value" (toJSON newSel)] []) NoChange
		= (Ok (mergeUIChanges cOptions cSel, (mbNewVal, newSel, multiple), if (cSel=:NoChange) ?None (?Just newSel)),vst)

	writeValue (_, sel, multiple)
		// Non-multi select choice are only valid with a single selected item
		| not multiple && lengthSel > 1 = Error "Multiple elements selected in component without multi-select"
		| otherwise                     = Ok sel
	where
		lengthSel = length sel

	mbValToOptions mbVal = encodeOption <$> maybe [] getOptions mbVal

genChoiceID :: a -> ChoiceID | JSONEncode{|*|} a
genChoiceID x = (gHash{|*|} $ toJSON x) bitand 0xFFFFFFFF
//Choice ID's are truncated to 32 bits because 64 bit hashes don't reliably fit in Javascript numbers
