definition module iTasks.UI.Editor.Common
/**
* This module provides some convenient editors
*/
from StdOverloadedList import class List
from iTasks.UI.Editor import :: Editor, :: EditorReport
from iTasks.UI.Editor.Generic import :: EditorPurpose
from iTasks.UI.Definition import :: UI, :: UIChildChange, :: UIChange
from Text.GenJSON import generic JSONEncode, :: JSONNode, generic JSONDecode
import iTasks.Internal.Generic.Defaults
from Data.GenEq import generic gEq

/**
* Editor that does nothing.
*
* @result the empty editor
*/
emptyEditor :: Editor a (EditorReport w)

/**
 * Indicates if and how a UI child can be updated to another one.
 */
:: ChildUpdate = ChildUpdateImpossible //* the child a cannot be update
               | NoChildUpdateRequired //* no update is required, i.e. the child already equals the existing one
               | ChildUpdate !UIChange //* the child has to be changed

/**
* Determines the diff between an old and a new list of children,
* consisting of insert, remove and move change instructions.
* If possible move instructors are generated instead of a remove/insert combination.
* The worst-case time complexity is O(nm) (where n is the length of the old
* and m the length of the new children list). The complexity however decreases with
* more similar old and new lists and is O(n) for equal lists.
*
* @param: Old:                The previous child list.
* @param: New:                The new child list.
* @param  UpdateFromOldToNew: If and how an old value can be updated to a new one.
* @param: To UI:              A function to map children to UIs.
* @return                     A list of index/change pairs as expected by 'iTasks.UI.Definition.ChangeUI'.
*/
diffChildren :: ![a] ![a] !(a a -> ChildUpdate) !(a -> UI) -> [(Int, UIChildChange)]

/**
* Simple dropdown that edits an index by choosing from a list of labels
*/
chooseWithDropdown :: ![String] -> Editor Int (EditorReport Int)

/**
 * An Editor for selecting multiple options using checkboxes or radio buttons.
 * @param The EditorPurpose.
 * @param The read function.
 * @param The write function.
 * @param A boolean indicating whether multiple items can be selected simultaneously.
 *     When True, checkboxes are used. When False, radio buttons are used.
 * @param The choices.
 * @result An Editor for selecting multiple options using checkboxes or radio buttons.
 */
choiceEditor ::
	!EditorPurpose !(v -> [a]) !([a] -> EditorReport v) !Bool ![a] -> Editor v (EditorReport v) | toString, == a

/**
 * An Editor for selecting multiple options using checkboxes or radio buttons, with custom labels for the choices.
 * @param The EditorPurpose.
 * @param The read function.
 * @param The write function.
 * @param The function for determining the labels.
 * @param A boolean indicating whether multiple items can be selected simultaneously.
 *     When True, checkboxes are used. When False, radio buttons are used.
 * @param The choices.
 * @result An Editor for selecting multiple options using checkboxes or radio buttons.
 */
choiceEditorWithCustomLabels ::
	!EditorPurpose !(v -> [a]) !([a] -> EditorReport v) !(a -> String) !Bool ![a] -> Editor v (EditorReport v) | == a

/**
 * An Editor for selecting multiple options using checkboxes.
 * @param The EditorPurpose.
 * @param Is selecting at least one checkbox required?
 * @param The read function.
 * @param The write function.
 * @param The choices.
 * @result An Editor for selecting multiple options using checkboxes.
 */
multipleChoiceEditor ::
	!EditorPurpose !Bool !(v -> [a]) !([a] -> v) ![a] -> Editor v (EditorReport v) | ==, toString a

/**
 * An Editor for selecting multiple options using checkboxes, with custom labels for the choices.
 * @param The EditorPurpose.
 * @param Is selecting at least one checkbox required?
 * @param The read function.
 * @param The write function.
 * @param The function for determining the labels.
 * @param The choices.
 * @result An Editor for selecting multiple options using checkboxes.
 */
multipleChoiceEditorWithCustomLabels ::
	!EditorPurpose !Bool !(v -> [a]) !([a] -> v) !(a -> String) ![a] -> Editor v (EditorReport v) | == a

/**
 * Editor for lists.
 *
 * @param View:            Create an editor for viewing a list
 * @param Add:             Determines whether new elements can be added.
 *                         If this is the case a function on the current values determines the element to add,
 *                         where the result of the function means:
 *                             `?None`: a new value has to be entered for the new element (Enter mode)
 *                             `?Just x`: a new element with value 'x' is added and can be updated (Update mode)
 * @param Remove:          Can elements be removed?
 * @param Reorder:         Can elements be reordered?
 * @param Summary:         Optionally generates a summary of the list (e.g. the nr of items)
 * @param Write function:  When an element is updated, this function provides the conversion to the write type for the other elements
 * @param Children editor: The editor for the children
 *
 * @result                 The list editor
 */
listEditor ::
	!Bool !(?((l w) -> ?r)) !Bool !Bool !(?((l w) -> String)) !(r -> w) !(Editor r w)
	-> Editor (l r) (l w)
	| gEq{|*|} w & List l r & List l w

//Version without overloading, for use in generic case
//The first argument should be gEq{|*|} which cannot be used by overloading within generic functions
listEditor_ ::
	!(w w -> Bool) !Bool !(?((l w) -> ?r)) !Bool !Bool !(?((l w) -> String)) !(r -> w) !(Editor r w)
	-> Editor (l r) (l w) | List l r & List l w
	special
		l = []; l = [!]; l = [!!]; l = [ !]
