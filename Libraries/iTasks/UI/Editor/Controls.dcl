definition module iTasks.UI.Editor.Controls
/**
* This module provides a set of editors for builtin controls
* of the client-side UI framework.
*/
from iTasks.UI.Editor import :: Editor, :: EditorReport
from iTasks.UI.Definition import :: UIAttributes, :: UIType
from Data.Map import :: Map
from Data.Integer import :: Integer
from Text.HTML import :: HtmlTag
from Text.GenJSON import :: JSONNode, generic JSONDecode, generic JSONEncode
from Data.GenEq import generic gEq

/**
* The editors that have a polymorphic write type of `a` are for viewing data only
*/

// ## Form components ##
// UITextField, UITextArea, UIPasswordField, UIIntegerField, UIDecimalField, UIDocumentField
// UICheckbox, UISlider, UIButton, UILabel, UIIcon

/**
* Basic textfield
* it checks for minimal string length using the minlength attribute (default 0)
* Supported attributes: minlength, maxlength, placeholder
*/
textField     :: Editor String (EditorReport String)
/**
* Basic textfield which does not contain a value by default, e.g: for textField the default value is "" (empty String).
* it checks for minimal string length using the minlength attribute (default 0)
* Supported attributes: minlength, maxlength, placeholder
*/
textFieldNoValueByDefault     :: Editor String (EditorReport String)
/**
* Multiple line text area
* it checks for minimal string length using the minlength attribute (default 0)
* Supported attributes: minlength, maxlength
*/
textArea      :: Editor String (EditorReport String)
/**
* Password field that hides what you type
* it checks for minimal string length using the minlength attribute (default 0)
* Supported attributes: minlength, maxlength
*/
passwordField :: Editor String (EditorReport String)
/**
* Textfield that only allows you to enter integer numbers
* Supported attributes:
*/
integerField  :: Editor Int (EditorReport Int)
/**
* Textfield that allows you to enter arbitrarily big integer numbers
*/
bigIntegerField  :: Editor Integer (EditorReport Integer)
/**
* Textfield that only allows you to enter decimal (or integer) numbers
* Supported attributes:
*/
decimalField  :: Editor Real (EditorReport Real)
/**
* Form field that allows you to upload files
* Supported attributes:
*/
documentField :: Editor (String,String,String,String,Int) (EditorReport (String,String,String,String,Int))
/**
* Simple checkbox
* Supported attributes:
*/
checkBox      :: Editor Bool Bool
/**
* Slider for integer values in a limited range
* Supported attributes:
*/
slider        :: Editor Int Int
/**
* A basic clickable button
* Supported attributes:
*/
button        :: Editor Bool Bool
/**
* A plain text label
* Supported attributes:
*/
label         :: Editor String ()
/**
* A small icon with a tooltip
* Supported attributes:
*/
icon          :: Editor (String,?String) ()

// ## Display components ##
// UITextView, UIHtmlView, UIProgressBar

/**
* A component that displays arbitrary text (html is automatically escaped)
* Supported attributes:
*/
textView      :: Editor String ()
/**
* A component that displays arbitrary HTML (nothing is escaped)
* Supported attributes:
*/
htmlView      :: Editor HtmlTag ()

/**
 * HTML view which translates JSON edit events to a write type returned by the editor.
 * To generate such JSON edit events, use `toHtmlEventCall` in combination with `HtmlAttr` events within the value
 * that is being displayed.
 * E.g `OnClickAttr (toHtmlEventCall w)`, then whenever the button is clicked, the editor will return `?Just w`.
 *
 * @result The editor for displaying HTML.
 */
htmlViewWithCustomEditEvents :: Editor HtmlTag (?w) | JSONDecode{|*|} w

/**
 * Generates a html event call for a JSON encodable value.
 * To be used in combination with `htmlViewWIthCustomEditEvents`.
 */
toHtmlEventCall :: !w -> String | JSONEncode{|*|} w

/**
* A progress bar with a percentage and a description of the progress
* Supported attributes:
*/
progressBar   :: Editor (?Int,?String) () //Percentage, description

/**
* A basic loading indicator
*/
loader :: Editor () ()

// ## Selection components ##
// UIDropdown, UIRadioGroup, UICheckboxGroup, UIChoiceList, UIGrid, UITree

:: ChoiceID :== Int
/**
* A dropdown box
* Supported attributes:
*/
dropdown      :: Editor ([ChoiceText], [ChoiceID]) [ChoiceID]
/**
* A dropdown box with grouped items
* Supported attributes:
*/
dropdownWithGroups :: Editor ([(ChoiceText, ?String)], [ChoiceID]) [ChoiceID]
/**
* A group of checkboxes or radiobuttons depending on whether the multiple
* attribute is set or not
* Supported attributes:
*/
checkGroup    :: Editor ([ChoiceText], [ChoiceID]) [ChoiceID]
/**
* A list of text items to choose from
* Supported attributes:
*/
choiceList    :: Editor ([ChoiceText], [ChoiceID]) [ChoiceID]
/**
* A typical grid component with a header
* Supported attributes:
*/
grid          :: Editor (ChoiceGrid,   [ChoiceID]) [ChoiceID]
/**
* A typical tree selection component with expanding "folders"
* Supported attributes:
*/
tree          :: Editor ([ChoiceNode], [ChoiceID]) [ChoiceID]

/**
* A horizontal bar with tabs to make a selection with
*/
tabBar        :: Editor ([ChoiceText], [ChoiceID]) [ChoiceID]

/**
 * Modifies the above editors for making choices such that they use a constant set of choices.
 */
withConstantChoices :: !choices !(Editor (!choices, ![ChoiceID]) [ChoiceID]) -> Editor [ChoiceID] [ChoiceID]

//Convenient types for describing the values of grids and trees

/**
 * An option in a `dropdown`, `dropdownWithGroups`, `checkGroup`, `choiceList`, or `tabBar` editor.
 *
 * Because option sets can change dynamically (this may for example happen when
 * using `editSelectionWithShared`), the types for specifying options (`ChoiceText`, `ChoiceRow`, `ChoiceNode`)
 * all have an `id` field that is assumed to be unique.
 * The read and write values of the editor are lists of these unique identifiers.
 *
 * When selecting from domains that already have intrinsic identifiers, such as database ID fields,
 * you can use those identifiers. For domains that don't have this property you can use the `genChoiceID`
 * function to generically create a unique identifier using hashing.
 */
:: ChoiceText =
	{ id     :: ChoiceID
	, text   :: String
    }

//* See `grid`.
:: ChoiceGrid =
	{ header  :: [String]
	, rows    :: [ChoiceRow]
	}

/**
 * An option in a `ChoiceGrid`, used in the `grid` editor.
 *
 * When the option set is changed dynamically (this may for example happen when
 * using `editSelectionWithShared`), the `id` field is assumed to be unique.
 */
:: ChoiceRow =
	{ id      :: ChoiceID
	, cells   :: [HtmlTag]
	}

/**
 * An option in a `tree` choice editor.
 *
 * When the option set is changed dynamically (this may for example happen when
 * using `editSelectionWithShared`), the `id` field is assumed to be unique.
 */
:: ChoiceNode =
	{ id       :: ChoiceID
	, label    :: String
	, icon     :: ?String
	, expanded :: Bool
	, children :: [ChoiceNode]
	}

/**
* Utility function for generically create identifiers
* for making choices from dynamically updating sets of options
*
* @param Option:			Any serializable value
*
* @return 					A unique identifier (based on a hash)
*/
genChoiceID :: a -> ChoiceID | JSONEncode{|*|} a
