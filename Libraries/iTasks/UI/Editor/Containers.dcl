definition module iTasks.UI.Editor.Containers
/**
* This module provides a set of editors for builtin controls
* of the client-side UI framework.
*
* To keep everything well-typed there are lots of boiler-plate versions to create the containers
*/
from iTasks.UI.Definition import :: UIType(..), :: UIAttributes
from iTasks.UI.Editor import :: Editor, :: EditorReport
from Data.Either import :: Either
from Data.Map import :: Map
from Text.GenJSON import :: JSONNode, generic JSONDecode, generic JSONEncode

//* Abstract reference to an existing editor (used when updating an editor)
:: EditorRef (=: EditorRef Int)

/**
* Specification of initialization and updating of a part of a tuple-based composed editor.
* When initializing you can specify if you want to create an empty editor or an editor containing data.
* When refreshing an existing editor, you can choose to replace it with a new editor,
* empty the existing editor, refresh the existing editor with new data or do nothing.
*/
:: TupleSubEditorItem a
	= NewTupleSubEditor (?a) //* Replace by a new editor
	| ExistingTupleSubEditor (?(?a)) //...
		//* Reuse the existing editor
		//* ?None -> keep editor, but don't update.
		//* ?Just ?None -> empty the editor
		//* ?Just (?Just x) -> refresh with a value
/**
* Specification of initialization and updating of a part of a list-based composed editor.
* When initializing you can specify if you want to create an empty editor or an editor containing data.
* When refreshing an existing editor, you can choose to replace it with a new editor,
* or reuse an existing editor.
* You can reference one of the existing editors to use with the abstract `EditorRef`.
* With the existing editor you can choose to empty it, refresh it with new data or use it
* as is.
* This allow you to also reorder the parts of the composition.
*/
:: ListSubEditorItem a
	= NewListSubEditor (?a) //* Replace by a new editor
	| ExistingListSubEditor EditorRef (?(?a)) //...
		//* Reuse an existing editor. The (?(?a)) means:
		//* ?None -> keep editor, but don't update.
		//* ?Just ?None -> empty the editor
		//* ?Just (?Just x) -> refresh with a value
/**
* The `group*` functions make it possible to compose editors.
* There are multiple versions:
*
* - `group` and `group1` create the empty and singleton containers.
* - `groupl` creates a container with a variable number of sub-editors of the same type.
* - `group2`,`group3`,`group4` and `group5` create containers with a fixed number of sub-editors of different types.
*
* All containers have two parameters in common, a `UIType` and a function that defines how the sub-editors are
* updated when (new) data flows into the editor.
* 
* Only the `UIContainer`, `UIPanel`, `UITabSet`, `UIWindow`, `UIMenu`, `UIToolbar`, `UIButtonBar`, `UIList`,
* `UIListItem`, and `UIDebug` constructors of `UIType` should be used, because only these refer to client side
* container components.
*
* The read policy function determines if, and how, the sub-editors of the composition are updated.
* For the common case of refreshing all elements in a composition utility read functions are provided.
*
* `groupc` is a special case for choosing between editors based on the edited value
*/
group :: !UIType -> Editor () ()
groupl :: !UIType 
			!((?rl) [(EditorRef,w)] -> [ListSubEditorItem r])
				!(Editor r w) -> Editor rl [w]

/**
 * Wraps a single editor with a UI component.
 * @param The component type.
 * @param The original editor.
 * @result The wrapped editor.
 */
group1 :: !UIType
			!(Editor r w) -> Editor r w

/**
 * Combines 2 editors to a single one.
 * @param The UI component type to combine the editors with.
 * @param Maps the read and all editors' current write values to sub-editors.
 * @param One of the original editors.
 * @param One of the original editors.
 * @result The combined editors.
 */
group2 :: !UIType
			!((?rt) (?(wa,wb)) -> (TupleSubEditorItem ra,TupleSubEditorItem rb))
				!(Editor ra wa) !(Editor rb wb) -> Editor rt (wa,wb)

/**
 * Combines 3 editors to a single one.
 * @param The UI component type to combine the editors with.
 * @param Maps the read and all editors' current write values to sub-editors.
 * @param One of the original editors.
 * @param One of the original editors.
 * @param One of the original editors.
 * @result The combined editors.
 */
group3 :: !UIType
			!((?rt) (?(wa,wb,wc)) -> (TupleSubEditorItem ra,TupleSubEditorItem rb,TupleSubEditorItem rc))
          		!(Editor ra wa) !(Editor rb wb) !(Editor rc wc) -> Editor rt (wa,wb,wc)

/**
 * Combines 4 editors to a single one.
 * @param The UI component type to combine the editors with.
 * @param Maps the read and all editors' current write values to sub-editors.
 * @param One of the original editors.
 * @param One of the original editors.
 * @param One of the original editors.
 * @param One of the original editors.
 * @result The combined editors.
 */
group4 :: !UIType
			!((?rt) (?(wa,wb,wc,wd)) -> (TupleSubEditorItem ra,TupleSubEditorItem rb,TupleSubEditorItem rc,TupleSubEditorItem rd))
          		!(Editor ra wa) !(Editor rb wb) !(Editor rc wc) !(Editor rd wd) -> Editor rt (wa,wb,wc,wd)

/**
 * Combines 5 editors to a single one.
 * @param The UI component type to combine the editors with.
 * @param Maps the read and all editors' current write values to sub-editors.
 * @param One of the original editors.
 * @param One of the original editors.
 * @param One of the original editors.
 * @param One of the original editors.
 * @param One of the original editors.
 * @result The combined editors.
 */
group5 :: !UIType
			!((?rt) (?(wa,wb,wc,wd,we)) -> (TupleSubEditorItem ra,TupleSubEditorItem rb,TupleSubEditorItem rc,TupleSubEditorItem rd,TupleSubEditorItem re))
              !(Editor ra wa) !(Editor rb wb) !(Editor rc wc) !(Editor rd wd) !(Editor re we) -> Editor rt (wa,wb,wc,wd,we)

/**
 * Combines 6 editors to a single one.
 * @param The UI component type to combine the editors with.
 * @param Maps the read and all editors' current write values to sub-editors.
 * @param One of the original editors.
 * @param One of the original editors.
 * @param One of the original editors.
 * @param One of the original editors.
 * @param One of the original editors.
 * @param One of the original editors.
 * @result The combined editors.
 */
group6 :: !UIType !((?rt) (?(wa,wb,wc,wd,we,wf)) -> (TupleSubEditorItem ra,TupleSubEditorItem rb,TupleSubEditorItem rc,TupleSubEditorItem rd,TupleSubEditorItem re,TupleSubEditorItem rf))
              !(Editor ra wa) !(Editor rb wb) !(Editor rc wc) !(Editor rd wd) !(Editor re we) !(Editor rf wf) -> Editor rt (wa,wb,wc,wd,we,wf)

/**
 * Combines 7 editors to a single one.
 * @param The UI component type to combine the editors with.
 * @param Maps the read and all editors' current write values to sub-editors.
 * @param One of the original editors.
 * @param One of the original editors.
 * @param One of the original editors.
 * @param One of the original editors.
 * @param One of the original editors.
 * @param One of the original editors.
 * @param One of the original editors.
 * @result The combined editors.
 */
group7 :: !UIType !((?rt) (?(wa,wb,wc,wd,we,wf,wg)) -> (TupleSubEditorItem ra,TupleSubEditorItem rb,TupleSubEditorItem rc,TupleSubEditorItem rd,TupleSubEditorItem re,TupleSubEditorItem rf,TupleSubEditorItem rg))
              !(Editor ra wa) !(Editor rb wb) !(Editor rc wc) !(Editor rd wd) !(Editor re we) !(Editor rf wf) !(Editor rg wg) -> Editor rt (wa,wb,wc,wd,we,wf,wg)

/**
* Choose an editor from a list of alternatives
*
* @param The container type
* @param The editor, for choosing the other editors
* @param A function to determine the index in the list based on the read value
* @param A list of editors to use as alternatives, the `(w -> ?r)` function is used to 'keep' the value when choosing another editor
*/
groupc :: !UIType !(Editor Int (EditorReport Int)) !(r -> Int) ![((EditorReport w) -> (?r), Editor r (EditorReport w))] -> Editor r (EditorReport w)

//Utility read functions for common cases
fullUpdate2 :: (?(a,b)) (?(wa,wb)) -> (TupleSubEditorItem a, TupleSubEditorItem b)
fullUpdate3 :: (?(a,b,c)) (?(wa,wb,wc)) -> (TupleSubEditorItem a, TupleSubEditorItem b, TupleSubEditorItem c)
fullUpdate4 :: (?(a,b,c,d)) (?(wa,wb,wc,wd)) -> (TupleSubEditorItem a, TupleSubEditorItem b, TupleSubEditorItem c, TupleSubEditorItem d)
fullUpdate5 :: (?(a,b,c,d,e)) (?(wa,wb,wc,wd,we)) -> (TupleSubEditorItem a, TupleSubEditorItem b, TupleSubEditorItem c, TupleSubEditorItem d, TupleSubEditorItem e)
fullUpdate6 :: (?(a,b,c,d,e,f)) (?(wa,wb,wc,wd,we,wf)) -> (TupleSubEditorItem a, TupleSubEditorItem b, TupleSubEditorItem c, TupleSubEditorItem d, TupleSubEditorItem e, TupleSubEditorItem f)
fullUpdate7 :: (?(a,b,c,d,e,f,g)) (?(wa,wb,wc,wd,we,wf,wg)) -> (TupleSubEditorItem a, TupleSubEditorItem b, TupleSubEditorItem c, TupleSubEditorItem d, TupleSubEditorItem e, TupleSubEditorItem f, TupleSubEditorItem g)

fullUpdateList :: (?[r]) [(EditorRef,w)] -> [ListSubEditorItem r]

//# UIContainer
container :== group UIContainer
container1 e :== group1 UIContainer e
container2 e1 e2 :== group2 UIContainer fullUpdate2 e1 e2
container3 e1 e2 e3 :== group3 UIContainer fullUpdate3 e1 e2 e3
container4 e1 e2 e3 e4 :== group4 UIContainer fullUpdate4 e1 e2 e3 e4
container5 e1 e2 e3 e4 e5 :== group5 UIContainer fullUpdate5 e1 e2 e3 e4 e5
container6 e1 e2 e3 e4 e5 e6 :== group6 UIContainer fullUpdate6 e1 e2 e3 e4 e5 e6
container7 e1 e2 e3 e4 e5 e6 e7 :== group7 UIContainer fullUpdate7 e1 e2 e3 e4 e5 e6 e7
containerl e :== groupl UIContainer fullUpdateList e
containerc ec es :== groupc UIContainer ec es

//# UIPanel
panel :== group UIPanel
panel1 e :== group1 UIPanel e
panel2 e1 e2 :== group2 UIPanel fullUpdate2 e1 e2
panel3 e1 e2 e3 :== group3 UIPanel fullUpdate3 e1 e2 e3
panel4 e1 e2 e3 e4 :== group4 UIPanel fullUpdate4 e1 e2 e3 e4
panel5 e1 e2 e3 e4 e5 :== group5 UIPanel fullUpdate5 e1 e2 e3 e4 e5
panel6 e1 e2 e3 e4 e5 e6 :== group6 UIPanel fullUpdate6 e1 e2 e3 e4 e5 e6
panel7 e1 e2 e3 e4 e5 e6 e7 :== group7 UIPanel fullUpdate7 e1 e2 e3 e4 e5 e6 e7
panell e :== groupl UIPanel fullUpdateList e
panelc ec es :== groupc UIPanel ec es

//# UITabSet
tabset :== group UITabSet
tabset1 e :== group1 UITabSet e
tabset2 e1 e2 :== group2 UITabSet fullUpdate2 e1 e2
tabset3 e1 e2 e3 :== group3 UITabSet fullUpdate3 e1 e2 e3
tabset4 e1 e2 e3 e4 :== group4 UITabSet fullUpdate4 e1 e2 e3 e4
tabset5 e1 e2 e3 e4 e5 :== group5 UITabSet fullUpdate5 e1 e2 e3 e4 e5
tabset6 e1 e2 e3 e4 e5 e6 :== group6 UITabSet fullUpdate6 e1 e2 e3 e4 e5 e6
tabset7 e1 e2 e3 e4 e5 e6 e7 :== group7 UITabSet fullUpdate7 e1 e2 e3 e4 e5 e6 e7
tabsetl e :== groupl UITabSet fullUpdateList e
tabsetc ec es :== groupc UITabSet ec es

//# UIWindow
window :== group UIWindow
window1 e :== group1 UIWindow e
window2 e1 e2 :== group2 UIWindow fullUpdate2 e1 e2
window3 e1 e2 e3 :== group3 UIWindow fullUpdate3 e1 e2 e3
window4 e1 e2 e3 e4 :== group4 UIWindow fullUpdate4 e1 e2 e3 e4
window5 e1 e2 e3 e4 e5 :== group5 UIWindow fullUpdate5 e1 e2 e3 e4 e5
window6 e1 e2 e3 e4 e5 e6 :== group6 UIWindow fullUpdate6 e1 e2 e3 e4 e5 e6
window7 e1 e2 e3 e4 e5 e6 e7 :== group7 UIWindow fullUpdate7 e1 e2 e3 e4 e5 e6 e7
windowl e :== groupl UIWindow fullUpdateList e
windowc ec es :== groupc UIWindow ec es

//# UIMenu
menu :== group UIMenu
menu1 e :== group1 UIMenu e
menu2 e1 e2 :== group2 UIMenu fullUpdate2 e1 e2
menu3 e1 e2 e3 :== group3 UIMenu fullUpdate3 e1 e2 e3
menu4 e1 e2 e3 e4 :== group4 UIMenu fullUpdate4 e1 e2 e3 e4
menu5 e1 e2 e3 e4 e5 :== group5 UIMenu fullUpdate5 e1 e2 e3 e4 e5
menu6 e1 e2 e3 e4 e5 e6 :== group6 UIMenu fullUpdate6 e1 e2 e3 e4 e5 e6
menu7 e1 e2 e3 e4 e5 e6 e7 :== group7 UIMenu fullUpdate7 e1 e2 e3 e4 e5 e6 e7
menul e :== groupl UIMenu fullUpdateList e
menuc ec es :== groupc UIMenu ec es

//# UIToolBar
toolbar :== group UIToolBar
toolbar1 e :== group1 UIToolBar e
toolbar2 e1 e2 :== group2 UIToolBar fullUpdate2 e1 e2
toolbar3 e1 e2 e3 :== group3 UIToolBar fullUpdate3 e1 e2 e3
toolbar4 e1 e2 e3 e4 :== group4 UIToolBar fullUpdate4 e1 e2 e3 e4
toolbar5 e1 e2 e3 e4 e5 :== group5 UIToolBar fullUpdate5 e1 e2 e3 e4 e5
toolbar6 e1 e2 e3 e4 e5 e6 :== group6 UIToolBar fullUpdate6 e1 e2 e3 e4 e5 e6
toolbar7 e1 e2 e3 e4 e5 e6 e7 :== group7 UIToolBar fullUpdate7 e1 e2 e3 e4 e5 e6 e7
toolbarl e :== groupl UIToolBar fullUpdateList e
toolbarc ec es :== groupc UIToolBar ec es

//# UIButtonBar
buttonbar :== group UIButtonBar
buttonbar1 e :== group1 UIButtonBar e
buttonbar2 e1 e2 :== group2 UIButtonBar fullUpdate2 e1 e2
buttonbar3 e1 e2 e3 :== group3 UIButtonBar fullUpdate3 e1 e2 e3
buttonbar4 e1 e2 e3 e4 :== group4 UIButtonBar fullUpdate4 e1 e2 e3 e4
buttonbar5 e1 e2 e3 e4 e5 :== group5 UIButtonBar fullUpdate5 e1 e2 e3 e4 e5
buttonbar6 e1 e2 e3 e4 e5 e6 :== group6 UIButtonBar fullUpdate6 e1 e2 e3 e4 e5 e6
buttonbar7 e1 e2 e3 e4 e5 e6 e7 :== group7 UIButtonBar fullUpdate7 e1 e2 e3 e4 e5 e6 e7
buttonbarl e :== groupl UIButtonBar fullUpdateList e
buttonbarc ec es :== groupc UIButtonBar ec es

//# UIList
list :== group UIList
list1 e :== group1 UIList e
list2 e1 e2 :== group2 UIList fullUpdate2 e1 e2
list3 e1 e2 e3 :== group3 UIList fullUpdate3 e1 e2 e3
list4 e1 e2 e3 e4 :== group4 UIList fullUpdate4 e1 e2 e3 e4
list5 e1 e2 e3 e4 e5 :== group5 UIList fullUpdate5 e1 e2 e3 e4 e5
list6 e1 e2 e3 e4 e5 e6 :== group6 UIList fullUpdate6 e1 e2 e3 e4 e5 e6
list7 e1 e2 e3 e4 e5 e6 e7 :== group7 UIList fullUpdate7 e1 e2 e3 e4 e5 e6 e7
listl e :== groupl UIList fullUpdateList e
listc ec es :== groupc UIList ec es

//# UIListItem
listitem :== group UIListItem
listitem1 e :== group1 UIListItem e
listitem2 e1 e2 :== group2 UIListItem fullUpdate2 e1 e2
listitem3 e1 e2 e3 :== group3 UIListItem fullUpdate3 e1 e2 e3
listitem4 e1 e2 e3 e4 :== group4 UIListItem fullUpdate4 e1 e2 e3 e4
listitem5 e1 e2 e3 e4 e5 :== group5 UIListItem fullUpdate5 e1 e2 e3 e4 e5
listitem6 e1 e2 e3 e4 e5 e6 :== group6 UIListItem fullUpdate6 e1 e2 e3 e4 e5 e6
listitem7 e1 e2 e3 e4 e5 e6 e7 :== group7 UIListItem fullUpdate7 e1 e2 e3 e4 e5 e6 e7
listiteml e :== groupl UIListItem fullUpdateList e
listitemc ec es :== groupc UIListItem ec es

//# UIDebug
debug :== group UIDebug
debug1 e :== group1 UIDebug e
debug2 e1 e2 :== group2 UIDebug fullUpdate2 e1 e2
debug3 e1 e2 e3 :== group3 UIDebug fullUpdate3 e1 e2 e3
debug4 e1 e2 e3 e4 :== group4 UIDebug fullUpdate4 e1 e2 e3 e4
debug5 e1 e2 e3 e4 e5 :== group5 UIDebug fullUpdate5 e1 e2 e3 e4 e5
debug6 e1 e2 e3 e4 e5 e6 :== group6 UIDebug fullUpdate6 e1 e2 e3 e4 e5 e6
debug7 e1 e2 e3 e4 e5 e6 e7 :== group7 UIDebug fullUpdate7 e1 e2 e3 e4 e5 e6 e7
debugl e :== groupl UIDebug fullUpdateList e
debugc ec es :== groupc UIDebug ec es

