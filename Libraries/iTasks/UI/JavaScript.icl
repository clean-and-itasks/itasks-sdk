implementation module iTasks.UI.JavaScript

import Data.Func
import qualified Data.Map

import ABC.Interpreter.JavaScript

import iTasks
import iTasks.Internal.IWorld
import iTasks.Internal.Task

/* Needed for step in tune instance for Task; not used */
JSONEncode{|PrelinkedInterpretationEnvironment|} _ _ = [JSONError]
JSONDecode{|PrelinkedInterpretationEnvironment|} _ j = (?None,j)

instance tune JavaScriptInitFunction (Task a)
where
	tune (JavaScriptInit f) task =
		get applicationOptions >>- \engineOptions ->
		getAbcEnv >>- \abcEnv ->
		let
			frontendOptions =
				{ FrontendEngineOptions
				| serverDirectory = engineOptions.EngineOptions.serverDirectory
				}
			serialized = jsSerializeGraph (wrapInitFunction (hyperstrict (f frontendOptions))) abcEnv
		in
		("initUI", JSONString serialized) @>> task
	where
		getAbcEnv = mkInstantTask \_ iworld=:{IWorld|abcInterpreterEnv} -> (Ok abcInterpreterEnv, iworld)

instance tune JavaScriptInitFunction (Editor r w)
where
	tune (JavaScriptInit f) editor=:{Editor|onReset,onEdit,onRefresh} =
		{ Editor | editor
		& onReset   = withClientSideInitOnReset f onReset
		, onEdit    = withClientSideInitOnEdit f onEdit
		, onRefresh = \val st vst -> withClientSideInitOnReplaceUi f $ onRefresh val st vst
		}

instance tune JavaScriptInitFunction (LeafEditor edit st r w)
where
	tune (JavaScriptInit f) editor=:{LeafEditor|onReset,onEdit,onRefresh} =
		{ LeafEditor | editor
		& onReset   = withClientSideInitOnReset f onReset
		, onEdit    = \ev st vst -> withClientSideInitOnReplaceUi f $ onEdit ev st vst
		, onRefresh = \val st vst -> withClientSideInitOnReplaceUi f $ onRefresh val st vst
		}

withClientSideInitOnReset ::
	!(FrontendEngineOptions JSVal *JSWorld -> *JSWorld)
	!(UIAttributes -> .(.a -> *(*VSt -> *(u:MaybeErrorString (!UI, !st, !?w), *VSt))))
	!UIAttributes !.a !*VSt
	-> *(!v:MaybeErrorString (!UI, !st, !?w), !*VSt)
	, [u <= v]
withClientSideInitOnReset f onReset attr val vst = case onReset attr val vst of
	(Ok (ui,mask, mbw),vst)
		# (ui, vst) = uiWithClientSideInit f ui vst
		= (Ok (ui,mask,mbw), vst)
	e = e

withClientSideInitOnEdit f onEdit event st vst = case onEdit event st vst of
	(Ok (?Just (ReplaceUI ui,st, mbw)),vst)
		# (ui, vst) = uiWithClientSideInit f ui vst
		= (Ok (?Just (ReplaceUI ui,st,mbw)), vst)
	e = e

withClientSideInitOnReplaceUi ::
	!(FrontendEngineOptions JSVal *JSWorld -> *JSWorld)
	!*(!u:MaybeErrorString v:(UIChange, st, w: ?w), !*VSt)
	-> *(!x:MaybeErrorString y:(UIChange, st, z: ?w), !*VSt)
	, [u <= v, v <= w, u <= x, x <= y, v <= y, y w <= z] // don't ask.
withClientSideInitOnReplaceUi f (Ok (ReplaceUI ui, st, val), vst)
	# (ui, vst) = uiWithClientSideInit f ui vst
	= (Ok (ReplaceUI ui, st, val), vst)
withClientSideInitOnReplaceUi _ res
	= res

uiWithClientSideInit :: !(FrontendEngineOptions JSVal *JSWorld -> *JSWorld) !UI !*VSt -> (!UI, !*VSt)
uiWithClientSideInit f (UI type attr items) vst=:{taskId,engineOptions,abcInterpreterEnv}
	# frontendOptions =
		{ FrontendEngineOptions
		| serverDirectory = engineOptions.EngineOptions.serverDirectory
		}
	# initUI = jsSerializeGraph (wrapInitFunction (hyperstrict (f frontendOptions))) abcInterpreterEnv
	# extraAttr = 'Data.Map'.fromList
		[("taskId",  JSONString taskId)
		,("initUI",  JSONString initUI)
		]
	= (UI type ('Data.Map'.union extraAttr attr) items, vst)
