implementation module iTasks.WF.Tasks.Interaction

from StdFunc import id, const, o, flip
import Data.Func
import Data.Functor
import Data.GenHash
from Data.Tuple import appSnd
from Data.List import isMemberGen
from Data.Map import qualified get, put
import qualified Data.Map as DM
import Data.Maybe

import StdBool, StdList, StdMisc, StdTuple, Data.Functor, StdString
import iTasks.WF.Derives
import iTasks.WF.Tasks.Core
import iTasks.WF.Tasks.SDS
import iTasks.WF.Combinators.Common
import iTasks.WF.Combinators.SDS
import iTasks.SDS.Combinators.Core
import iTasks.SDS.Sources.Core
import iTasks.SDS.Sources.System
import iTasks.SDS.Combinators.Common
import iTasks.Internal.Util
import iTasks.Internal.SDS
import iTasks.Internal.Task
import iTasks.Internal.TaskEval
import iTasks.UI.Layout, iTasks.UI.Definition, iTasks.UI.Editor, iTasks.UI.Editor.Controls, iTasks.UI.Editor.Modifiers
import Text.HTML, Text.HTML.GenJSON

derive class iTask ChoiceText, ChoiceGrid, ChoiceRow, ChoiceNode

//Boilerplate access functions
selectAttributes :: [SelectOption a b] -> UIAttributes
selectAttributes options = foldr addOption 'DM'.newMap options
where
	addOption (SelectMultiple multiple) attr = 'DM'.union (multipleAttr multiple) attr
	addOption _ attr = attr

viewEditor :: [ViewOption m] -> ViewOption m | iTask m
viewEditor [ViewUsing tof editor:_] = ViewUsing tof editor
viewEditor [ViewAs tof:_] = ViewUsing tof (gEditor{|*|} ViewValue)
viewEditor [_:es] = viewEditor es
viewEditor [] =  ViewUsing id (gEditor{|*|} ViewValue)

enterEditor :: [EnterOption m] -> EnterOption m | iTask m
enterEditor [EnterUsing fromf editor:_] = EnterUsing fromf editor
enterEditor [EnterAs fromf:_] = EnterUsing fromf (gEditor{|*|} EditValue)
enterEditor [_:es] = enterEditor es
enterEditor [] =  EnterUsing id (gEditor{|*|} EditValue)

updateEditor :: [UpdateOption m] -> UpdateOption m | iTask m
updateEditor [UpdateUsing tof fromf editor:_] = UpdateUsing tof fromf editor
updateEditor [UpdateAs tof fromf:_] = UpdateUsing tof fromf (gEditor{|*|} EditValue)
updateEditor [_:es] = updateEditor es
updateEditor [] =  UpdateUsing id (flip const) (gEditor{|*|} EditValue)

updateSharedEditor :: [UpdateSharedOption r w] -> UpdateSharedOption r w | iTask r & iTask w
updateSharedEditor [UpdateSharedUsing tof fromf editor:_] = UpdateSharedUsing tof fromf editor
updateSharedEditor [UpdateSharedAs tof fromf :_] = UpdateSharedUsing tof fromf (gEditor{|*|} EditValue)
updateSharedEditor [_:es] = updateSharedEditor es
updateSharedEditor [] = UpdateSharedUsing id (\_ v -> dynid v) (gEditor{|*|} EditValue)
where
	//If r == w then this is just the identity, otherwise the editor will use a default value
	//A default is only possible when r == w
	dynid x = case dynamic id :: A.a: (a -> a) of
		(rtow :: r^ -> w^) = rtow x
		_                  = abort "Update shared information with default editor only possible for symmetric sds's\n"

selectEditor :: [SelectOption c a] -> SelectOption c a
selectEditor [SelectInDropdown toView fromView:_] = SelectUsing toView fromView dropdown
selectEditor [SelectInCheckGroup toView fromView:_] = SelectUsing toView fromView checkGroup
selectEditor [SelectInList toView fromView:_] = SelectUsing toView fromView choiceList
selectEditor [SelectInGrid toView fromView:_] = SelectUsing toView fromView grid
selectEditor [SelectInTree toView fromView:_] = SelectUsing toView fromView tree
selectEditor [SelectInTabs toView fromView:_] = SelectUsing toView fromView tabBar
selectEditor [SelectUsing toView fromView editor:_] = SelectUsing toView fromView editor
selectEditor [_:es] = selectEditor es
selectEditor [] = SelectUsing (const []) (\_ _ -> []) dropdown //Empty dropdown

//Convert choice options to select options
selectOptions :: (o -> s) [ChoiceOption o] -> [SelectOption [o] s] | gText{|*|} o & JSONEncode{|*|} o
selectOptions target options = selectOptions` False options
where
	selectOptions` _ [ChooseFromDropdown f:os] = [SelectInDropdown (toTexts f) (findSelection (genChoiceID o f) target):selectOptions` True os]
	selectOptions` _ [ChooseFromCheckGroup f:os] = [SelectInCheckGroup (toTexts f) (findSelection (genChoiceID o f) target):selectOptions` True os]
	selectOptions` _ [ChooseFromList f:os] = [SelectInList (toTexts f) (findSelection (genChoiceID o f) target):selectOptions` True os]
	selectOptions` _ [ChooseFromGrid f:os] = [SelectInGrid (toGrid f) (findSelection (genChoiceID o f) target):selectOptions` True os]
	selectOptions` _ [ChooseFromTabs f:os] = [SelectInTabs (toTexts f) (findSelection (genChoiceID o f) target):selectOptions` True os]
	selectOptions` True [] = []
	selectOptions` False [] = [SelectInDropdown (toTexts id) (findSelection genChoiceID target)]

	toTexts f options = [let v = f o in {ChoiceText|id=genChoiceID v,text=toSingleLineText v} \\ o <- options]
	toGrid f options =
		{ChoiceGrid|header=gText{|*|} AsHeader (fixtype vals),rows =
			[{ChoiceRow|id=genChoiceID v,cells=map Text (gText{|*|} AsRow (?Just v))} \\ v <- vals]}
	where
		vals = map f options
		fixtype :: [a] -> ?a
		fixtype _ = ?None

//IMPORTANT: This function needs to be kept in sync with `selectOptions`.
//Whatever identification is used in `selectOptions` should be returned here.
choiceIdentifier :: ![ChoiceOption o] -> (o -> ChoiceID) | JSONEncode{|*|} o
choiceIdentifier [ChooseFromDropdown f:_] = genChoiceID o f
choiceIdentifier [ChooseFromCheckGroup f:_] = genChoiceID o f
choiceIdentifier [ChooseFromList f:_] = genChoiceID o f
choiceIdentifier [ChooseFromGrid f:_] = genChoiceID o f
choiceIdentifier [ChooseFromTabs f:_] = genChoiceID o f
choiceIdentifier [_:os] = choiceIdentifier os
choiceIdentifier [] = genChoiceID

findSelection :: (o -> ChoiceID) (o -> s) [o] [ChoiceID] -> [s]
findSelection identify target options selection = [target option \\ option <- options | isMember (identify option) selection]

enterInformation :: ![EnterOption m] -> Task m | iTask m
enterInformation options = enterInformation` (enterEditor options)
enterInformation` (EnterUsing fromf editor)
	= withShared ?None (interactRW (ignoreEditorReads $ mapEditorWrite (editorReportToMaybe o fmap fromf) editor))

viewInformation :: ![ViewOption m] !m -> Task m | iTask m
viewInformation options m = viewInformation` (viewEditor options) m
viewInformation` (ViewUsing tof editor) m
	// A task id is generated to create a unique SDS identifier for the constShare.
	= mkInstantTask (\{TaskEvalOpts|taskId} iworld = (Ok taskId, iworld)) >>- \taskId =
		interactR
			(mapEditorWrite (const ()) $ mapEditorRead tof editor)
			(constShareNamed (toString $ gHash {|*|} taskId) (?Just m))

updateInformation :: ![UpdateOption m] m -> Task m | iTask m
updateInformation options m = updateInformation` (updateEditor options) m
updateInformation` (UpdateUsing tof fromf editor) m
	= withShared (?Just m) (interactRW $ mapEditorRead tof $ mapEditorWrite (editorReportToMaybe o fmap (fromf m)) editor)

updateSharedInformation :: ![UpdateSharedOption r w] !(sds () r w) -> Task r | iTask r & iTask w & RWShared sds
updateSharedInformation options sds = updateSharedInformation` (updateSharedEditor options) sds
updateSharedInformation` (UpdateSharedUsing tof fromf editor) sds =
	// We have to keep track of the editor's validity in a local SDS. Before the first task eval we don't know the
	// validity of the value which depends on the editor. If the editors writes back a value the first time, the
	// validity is known and we can provide a task value.
	withShared ?None \valid ->
	interactRW
		(mapEditorWithState () toEditorR (\w unit = (w, unit)) $ editor)
		(sdsParallel
			"updateSharedInformation" (\p = (p, p)) toSdsR (SDSWrite toSdsW1) (SDSWriteConst toSdsW2) sds valid)
	@? \res -> case res of
		NoValue = NoValue
		Value (v,valid) s = if (valid =: (?Just True)) (Value v s) NoValue
where
	// We have to provide `?None` to `interactRW`, if the editor is not valid.
	toSdsR :: !(r, !?Bool) -> ?(r, ?Bool)
	toSdsR val=:(_, valid) = if (valid =: (?Just False)) ?None (?Just val)

	// `fromf` has to be applied in the SDS combinator, as we need the current, original value read from the SDS.
	toSdsW1 :: E.^ v r w: idc !r !(EditorReport v) -> MaybeError TaskException (?w)
	toSdsW1 _ r editorRep = Ok $ fromf r <$> editorReportToMaybe editorRep

	toSdsW2 :: idc !(EditorReport v) -> MaybeError TaskException (?(?Bool))
	toSdsW2 _ editorRep = Ok $ ?Just $ ?Just $ editorRep =: (ValidEditor _)

	toEditorR :: E.^ v r: !(!r, !?Bool) !() -> (?v, !())
	toEditorR (r, valid) unit = (if (valid =: (?Just False)) ?None (?Just $ tof r), unit)

updateInformationWithShared :: ![UpdateSharedOption (r,m) m] !(sds () r w) m -> Task m | iTask r & iTask m & TC w & RWShared sds
updateInformationWithShared options sds m = updateInformationWithShared` (updateSharedEditor options) sds m
updateInformationWithShared` (UpdateSharedUsing tof fromf editor) sds model =
	// We have to keep track of the editor's validity (together with the current model value) in a local SDS. Before the
	// first task eval we don't know the validity of the value which depends on the editor. If the editors writes back a
	// value the first time, the validity is known and we can provide a task value.
	withShared (model, ?None) \sdsm ->
	interactRW (mapEditorRead toEditorR editor) (mapReadWrite (toSdsR, toSdsW) ?None (sds |*< sdsm))
	@? \res -> case res of
		NoValue = NoValue
		Value (_, (m, valid)) s = if (valid =: (?Just True)) (Value m s) NoValue
where
	// We have to provide `?None` to `interactRW`, if the editor is not valid.
	toSdsR :: !(r, !(m, !?Bool)) -> ?(r, (m, ?Bool))
	toSdsR val=:(_, (_, valid)) = if (valid =: (?Just False)) ?None (?Just val)

	// `fromf` has to be applied in the SDS combinator, as we need the current, original value read from the SDS.
	toSdsW :: E.^ v r m: !(EditorReport v) !(!r, !(!m, idc)) -> ?(m, ?Bool)
	toSdsW v (r, (m, _)) = ?Just (maybe m (fromf (r, m)) $ editorReportToMaybe v, ?Just $ v =: (ValidEditor _))

	toEditorR :: E.^ v r m: !(!r, !(!m, idc)) -> v
	toEditorR (r, (m, _)) = tof (r, m)

viewSharedInformation :: ![ViewOption r] !(sds () r w) -> Task r | iTask r & TC w & RWShared sds
viewSharedInformation options sds = viewSharedInformation` (viewEditor options) sds
viewSharedInformation` (ViewUsing tof editor) sds
	= interactR (mapEditorWrite (const ()) $ mapEditorRead tof editor) (mapRead ?Just $ toReadOnly sds)

editSelection :: ![SelectOption c a] c [ChoiceID] -> Task [a] | iTask a
editSelection options container sel = editSelection` (selectAttributes options) (selectEditor options) container sel
editSelection` attributes (SelectUsing toView fromView editor) container sel
	= withShared (?Just sel) (interactRW
		(mapEditorRead (\sel -> (toView container,sel)) $ mapEditorWrite ?Just (withAttributes attributes editor)))
	@ (fromView container)

editSelectionWithShared :: ![SelectOption c a] (sds () c w) (c -> [ChoiceID]) -> Task [a] | iTask c & iTask a & TC w & RWShared sds
editSelectionWithShared options sharedContainer initSel = editSelectionWithShared` (selectAttributes options) (selectEditor options) sharedContainer initSel
editSelectionWithShared` attributes (SelectUsing toView fromView editor) sharedContainer initSel
	= withShared [] \selsds -> let state = sharedContainer |*< selsds in
		    upd (\(c,_) -> initSel c) state //Initialize the selection
		>-| interactRW (mapEditorRead (\(c,r) -> (toView c,r)) (withAttributes attributes editor)) (mapRead ?Just state)
	@ (\(container,sel) -> fromView container sel)

editSharedSelection :: ![SelectOption c a] c (Shared sds [ChoiceID]) -> Task [a] | iTask c & iTask a & RWShared sds
editSharedSelection options container sharedSel = editSharedSelection` (selectAttributes options) (selectEditor options) container sharedSel
editSharedSelection` attributes (SelectUsing toView fromView editor) container sharedSel
	= interactRW (mapEditorRead (\r -> (toView container,r)) (withAttributes attributes editor)) (mapRead ?Just sharedSel)
	@ (fromView container)

editSharedSelectionWithShared :: ![SelectOption c a] (sds1 () c w) (Shared sds2 [ChoiceID]) -> Task [a] | iTask c & iTask a & TC w & RWShared sds1 & RWShared sds2
editSharedSelectionWithShared options sharedContainer sharedSel
	= editSharedSelectionWithShared` (selectAttributes options) (selectEditor options) sharedContainer sharedSel
editSharedSelectionWithShared` attributes (SelectUsing toView fromView editor) sharedContainer sharedSel
	= interactRW (mapEditorRead (\(rc,rs) -> (toView rc,rs)) (withAttributes attributes editor)) (mapRead ?Just (sharedContainer |*< sharedSel))
	@ (\(container,sel) -> fromView container sel)

//Core choice tasks
editChoice :: ![ChoiceOption a] ![a] (?a) -> Task a | iTask a
editChoice options container mbSel = editChoiceAs options container id mbSel

editChoiceAs :: ![ChoiceOption o] ![o] !(o -> a) (?a) -> Task a | iTask o & iTask a
editChoiceAs vopts container target mbSel
	= editSelection [SelectMultiple False:selectOptions target vopts] container (findChoiceIDs vopts target container $ maybeToList mbSel) @? tvHd

editMultipleChoice :: ![ChoiceOption a] ![a] [a] -> Task [a] | iTask a
editMultipleChoice options container mbSel = editMultipleChoiceAs options container id mbSel

editMultipleChoiceAs :: ![ChoiceOption o] ![o] !(o -> a) [a] -> Task [a] | iTask o & iTask a
editMultipleChoiceAs vopts container target sel
	= editSelection [SelectMultiple True:selectOptions target vopts] container (findChoiceIDs vopts target container sel)

enterChoice :: ![ChoiceOption a] ![a] -> Task a | iTask a
enterChoice options container = editChoice options container ?None

enterChoiceAs :: ![ChoiceOption o] ![o] !(o -> a) -> Task a | iTask o & iTask a
enterChoiceAs options container targetFun = editChoiceAs options container targetFun ?None

enterMultipleChoice :: ![ChoiceOption a] ![a] -> Task [a] | iTask a
enterMultipleChoice options container = editMultipleChoice options container []

enterMultipleChoiceAs :: ![ChoiceOption o] ![o] !(o -> a) -> Task [a] | iTask o & iTask a
enterMultipleChoiceAs options container targetFun = editMultipleChoiceAs options container targetFun []

updateChoice :: ![ChoiceOption a] ![a] a -> Task a | iTask a
updateChoice options container sel = editChoice options container (?Just sel)

updateChoiceAs :: ![ChoiceOption o] ![o] !(o -> a) a -> Task a | iTask o & iTask a
updateChoiceAs options container targetFun sel = editChoiceAs options container targetFun (?Just sel)

updateMultipleChoice :: ![ChoiceOption a] ![a] [a] -> Task [a] | iTask a
updateMultipleChoice options container sel = editMultipleChoice options container sel

updateMultipleChoiceAs :: ![ChoiceOption o] ![o] !(o -> a) [a] -> Task [a] | iTask o & iTask a
updateMultipleChoiceAs options container targetFun sel = editMultipleChoiceAs options container targetFun sel

editChoiceWithShared :: ![ChoiceOption a] !(sds () [a] w) (?a) -> Task a | iTask a & TC w & RWShared sds
editChoiceWithShared options container mbSel = editChoiceWithSharedAs options container id mbSel

editChoiceWithSharedAs :: ![ChoiceOption o] !(sds () [o] w) (o -> a) (?a) -> Task a | iTask o & TC w & iTask a & RWShared sds
editChoiceWithSharedAs vopts sharedContainer target mbSel
	= editSelectionWithShared [SelectMultiple False:selectOptions target vopts] sharedContainer
		(\container -> findChoiceIDs vopts target container $ maybeToList mbSel) @? tvHd

editMultipleChoiceWithShared :: ![ChoiceOption a] !(sds () [a] w) [a] -> Task [a] | iTask a & TC w & RWShared sds
editMultipleChoiceWithShared options container sel = editMultipleChoiceWithSharedAs options container id sel

editMultipleChoiceWithSharedAs :: ![ChoiceOption o] !(sds () [o] w) (o -> a) [a] -> Task [a] | iTask o & TC w & iTask a & RWShared sds
editMultipleChoiceWithSharedAs vopts sharedContainer target sel
	= editSelectionWithShared [SelectMultiple True:selectOptions target vopts] sharedContainer (\container -> findChoiceIDs vopts target container sel)

enterChoiceWithShared :: ![ChoiceOption a] !(sds () [a] w) -> Task a | iTask a & TC w & RWShared sds
enterChoiceWithShared options container = editChoiceWithShared options container ?None

enterChoiceWithSharedAs :: ![ChoiceOption o] !(sds () [o] w) (o -> a) -> Task a | iTask o & TC w & iTask a & RWShared sds
enterChoiceWithSharedAs options container targetFun = editChoiceWithSharedAs options container targetFun ?None

enterMultipleChoiceWithShared :: ![ChoiceOption a] !(sds () [a] w) -> Task [a] | iTask a & TC w & RWShared sds
enterMultipleChoiceWithShared options container = editMultipleChoiceWithShared options container []

enterMultipleChoiceWithSharedAs :: ![ChoiceOption o] !(sds () [o] w) (o -> a) -> Task [a] | iTask o & TC w & iTask a & RWShared sds
enterMultipleChoiceWithSharedAs options container targetFun = editMultipleChoiceWithSharedAs options container targetFun []

updateChoiceWithShared :: ![ChoiceOption a] !(sds () [a] w) a -> Task a | iTask a & TC w & RWShared sds
updateChoiceWithShared options container sel = editChoiceWithShared options container (?Just sel)

updateChoiceWithSharedAs :: ![ChoiceOption o] !(sds () [o] w) (o -> a) a -> Task a | iTask o & TC w & iTask a & RWShared sds
updateChoiceWithSharedAs options container targetFun sel = editChoiceWithSharedAs options container targetFun (?Just sel)

updateMultipleChoiceWithShared :: ![ChoiceOption a] !(sds () [a] w) [a] -> Task [a] | iTask a & TC w & RWShared sds
updateMultipleChoiceWithShared options container sel = editMultipleChoiceWithShared options container sel

updateMultipleChoiceWithSharedAs :: ![ChoiceOption o] !(sds () [o] w) (o -> a) [a] -> Task [a] | iTask o & TC w & iTask a & RWShared sds
updateMultipleChoiceWithSharedAs options container targetFun sel = editMultipleChoiceWithSharedAs options container targetFun sel

editSharedChoice :: ![ChoiceOption a] ![a] (Shared sds (?a)) -> Task a | iTask a & RWShared sds
editSharedChoice options container sharedSel = editSharedChoiceAs options container id sharedSel

editSharedChoiceAs :: ![ChoiceOption o] ![o] !(o -> a) (Shared sds (?a)) -> Task a | iTask o & iTask a & RWShared sds
editSharedChoiceAs vopts container target sharedSel
	= editSharedSelection [SelectMultiple False:selectOptions target vopts] container (choiceIDShare vopts target container sharedSel) @? tvHd

editSharedMultipleChoice :: ![ChoiceOption a] ![a] (Shared sds [a]) -> Task [a] | iTask a & RWShared sds
editSharedMultipleChoice options container sharedSel = editSharedMultipleChoiceAs options container id sharedSel

editSharedMultipleChoiceAs :: ![ChoiceOption o] ![o] !(o -> a) (Shared sds [a]) -> Task [a] | iTask o & iTask a & RWShared sds
editSharedMultipleChoiceAs vopts container target sharedSel
	= editSharedSelection [SelectMultiple True:selectOptions target vopts] container (choiceIDsShare vopts target container sharedSel)

editSharedChoiceWithShared :: ![ChoiceOption a] !(sds1 () [a] w) (Shared sds2 (?a)) -> Task a | iTask a & TC w & RWShared sds1 & RWShared sds2
editSharedChoiceWithShared options sharedContainer sharedSel = editSharedChoiceWithSharedAs options sharedContainer id sharedSel

editSharedChoiceWithSharedAs :: ![ChoiceOption o] !(sds1 () [o] w) (o -> a) (Shared sds2 (?a)) -> Task a | iTask o & TC w & iTask a & RWShared sds1 & RWShared sds2
editSharedChoiceWithSharedAs vopts sharedContainer target sharedSel
	= editSharedSelectionWithShared [SelectMultiple False:selectOptions target vopts] sharedContainer
		(choiceIDShareWithShared vopts target (sharedContainer |*< sharedSel)) @? tvHd

editSharedMultipleChoiceWithShared :: ![ChoiceOption a] !(sds1 () [a] w) (Shared sds2 [a]) -> Task [a] | iTask a & TC w & RWShared sds1 & RWShared sds2
editSharedMultipleChoiceWithShared options sharedContainer sharedSel = editSharedMultipleChoiceWithSharedAs options sharedContainer id sharedSel

editSharedMultipleChoiceWithSharedAs :: ![ChoiceOption o] !(sds1 () [o] w) (o -> a) (Shared sds2 [a]) -> Task [a] | iTask o & TC w & iTask a & RWShared sds1 & RWShared sds2
editSharedMultipleChoiceWithSharedAs vopts sharedContainer target sharedSel
	= editSharedSelectionWithShared [SelectMultiple True:selectOptions target vopts] sharedContainer (choiceIDsShareWithShared vopts target (sharedContainer |*< sharedSel))

findChoiceIDs :: ![ChoiceOption o]  (o -> a) ![o] [a] -> [ChoiceID] | gEq{|*|} a & JSONEncode{|*|} o
findChoiceIDs vopts target options selection = [identify o \\ o <- options | isMemberGen (target o) selection]
where
	identify = choiceIdentifier vopts

choiceIDShare :: ![ChoiceOption o] (o -> a) [o] (Shared sds (?a)) -> SimpleSDSLens [ChoiceID] | TC a & RWShared sds & gEq{|*|} a & JSONEncode{|*|} o
choiceIDShare vopts target options sds = mapReadWrite (tof,fromf) ?None sds
where
	tof mbv = findChoiceIDs vopts target options (maybeToList mbv)
	fromf w _ = ?Just (listToMaybe (findSelection (choiceIdentifier vopts) target options w))

choiceIDsShare :: ![ChoiceOption o] (o -> a) [o] (Shared sds [a]) -> SimpleSDSLens [ChoiceID] | TC a & RWShared sds & gEq{|*|} a & JSONEncode{|*|} o
choiceIDsShare vopts target options sds = mapReadWrite (tof,fromf) ?None sds
where
	tof v = findChoiceIDs vopts target options v
	fromf w _ = ?Just (findSelection (choiceIdentifier vopts) target options w)

choiceIDShareWithShared :: ![ChoiceOption o] (o -> a) (sds () ([o], ?a) (?a)) -> SimpleSDSLens [ChoiceID] | TC o & TC a & RWShared sds & gEq{|*|} a & JSONEncode{|*|} o
choiceIDShareWithShared vopts target sds = mapReadWrite (tof,fromf) ?None sds
where
	tof (options,mbv) = findChoiceIDs vopts target options (maybeToList mbv)
	fromf w (options,_) = ?Just (listToMaybe (findSelection (choiceIdentifier vopts) target options w))

choiceIDsShareWithShared :: ![ChoiceOption o] (o -> a) (sds () ([o],[a]) [a]) -> SimpleSDSLens [ChoiceID] | TC o & TC a & RWShared sds & gEq{|*|} a & JSONEncode{|*|} o
choiceIDsShareWithShared vopts target sds = mapReadWrite (tof,fromf) ?None sds
where
	tof (options,v) = findChoiceIDs vopts target options v
	fromf w (options,_) = ?Just (findSelection (choiceIdentifier vopts) target options w)

wait :: (r -> Bool) !(sds () r w) -> Task r | iTask r & TC w & RWShared sds
wait pred shared
	=	viewSharedInformation [ViewAs (const "Waiting for information update")] shared
	>>* [OnValue (ifValue pred return)]

chooseAction :: ![(Action,a)] -> Task a | iTask a
chooseAction actions
	=	viewInformation [] ()
	>>* [OnAction action (always (return val)) \\ (action,val) <- actions]

viewTitle :: !a -> Task a | iTask a
viewTitle a = Title title @>> viewInformation [ViewAs view] a
where
	title = toSingleLineText a
	view a	= DivTag [] [SpanTag [StyleAttr "font-size: 30px"] [Text title]]

viewSharedTitle :: !(sds () r w) -> Task r | iTask r & RWShared sds & TC w
viewSharedTitle s = whileUnchanged s viewTitle

crudWith :: ![ChoiceOption r] [EnterOption r] [ViewOption r] [UpdateOption r]
            !((f r) -> [r]) !(r (f r) -> f` w) !(r (f r) -> f` w)
            (sds () (f r) (f` w))
         -> Task r | iTask r & iTask (f r) & iTask w & iTask (f` w) & RWShared sds
crudWith choiceOpts enterOpts viewOpts updateOpts toList putItem delItem sh = goCRUD
  where
  goCRUD
    =   enterChoiceWithShared choiceOpts (mapRead toList sh)
    >>* [ OnAction (Action "New")    (always   newItem)
        , OnAction (Action "View")   (hasValue viewItem)
        , OnAction (Action "Edit")   (hasValue editItem)
        , OnAction (Action "Delete") (hasValue deleteItem)
        ]
  newItem
    =            Title "New item" @>> enterInformation enterOpts
    >>! \item -> upd (putItem item) sh
    >-|          goCRUD
  viewItem x
    =            Title "View item" @>> viewInformation viewOpts x
    >!|          goCRUD
  editItem x
    =            Title "Edit item" @>> updateInformation updateOpts x
    >>! \item -> upd (putItem item) sh
    >-|          goCRUD
  deleteItem x
    =            upd (delItem x) sh
    >-|          goCRUD

crud :: !((f r) -> [r]) !(r (f r) -> f` w) !(r (f r) -> f` w)
        (sds () (f r) (f` w))
     -> Task r | iTask r & iTask (f r) & iTask w & iTask (f` w) & RWShared sds
crud toList putItem delItem sh = crudWith [] [] [] [] toList putItem delItem sh

// required to solve overloading
withAttributes :: !UIAttributes !(Editor a w) -> Editor a w
withAttributes attributes editor = attributes @>> editor
