definition module iTasks.WF.Tasks.IO
/**
* This modules provides tasks that support interaction with other systems.
* Either by running external programs, creating network clients and servers, or exchanging files
*/
import iTasks.WF.Definition
import iTasks.SDS.Definition
import System.OS
from System.FilePath import :: FilePath
from System.Process import :: ProcessPtyOptions
from AsyncIO import :: Port
from Data.Error import :: MaybeError, :: MaybeErrorString
from AsyncIO import :: ConnectionId
from iTasks.Internal.AsyncIO import :: ConnectionHandlers

/**
 * On windows, a graceful exit has exitcode 0.
 * On posix, the application can exit gracefully with signal 15 (SIGTERM).
 */
externalProcessGraceful :== IF_POSIX 15 0

:: ProcessOption
	= Arguments ![String] //* Arguments to pass to the executable (default: none).
	| StartupDirectory !FilePath
		//* Custom directory from which the command should be performed.
		//* default: the directory from which the iTasks executable is started.
	| ExitCodeOrSignal !Int
		//* The exit code (Windows) or signal (POSIX) to send when the task is destroyed.
		//* Default: exit code 0 on Windows, SIGTERM (15) on posix.
		//* When running an iTasks application which performs an expensive computation as an exteral process,
		//* it is recommended to use SIGKILL (9).
	| ProcessPtyOptions !ProcessPtyOptions //* Optional custom settings for using a pseudoterminal (POSIX only).
	| DisableIoRedirectionToShares
		//* stdin/stdout/stderr are not redirected to the SDSs provided. The SDSs' content remains empty and writing
		//* has no effect. stdout/stderr of the child process are relayed to the parent process's stdout/stderr.
		//* `ProcessPtyOptions` has no effect if this option is set.

/**
 * Execute an external process. Data placed in the stdin sds is sent to the process.
 * Data received is placed in the (stdout, stderr) sds.
 * N.B. The provided shares must be synchronous.
 *
 * @param Poll rate.
 * @param Path to executable.
 * @param Set of optional settings for executing/terminating external processes.
 * @param Stdin queue.
 * @param (stdout, stderr) queue.
 * @result Task returning the exit code on termination.
 */
externalProcess ::
	!Timespec !FilePath ![ProcessOption] !(Shared sds1 [String]) !(Shared sds2 ([String], [String]))
	-> Task Int | RWShared sds1 & RWShared sds2

/**
 * Connect to an external system using TCP. This task's value becomes stable when the connection is closed.
 * N.B. The provided share must be synchronous.
 *
 * @param Hostname.
 * @param Port.
 * @param The timeout (in ms) for opening the connection.
 * @param A reference to shared data the task has access to.
 * @param The event handler functions.
 */
tcpconnect :: !String !Port !(sds () r w) !(ConnectionHandlers l r w) -> Task l
	| iTask l & iTask r & iTask w & RWShared sds

/**
 * Listen for connections from external systems using TCP.
 * N.B. The provided share must be synchronous.
 *
 * @param Port.
 * @param Remove closed connections. If this is true, closed connections are removed from the task value, if not they are kept in the list.
 * @param A reference to shared data the task has access to.
 * @param Initialization function: function that is called when a new connection is established.
 * @param Communication function: function that is called when data arrives, the connection is closed or the observed share changes.
 */
tcplisten :: !Port !Bool !(sds () r w) !(ConnectionHandlers l r w) -> Task [l] | iTask l & iTask r & iTask w & RWShared sds
