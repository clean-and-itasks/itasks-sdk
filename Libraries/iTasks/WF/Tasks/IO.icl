implementation module iTasks.WF.Tasks.IO

from Data.Foldable import class Foldable
import Data.Functor
import Data.GenHash
import qualified Data.Map as DM
import Data.Maybe
from Data.Set import instance Foldable Set
import qualified Data.Set as DS
import qualified Data.List as DL
import System.OS

import iTasks.SDS.Combinators.Common
import iTasks.SDS.Definition
import iTasks.WF.Definition
import iTasks.WF.Derives
import iTasks.UI.Definition
import iTasks.UI.Editor
import iTasks.Internal.AsyncIO
import iTasks.Internal.Generic.Visualization
import iTasks.Internal.Generic.Defaults
import iTasks.Internal.IWorld
import iTasks.Internal.SDS
import iTasks.Internal.Task
import iTasks.Internal.TaskEval
import iTasks.Internal.TaskIO
import iTasks.Internal.TaskServer
import iTasks.Internal.TaskState
import iTasks.Internal.Util
import iTasks.WF.Tasks.Core

import System.Process
import StdEnv
import Text, Text.GenJSON, Data.Tuple, Data.Func
import qualified Data.Map as DM
import Data.Maybe
import qualified Data.Set as DS

:: ExitCode = ExitCode !Int
:: ExternalProcessHandlers l r w =
    { onStartup     :: !(           r -> (MaybeErrorString l, ?w, [String], Bool))
    , onOutData     :: !(String   l r -> (MaybeErrorString l, ?w, [String], Bool))
    , onErrData     :: !(String   l r -> (MaybeErrorString l, ?w, [String], Bool))
    , onShareChange :: !(         l r -> (MaybeErrorString l, ?w, [String], Bool))
    , onExit        :: !(ExitCode l r -> (MaybeErrorString l, ?w                ))
    }

derive JSONEncode ProcessHandle, ProcessIO, ReadPipe, WritePipe
derive JSONDecode ProcessHandle, ProcessIO, ReadPipe, WritePipe

liftOSErr f iw = case (liftIWorld f) iw of
	(Error (_, e), iw) = (Error (exception e), iw)
	(Ok a, iw) = (Ok a, iw)

externalProcess ::
	!Timespec !FilePath ![ProcessOption] !(Shared sds1 [String]) !(Shared sds2 ([String], [String])) ->
	Task Int | RWShared sds1 & RWShared sds2
externalProcess poll cmd processOptions sdsin sdsout = Task evalinit
where
	(args, dir, ptyOpts, exitcodeOrSignal, disableIoRedir) =
		foldl
			(\(args, dir, ptyOpts, exitcodeOrSignal, disableIoRedir) opt =
				case opt of
					Arguments updArgs = (updArgs, dir, ptyOpts, exitcodeOrSignal, disableIoRedir)
					StartupDirectory updDir = (args, ?Just updDir, ptyOpts, exitcodeOrSignal, disableIoRedir)
					ProcessPtyOptions updPtyOpts = (args, dir, ?Just updPtyOpts, exitcodeOrSignal, disableIoRedir)
					ExitCodeOrSignal updExitcodeOrSignal = (args, dir, ptyOpts, updExitcodeOrSignal, disableIoRedir)
					DisableIoRedirectionToShares = (args, dir, ptyOpts, exitcodeOrSignal, True)
			) ([], ?None, ?None, IF_WINDOWS 0 15, False) processOptions

	evalinit :: !Event !TaskEvalOpts !*IWorld -> (!TaskResult Int, !*IWorld)
	evalinit event _ iworld
		| isDestroyOrInterrupt event = (DestroyedResult, iworld)
	evalinit event evalOpts=:{TaskEvalOpts|taskId} iworld
		# runProcessFunc =
			if disableIoRedir
				(appFst (fmap \handle = (handle, ?None)) o runProcess cmd args dir)
				(appFst (fmap $ appSnd ?Just) o maybe (runProcessIO cmd args dir) (runProcessPty cmd args dir) ptyOpts)
		= case liftOSErr runProcessFunc iworld of
			(Error e, iworld)  = (ExceptionResult e, iworld)
			(Ok phpio, iworld) = eval NoValue phpio event evalOpts iworld

	eval :: !(TaskValue Int) !(!ProcessHandle, !?ProcessIO) !Event !TaskEvalOpts !*IWorld -> (!TaskResult Int, !*IWorld)
	eval _ (ph, pio) event {TaskEvalOpts|taskId} iworld
		| isDestroyOrInterrupt event
			# iworld = clearTaskSDSRegistrations ('DS'.singleton taskId) iworld
			= apIWTransformer iworld
			$ maybe (\w = (Ok (), w)) (\pio = liftOSErr (closeProcessIO pio)) pio
			>-= \_ = liftOSErr (terminateProcessCode ph exitcodeOrSignal)
			>-= \_ = tuple (Ok DestroyedResult)
	//TODO: Support async sdss
	eval lastValue (ph, pio) event {TaskEvalOpts|taskId} iworld
		| not (isRefreshForTask event taskId)
			= (ValueResult lastValue mkTaskEvalInfo (mkUIIfReset event rep) (Task (eval lastValue (ph, pio))), iworld)
		= apIWTransformer iworld $
			read sdsout EmptyContext >-= \(ReadingDone (stdoutq, stderrq))->
			maybe (\w = (Ok "", w)) (\pio = liftOSErr (readPipeNonBlocking pio.stdOut)) pio >-= \stdoutData->
			maybe (\w = (Ok "", w)) (\pio = liftOSErr (readPipeNonBlocking pio.stdErr)) pio >-= \stderrData->
			(if (stdoutData == "" && stderrData == "")
				(tuple (Ok WritingDone))
				(write (stdoutq ++ filter ((<>)"") [stdoutData]
				       ,stderrq ++ filter ((<>)"") [stderrData]
				       ) sdsout EmptyContext))          >-= \WritingDone->
			liftOSErr (checkProcess ph)                 >-= \mexitcode = case mexitcode of
				?Just i
					# val = Value i True
					= tuple (Ok (ValueResult val mkTaskEvalInfo
						(mkUIIfReset event rep) (Task (eval val (ph, pio)))))
				?None =
					readRegister (TaskContext taskId) taskId clock >-= \_->
					readRegister (TaskContext taskId) taskId sdsin >-= \(ReadingDone stdinq)->
					if (stdinq =: [] || isNone pio)
						(tuple $ Ok WritingDone)
						(	// the process might have terminated since `checkProcess`,
							// for this case we ignore the EPIPE error and handle termination at next task evaluation
							liftOSErr (writePipeNoErrorOnBrokenPipe (concat stdinq) (fromJust pio).stdIn) >-= \_ ->
							write [] sdsin EmptyContext
						)
					>-= \WritingDone ->
					tuple
						(Ok
							(ValueResult NoValue
								mkTaskEvalInfo (mkUIIfReset event rep) (Task (eval NoValue (ph, pio)))))

	writePipeNoErrorOnBrokenPipe :: !String !WritePipe !*World -> (!MaybeOSError (), !*World)
	writePipeNoErrorOnBrokenPipe str pipe world
		# (res, world)= writePipe str pipe world
		= case res of
			Error (32, _) = (Ok (), world)
			res           = (res,   world)

	rep = stringDisplay ("External process: " <+++ cmd)
	clock = sdsFocus {start=zero,interval=poll} iworldTimespec

tcplisten :: !Port !Bool !(sds () r w) !(ConnectionHandlers l r w) -> Task [l] | iTask l & iTask r & iTask w & RWShared sds
tcplisten port removeClosed sds handlers = Task evalinit
where
	evalinit :: !Event !TaskEvalOpts !*IWorld -> (!TaskResult [l], !*IWorld) | iTask l
	evalinit event evalOpts=:{TaskEvalOpts|taskId} iworld
		| isDestroyOrInterrupt event = (DestroyedResult, iworld)
		= case addListener (?Just taskId) port removeClosed (wrapConnectionContext handlers sds) iworld of
			(Ok ioHandle, iworld) = eval ioHandle event evalOpts iworld
			(Error e, iworld) = (ExceptionResult $ appSnd (\s -> "tcplisten: " +++ s) e, iworld)

	eval :: !IOHandle !Event !TaskEvalOpts !*IWorld -> (!TaskResult [l], !*IWorld) | iTask l
	eval ioHandle event taskEvalOpts iworld
		= case getIOState ioHandle iworld of
			(Ok ioState, iworld) = case ioState.ioStatus of
				IOListener cId
					# (mbValues, iworld) = gatherValues ('DS'.toList ioState.listenerMap) iworld
					| isDestroyOrInterrupt event
						# (_, iworld) = closeListener ioHandle iworld
						= (DestroyedResult, iworld)
					| isError mbValues = (ExceptionResult (exception ("tcplisten error gathering values: " +++ fromError mbValues)), iworld)
					= (ValueResult (Value (fromOk mbValues) False) mkTaskEvalInfo (mkUIIfReset event (rep port)) (Task (eval ioHandle)), iworld)
				IOException m
					| isDestroyOrInterrupt event = (DestroyedResult, iworld)
					= (ExceptionResult (exception ("tcplisten: " +++ m)), iworld)
				ioStatus
					| isDestroyOrInterrupt event
						= (DestroyedResult, iworld)
					= (ExceptionResult (exception ("tcplisten: corrupt IO task result, expected IOListener or IOException but got: " +++ toString ioStatus)), iworld)
			(Error e, iworld)
				= (ExceptionResult (exception ("tcplisten: " +++ e)), iworld)
	where
		gatherValues :: ![IOHandle] !*IWorld -> (!MaybeErrorString [l], !*IWorld) | TC l
		//* Gather values of closed connections
		gatherValues [] iworld = (Ok [], iworld)
		//* Gather values of active connections
		gatherValues [ioHandle:ioHandles] iworld = case getIOState ioHandle iworld of
			(Ok ioState , iworld)
				// Shouldn't happen but okay, we ignore, exceptions of individual clients are ignored
				| ioState.ioStatus =: (IOConnecting _) || ioState.ioStatus =: (IOException _)
					= gatherValues ioHandles iworld
				= case getStateFromIoStatus ioState.ioStatus of
					Ok l = case gatherValues ioHandles iworld of
						(Ok ls, iworld) = (Ok [l:ls], iworld)
						(Error e, iworld) = (Error e, iworld)
					Error e = (Error e, iworld)
			(Error e, iworld) = (Error e, iworld)

	rep port = stringDisplay ("Listening for connections on port "<+++ port)

tcpconnect :: !String !Port !(sds () r w) !(ConnectionHandlers l r w)
	-> Task l | iTask l & iTask r & iTask w & RWShared sds
tcpconnect host port sds handlers = Task evalinit
where
	//We cannot make ioStates local since the engine uses it
	evalinit :: !Event !TaskEvalOpts !*IWorld -> (!TaskResult l, !*IWorld) | TC l
	evalinit event evalOpts=:{TaskEvalOpts|taskId} iworld
		| isDestroyOrInterrupt event = (DestroyedResult, iworld)
		= case addConnection (?Just taskId) host port (wrapConnectionContext handlers sds) iworld of
			(Ok ioHandle, iworld) = eval ioHandle event evalOpts iworld
			(Error e, iworld) = (ExceptionResult e, iworld)

	eval :: !IOHandle !Event !TaskEvalOpts !*IWorld -> (!TaskResult l, !*IWorld) | TC l
	eval ioHandle event evalOpts iworld
		= case withIOState ioHandle (\s->Ok s.ioStatus) iworld of
			(Ok (IOConnecting cId), iworld)
				| isDestroyOrInterrupt event
					// Cleaning up the connection and IO State is usually done
					// in the onDestroy handler of the task server but in this
					// case the connection hasn't even been set up yet so it
					// has to be done here
					= (DestroyedResult, closeConnection ioHandle iworld)
				= (ValueResult NoValue mkTaskEvalInfo (mkUIIfReset event rep) (Task (eval ioHandle)), iworld)
			(Ok (IOActive d=:(l :: l^) cId), iworld)
				| isDestroyOrInterrupt event
					// Set the IOState to destroyed, the engine will clean up the io state
					# (_, iworld) = updIOState ioHandle (\ioState->Ok {ioState & ioStatus=IODestroyed d cId}) iworld
					= (DestroyedResult, iworld)
				= (ValueResult (Value l False) mkTaskEvalInfo (mkUIIfReset event rep) (Task (eval ioHandle)), iworld)
			(Ok (IOClosed (l :: l^)), iworld)
				// Delete the ioState, the engine doesn't need to clean up anything because the connection was already closed
				# (_, iworld) = delIOState ioHandle iworld
				| isDestroyOrInterrupt event
					= (DestroyedResult, iworld)
				= (ValueResult (Value l True) mkTaskEvalInfo (mkUIIfReset event rep) (return l), iworld)
			(Ok (IOException e), iworld)
				| isDestroyOrInterrupt event
					= (DestroyedResult, iworld)
				= (ExceptionResult (exception $ "tcpconnect: " +++ e), iworld)
			(Ok ioStatus, iworld)
				| isDestroyOrInterrupt event
					= (DestroyedResult, iworld)
				= (ExceptionResult (exception (concat4 "tcpconnect: corrupt IO task result, expected IOConnecting, IOActive, IOClosed, IOException of type " (toString (typeCodeOfDynamic (dynamic undef :: l^))) " but got: " (toString ioStatus))), iworld)
			(Error e, iworld)
				| isDestroyOrInterrupt event
					= (DestroyedResult, iworld)
				= (ExceptionResult (exception ("tcpconnect: " +++ e)), iworld)

	rep = stringDisplay ("TCP client " <+++ host <+++ ":" <+++ port)
