implementation module iTasks.WF.Tasks.Core

import iTasks.SDS.Sources.Core
import iTasks.WF.Derives
import iTasks.WF.Definition
import iTasks.UI.Definition
import iTasks.SDS.Definition
import iTasks.Internal.Task
import iTasks.Internal.TaskIO
import iTasks.Internal.TaskState
import iTasks.Internal.TaskEval
import iTasks.Internal.IWorld
import qualified iTasks.Internal.SDS as SDS
import iTasks.Internal.AsyncSDS
import iTasks.Internal.Util

import Data.Error, Data.Func, Data.Functor, Data.Either, Data.Tuple
import Text.GenJSON
import StdString, StdBool, StdInt, StdMisc, StdFunc
import qualified Data.Set as DS
import qualified Data.Map as DM
import Data.Maybe

return :: !a -> (Task a)
return a  = mkInstantTask (\_ iworld-> (Ok a, iworld))

throw :: !e -> Task a | TC, toString e
throw e = mkInstantTask (\_ iworld -> (Error (exception e), iworld))

throwWithString :: !String !e -> Task a | TC e
throwWithString s e = mkInstantTask \_ iworld -> (Error (dynamic e,s), iworld)

appWorld :: !(*World -> *World) -> Task ()
appWorld fun = accWorld $ tuple () o fun

accWorld :: !(*World -> *(a, *World)) -> Task a
accWorld fun = accWorldError (appFst ok o fun) id
where
	ok :: !a -> MaybeError String a
	ok x = Ok x

accWorldError :: !(*World -> (MaybeError e a, *World)) !(e -> err) -> Task a | TC e & toString err
accWorldError fun errf = mkInstantTask eval
where
	eval evalOpts iworld=:{IWorld|world}
		# (res,world) = fun world
		= case res of
			Error e = (Error (dynamic e, toString (errf e)), {IWorld|iworld & world = world})
			Ok v    = (Ok v, {IWorld|iworld & world = world})

accWorldOSError :: !(*World -> (MaybeOSError a, *World)) -> Task a
accWorldOSError fun = accWorldError fun OSException

instance toString OSException
where
	toString (OSException (_,err)) = "Error performing OS operation: " +++ err

interactRW :: !(Editor r w) !(sds () (?r) w) -> Task r | TC r & TC w & RWShared sds
interactRW editor sds = Task
	(readRegisterCompletely sds NoValue
		(\event -> mkUIIfReset event (asyncSDSLoaderUI Read))
		(evalInteractInit sds editor (maybe (?Just ?None) (?Just o ?Just)) writeCompletely`)
	)
where
	writeCompletely` w sds val continue event opts iworld
		= writeCompletely w sds val (const (ReplaceUI (ui UILoader))) continue event opts iworld

interactR :: !(Editor r w) !(sds () (?r) w) -> Task r | TC r & TC w & Registrable sds
interactR editor sds = Task
	(readRegisterCompletely sds NoValue
		(\event-> mkUIIfReset event (asyncSDSLoaderUI Read))
		(evalInteractInit sds editor (fmap ?Just) dontWrite)
	)
where
	dontWrite _ _ _ continue event opts iworld = continue event opts iworld

//This initializes the editor state and continues with the actual interact task
evalInteractInit sds editor modefun writefun mbr event evalOpts=:{TaskEvalOpts|taskId} iworld
	= evalInteract NoChange mbr ?None zero sds editor modefun writefun ResetEvent evalOpts iworld

evalInteract ::
	UIChange
		/* When evalInteract is guaranteed to be called another time (e.g. when
		 * onRefresh returns a ?Just write value), no changes are sent to the
		 * client yet. Instead, evalInteract is called with the UIChange of the
		 * previous evalInteract call, and these are merged with the new
		 * changes. */
	(?r)
	(?EditState)
	EditorId
	(sds () (?r) w)
	(Editor r w)
	((?r) -> ?(?r))
	(
		w
		(sds () (?r) w)
		(TaskValue r)
		(Event -> TaskEvalOpts -> *IWorld -> *(TaskResult r, *IWorld))
		Event
		TaskEvalOpts
		*IWorld
		-> *(TaskResult r,*IWorld))
	Event
	TaskEvalOpts
	*IWorld
	-> *(TaskResult r,*IWorld)
	| TC r & TC w & Registrable sds
evalInteract _ _ _ _ _ _ _ _ ServerInterruptedEvent _ iworld
	= (DestroyedResult, iworld)
evalInteract _ _ _ _ _ _ _ _ DestroyEvent {TaskEvalOpts|taskId} iworld
	= (DestroyedResult, iworld)
evalInteract _ mbr ?None _ sds editor modefun writefun event=:(EditEvent eTaskId name edit) evalOpts iworld
	= (ExceptionResult (exception "corrupt editor state"), iworld)
evalInteract prevChange mbr (?Just st) editorId sds editor modefun writefun event=:(EditEvent eTaskId name edit) evalOpts=:{TaskEvalOpts|taskId} iworld | eTaskId == taskId
	# (res, editorId, iworld) = withVSt taskId editorId (editor.Editor.onEdit (fromString name,edit) st) iworld
	= case res of
		Ok (?Just (change, st, mbw)) = case mbw of
			//We have a value to write to the shared state
			?Just w = writefun w sds NoValue
				// We cannot just do this because this will loop endlessly:
				// Therefore we delay it by returning the continuation in a value instead of directly:
				(\event evalOpts iworld ->
					(ValueResult
						(maybe NoValue (\r -> Value r False) mbr)
						mkTaskEvalInfo
						(mergeUIChanges prevChange change)
						(Task (evalInteract NoChange mbr (?Just st) editorId sds editor modefun writefun))
					, iworld))
				event evalOpts iworld
			//There is no update function
			?None
				= (ValueResult
					(maybe NoValue (\r -> Value r False) mbr)
					mkTaskEvalInfo
					(mergeUIChanges prevChange change)
					(Task (evalInteract NoChange mbr (?Just st) editorId sds editor modefun writefun))
				, iworld)
		Ok ?None =
			// No editor claims this event. This can happen when an edit event has arrived delayed; we ignore it.
			( ValueResult
				(maybe NoValue (\r -> Value r False) mbr)
				mkTaskEvalInfo
				prevChange
				(Task (evalInteract NoChange mbr (?Just st) editorId sds editor modefun writefun))
			, iworld
			)
		Error e = (ExceptionResult (exception e), iworld)
evalInteract _ mbr mst editorId sds editor modefun writefun ResetEvent evalOpts=:{TaskEvalOpts|taskId} iworld
	= case modefun mbr of
		?Just mode
			= case withVSt taskId editorId (editor.Editor.onReset 'DM'.newMap mode) iworld of
				(Error e, _, iworld) = (ExceptionResult (exception e), iworld)
				(Ok (UI type attr items, st, mbw), editorId, iworld)
					# change = ReplaceUI (UI type (addClassAttr "interact" attr) items)
					= case mbw of
						?Just w =
							// refresh editor with newly written value after write
							writefun
								w
								sds
								NoValue
								(evalInteract change mbr (?Just st) editorId sds editor modefun writefun)
								(RefreshEvent $ 'DS'.singleton taskId)
								evalOpts
								iworld
							= (ValueResult
								(maybe NoValue (\r -> Value r False) mbr)
								mkTaskEvalInfo
								change
								(Task (evalInteract NoChange mbr (?Just st) editorId sds editor modefun writefun))
							, iworld)
		?None
			= (ValueResult
				NoValue
				mkTaskEvalInfo
				(ReplaceUI (UI UIEmpty (addClassAttr "interact" 'DM'.newMap) []))
				(Task (evalInteract NoChange ?None mst editorId sds editor modefun writefun))
			, iworld)

evalInteract _ _ ?None _ _ _ _ _ (RefreshEvent _) _ iworld
	= (ExceptionResult (exception "corrupt editor state"), iworld)
evalInteract prevChange mbr (?Just st) editorId sds editor modefun writefun event=:(RefreshEvent taskIds) evalOpts=:{TaskEvalOpts|taskId} iworld
 	| 'DS'.member taskId taskIds
		= readRegisterCompletely sds (maybe NoValue (\r -> Value r False) mbr) (\e->mkUIIfReset e (asyncSDSLoaderUI Read))
			(\mbr event evalOpts iworld
				# mbChange = case mbr of
					?Just r = withVSt taskId editorId (editor.Editor.onRefresh (?Just r) st) iworld
					?None   = (Ok (NoChange, st, ?None), editorId, iworld)
				= case mbChange of
					(Ok (change, st, mbw), editorId, iworld)
						# change = mergeUIChanges prevChange change
						# change = case change of
							ReplaceUI (UI type attr items) = ReplaceUI (UI type (addClassAttr "interact" attr) items)
							_ = change
						= case mbw of
							?Just w = writefun w sds NoValue
								(evalInteract change mbr (?Just st) editorId sds editor modefun writefun)
								event evalOpts iworld
							?None
								= (ValueResult
									(maybe NoValue (\r -> Value r False) mbr)
									mkTaskEvalInfo
									change
									(Task (evalInteract NoChange mbr (?Just st) editorId sds editor modefun writefun))
								, iworld)
					(Error e, _, iworld) = (ExceptionResult (exception e), iworld)
			)
			event evalOpts iworld

evalInteract prevChange mbr mst editorId sds editor modefun writefun event evalOpts iworld
	//An event for a sibling?
	= (ValueResult
		(maybe NoValue (\r->Value r False) mbr)
		mkTaskEvalInfo
		prevChange
		(Task (evalInteract NoChange mbr mst editorId sds editor modefun writefun))
	, iworld)
