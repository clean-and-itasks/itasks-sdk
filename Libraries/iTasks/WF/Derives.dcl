definition module iTasks.WF.Derives
/**
* This module provides derived instances for common types from StdEnv and Platform
* such that you don't have to derive them when you use these libraries.
*/

from Data.Error import :: MaybeError
from Data.GenHash import generic gHash
from Data.Map import :: Map
from Data.Set import :: Set
from Text.HTML.GenJSON import generic JSONEncode, generic JSONDecode
from System.Time import :: Timestamp, :: Timespec, :: Tm, :: Clock
import System.Time.GenJSON // Export instances for backwards compatibility.

from Text.HTML import :: HtmlAttr
from Text.HTML import :: SVGElt, :: SVGAttr, :: SVGAlign, :: SVGColor, :: SVGDefer, :: SVGFillOpacity, :: SVGFuncIRI, :: SVGLengthAdjust, :: SVGLength, :: SVGICCColor, :: SVGNumber
from Text.HTML import :: SVGLengthUnit, :: SVGLineCap, :: SVGFillRule, :: SVGLineJoin, :: SVGMeetOrSlice, :: SVGStrokeMiterLimit, :: SVGPaint
from Text.HTML import :: SVGStrokeDashArray, :: SVGStrokeDashOffset, :: SVGStrokeWidth, :: SVGTransform, :: SVGZoomAndPan

from iTasks.Internal.IWorld import :: ClockParameter
from iTasks.Internal.TaskState import :: TaskChange, :: ExtendedTaskListFilter
from iTasks.SDS.Sources.System import :: TaskInstance, :: ValueStatus
from iTasks.WF.Combinators.Core import :: Action, :: TaskListItem, :: TaskListFilter, :: AttachmentStatus
import iTasks.WF.Definition

//Common library types
derive gEq	    (->), Dynamic
derive gEditor  HtmlAttr
derive gText    HtmlAttr

derive gEditor    SVGElt, SVGAttr, SVGAlign, SVGColor, SVGDefer, SVGFillOpacity, SVGFuncIRI, SVGLengthAdjust, SVGLengthUnit, SVGLineCap, SVGFillRule, SVGLineJoin, SVGMeetOrSlice, SVGStrokeMiterLimit, SVGPaint, SVGStrokeDashArray, SVGStrokeDashOffset, SVGStrokeWidth, SVGTransform, SVGZoomAndPan, SVGLength, SVGICCColor
derive gText      SVGElt, SVGAttr, SVGAlign, SVGColor, SVGDefer, SVGFillOpacity, SVGFuncIRI, SVGLengthAdjust, SVGLengthUnit, SVGLineCap, SVGFillRule, SVGLineJoin, SVGMeetOrSlice, SVGStrokeMiterLimit, SVGPaint, SVGStrokeDashArray, SVGStrokeDashOffset, SVGStrokeWidth, SVGTransform, SVGZoomAndPan, SVGLength, SVGICCColor

derive gHash      Dynamic, Map, Set, JSONNode

//Common iTasks system types
derive class iTask TaskId, TaskListFilter, AttachmentStatus
derive gHash       TaskId, TaskListFilter, AttachmentStatus

derive JSONEncode		TaskValue, TaskListItem, TaskInstance, ValueStatus, Action, ClockParameter
derive JSONDecode		TaskValue, TaskListItem, TaskInstance, ValueStatus, Action, ClockParameter
derive gEq				TaskValue, TaskListItem, TaskInstance, ValueStatus, Action, Timespec, ClockParameter

derive gText	        TaskValue, TaskListItem, TaskInstance, ValueStatus, Action, Set
derive gEditor			TaskValue, TaskListItem, TaskInstance, ValueStatus, Action, Timespec, ClockParameter, Set
derive gHash			TaskValue, TaskListItem, TaskInstance, ValueStatus, Action, Timespec, ClockParameter

derive gHash Timestamp, TaskChange, ExtendedTaskListFilter
