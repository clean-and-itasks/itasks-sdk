definition module iTasks.Internal.TaskEval
/**
* This module provides functions for creation, evaluation and removal of task/workflow instances.
*/

from iTasks.WF.Definition           import :: Task, :: TaskResult, :: TaskException, :: TaskValue, :: TaskAttributes, :: Event, :: TaskId, :: InstanceNo, :: TaskNo
from iTasks.WF.Combinators.Core     import :: TaskListItem
from iTasks.Internal.IWorld		import :: IWorld
import iTasks.Internal.SDS
from iTasks.Internal.TaskState import :: DeferredJSON
from Text.GenJSON import :: JSONNode
from Data.Map import :: Map
from Data.Error import :: MaybeErrorString, :: MaybeError
from Data.CircularStack import :: CircularStack

/**
 * External (read-only) evaluation passed to the task under execution.
 * Always create this with `mkTaskEvalOpts` to maintain (some) forwards compatibility.
 */
:: TaskEvalOpts	=
	{ noUI            :: !Bool        //* Whether to generate a UI
	, taskId          :: !TaskId      //* The id of the task
	, lastEval        :: !TaskTime    //* The last evaluation
	, sessionInstance :: !?InstanceNo //* If we are evaluating a task in response to an event from a session
	, attachmentChain :: ![TaskId]    //* The current way the evaluated task instance is attached to other instances
	}

/**
 * External information passed from the task (information about the result of the task's execution.
 * Always create this with `mkTaskEvalInfo` to maintain (some) forwards compatibility.
 */
:: TaskEvalInfo =
	{ removedTasks :: ![#RemovedTask!] //* Which embedded parallel tasks were removed
	}
instance +++ TaskEvalInfo

//* A task removed from a list. This type is used in `TaskEvalInfo`.
:: RemovedTask =
	{ removedTaskId     :: !TaskId //* The ID of the removed task.
	, removedTaskListId :: !TaskId //* The list the task was removed from.
	}

:: TaskTime :== Int

/**
 * Get the next TaskId
 */
getNextTaskId :: !*IWorld -> (!TaskId,!*IWorld)

/**
* Dequeues events from the event queue and evaluates the tasks instances
* @param Maximum amount of events to process at once
*/
processEvents :: !Int *IWorld -> *(!MaybeError TaskException (), !*IWorld)

/**
* Evaluate a task instance
*
* @param The instance id
* @param The event to process
* @param The IWorld state
*
* @result Unit or an error.
* @result The IWorld state.
*/
evalTaskInstance :: !InstanceNo !Event !*IWorld -> (!MaybeErrorString (),!*IWorld)
