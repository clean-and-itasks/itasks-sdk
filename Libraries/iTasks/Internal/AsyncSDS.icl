implementation module iTasks.Internal.AsyncSDS

import Data.Either
import Data.Error
import Data.Func
import qualified Data.Map as DM
import Data.Maybe
import StdEnv
import Text

import iTasks.Engine
import iTasks.Internal.AsyncIO
import iTasks.Internal.IWorld
import iTasks.Internal.SDSService
import iTasks.Internal.Serialization
import iTasks.Internal.TaskEval
import iTasks.Internal.TaskServer
import iTasks.Internal.Util
import iTasks.SDS.Definition
import iTasks.SDS.Sources.Core
import iTasks.UI.Definition
import iTasks.WF.Definition

derive JSONEncode SDSNotifyRequest, RemoteNotifyOptions

createRequestString :: !(SDSRequest p r w a) -> String
createRequestString req = serializeToBase64 req

onConnect :: !(SDSRequest p r w a) !IOHandle !String !() -> (!MaybeErrorString (Either [String] v), !?(), ![String], !Bool)
onConnect reqq connId _ _ = (Ok (Left []), ?None, [createRequestString reqq +++ "\n"], False)

onData :: !IOHandle !String !(Either [String] v) !() -> (!MaybeErrorString (?(Either [String] v)), !?(), ![String], !Bool)
onData _ data (Left acc) _ = (Ok (?Just (Left (acc ++ [data]))), ?None, [], False)

onShareChange :: !(Either [String] v) !() -> (!MaybeErrorString (?(Either [String] v)), !?(), ![String], !Bool)
onShareChange _ _ = (Ok ?None, ?None, [], False)

onDestroy :: !v -> (!MaybeErrorString (?v), ![String])
onDestroy _ = (Ok ?None, [])

queueSDSRequest :: !(SDSRequest p r w a) !String !Port !TaskId !*IWorld -> (!MaybeError TaskException IOHandle, !*IWorld) | TC r
queueSDSRequest req host port taskId iworld=:{symbols}
	= addConnection (?Just taskId) host port connectionContext iworld
where
	connectionContext = wrapConnectionContext (handlers req) unitShare

	handlers :: (SDSRequest p r w a) -> ConnectionHandlers (Either [String] (MaybeError TaskException r)) () () | TC r
	handlers _ = {ConnectionHandlers| onConnect = onConnect req,
		onData = onData,
		onShareChange = onShareChange,
		onDisconnect = onDisconnect,
		onDestroy = onDestroy}

	onDisconnect _ (Left acc) _
	# textResponse = concat acc
	| size textResponse < 1 = (Error ("queueSDSRequest: Server " +++ host +++ " disconnected without responding"), ?None)
	= (Ok (?Just (Right (deserializeFromBase64 textResponse symbols))), ?None)

queueModifyRequest :: !(SDSRequest p r w a) !String !Port !TaskId !*IWorld -> (!MaybeError TaskException IOHandle, !*IWorld) | TC r & TC w
queueModifyRequest req=:(SDSModifyRequest p r w) host port taskId iworld=:{symbols}
	= addConnection (?Just taskId) host port connectionContext iworld
where
	connectionContext = wrapConnectionContext (handlers req) unitShare

	handlers :: (SDSRequest p r w a) -> ConnectionHandlers (Either [String] (MaybeError TaskException (r, ?w, a))) () () | TC r & TC w
	handlers _ = {ConnectionHandlers| onConnect = onConnect req,
		onData = onData,
		onShareChange = onShareChange,
		onDisconnect = onDisconnect,
		onDestroy=onDestroy}

	onDisconnect _ (Left acc) _
	# textResponse = concat acc
	| size textResponse == 0 = (Error ("queueModifyRequest: Server" +++ host +++ " disconnected without responding"), ?None)
	= (Ok (?Just (Right (deserializeFromBase64 textResponse symbols))), ?None)

queueWriteRequest :: !(SDSRequest p r w a) !String !Port !TaskId !*IWorld ->  (!MaybeError TaskException IOHandle, !*IWorld) | TC r & TC w
queueWriteRequest req=:(SDSWriteRequest sds p w) host port taskId iworld=:{symbols}
	= addConnection (?Just taskId) host port connectionContext iworld
where
	connectionContext = wrapConnectionContext (handlers req) unitShare

	handlers :: (SDSRequest p r w a) -> ConnectionHandlers (Either [String] (MaybeError TaskException ())) () () | TC r & TC w
	handlers req = {ConnectionHandlers| onConnect = onConnect req,
		onData = onData,
		onShareChange = onShareChange,
		onDisconnect = onDisconnect,
		onDestroy = onDestroy}

	onDisconnect _ (Left acc) _
	# textResponse = concat acc
	| size textResponse == 0 = (Error ("queueWriteRequest: Server" +++ host +++ " disconnected without responding"), ?None)
	= (Ok (?Just (Right (deserializeFromBase64 textResponse symbols))), ?None)

queueServiceRequest :: !(SDSRemoteService p r w) p !TaskId !Bool !*IWorld -> (!MaybeError TaskException IOHandle, !*IWorld) | TC p & TC r
queueServiceRequest (SDSRemoteService (?Just _) _) _ _ _ iworld = (Error (exception "SDSRemoteService queueing request while still a connection id"), iworld)
queueServiceRequest service=:(SDSRemoteService _ (HTTPShareOptions {host, port, createRequest, fromResponse})) p taskId _ iworld
	= addConnection (?Just taskId) host port connectionContext iworld
where
	connectionContext = wrapConnectionContext (handlers service) unitShare

	handlers req = {ConnectionHandlers| onConnect = onConnect,
		onData = onData,
		onShareChange = onShareChange,
		onDisconnect = onDisconnect,
		onDestroy = onDestroy}

	onConnect _ _ _
	# req = createRequest p
	# sreq = toString {HTTPRequest|req & req_headers = 'DM'.put "Connection" "Close" req.HTTPRequest.req_headers}
	= (Ok (Left []), ?None, [sreq], False)

	onData _ data (Left acc) _ = (Ok (?Just (Left (acc ++ [data]))), ?None, [], False)

	onShareChange _ _ = (Ok ?None, ?None, [], False)

	onDisconnect _ (Left []) _ = (Error ("queueServiceRequest: Server" +++ host +++ ":" +++ toString port +++ " disconnected without responding"), ?None)
	onDisconnect _ (Left acc) _
	# textResponse = concat acc
	= case parseResponse textResponse of
		?None = (Error ("Unable to parse HTTP response, got: " +++ textResponse), ?None)
		?Just parsed = case fromResponse parsed p of
			(Error error) = (Error error, ?None)
			(Ok a) = (Ok (?Just (Right a)), ?None)

queueServiceRequest service=:(SDSRemoteService _ (TCPShareOptions {host, port, createMessage, fromTextResponse})) p taskId register iworld
	= addConnection (?Just taskId) host port connectionContext iworld
where
	connectionContext = wrapConnectionContext handlers unitShare
	handlers = {ConnectionHandlers| onConnect = onConnect,
		onData = onData,
		onShareChange = onShareChange,
		onDisconnect = onDisconnect,
		onDestroy = onDestroy}

	onConnect connId _ _	= (Ok (?None, []), ?None, [createMessage p +++ "\n"], False)

	onData _ data (previous, acc) _
	# newacc = acc ++ [data]
	// If already a result, and we are registering, then we have received a refresh notification from the server.
	| register && isJust previous = (Ok (?Just (previous, newacc)), ?None, [], True)
	= case fromTextResponse (concat newacc) p register of
		Error e = (Error e, ?None, [], True)
		// No full response yet, keep the old value.
		Ok (?None, response)     = (Ok (?Just (previous, newacc)), ?None, maybe [] (\resp. [resp +++ "\n"]) response, False)
		Ok (?Just r, ?Just resp) = (Ok (?Just (?Just r, [])), ?None, [resp +++ "\n"], False)
		// Only close the connection when we have a value and when we are not registering.
		Ok (?Just r, ?None)      = (Ok (?Just (?Just r, [])), ?None, [], not register)

	onShareChange state _ = (Ok ?None, ?None, [], False)
	onDisconnect _ state _ = (Ok ?None, ?None)

queueServiceWriteRequest :: !(SDSRemoteService p r w) !p !w !TaskId !*IWorld -> (MaybeError TaskException (?IOHandle), !*IWorld) | TC p & TC w
queueServiceWriteRequest service=:(SDSRemoteService (?Just _) _) _ _ _ iworld = (Error (exception "SDSRemoteService queueing write request while still containing a connection id"), iworld)
queueServiceWriteRequest service=:(SDSRemoteService _ (HTTPShareOptions {host, port, writeHandlers})) p w taskId iworld
| isNone writeHandlers = (Ok ?None, iworld) // Writing not supported for this share.
= case addConnection (?Just taskId) host port connectionContext iworld of
	(Ok id, iworld)   = (Ok (?Just id), iworld)
	(Error e, iworld) = (Error e, iworld)
where
	(toWriteRequest, fromWriteResponse) = fromJust writeHandlers
	connectionContext = wrapConnectionContext handlers unitShare
	handlers = { ConnectionHandlers|onConnect = onConnect
				, onData 		= onData
				, onShareChange = onShareChange
				, onDisconnect 	= onDisconnect
				, onDestroy  	= onDestroy
				}
	onConnect connId _ _
	# req = toWriteRequest p w
	# sreq = toString {HTTPRequest|req & req_headers = 'DM'.put "Connection" "Close" req.HTTPRequest.req_headers}
	= (Ok (Left []), ?None, [sreq], False)

	onData _ data (Left acc) _ = (Ok (?Just (Left (acc ++ [data]))), ?None, [], False)

	onShareChange _ _ = (Ok ?None, ?None, [], False)

	onDisconnect _ (Left []) _ = (Error ("queueServiceWriteRequest: Server" +++ host +++ ":" +++ toString port +++ " disconnected without responding"), ?None)
	onDisconnect _ (Left acc) _
	# textResponse = concat acc
	= case parseResponse textResponse of
		?None = (Error ("Unable to parse HTTP response, got: " +++ textResponse), ?None)
		?Just parsed = case fromWriteResponse p parsed of
			Error e = (Error e, ?None)
			Ok pred = (Ok (?Just (Right pred)), ?None)

queueServiceWriteRequest service=:(SDSRemoteService _ (TCPShareOptions {host, port, writeMessageHandlers})) p w taskId iworld
| isNone writeMessageHandlers = (Ok ?None, iworld)
= case addConnection (?Just taskId) host port connectionContext iworld of
	(Ok id, iworld)   = (Ok (?Just id), iworld)
	(Error e, iworld) = (Error e, iworld)
where
	(toWriteMessage, fromWriteMessage) = fromJust writeMessageHandlers
	connectionContext = wrapConnectionContext handlers unitShare

	handlers = {ConnectionHandlers|onConnect = onConnect,
		onData = onData,
		onShareChange = onShareChange,
		onDisconnect = onDisconnect,
		onDestroy = onDestroy}

	onConnect connId _ _	= (Ok (Left ""), ?None, [toWriteMessage p w +++ "\n"], False)

	onData _ data (Left acc) _
	# newacc = acc +++ data
	= case fromWriteMessage p newacc of
		Error e         = (Error e, ?None, [], True)
		Ok ?None        = (Ok (?Just (Left newacc)), ?None, [], False)
		Ok (?Just pred) = (Ok (?Just (Right pred)), ?None, [], True)
	onData _ data state _ = (Ok ?None, ?None, [], True)

	onShareChange _ _ = (Ok ?None, ?None, [], False)
	onDisconnect _ _ _ = (Ok ?None, ?None)

queueRead :: !(SDSRemoteSource p r w) p !TaskId !Bool !SDSIdentity !*IWorld
	-> (!MaybeError TaskException IOHandle, !*IWorld)
	| TC p & TC r & TC w
queueRead rsds=:(SDSRemoteSource sds (?Just _) _) _ _ _ _ iworld = (Error $ exception "queueRead while already a connection id", iworld)
queueRead rsds=:(SDSRemoteSource sds ?None {SDSShareOptions|domain, port}) p taskId register reqSDSId iworld
| isNone iworld.options.distributed
	= (Error (exception "Distributed engineoption is not enabled"), iworld)
# (request, iworld) = buildRequest register iworld
= queueSDSRequest request domain port taskId iworld
where
	buildRequest True iworld=:{options}
		= (SDSRegisterRequest sds p reqSDSId (sdsIdentity rsds) taskId (Port (fromJust options.distributed)), iworld)
	buildRequest False iworld = (SDSReadRequest sds p, iworld)

queueRemoteRefresh :: ![(TaskId, RemoteNotifyOptions)] !*IWorld -> *IWorld
queueRemoteRefresh [] iworld = iworld
queueRemoteRefresh [(reqTaskId, remoteOpts) : reqs] iworld=:{options}
# request = reqq reqTaskId remoteOpts.remoteSdsId
= case queueSDSRequest request remoteOpts.hostToNotify remoteOpts.portToNotify SDSSERVICE_TASK_ID iworld of
	(_, iworld) = queueRemoteRefresh reqs iworld
where
	reqq :: !TaskId !SDSIdentity -> SDSRequest () String () ()
	reqq taskId sdsId = SDSRefreshRequest taskId sdsId

queueWrite :: !w !(SDSRemoteSource p r w) p !TaskId !*IWorld -> (!MaybeError TaskException IOHandle, !*IWorld) | TC p & TC r & TC w
queueWrite w rsds=:(SDSRemoteSource sds (?Just _) _) _ _ iworld = (Error $ exception "queueWrite while already a connection id", iworld)
queueWrite w rsds=:(SDSRemoteSource sds ?None share=:{SDSShareOptions|domain, port}) p taskId iworld
# request = SDSWriteRequest sds p w
= queueWriteRequest request domain port taskId iworld

queueModify :: !(r -> MaybeError TaskException (a, ?w)) !(SDSRemoteSource p r w) p !TaskId !*IWorld -> (!MaybeError TaskException IOHandle, !*IWorld) | TC p & TC r & TC w & TC a
queueModify f rsds=:(SDSRemoteSource sds (?Just _)_) _ _ iworld = (Error $ exception "queueModify while already a connection id", iworld)
queueModify f rsds=:(SDSRemoteSource sds ?None share=:{SDSShareOptions|domain, port}) p taskId iworld
# request = SDSModifyRequest sds p f
= queueModifyRequest request domain port taskId iworld

asyncSDSLoaderUI :: !AsyncAction -> UI
asyncSDSLoaderUI Read = uia UIProgressBar (textAttr "Reading data")
asyncSDSLoaderUI Write = uia UIProgressBar (textAttr "Writing data")
asyncSDSLoaderUI Modify = uia UIProgressBar (textAttr "Modifying data")

readCompletely :: (sds () r w) (TaskValue a) (Event -> UIChange) (r Event TaskEvalOpts *IWorld -> *(TaskResult a, *IWorld)) Event TaskEvalOpts !*IWorld
	-> *(TaskResult a, *IWorld) | Readable sds & TC r & TC w
readCompletely sds tv e2ui cont event evalOpts=:{TaskEvalOpts|taskId} iworld
	| isDestroyOrInterrupt event = (DestroyedResult, iworld)
	= case read sds (TaskContext taskId) iworld of
		(Error e, iworld) = (ExceptionResult e, iworld)
		(Ok (ReadingDone r), iworld)
			= cont r event evalOpts iworld
		(Ok (Reading sds), iworld)
			= (ValueResult tv mkTaskEvalInfo (e2ui event) (Task (readCompletely sds tv e2ui cont)), iworld)

writeCompletely :: w (sds () r w) (TaskValue a) (Event -> UIChange) (Event TaskEvalOpts *IWorld -> *(TaskResult a, *IWorld)) Event TaskEvalOpts !*IWorld
	-> *(TaskResult a, *IWorld) | Writeable sds & TC r & TC w
writeCompletely w sds tv e2ui cont event evalOpts=:{TaskEvalOpts|taskId} iworld
	| isDestroyOrInterrupt event = (DestroyedResult, iworld)
	= case write w sds (TaskContext taskId) iworld of
		(Error e, iworld) = (ExceptionResult e, iworld)
		(Ok (WritingDone), iworld)
			= cont event evalOpts iworld
		(Ok (Writing sds), iworld)
			= (ValueResult tv mkTaskEvalInfo (e2ui event) (Task (writeCompletely w sds tv e2ui cont)), iworld)

modifyCompletely :: (r -> w) (sds () r w) (TaskValue a) (Event -> UIChange) (w Event TaskEvalOpts *IWorld -> *(TaskResult a, *IWorld)) Event TaskEvalOpts !*IWorld
	-> *(TaskResult a, *IWorld) | TC r & TC w & Modifiable sds
modifyCompletely modfun sds tv e2ui cont event evalOpts iworld = amendCompletely (\r->((), ?Just (modfun r))) sds tv e2ui cont` event evalOpts iworld
where
	cont` :: E.^ a w: !(?w) !() !Event !TaskEvalOpts !*IWorld -> *(TaskResult a, !*IWorld)
	cont` (?Just w) () event evalOpts iworld = cont w event evalOpts iworld
	cont` ?None () event evalOpts iworld = (ExceptionResult (exception "modifyCompletely's call to amend returned a ?None?"), iworld)

amendCompletely :: !(r -> (a, ?w)) !(sds () r w) !(TaskValue b) !(Event -> UIChange) !((?w) a Event TaskEvalOpts *IWorld -> *(TaskResult b, *IWorld))
	!Event !TaskEvalOpts !*IWorld -> *(!TaskResult b, !*IWorld) | TC r & TC w & TC a & Modifiable sds
amendCompletely modfun sds tv e2ui cont event evalOpts=:{TaskEvalOpts|taskId} iworld
	| isDestroyOrInterrupt event = (DestroyedResult, iworld)
	= case amend` modfun sds (TaskContext taskId) iworld of
		(Error e, iworld) = (ExceptionResult e, iworld)
		(Ok (ModifyingDone mw a), iworld)
			= cont mw a event evalOpts iworld
		(Ok (Modifying sds modfun), iworld)
			= (ValueResult tv mkTaskEvalInfo (e2ui event) (Task (amendCompletely modfun sds tv e2ui cont)), iworld)

readRegisterCompletely :: (sds () r w) (TaskValue a) (Event -> UIChange) (r Event TaskEvalOpts *IWorld -> *(TaskResult a, *IWorld)) Event TaskEvalOpts !*IWorld
	-> *(TaskResult a, *IWorld) | TC r & TC w & Registrable sds
readRegisterCompletely sds tv e2ui cont event evalOpts=:{TaskEvalOpts|taskId} iworld
	| isDestroyOrInterrupt event = (DestroyedResult, iworld)
	| not (isRefreshForTask event taskId) =
		(ValueResult tv mkTaskEvalInfo (e2ui event) (Task (readRegisterCompletely sds tv e2ui cont))
		, iworld)
	= case readRegister (TaskContext taskId) taskId sds iworld of
		(Error e, iworld) = (ExceptionResult e, iworld)
		(Ok (ReadingDone r), iworld)
			= cont r event evalOpts iworld
		(Ok (Reading sds), iworld)
			= (ValueResult tv mkTaskEvalInfo (e2ui event) (Task (readRegisterCompletely sds tv e2ui cont)) , iworld)
