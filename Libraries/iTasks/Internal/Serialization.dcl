definition module iTasks.Internal.Serialization

from Text.GenJSON import generic JSONEncode, generic JSONDecode, ::JSONNode
from Data.Error import ::MaybeError, ::MaybeErrorString

from iTasks.Internal.IWorld import :: IWorld
from symbols_in_program import :: Symbol

serialize :: !a -> *String
deserialize	:: !*String -> MaybeErrorString a
serializeDynamic :: !Dynamic -> *String
deserializeDynamic :: !*String -> Dynamic

derive JSONEncode Dynamic, (->)
derive JSONDecode Dynamic, (->)

//Check if a JSON serialization contains encoded functions or dynamics
functionFree		:: !JSONNode -> Bool

dynamicJSONEncode :: !a -> JSONNode
dynamicJSONDecode :: !JSONNode -> ?a

/**
 * Deserialize for distributed iTasks
 *
 * @param data to deserialize
 * @param symbol retrieved from `{{read_symbols}}`
 * @result the expression
 */
deserializeFromBase64 :: !String !{#Symbol} -> a

/**
 * Serialize with symbols for distributed iTasks
 */
serializeToBase64 :: !a -> String
