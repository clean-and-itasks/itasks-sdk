implementation module iTasks.Internal.TaskEval

import StdEnv
import StdOverloadedList

import Data.Func
import Data.Functor
import Data.GenEq
import Data.GenHash
import qualified Data.Map as DM
from Data.Map import derive gEq Map
import Data.Maybe
import qualified Data.Queue as DQ
from Data.Set import derive gEq Set
import System.Time

import iTasks.Internal.IWorld
import iTasks.Internal.TaskEval
import iTasks.Internal.TaskIO
import iTasks.Internal.TaskState
import iTasks.Internal.Util
import iTasks.SDS.Combinators.Common
import iTasks.UI.Definition
import iTasks.WF.Definition
import iTasks.WF.Derives

derive gEq TaskMeta, InstanceType, TaskChange

instance +++ TaskEvalInfo where
	(+++) l r = { TaskEvalInfo | removedTasks = l.removedTasks ++| r.removedTasks }

getNextTaskId :: !*IWorld -> (!TaskId,!*IWorld)
getNextTaskId iworld=:{currentInstance,current=current=:{TaskEvalState|nextTaskNo}}
	= (TaskId currentInstance nextTaskNo, {IWorld|iworld & current = {TaskEvalState|current & nextTaskNo = nextTaskNo + 1}})

processEvents :: !Int *IWorld -> *(!MaybeError TaskException (), !*IWorld)
processEvents max iworld
	| max <= 0 = (Ok (), iworld)
	| otherwise
		= case dequeueEvent iworld of
			(Error e, iworld) = (Error e, iworld)
			(Ok ?None, iworld) = (Ok (), iworld)
			(Ok (?Just {instanceNo,event}), iworld)
				= case evalTaskInstance instanceNo event iworld of
					(Ok _,iworld)
						= processEvents (max - 1) iworld
					(Error msg,iworld=:{IWorld|world})
						= (Ok (),{IWorld|iworld & world = world})

evalTaskInstance :: !InstanceNo !Event !*IWorld -> (!MaybeErrorString (),!*IWorld)
evalTaskInstance instanceNo event iworld
	# iworld            = mbResetUIState instanceNo event iworld
	# (res,iworld)      = evalTaskInstance` instanceNo event iworld
	= (res,iworld)
where
	evalTaskInstance` :: !InstanceNo !Event !*IWorld -> (!MaybeErrorString (), !*IWorld)
	evalTaskInstance` instanceNo event iworld
	// Read the task reduct. If it does not exist, the task has been deleted.
	# (curReduct, iworld)		= read (sdsFocus instanceNo taskInstanceTask) EmptyContext iworld
	| isError curReduct			= exitWithException instanceNo ((\(Error (e,msg)) -> msg) curReduct) iworld
	# curReduct                 = directResult (fromOk curReduct)
	// Determine the task type (startup,session,local)
	# (type,iworld)             = determineInstanceType instanceNo iworld
	// Determine the progress of the instance
	# (curProgress=:{TaskMeta|nextTaskTime,nextTaskNo,status,attachedTo,connectedTo},iworld) = determineInstanceProgress instanceNo iworld
	//Check exception
	| status =: (Left _) = let (Left message) = status in exitWithException instanceNo message iworld
	//Evaluate instance
	# (currentSession,currentAttachment) = case (type,attachedTo) of
		(SessionInstance,_)                    = (?Just instanceNo,[])
		(_,[])                                 = (?None,[])
		(_,attachment=:[TaskId sessionNo _:_]) = (?Just sessionNo,attachment)
	//Update current process id & eval stack in iworld
	# taskId = TaskId instanceNo 0
	// Set the current instance
	# iworld & currentInstance = instanceNo
	// Set the current taskEvalState
	# iworld & current = mkTaskEvalState nextTaskNo
	//Apply task's eval function and take updated nextTaskId from iworld
	# (newResult,iworld) = apTask curReduct event
		(mkTaskEvalOpts taskId nextTaskTime currentSession currentAttachment)
		iworld
	//the 'nextTaskNo' is possibly incremented during evaluation and we need to store it
	# (nextTaskNo, iworld) = iworld!current.TaskEvalState.nextTaskNo
	# newTask = case newResult of
		(ValueResult _ _ _ newTask) = newTask
		_                           = curReduct
	# newValue = case newResult of
		ValueResult val _ _ _   = val
		ExceptionResult (e,str) = NoValue
		DestroyedResult         = NoValue
	// Use event instead of result to make sure the task state is cleaned up even if the task does not provide a
	// `DestroyedResult`.
	# destroyed = event =: DestroyEvent
	// Reset the instance again
	# iworld & currentInstance = 0
	//Read unsynced cookies from meta-data //TODO: we should be able to this during updateTaskState, but SDS.modify can't return a value anymore
	# (mbErr,iworld) = if destroyed
		(Ok [],iworld)
		(readUnsyncedCookies instanceNo iworld)
	| mbErr=:(Error _)
		# (Error (_,description)) = mbErr
		= exitWithException instanceNo description iworld
	# syncCookies = fromOk mbErr
	//Write the updated state, or cleanup
	# (mbErr,iworld) = if destroyed
		(cleanupTaskState instanceNo iworld)
		(updateTaskState instanceNo newResult newTask newValue nextTaskNo nextTaskTime iworld)
	| mbErr=:(Error _)
		# (Error (_,description)) = mbErr
		= exitWithException instanceNo description iworld
	= case newResult of
		ValueResult value _ change _
			| destroyed = (Ok (), iworld)
			| isNone currentSession && isNone connectedTo = (Ok (), iworld)
			| otherwise = case (compactUIChange change, syncCookies) of
				//Only queue output if something interesting is changed
				(NoChange,[]) = (Ok (),iworld)
				(change,_) = (Ok (), queueOutput instanceNo [TOUIChange change:[TOSetCookie k v ttl \\ (k,v,ttl) <- reverse syncCookies]] iworld)
		ExceptionResult (e,description)
			# iworld = if (type =: StartupInstance)
				(iShowErr [description] {iworld & shutdown= ?Just 1})
				 iworld
			= exitWithException instanceNo description iworld
		DestroyedResult
			= (Ok (), iworld)

	readUnsyncedCookies :: !InstanceNo !*IWorld -> (!MaybeError TaskException [(String, String, ? Int)], !*IWorld)
	readUnsyncedCookies instanceNo iworld
		# (mbErr,iworld) = read (sdsFocus (instanceNo,False,True) taskInstance) EmptyContext iworld
		= (fmap ((\{TaskMeta|unsyncedCookies} -> unsyncedCookies) o directResult) mbErr, iworld)

	updateTaskState :: !InstanceNo !(TaskResult DeferredJSON) !(Task DeferredJSON) !(TaskValue DeferredJSON) !TaskNo !TaskTime !*IWorld -> (!MaybeError TaskException (), !*IWorld)
	updateTaskState instanceNo newResult newTask newValue nextTaskNo nextTaskTime iworld=:{clock}
		//Store progress
		# (mbErr,iworld) = modify (updateProgress clock newResult nextTaskNo nextTaskTime) (sdsFocus (instanceNo,False,True) taskInstance) EmptyContext iworld
		| mbErr =: (Error _) = (liftError mbErr, iworld)
		//Store reduct
		# (mbErr,iworld) = write (?Just newTask) (sdsFocus instanceNo taskInstanceTask) EmptyContext iworld
		| mbErr =: (Error _) = (liftError mbErr, iworld)
		//Store value
		# (mbErr,iworld) = write (?Just newValue) (sdsFocus instanceNo taskInstanceValue) EmptyContext iworld
		| mbErr =: (Error _) = (liftError mbErr, iworld)
		= (Ok (),iworld)

	cleanupTaskState :: !InstanceNo !*IWorld -> (!MaybeError TaskException (), !*IWorld)
	cleanupTaskState instanceNo iworld
		//Remove value
		# (mbErr,iworld) = write ?None (sdsFocus instanceNo taskInstanceValue) EmptyContext iworld
		| mbErr =: (Error _) = (liftError mbErr, iworld)
		//Remove reduct
		# (mbErr,iworld) = write ?None (sdsFocus instanceNo taskInstanceTask) EmptyContext iworld
		| mbErr =: (Error _) = (liftError mbErr, iworld)
		//Remove local shares
		# (mbErr,iworld) = write ?None (sdsFocus instanceNo taskInstanceShares) EmptyContext iworld
		| mbErr =: (Error _) = (liftError mbErr, iworld)
		//Remove residual queued output
		# (mbErr,iworld) = modify (\output -> 'DM'.del instanceNo output) taskOutput EmptyContext iworld
		| mbErr =: (Error _) = (liftError mbErr, iworld)
		= (Ok (),iworld)

	exitWithException :: !InstanceNo !String !*IWorld -> (!MaybeErrorString a, !*IWorld)
	exitWithException instanceNo description iworld
		# iworld = queueException instanceNo description iworld
		= (Error description, iworld)

	determineInstanceType :: !InstanceNo !*IWorld -> (!InstanceType, !*IWorld)
	determineInstanceType instanceNo iworld
		# (meta, iworld) = read (sdsFocus (instanceNo,False,False) taskInstance) EmptyContext iworld
		| isError meta = (SessionInstance,iworld)
		# {TaskMeta|instanceType} = directResult (fromOk meta)
		= (instanceType,iworld)

	determineInstanceProgress :: !InstanceNo !*IWorld -> (!TaskMeta, !*IWorld)
	determineInstanceProgress instanceNo iworld
		# (meta,iworld)      = read (sdsFocus (instanceNo,False,False) taskInstance) EmptyContext iworld
		| isError meta       = ({defaultValue & nextTaskNo=1, nextTaskTime=1},iworld)
		= (directResult (fromOk meta),iworld)

	updateProgress :: !Timespec !(TaskResult DeferredJSON) !TaskNo !TaskTime !TaskMeta -> TaskMeta
	updateProgress now result nextTaskNo nextTaskTime meta
		# attachedTo = case meta.TaskMeta.attachedTo of //Release temporary attachment after first evaluation
			(?Just (_,[])) = ?None
			attachment     = attachment
		# status = case result of
			(ExceptionResult (_,msg))             = Left msg
			(ValueResult (Value _ stable) _  _ _) = Right stable
			_                                     = Right False
		# taskAttributes = case result of
			(ValueResult _ _ change _) = foldr applyUIAttributeChange meta.TaskMeta.taskAttributes $ getAttributeChanges change
			_                          = meta.TaskMeta.taskAttributes
		= {TaskMeta| meta
			& status = status
			, firstEvent = ?Just (fromMaybe now meta.TaskMeta.firstEvent)
			, lastEvent = ?Just now
			, nextTaskNo = nextTaskNo
			, nextTaskTime = nextTaskTime + 1
			, taskAttributes = taskAttributes
			, unsyncedCookies = []
			}
	where
		getAttributeChanges :: !UIChange -> [UIAttributeChange]
		getAttributeChanges NoChange = []
		getAttributeChanges (ChangeUI changes _) = changes
		getAttributeChanges (ReplaceUI (UI _ attrs _)) = [SetAttribute attr val \\ (attr,val) <- 'DM'.toList attrs]

	mbResetUIState :: !InstanceNo !Event !*IWorld -> *IWorld
	mbResetUIState instanceNo ResetEvent iworld
		# (_,iworld) = write 'DQ'.newQueue (sdsFocus instanceNo taskInstanceOutput) EmptyContext iworld
		= iworld

	mbResetUIState _ _ iworld = iworld
