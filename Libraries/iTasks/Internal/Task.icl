implementation module iTasks.Internal.Task

import Data.GenHash
from StdFunc import const, id
import StdClass, StdArray, StdTuple, StdInt, StdList, StdBool, StdMisc, Data.Func
from Data.Map import :: Map
import qualified Data.Map as DM
import Data.Maybe
import Text.HTML, Internet.HTTP, Data.Error, Data.Functor, Text.GenJSON
import iTasks.Internal.IWorld, iTasks.UI.Definition, iTasks.Internal.Util, iTasks.Internal.DynamicUtil
from System.OSError import :: MaybeOSError, :: OSError, :: OSErrorCode, :: OSErrorMessage

import graph_copy

import iTasks.WF.Definition
import iTasks.WF.Tasks.IO
import iTasks.WF.Tasks.Core
from   iTasks.WF.Combinators.Core import :: AttachmentStatus
import iTasks.UI.Editor, iTasks.UI.Editor.Modifiers, iTasks.UI.Editor.Common
import iTasks.Internal.SDS
from iTasks.UI.Layout import :: LUI, :: LUIMoves, :: LUIMoveID, :: LUIEffectStage, :: LUINo

from iTasks.Util.DeferredJSON import :: DeferredJSON(..)
from iTasks.Internal.TaskState import :: TaskMeta(..) , :: InstanceType(..), :: TaskChange(..)

import iTasks.Internal.TaskEval
from iTasks.SDS.Combinators.Common import toDynamic
from iTasks.Internal.Serialization    import JSONEncode, JSONDecode, dynamicJSONEncode, dynamicJSONDecode
import iTasks.Util.DeferredJSON

fromJSONOfDeferredJSON :: !DeferredJSON -> ?a | TC a & JSONDecode{|*|} a
fromJSONOfDeferredJSON (DeferredJSON v)
	= case make_dynamic v of
		(v :: a^)
			-> ?Just v
fromJSONOfDeferredJSON (DeferredJSONNode json)
	= fromJSON json

make_dynamic v = dynamic v

JSONEncode{|Task|} _ _ tt = [dynamicJSONEncode tt]
JSONDecode{|Task|} _ _ [tt:c] = (dynamicJSONDecode tt,c)
JSONDecode{|Task|} _ _ c = (?None,c)

gText{|Task|} _ _ _ = ["<Task>"]
gEditor{|Task|} _ _ _ purpose
	= mapEditorWithState ?None
		(\f _ -> (?None, ?Just (copy_to_string f)))
		(\_ s -> (maybe EmptyEditor (\f -> ValidEditor (fst (copy_from_string {c \\ c <-: f}))) s,s))
		emptyEditor

gEq{|Task|} _ _ _			= True // tasks are always equal??

gDefault{|Task|} gDefx = Task (\_ -> abort error)
where
	error = "Creating default task functions is impossible"

mkInstantTask :: !(TaskEvalOpts *IWorld -> (MaybeError TaskException a,*IWorld)) -> Task a
mkInstantTask iworldfun = Task eval
where
	eval event _ iworld
		| isDestroyOrInterrupt event = (DestroyedResult, iworld)
	eval event evalOpts iworld
		= case iworldfun evalOpts iworld of
			(Ok a,iworld)     = (ValueResult (Value a True) mkTaskEvalInfo (mkEmptyUI event) (treturn a), iworld)
			(Error e, iworld) = (ExceptionResult e, iworld)

nopTask :: Task a | iTask a
nopTask = Task eval
where
	eval event _ iworld
		| isDestroyOrInterrupt event = (DestroyedResult, iworld)
	eval event evalOpts iworld
		= (ValueResult NoValue mkTaskEvalInfo (mkEmptyUI event) (Task eval), iworld)
