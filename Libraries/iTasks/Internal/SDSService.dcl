definition module iTasks.Internal.SDSService

import iTasks.Internal.WebService
import iTasks.WF.Definition
import iTasks.Internal.AsyncIO

SDSSERVICE_TASK_ID :== TaskId 1 1

sdsServiceTask :: Port -> Task ()
