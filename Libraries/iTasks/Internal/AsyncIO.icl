implementation module iTasks.Internal.AsyncIO

import qualified AsyncIO
from AsyncIO import qualified :: ConnectionHandlers (..)
from AsyncIO import
	class containsAIOState
	, instance < ConnectionId, instance toString ConnectionId
	, :: ConnectionId, :: InputData, :: OutputData, :: CloseConnection, :: IPAddr(..)
	, :: AIOState {writeQueues}
import Data.Error
import Data.Func
import Data.Functor
import Data.GenHash
import qualified Data.Map as DM
import qualified Data.Set as DS
import Data.Maybe
import Data.Tuple
import StdEnv
from Text import class Text(concat), instance Text String, concat3, concat4, concat5

import iTasks.Internal.DynamicUtil
import iTasks.Internal.IWorld
import iTasks.Internal.SDS
import iTasks.Internal.TaskIO
import iTasks.Internal.Util
import iTasks.SDS.Combinators.Common
import iTasks.SDS.Definition
import iTasks.WF.Definition

derive class iTask ConnectionId
derive class iTask Port

instance containsAIOState IWorld where
	getAIOState :: !*IWorld -> (!AIOState *IWorld, !*IWorld)
	getAIOState iworld = (iworld.aioState, {iworld & aioState = iworld.aioState})
	updAIOState :: !(AIOState IWorld) !*IWorld -> *IWorld
	updAIOState aioState iworld = {iworld & aioState = aioState}

wrapConnectionContext :: !(ConnectionHandlers l r w) !(sds () r w) -> ConnectionContext | TC l & TC r & TC w & RWShared sds
wrapConnectionContext ch=:{ConnectionHandlers|onConnect,onData,onShareChange,onDisconnect,onDestroy} sds
	=
		{ handlers =
			{ ConnectionHandlersIWorld
			| onConnect=onConnect`
			, onData=onData`
			, onShareChange=onShareChange`
			, onTick=onTick`
			, onDisconnect=onDisconnect`
			, onDestroy=onDestroy`
			}
	 	, sdsModifiedByHandlers = (toDynamic sds)
	 	}
where
	onConnect` connId host (r :: r^) env
		# (mbl, mbw, out, close) = onConnect connId host r
		= (toDyn <$> mbl, toDyn <$> mbw, out, close, env)
	onConnect` _ _ val env = abort ("onConnect does not match with type " +++ toString (typeCodeOfDynamic val))

	onData` connId data (l :: l^) (r :: r^) env
		# (mbl, mbw, out, close) = onData connId data l r
		= (fmap toDyn <$> mbl, toDyn <$> mbw, out, close, env)
	onData` _ _ _ val env = abort ("onData does not match with type " +++ toString (typeCodeOfDynamic val))

	onShareChange` (l :: l^) (r :: r^) env
		# (mbl, mbw, out, close) = onShareChange l r
		= (fmap toDyn <$> mbl, toDyn <$> mbw, out, close, env)
	onShareChange` l r env = abort ("onShareChange does not match with type l=" +++ toString (typeCodeOfDynamic l) +++ ", r=" +++ toString (typeCodeOfDynamic r) +++ ". Expected l=" +++ toString (typeCodeOfDynamic (dynamic ch)))

	// do nothing
	onTick` _ _ env = (Ok ?None, ?None, [], False, env)

	onDisconnect` connId (l :: l^) (r :: r^) env
		# (mbl, mbw) = onDisconnect connId l r
		= (fmap toDyn <$> mbl, toDyn <$> mbw, env)
	onDisconnect` _ l r env = abort ("onDisconnect does not match with type l=" +++ toString (typeCodeOfDynamic l) +++ ", r=" +++ toString (typeCodeOfDynamic r))
	onDestroy` (l :: l^) env
		# (mbl, out) = onDestroy l
		= (fmap toDyn <$> mbl, out, env)
	onDestroy` l env = abort ("onDestroy does not match with type l=" +++ toString (typeCodeOfDynamic l))

wrapIWorldConnectionContext :: !(ConnectionHandlersIWorld l r w) !(sds () r w) -> ConnectionContext | TC l & TC r & TC w & RWShared sds
wrapIWorldConnectionContext {ConnectionHandlersIWorld|onConnect,onData,onShareChange,onTick,onDisconnect,onDestroy} sds
	=
		{ handlers =
			{ ConnectionHandlersIWorld
			| onConnect=onConnect`
			, onData=onData`
			, onShareChange=onShareChange`
			, onTick=onTick`
			, onDisconnect=onDisconnect`
			, onDestroy=onDestroy`
			}
		, sdsModifiedByHandlers = (toDynamic sds)
		}
where
	onConnect` connId host (r :: r^) env
		# (mbl, mbw, out, close, env) = onConnect connId host r env
		= (toDyn <$> mbl, toDyn <$> mbw, out, close, env)

	onConnect` _ _ val env = abort ("onConnect does not match with type " +++ toString (typeCodeOfDynamic val))

	onData` connId data (l :: l^) (r :: r^) env
		# (mbl, mbw, out, close, env) = onData connId data l r env
		= (fmap toDyn <$> mbl, toDyn <$> mbw, out, close, env)

	onData` _ _ _ val env = abort ("onData does not match with type " +++ toString (typeCodeOfDynamic val))

	onShareChange` (l :: l^) (r :: r^) env
		# (mbl, mbw, out, close, env) = onShareChange l r env
		= (fmap toDyn <$> mbl, toDyn <$> mbw, out, close, env)
	onShareChange` l r env = abort ("onShareChange does not match with type l=" +++ toString (typeCodeOfDynamic l) +++ ", r=" +++ toString (typeCodeOfDynamic r))

	onTick` (l :: l^) (r :: r^) env
		# (mbl, mbw, out, close, env) = onTick l r env
		= (fmap toDyn <$> mbl, toDyn <$> mbw, out, close, env)
	onTick` l r env = abort ("onTick does not match with type l=" +++ toString (typeCodeOfDynamic l) +++ ", r=" +++ toString (typeCodeOfDynamic r))

	onDisconnect` connId (l :: l^) (r :: r^) env
		# (mbl, mbw, env) = onDisconnect connId l r env
		= (fmap toDyn <$> mbl, toDyn <$> mbw, env)
	onDisconnect` _ l r env = abort ("onDisconnect does not match with type l=" +++ toString (typeCodeOfDynamic l) +++ ", r=" +++ toString (typeCodeOfDynamic r))
	onDestroy` (l :: l^) env
		# (mbl, out, env) = onDestroy l env
		= (fmap toDyn <$> mbl, out, env)
	onDestroy` l env = abort ("onDestroy does not match with type l=" +++ toString (typeCodeOfDynamic l))

tslConnectionHandlersIWorld :: !HandlerSource !(ConnectionHandlersIWorld Dynamic Dynamic Dynamic)
                               !(?TaskId)
                               !(SDSLens () Dynamic Dynamic)
                            -> 'AsyncIO'.ConnectionHandlers IWorld
tslConnectionHandlersIWorld handlerSource {ConnectionHandlersIWorld|onConnect, onData, onTick, onDisconnect, onShareChange, onDestroy} taskId sds
	= case handlerSource of
		HSClient ioH =
			{ 'AsyncIO'.ConnectionHandlers
			| 'AsyncIO'.onConnect    = onConnect` ioH
			, 'AsyncIO'.onData       = onData` ioH
			, 'AsyncIO'.onTick       = onTick` ioH
			, 'AsyncIO'.onDisconnect = onDisconnect` ioStatusUpdateFunUpdate ioH
			, 'AsyncIO'.onError      = onError`
			}
		HSListener removeOnClose ioH =
			{ 'AsyncIO'.ConnectionHandlers
			| 'AsyncIO'.onConnect    = onConnectListener ioH
			, 'AsyncIO'.onData       = onDataListener ioH
			, 'AsyncIO'.onTick       = onTickListener ioH
			, 'AsyncIO'.onDisconnect = onDisconnectListener ioH removeOnClose
			, 'AsyncIO'.onError      = onError`
			}
where
	//* For listeners, there is some administration necessary before calling the onConnect` function.
	onConnectListener :: !IOHandle !ConnectionId !IPAddr !*IWorld -> (![OutputData], !CloseConnection, !*IWorld)
	onConnectListener origIOHandle cId ip=:(IPAddr ipAddr) iworld=:{nextIOHandle=ioHandle}
		// Generate a new ioHandle for the new client but save the original one (that is of the listener)
		# iworld & nextIOHandle = ioHandle + 1
		// Store the ioHandle in the IoHandleMap
		# iworld & ioHandleMap = 'DM'.put cId ioHandle iworld.ioHandleMap
		// Store the initial ioState of the connection
		# iworld & ioStates = 'DM'.put ioHandle
			{ IOState
			| onShareChangeHandler=onShareChange
			, onDestroyHandler=onDestroy
			, sdsModifiedByHandlers=sds
			, taskId=taskId
			, ioStatus=IOConnecting cId
			, listenerMap='DS'.newSet
			, clientInstances='DS'.newSet
			, listener=False
			} iworld.ioStates
		// Add the client to the listenerMap
		# (mbr, iworld) = updIOState origIOHandle (\ioState=Ok {ioState & listenerMap='DS'.insert ioHandle ioState.listenerMap}) iworld
		| isError mbr = ([], True, handleIOException (fromError mbr) origIOHandle taskId iworld)
		// There is a new connection so the origIOHandle is not used but the nextIOHandle
		= onConnect` ioHandle cId ip iworld

	onConnect` :: !IOHandle !ConnectionId !IPAddr !*IWorld -> (![OutputData], !CloseConnection, !*IWorld)
	onConnect` ioHandle cId (IPAddr ipAddr) iworld
		// Get IO State
		# (mbIoState, iworld) = getIOState ioHandle iworld
		| isError mbIoState = ([], True, handleIOException (fromError mbIoState) ioHandle taskId iworld)
		# (Ok ioState) = mbIoState
		// If the task is destroyed, do nothing, the engine will clean it up
		| ioState.ioStatus =: (IODestroyed _ _) = ([], False, iworld)
		// If the connection was in an exception state, do nothing
		| ioState.ioStatus =: (IOException _) = ([], True, iworld)
		// Read the sds
		# (mbr, iworld) = read sds EmptyContext iworld
		| isError mbr = ([], True, handleIOException (fromError mbr) ioHandle taskId iworld)
		# r = directResult o fromOk $ mbr
		// Process the onConnect callback.
		# (mbl, mbw, out, close, iworld) = onConnect ioHandle ipAddr r iworld
		| isError mbl = ([], True, handleIOException (fromError mbl) ioHandle taskId iworld)
		# l = fromOk mbl
		# (mbSdsErr, iworld) = writeShareIfNeeded sds mbw iworld
		| isError mbSdsErr = ([], True, handleIOException (snd $ fromError mbSdsErr) ioHandle taskId iworld)
		// Put the new state in the ioStates
		# (mbErr, iworld) = updIOState ioHandle (\ioState=Ok {ioState & ioStatus=IOActive l cId}) iworld
		| isError mbErr = ([], True, handleIOException (fromError mbErr) ioHandle taskId iworld)
		// Here we always refresh because the onConnect produced the initial dynamic state.
		# iworld = queueRefreshIfNeeded taskId iworld
		= (out, close, iworld)

	onDataListener :: !IOHandle !ConnectionId !InputData !*IWorld -> (![OutputData], !CloseConnection, !*IWorld)
	onDataListener origIOHandle cId data iworld
		// Retrieve the IO Handle of the client
		# (ioHandleMap, iworld) = iworld!ioHandleMap
		# mIoHandle = 'DM'.get cId ioHandleMap
		// The error is reported on the origIOHandle (the handle of the client)
		| isNone mIoHandle = ([], True, handleIOException "onData` cId not in the ioHandleMap" origIOHandle taskId iworld)
		# (?Just ioHandle) = mIoHandle
		= onData` ioHandle cId data iworld

	onData` :: !IOHandle !ConnectionId !InputData !*IWorld -> (![OutputData], !CloseConnection, !*IWorld)
	onData` ioHandle cId data iworld
		// Get IO State
		# (mbIoState, iworld) = getIOState ioHandle iworld
		| isError mbIoState = ([], True, handleIOException (fromError mbIoState) ioHandle taskId iworld)
		# (Ok ioState) = mbIoState
		// If the task is destroyed, do nothing
		| ioState.ioStatus =: (IODestroyed _ _) = ([], False, iworld)
		// If the connection was in an exception state, do nothing
		| ioState.ioStatus =: (IOException _) = ([], True, iworld)
		// Otherwise, read the sds
		# (mbr, iworld) = read sds EmptyContext {iworld & lastOnDataCId = ?Just ioHandle}
		| isError mbr = ([], True, handleIOException (snd $ fromError mbr) ioHandle taskId iworld)
		# r = directResult o fromOk $ mbr
		// Retrieve the dynState
		# mbDynSt = getDynStateFromIoStatus ioState.ioStatus
		| isError mbDynSt = ([], True, handleIOException (fromError mbDynSt) ioHandle taskId iworld)
		# (Ok dynSt) = mbDynSt
		// Process the onData callback.
		# (mbNewDynSt, mbw, out, close, iworld) = onData ioHandle data dynSt r iworld
		| isError mbNewDynSt = ([], True, handleIOException (fromError mbNewDynSt) ioHandle taskId iworld)
		# (Ok newDynSt) = mbNewDynSt
		# (mbSdsErr, iworld) = writeShareIfNeeded sds mbw iworld
		| isError mbSdsErr = ([], True, handleIOException (snd $ fromError mbSdsErr) ioHandle taskId iworld)
		// If there is no dynstate, we don't replace it and we don't refresh
		| isNone newDynSt = (out, close, iworld)
		// Update the connection state of the client with the new connection state returned by the onData callback.
		# (mbErr, iworld) = updIOState ioHandle (\ioState->Ok {ioState & ioStatus=replaceDynStateInIoStatus (fromJust newDynSt) ioState.ioStatus}) iworld
		| isError mbErr = ([], True, handleIOException (fromError mbErr) ioHandle taskId iworld)
		# iworld = queueRefreshIfNeeded taskId iworld
		= (out, close, iworld)

	onTickListener :: !IOHandle !ConnectionId !*IWorld -> (![OutputData], !CloseConnection, !*IWorld)
	onTickListener origIOHandle cId iworld
		// Retrieve the IO Handle of the client
		# (ioHandleMap, iworld) = iworld!ioHandleMap
		# mIoHandle = 'DM'.get cId ioHandleMap
		// The error is reported on the io handle of the listener
		| isNone mIoHandle = ([], True, handleIOException "onTick`: cId not in the ioHandleMap" origIOHandle taskId iworld)
		# (?Just ioHandle) = mIoHandle
		= onTick` ioHandle cId iworld

	onTick` :: !IOHandle !ConnectionId !*IWorld -> (![OutputData], !CloseConnection, !*IWorld)
	onTick` ioHandle cId iworld
		// Get IOState
		# (mbIoState, iworld) = getIOState ioHandle iworld
		| isError mbIoState = ([], True, handleIOException (fromError mbIoState) ioHandle taskId iworld)
		# (Ok ioState) = mbIoState
		// If the task is destroyed, do nothing, the onDestroy handler with clean up the connection
		| ioState.ioStatus =: (IODestroyed _ _) = ([], False, iworld)
		// If the connection was in an exception state, do nothing but close the connection
		| ioState.ioStatus =: (IOException _) = ([], True, iworld)
		// Otherwise, read the sds
		# (mbr, iworld) = read sds EmptyContext {iworld & lastOnDataCId = ?Just ioHandle}
		| isError mbr = ([], True, handleIOException (snd $ fromError mbr) ioHandle taskId iworld)
		# r = directResult o fromOk $ mbr
		// Retrieve the dynState
		# mbDynSt = getDynStateFromIoStatus ioState.ioStatus
		| isError mbDynSt = ([], True, handleIOException (fromError mbDynSt) ioHandle taskId iworld)
		# (Ok dynSt) = mbDynSt
		// Process the onData callback.
		# (mbNewDynSt, mbw, out, close, iworld) = onTick dynSt r iworld
		| isError mbNewDynSt = ([], True, handleIOException (fromError mbNewDynSt) ioHandle taskId iworld)
		# (Ok newDynSt) = mbNewDynSt
		// Write SDS
		# (mbSdsErr, iworld) = writeShareIfNeeded sds mbw iworld
		| isError mbSdsErr = ([], True, handleIOException (snd $ fromError mbSdsErr) ioHandle taskId iworld)
		// If there is no dynstate, we don't replace it and we don't refresh
		| isNone newDynSt = (out, close, iworld)
		// Update the connection state of the client with the new connection state returned by the onData callback.
		# (mbErr, iworld) = updIOState ioHandle (\ioState->Ok {ioState & ioStatus=replaceDynStateInIoStatus (fromJust newDynSt) ioState.ioStatus}) iworld
		| isError mbErr = ([], True, handleIOException (fromError mbErr) ioHandle taskId iworld)
		# iworld = queueRefreshIfNeeded taskId iworld
		= (out, close, iworld)

	onDisconnect` :: !(IOHandle (?Dynamic) *IWorld -> (MaybeErrorString (), *IWorld)) !IOHandle !Bool !ConnectionId !*IWorld -> *IWorld
	onDisconnect` ioStatusUpdateFun ioHandle closedByPeer cId iworld
		// Get IOState
		# (mbIoState, iworld) = getIOState ioHandle iworld
		| isError mbIoState = handleIOException (fromError mbIoState) ioHandle taskId iworld
		# (Ok ioState) = mbIoState
		// If the task is destroyed, do nothing, the onDestroy handler with clean up the connection
		| ioState.ioStatus =: (IODestroyed _ _) = iworld
		// If the connection was in an exception state, do nothing but close the connection
		| ioState.ioStatus =: (IOException _) = iworld
		// Read the SDS to retrieve the read value
		# (mbr, iworld) = read sds EmptyContext iworld
		| isError mbr = handleIOException (snd $ fromError mbr) ioHandle taskId iworld
		# r = directResult o fromOk $ mbr
		// Retrieve the dynState
		# mbDynSt = getDynStateFromIoStatus ioState.ioStatus
		| isError mbDynSt = handleIOException (fromError mbDynSt) ioHandle taskId iworld
		# (Ok dynSt) = mbDynSt
		// Call the handler if the peer closed the connection
		# (mbNewDynSt, mbw, iworld) = if closedByPeer (onDisconnect ioHandle dynSt r iworld) (Ok ?None, ?None, iworld)
		| isError mbNewDynSt = handleIOException (fromError mbNewDynSt) ioHandle taskId iworld
		# (Ok newDynSt) = mbNewDynSt
		# (mbSdsErr, iworld) = writeShareIfNeeded sds mbw iworld
		| isError mbSdsErr = handleIOException (snd $ fromError mbSdsErr) ioHandle taskId iworld
		// Remove the connection Id from the ioHandleMap
		# iworld & ioHandleMap = 'DM'.del cId iworld.ioHandleMap
		# (mbErr, iworld) = ioStatusUpdateFun ioHandle newDynSt iworld
		| isError mbErr = handleIOException (fromError mbErr) ioHandle taskId iworld
		// In case of a disconnect, we always refresh, even if there was no new dynState because the state has changed.
		# iworld = queueRefreshIfNeeded taskId iworld
		= iworld

	ioStatusUpdateFunUpdate :: !IOHandle !(?Dynamic) !*IWorld -> (!MaybeErrorString (), !*IWorld)
	ioStatusUpdateFunUpdate ioHandle newDynSt iworld
		= updIOState ioHandle (\ioState->case ioState.ioStatus of
				IOActive d    _ = Ok ({ioState & ioStatus=IOClosed (fromMaybe d newDynSt)})
				IODestroyed d _ = Ok ({ioState & ioStatus=IOClosed (fromMaybe d newDynSt)})
				IOException m = Ok ({ioState & ioStatus=IOException m})
				IOClosed d    = Ok ({ioState & ioStatus=IOClosed (fromMaybe d newDynSt)})
				_ = Error "IOState was corrupt when closing the connection..."
			) iworld

	onDisconnectListener :: !IOHandle !Bool !Bool !ConnectionId !*IWorld -> *IWorld
	onDisconnectListener origIOHandle removeOnClose closedByPeer cId iworld
		// Retrieve the IO Handle of the client
		# (ioHandleMap, iworld) = iworld!ioHandleMap
		# mIoHandle = 'DM'.get cId ioHandleMap
		// Report the error to the listener IOHandle
		| isNone mIoHandle = handleIOException "cId not in the ioHandleMap" origIOHandle taskId iworld
		# (?Just ioHandle) = mIoHandle
		= onDisconnect` (if removeOnClose ioStatusUpdateFunDelete ioStatusUpdateFunUpdate) ioHandle closedByPeer cId iworld
	where
		ioStatusUpdateFunDelete :: !IOHandle (?Dynamic) !*IWorld -> (!MaybeErrorString (), !*IWorld)
		ioStatusUpdateFunDelete ioHandle _ iworld
			# iworld & ioStates = 'DM'.del ioHandle iworld.ioStates
			// remove from listener map
			= updIOState origIOHandle (\ioState->Ok {ioState & listenerMap='DS'.delete ioHandle ioState.listenerMap}) iworld

	prependNewDynSt :: !Dynamic !IOStatus -> MaybeErrorString IOStatus
	prependNewDynSt (l :: l) ioStatus
		= case getDynStateFromIoStatus ioStatus of
			Ok (ls :: [l]) = Ok (replaceDynStateInIoStatus (dynamic [l:ls]) ioStatus)
			Error e = Error e

	onError` :: !ConnectionId !(!OSErrorCode, !OSErrorMessage) !*IWorld -> *IWorld
	onError` cId (errorCode, errorMsg) iworld
		# (ioHandleMap, iworld) = iworld!ioHandleMap
		# msg = concat5 "IO exception with error code: " (toString errorCode) ", error message: " errorMsg "."
		= case 'DM'.get cId ioHandleMap of
			?Just ioHandle = handleIOException msg ioHandle taskId iworld
			// If the error occurs before opening the connection (e.g. connection refused), then there is no IO handle yet
			?None = abort "onError` called on a connection that was not in the ioHandleMap\n"

handleIOException ::!e !IOHandle !(?TaskId) !*IWorld -> *IWorld | toString e
handleIOException exception cId taskId iworld
	# iworld = queueRefreshIfNeeded taskId iworld
	# (_, iworld) = updIOState cId (\ioState=Ok {ioState & ioStatus=IOException (toString exception)}) iworld
	= iworld

queueRefreshIfNeeded :: !(?TaskId) !*IWorld -> *IWorld
queueRefreshIfNeeded ?None iworld = iworld
queueRefreshIfNeeded (?Just taskId) iworld = queueRefresh taskId iworld

processOnShareChangeOnDestroy :: !*IWorld -> *IWorld
processOnShareChangeOnDestroy iworld
	# (ioStates, iworld) = iworld!ioStates
	= 'DM'.foldrWithKey` process iworld ioStates
where
	process :: !IOHandle !IOState !*IWorld -> *IWorld
	process ioHandle ioState=:{onShareChangeHandler, onDestroyHandler, sdsModifiedByHandlers, taskId, ioStatus} iworld
		= case ioStatus of
			// Not yet connected, skip
			IOConnecting _ = iworld
			// Listener, no need to do anything
			IOListener _ = iworld
			// Exception occured, leave as is
			IOException _ = iworld
			// Task is destroyed, call onDestroyhandler and clean up
			IODestroyed dynSt cId
				# (newDynSt, out, iworld) = onDestroyHandler dynSt iworld
				# iworld = 'AsyncIO'.writeData cId out iworld
				# iworld = 'AsyncIO'.closeConnection cId iworld
				# (_, iworld) = delIOState ioHandle iworld
				= iworld
			// IO is closed, value is stable, do nothing
			IOClosed dynSt = iworld
			// IOActive or IOClosed
			IOActive dynSt cId
				// Check if it is closed
				# (closed, iworld) = 'AsyncIO'.isConnectionBeingClosed cId iworld
				| closed = iworld
				// Process onShareChange
				# (mbr, iworld) = read sdsModifiedByHandlers EmptyContext iworld
				| isError mbr
					# iworld = 'AsyncIO'.closeConnection cId iworld
					= handleIOException (snd $ fromError mbr) ioHandle taskId iworld
				# r = directResult o fromOk $ mbr
				# (mbNewSt, mbw, out, close, iworld) = onShareChangeHandler dynSt r iworld
				| isError mbNewSt
					# iworld = 'AsyncIO'.closeConnection cId iworld
					= handleIOException (fromError mbNewSt) ioHandle taskId iworld
				# (Ok newSt) = mbNewSt
				# iworld = 'AsyncIO'.writeData cId out iworld
				// Write share if needed
				# (mbSdsErr, iworld) = writeShareIfNeeded sdsModifiedByHandlers mbw iworld
				| isError mbSdsErr
					# iworld = 'AsyncIO'.closeConnection cId iworld
					= handleIOException (snd $ fromError mbSdsErr) ioHandle taskId iworld
				| close
					// Clean up the connection
					# iworld = 'AsyncIO'.closeConnection cId iworld
					// Set the state to closed
					# newSt = fromMaybe dynSt newSt
					# (mbErr, iworld) = updIOState ioHandle (\ioState=Ok {ioState & ioStatus=IOClosed newSt}) iworld
					// Always refresh because the state changed
					# iworld = queueRefreshIfNeeded taskId iworld
					= iworld
				//
				| otherwise
					| isNone newSt = iworld
					# (mbErr, iworld) = updIOState ioHandle (\ioState=Ok {ioState & ioStatus=IOActive (fromJust newSt) cId}) iworld
					| isError mbErr = handleIOException (fromError mbErr) ioHandle taskId iworld
					# iworld = queueRefreshIfNeeded taskId iworld
					= iworld

emptyAsyncIOWriteQueues :: !*IWorld -> *IWorld
emptyAsyncIOWriteQueues iworld = {iworld & aioState.writeQueues = 'DM'.newMap}
