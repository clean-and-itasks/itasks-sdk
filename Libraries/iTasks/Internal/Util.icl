implementation module iTasks.Internal.Util

from AsyncIO import instance toString ConnectionId
import Base64
import qualified Control.Monad as M
import Data.Error
import Data.Error
from Data.Foldable import class Foldable, instance Foldable []
import Data.Func
import Data.Functor
import Data.GenEq
import Data.List
import qualified Data.Map as DM
from Data.Map import :: Map
import Data.Maybe
import Data.Queue
import qualified Data.Set as DS
from Data.Set import :: Set, instance Foldable Set
import Data.Tuple
import StdEnv
import StdDebug
import StdOverloadedList
import System.Directory
import System.File
import System.FilePath
import System.OS
import System.OSError
import System.Time
import Text
import Text.GenJSON

import iTasks.Engine
from iTasks.Extensions.DateTime import :: Date{..}, :: Time{..}, :: DateTime(..)
import iTasks.Internal.IWorld
import iTasks.Internal.IWorld
import iTasks.Internal.TaskEval
import iTasks.UI.Definition
import iTasks.WF.Definition

showOut :: ![String] !*World -> *World
showOut lines world
	# (console, world) = stdio world
	# console          = seqSt (\s c -> fwrites (s +++ OS_NEWLINE) c) lines console
	# (_,world)        = fclose console world
	= world

showErr :: ![String] !*World -> *World
showErr lines world
	# console          = seqSt (\s c -> fwrites (s +++ OS_NEWLINE) c) lines stderr
	# (_,world)        = fclose console world
	= world

iShowOut :: ![String] !*IWorld -> *IWorld
iShowOut lines iworld = {iworld & world = showOut lines iworld.world}

iShowErr :: ![String] !*IWorld -> *IWorld
iShowErr lines iworld = {iworld & world = showErr lines iworld.world}

showWhenVerbose :: ![String] !*IWorld -> *IWorld
showWhenVerbose lines iworld
	| iworld.options.verboseOperation = iShowOut lines iworld
	| otherwise                       = iworld

tmToDateTime :: !Tm -> DateTime
tmToDateTime tm
	= {DateTime| day = tm.Tm.mday, mon = 1 + tm.Tm.mon, year = 1900 + tm.Tm.year
	  ,hour = tm.Tm.hour, min = tm.Tm.min, sec= tm.Tm.sec}

toCanonicalPath	:: !FilePath !*World -> (!FilePath,!*World)
toCanonicalPath path world
	| isAbsolute path = (canonicalize path,world)
	| otherwise
		= case getCurrentDirectory world of
			(Ok curDir,world)	= (canonicalize (curDir</>path), world)
			(_,world)		= (canonicalize path,world)
where
	isAbsolute path = IF_POSIX_OR_WINDOWS (startsWith {pathSeparator} path) (size path >= 2 && isUpper path.[0] && path.[1] == ':')

	canonicalize path = join {pathSeparator} (undot [] (split {pathSeparator} path))

	undot acc []				= reverse acc
	undot []  ["..":ds]			= undot [] ds
	undot [_:acc] ["..":ds]			= undot acc ds
	undot acc [".":ds]			= undot acc ds
	undot [] ["":ds]			= undot [""] ds //Only allowed at the beginning
	undot acc ["":ds]			= undot acc ds
	undot acc [d:ds] 			= undot [d:acc] ds

(>-=) infixl 1 :: (*env -> *(MaybeError e a, *env)) (a -> *(*env -> (MaybeError e b, *env))) *env -> (MaybeError e b, *env)
(>-=) a b w
	# (mca, w) = a w
	= case mca of
		Error e = (Error e, w)
		Ok a = (b a) w

liftIWorld :: .(*World -> *(.a, *World)) *IWorld -> *(.a, *IWorld)
liftIWorld f iworld
# (a, world) = f iworld.world
= (a, {iworld & world=world})

apIWTransformer :: *env (*env -> *(MaybeError TaskException (TaskResult a), *env)) -> *(TaskResult a, *env)
apIWTransformer iw f = case f iw of
	(Error e, iw) = (ExceptionResult e, iw)
	(Ok tv, iw) = (tv, iw)

generateRandomString :: !Int !*IWorld -> (!String, !*IWorld)
generateRandomString length iworld=:{IWorld|random}
	= (toString (take length [toChar (97 +  abs (i rem 26)) \\ i <- random]) , {IWorld|iworld & random = drop length random})

isRefreshForTask :: !Event !TaskId -> Bool
isRefreshForTask (RefreshEvent taskIds) taskId = 'DS'.member taskId taskIds
isRefreshForTask ResetEvent _ = True
isRefreshForTask _ _ = False

isDestroyOrInterrupt :: !Event -> Bool
isDestroyOrInterrupt DestroyEvent = True
isDestroyOrInterrupt ServerInterruptedEvent = True
isDestroyOrInterrupt _ = False

mkTaskEvalInfo :: TaskEvalInfo
mkTaskEvalInfo = {TaskEvalInfo | removedTasks = [|]}

mkTaskEvalOpts :: !TaskId !TaskTime !(?InstanceNo) ![TaskId] -> TaskEvalOpts
mkTaskEvalOpts taskId lastEval sessionInstance attachmentChain =
	{ TaskEvalOpts
	| noUI = False
	, taskId = taskId
	, lastEval = lastEval
	, sessionInstance = sessionInstance
	, attachmentChain = attachmentChain
	}

mkTaskEvalState :: !TaskNo -> .TaskEvalState
mkTaskEvalState nextTaskNo =
	{ TaskEvalState
	| nextTaskNo = nextTaskNo
	}

mkUIIfReset :: !Event !UI -> UIChange
mkUIIfReset ResetEvent ui = ReplaceUI ui
mkUIIfReset _ ui = NoChange

mkEmptyUI :: !Event -> UIChange
mkEmptyUI e = mkUIIfReset e (ui UIEmpty)

appNameFor :: !FilePath -> String
appNameFor appPath = if (takeExtension appPath == "exe") dropExtension id o dropDirectory $ appPath

safeDequeue :: !(Queue a) -> (!?a, !?(Queue a))
safeDequeue q = case dequeue q of
	(?None, newq) = (?None, ?None)
	(?Just qitem, newq) = (?Just qitem, ?Just newq)

traceIOStates :: !String !*IWorld -> IWorld
traceIOStates msg iworld
	# iworld = trace_n msg iworld
	# (ioStates, iworld) = iworld!ioStates
	| not (trace_tn ("\tioStates: " +++ toString ioStates)) = undef
	= iworld

instance toString (Map a b) | toString a & toString b where
	toString m = concat3 "toList [" (concat (intersperse ", " (map toString ('DM'.toList m)))) "]"
instance toString (?a) | toString a where
	toString ?None = "?None"
	toString (?Just a) = "?Just " +++ toString a
instance toString IOState where
	toString {taskId,ioStatus} = concat ["{IOState | taskId=", toString taskId, ", ioStatus=", toString ioStatus, "}"]
instance toString Dynamic where
	toString d = concat ["dynamic (": dynToString d [")"]]
	where
		dynToString ([] :: [a]) acc = ["empty [", toString (typeCodeOfDynamic (dynamic undef :: a)), "]" : acc]
		dynToString (l=:[a:_] :: [a]) acc = ["[" : dynToString (dynamic a) ["] of length ", toString (length l) : acc]]
		dynToString ([!] :: [!a]) acc = ["empty [!", toString (typeCodeOfDynamic (dynamic undef :: a)), "]" : acc]
		dynToString (l=:[!a:_] :: [!a]) acc = ["[!": dynToString (dynamic a) ["!] of length ", toString (Length l) : acc]]
		dynToString ([ !] :: [a!]) acc = ["empty [", toString (typeCodeOfDynamic (dynamic undef :: a)), "!]":acc]
		dynToString (l=:[a:_!] :: [a!]) acc = ["[" : dynToString (dynamic a) ["!] of length ", toString (Length l) : acc]]
		dynToString ([!!] :: [!a!]) acc = ["empty [!", toString (typeCodeOfDynamic (dynamic undef :: a)), "!]" : acc]
		dynToString (l=:[!a:_!] :: [!a!]) acc = ["[!" : dynToString (dynamic a) ["!] of length ", toString (Length l) : acc]]
		dynToString (l :: [#a!]) acc = ["[#", toString (typeCodeOfDynamic (dynamic undef :: a)), "!]" : acc]
		dynToString (l :: [#a]) acc = ["[#", toString (typeCodeOfDynamic (dynamic undef :: a)), "]" : acc]
		dynToString (?None :: ?a) acc = ["?", toString (typeCodeOfDynamic (dynamic undef :: a)), " None" : acc]
		dynToString (?Just a :: ?a) acc = ["?" : dynToString (dynamic a) acc]
		dynToString (?^None :: ?^a) acc = ["?^", toString (typeCodeOfDynamic (dynamic undef :: a)), " None" : acc]
		dynToString (?^Just a :: ?^a) acc = ["?^" : dynToString (dynamic a) acc]
		dynToString (_ :: ?#a) acc = ["?^", toString (typeCodeOfDynamic (dynamic undef :: a)) : acc]
		dynToString (a :: {a}) acc
			| size a == 0 = ["{", toString (typeCodeOfDynamic (dynamic undef :: a)), "} of size ", toString (size a) : acc]
			| otherwise   = ["{": dynToString (dynamic a.[0]) ["} of size ", toString (size a) : acc]]
		dynToString (a :: {!a}) acc
			| size a == 0 = ["{!", toString (typeCodeOfDynamic (dynamic undef :: a)), "} of size ", toString (size a) : acc]
			| otherwise   = ["{!": dynToString (dynamic a.[0]) ["} of size ", toString (size a) : acc]]
		dynToString (_ :: {#a}) acc = ["{#", toString (typeCodeOfDynamic (dynamic undef :: a)), "}" : acc]
		dynToString (a :: {32#Int}) acc = ["{32#Int} of size ", toString (size a) : acc]
		dynToString (a :: {32#Real}) acc = ["{32#Real} of size ", toString (size a) : acc]
		dynToString (_ :: ()) acc = ["()" : acc]
		dynToString ((a, b) :: (a, b)) acc =
			[ "( ": dynToString (dynamic a)
			[ ", ": dynToString (dynamic b)
			[ ")" : acc ]]]
		dynToString ((a, b, c) :: (a, b, c)) acc =
			[ "( ": dynToString (dynamic a)
			[ ", ": dynToString (dynamic b)
			[ ", ": dynToString (dynamic c)
			[ ")" : acc ]]]]
		dynToString ((a, b, c, d) :: (a, b, c, d)) acc =
			[ "( ": dynToString (dynamic a)
			[ ", ": dynToString (dynamic b)
			[ ", ": dynToString (dynamic c)
			[ ", ": dynToString (dynamic d)
			[ ")" : acc]]]]]
		dynToString ((a, b, c, d, e) :: (a, b, c, d, e)) acc =
			[ "( ": dynToString (dynamic a)
			[ ", ": dynToString (dynamic b)
			[ ", ": dynToString (dynamic c)
			[ ", ": dynToString (dynamic d)
			[ ", ": dynToString (dynamic e)
			[ ")" : acc]]]]]]
		dynToString ((a, b, c, d, e, f) :: (a, b, c, d, e, f)) acc =
			[ "( ": dynToString (dynamic a)
			[ ", ": dynToString (dynamic b)
			[ ", ": dynToString (dynamic c)
			[ ", ": dynToString (dynamic d)
			[ ", ": dynToString (dynamic e)
			[ ", ": dynToString (dynamic f)
			[ ")" : acc]]]]]]]
		dynToString ((a, b, c, d, e, f, g) :: (a, b, c, d, e, f, g)) acc =
			[ "( ": dynToString (dynamic a)
			[ ", ": dynToString (dynamic b)
			[ ", ": dynToString (dynamic c)
			[ ", ": dynToString (dynamic d)
			[ ", ": dynToString (dynamic e)
			[ ", ": dynToString (dynamic f)
			[ ", ": dynToString (dynamic g)
			[ ")" : acc]]]]]]]]
		dynToString d acc = [toString (typeCodeOfDynamic d) : acc]

base64URLDecodeUnsafe :: !.String -> .String
base64URLDecodeUnsafe s = case base64URLDecode s of
	Ok a = a
	Error a = abort ("base64URLDecodeUnsafe failed with error: " +++ a +++ "\n")
base64DecodeUnsafe :: !.String -> .String
base64DecodeUnsafe s = case base64Decode s of
	Ok a = a
	Error a = abort ("base64DecodeUnsafe failed with error: " +++ a +++ "\n")
