implementation module iTasks.Internal.SDS

import StdString, StdTuple, StdMisc, StdBool, StdInt, StdChar, StdFunctions, StdArray, StdStrictLists
from StdList import flatten, map, take, drop, isMember, instance toString [a], instance length []
from Text import class Text, instance Text String, concat4
import qualified Text
from Data.Map import :: Map
import qualified Data.Map as DM
import Data.Error, Data.Func, Data.Functor, Data.Tuple, System.OS, System.Time, Text.GenJSON, Data.Foldable
import Data.Maybe
import Data.GenHash
from Data.Set import instance Foldable Set, instance < (Set a)
import qualified Data.Set as Set
import iTasks.Engine
import iTasks.Internal.IWorld
import iTasks.Internal.Task, iTasks.Internal.TaskState, iTasks.Internal.TaskEval
import iTasks.Internal.TaskIO
from iTasks.SDS.Definition import instance == PossiblyRemoteTaskId
import iTasks.SDS.Sources.Core
import iTasks.WF.Tasks.IO
import Text.GenJSON
import iTasks.Internal.AsyncSDS
import iTasks.Internal.AsyncIO
import iTasks.Internal.Util
from Text import instance + String

createReadWriteSDS ::
	!Bool
	!String
	!String
	!(p *IWorld -> *(MaybeError TaskException r, *IWorld))
	!(p w *IWorld -> *(MaybeError TaskException (SDSNotifyPred p), *IWorld))
	-> SDSSource p r w
	| gHash{|*|} p
createReadWriteSDS cacheRead ns id read write
	= createSDS cacheRead ns id read write amendf
where
	amendf p f iworld = case read p iworld of
		(Ok r, iworld) = case f r of
			Ok (a, ?None) = (Ok (a, ?None), iworld)
			Ok (a, ?Just w) = case write p w iworld of
				(Ok npred, iworld) = (Ok (a, ?Just (w, npred)), iworld)
				(Error e, iworld) = (Error e, iworld)
			Error e = (Error e, iworld)
		(Error e, iworld) = (Error e, iworld)

createReadWriteAmendSDS ::
	!Bool
	!String
	!String
	!(p *IWorld -> *(MaybeError TaskException r, *IWorld))
	!(p w *IWorld -> *(MaybeError TaskException (SDSNotifyPred p), *IWorld))
	!(A.a: p -> (r -> MaybeError TaskException (a, ?w)) -> *IWorld -> *(MaybeError TaskException (a, ?(w, SDSNotifyPred p)), *IWorld))
	-> SDSSource p r w
	| gHash{|*|} p
createReadWriteAmendSDS cacheRead ns id read write amend = createSDS cacheRead ns id read write amend

createReadOnlySDS ::
	!Bool !String !String !(p *IWorld -> *(MaybeError TaskException r, *IWorld)) -> SDSSource p r () | gHash{|*|} p
createReadOnlySDS cacheRead namespace id read
	= createReadWriteSDS cacheRead namespace id read (\_ _ iworld -> (Ok (const (const True)), iworld))

createInternalReadOnlySDS ::
	!Bool !String !(p *IWorld -> *(MaybeError TaskException r, *IWorld)) -> SDSSource p r () | gHash{|*|} p
createInternalReadOnlySDS cacheRead name read = createReadOnlySDS cacheRead "iTask" name read

createSDS ::
	!Bool
	!String
	!String
	!(p *IWorld -> *(MaybeError TaskException r, *IWorld))
	!(p w *IWorld -> *(MaybeError TaskException (SDSNotifyPred p), *IWorld))
	!(A.a: p -> (r -> MaybeError TaskException (a, ?w)) -> *IWorld -> *(MaybeError TaskException (a, ?(w, SDSNotifyPred p)), *IWorld))
	-> SDSSource p r w
	| gHash{|*|} p
createSDS cacheRead ns id read write amend = SDSSource
	{ SDSSourceOptions
	| name = ns +++ ":" +++ id
	, read = read
	, write = write
	, amend = amend
	, cacheRead = cacheRead
	}

iworldNotifyPred :: !(p -> Bool) !p !*IWorld -> (!Bool,!*IWorld)
iworldNotifyPred npred p iworld = (npred p, iworld)

read            :: !(sds () r w) !TaskContext !*IWorld
	-> (!MaybeError TaskException (AsyncRead r w), !*IWorld) | TC r & TC w & Readable sds
read sds c iworld = case readSDS sds () c iworld of
	(ReadResult r sds, iworld) = (Ok (ReadingDone r), iworld)
	(AsyncRead sds, iworld)    = (Ok (Reading sds), iworld)
	(ReadException e, iworld)  = (Error e, iworld)

readRegister :: !TaskContext !TaskId !(sds () r w) !*IWorld -> (!MaybeError TaskException (AsyncRead r w), !*IWorld) | TC r & TC w & Readable, Registrable sds
readRegister context taskId sds iworld = case readRegisterSDS sds () context taskId iworld of
	(ReadResult r sds, iworld) = (Ok (ReadingDone r), iworld)
	(AsyncRead sds, iworld)    = (Ok (Reading sds), iworld)
	(ReadException e, iworld)  = (Error e, iworld)

// When a remote requests a register, we do not have a local task id rather a remote task context which we use to record the request.
mbRegister p sds mbRegister context iworld=:{sdsNotifyRequests,sdsNotifyReqsByTask,clock} :== case mbRegister of
	JustRead = iworld
	ReadAndRegister taskId = register p sds context taskId iworld

register :: !p !(sds p r w) !TaskContext !TaskId !*IWorld -> *IWorld | TC, gHash{|*|} p & Registrable sds
register p sds context taskId iworld=:{sdsNotifyRequests,sdsNotifyReqsByTask,clock}
	# remoteOpts = case context of
		RemoteTaskContext reqTaskId currTaskId remoteSDSId host port =
			?Just {hostToNotify=host, portToNotify=port, remoteSdsId=remoteSDSId}
		_ =
			?None
	# taskIdAndRemoteOpts = {taskId = taskId, remoteNotifyOptions = remoteOpts}
	# req = {cmpParam=dynamic p, cmpParamHash = gHash{|*|} p}
	# {id_hash} = sdsIdentity sds
	= { iworld
		& sdsNotifyRequests =
			'DM'.alter
				(?Just o notifyRequestsWithRequestFor taskIdAndRemoteOpts req clock) id_hash sdsNotifyRequests
		, sdsNotifyReqsByTask = case context of
			// We do not store remote requests in the tasks map, the task ID's are not local to this instance.
			RemoteTaskContext _ _ _ _ _
				= sdsNotifyReqsByTask
				= 'DM'.alter (?Just o maybe ('Set'.singleton id_hash) ('Set'.insert id_hash)) taskId sdsNotifyReqsByTask
		}

write :: !w !(sds () r w) !TaskContext !*IWorld -> (!MaybeError TaskException (AsyncWrite r w), !*IWorld) | TC r & TC w & Writeable sds
write w sds c iworld
= case writeSDS sds () c w iworld of
		(WriteResult notify _, iworld) = (Ok WritingDone, queueNotifyEvents (sdsIdentity sds) notify iworld)
		(AsyncWrite sds, iworld)       = (Ok (Writing sds), iworld)
		(WriteException e,iworld)      = (Error e,iworld)

writeShareIfNeeded :: !(sds () r w) !(?w) !*IWorld -> (!MaybeError TaskException (), !*IWorld) | TC r & TC w & Writeable sds
writeShareIfNeeded sds ?None iworld  = (Ok (), iworld)
writeShareIfNeeded sds (?Just w) iworld = case write w sds EmptyContext iworld of
	(Error e, iworld) = (Error e, iworld)
	(Ok WritingDone, iworld) = (Ok (), iworld)

directResult :: (AsyncRead r w) -> r
directResult (ReadingDone r) = r
directResult _ = abort "No direct result!"

//Check the registrations and find the set of id's for which the current predicate holds
//and for which id's it doesn't

checkRequests :: !SDSIdentity !(SDSNotifyPred p) !*IWorld -> (!Set PossiblyRemoteTaskId, !*IWorld) | TC p
checkRequests sdsId pred iworld
	# (registrations, iworld) = lookupRequests sdsId iworld
	# (taskIdList, iworld) = foldedRequestsIn match registrations ([], iworld)
	= ('Set'.fromDistinctDescList taskIdList, iworld)
where
	match ::
		!PossiblyRemoteTaskId !SDSNotifyRequest !Timespec !(![PossiblyRemoteTaskId], !*IWorld)
		-> (![PossiblyRemoteTaskId], !*IWorld)
	match taskIdAndRemoteOpts req reqTimespec st=:(matches, iworld)
		// If we have a match for the task ID already, we don't have to check the predicate again.
		// It is sufficient to check the head of the list, as data is ordered on `taskIdAndRemoteOpt`.
		# matchesAlready =
			case matches of
				[]              = False
				[lastTaskId: _] = taskIdAndRemoteOpts == lastTaskId
		| matchesAlready = st
		# (match, iworld) = registrationMatches pred req reqTimespec iworld
		= (if match [taskIdAndRemoteOpts:matches] matches, iworld)

/**
 * Filters out tasks for which the registration is blocked by any of the registration for the specified SDS.
 * The registration is filtered if the predicate for any registration does not hold
 * and there is no registration with for which the predicate holds.
 */
filterRequests ::
	!SDSIdentity !(SDSNotifyPred p) !(Set PossiblyRemoteTaskId) !*IWorld -> (!Set PossiblyRemoteTaskId, !*IWorld) | TC p
filterRequests sdsId pred childRegistrations iworld
	# (registrations, iworld) = lookupRequests sdsId iworld
	= foldl` (filtered registrations) (childRegistrations, iworld) childRegistrations
where
	filtered ::
		!SdsNotifyRequests !(!Set PossiblyRemoteTaskId, !*IWorld) !PossiblyRemoteTaskId
		-> (!Set PossiblyRemoteTaskId, !*IWorld)
	filtered registrations (childRequests, iworld) taskId
		# (shouldFilterTask, iworld) =
			foldedRequestsFor shouldFilter taskId registrations (?None, iworld)
		| shouldFilterTask =: (?Just True) =
			('Set'.delete taskId childRequests, iworld)
		| otherwise =
			(childRequests, iworld)

	shouldFilter :: !SDSNotifyRequest !Timespec !(!?Bool, !*IWorld) -> (!?Bool, !*IWorld)
	shouldFilter _ _ res=:(?Just False, _) = res // no further check required as soon as we found a match
	shouldFilter req reqTimespec (_, iworld)
		# (matches, iworld) = registrationMatches pred req reqTimespec iworld
		= (?Just $ not matches, iworld)

/**
 * Checks whether the given predicate matches the request.
 * An error is printed in case the same SDS is used with parameters of different type.
 */
registrationMatches :: !(SDSNotifyPred p) !SDSNotifyRequest !Timespec !*IWorld -> (!Bool, !*IWorld) | TC p
registrationMatches pred {cmpParam} reqTimespec iworld =
	case cmpParam of
		(p :: p^)
			= (pred reqTimespec p, iworld)
		_
			# iworld = iShowErr
				[ "Dynamic type mismatch in iTasks.Internal.SDS.registrationMatches, "
				+ "this may be due to duplicate sds identities."
				] iworld
			= (True, iworld)

//Find all notify requests for the given share id
lookupRequests :: !SDSIdentity !*IWorld -> (!SdsNotifyRequests, !*IWorld)
lookupRequests {id_hash} iworld=:{sdsNotifyRequests} =
	('DM'.findWithDefault emptyNotifyRequests id_hash sdsNotifyRequests, iworld)

modify :: !(r -> w) !(sds () r w) !TaskContext !*IWorld -> (!MaybeError TaskException (AsyncModify r w ()), !*IWorld) | TC r & TC w & Modifiable sds
modify f sds context iworld = amend` (\r->((), ?Just (f r))) sds context iworld

amend` :: !(r -> (a, ?w)) !(sds () r w) !TaskContext !*IWorld -> (!MaybeError TaskException (AsyncModify r w a), !*IWorld) | TC r & TC w & TC a & Modifiable sds
amend` f sds context iworld = case modifySDS (\r. Ok (f r)) sds () context iworld of
	(ModifyResult notify r mw a _, iworld)
		# iworld = case mw of
				?None = iworld
				// Only queue events when we have actually written.
				?Just w = queueNotifyEvents (sdsIdentity sds) notify iworld
		= (Ok (ModifyingDone mw a), iworld)
	(AsyncModify sds _, iworld)
		= (Ok (Modifying sds f), iworld)
	(ModifyException e, iworld)
		= (Error e, iworld)

queueNotifyEvents :: !SDSIdentity !(Set PossiblyRemoteTaskId) !*IWorld -> *IWorld
queueNotifyEvents sdsId notify iworld
	# (remotes,locals) = partition_notify [] 'Set'.newSet ('Set'.toList notify)
	# iworld = queueRefreshes locals iworld
	= queueRemoteRefresh remotes iworld
where
	partition_notify rem loc [] = (rem,loc)
	partition_notify rem loc [{taskId,remoteNotifyOptions}:rest] = case remoteNotifyOptions of
		?Just opts = partition_notify [(taskId,opts):rem] loc rest
		?None      = partition_notify rem ('Set'.insert taskId loc) rest

clearTaskSDSRegistrations :: !(Set TaskId) !*IWorld -> *IWorld
clearTaskSDSRegistrations taskIds iworld=:{IWorld|sdsNotifyRequests, sdsNotifyReqsByTask}
	# sdsIdsToClear = foldl
		(\sdsIdsToClear taskId -> 'Set'.union ('DM'.findWithDefault 'Set'.newSet taskId sdsNotifyReqsByTask) sdsIdsToClear)
		'Set'.newSet
		taskIds
	= { iworld
	  & sdsNotifyRequests   = foldl clearRequestRequests sdsNotifyRequests sdsIdsToClear
	  , sdsNotifyReqsByTask = foldl (flip 'DM'.del) sdsNotifyReqsByTask taskIds
	  }
where
	clearRequestRequests ::
		!(Map SDSIdentityHash SdsNotifyRequests) !SDSIdentityHash -> Map SDSIdentityHash SdsNotifyRequests
	clearRequestRequests requests sdsId
		| isEmptyNotifyRequests filteredReqsForSdsId = 'DM'.del sdsId requests
		| otherwise                                  = 'DM'.put sdsId filteredReqsForSdsId requests
	where
		filteredReqsForSdsId = withoutRequestsFor taskIds reqsForSdsId
		reqsForSdsId = 'DM'.findWithDefault emptyNotifyRequests sdsId requests

listAllSDSRegistrations :: !*IWorld -> (![(InstanceNo,[(TaskId,SDSIdentityHash)])], !*IWorld)
listAllSDSRegistrations iworld=:{IWorld|sdsNotifyRequests} =
	('DM'.toList $ 'DM'.foldrWithKey` withRequestInfoFor 'DM'.newMap sdsNotifyRequests, iworld)

formatRequests :: [(InstanceNo,[(TaskId,SDSIdentityHash)])] -> String
formatRequests list = 'Text'.join "\n" lines
where
	lines = [toString instanceNo +++ " -> " +++
				('Text'.join "\n\t" ['Text'.concat [toString tId,":",toString sdsId] \\ (tId, sdsId) <- requests])
			\\ (instanceNo, requests) <- list]

flushDeferredSDSWrites :: !*IWorld -> (!MaybeError TaskException (), !*IWorld)
flushDeferredSDSWrites iworld=:{writeCache}
	# (errors,iworld) = flushAll ('DM'.toList writeCache) iworld
	# iworld & writeCache = 'DM'.newMap
	| errors =: [] = (Ok (), iworld)
	# msg = 'Text'.join OS_NEWLINE ["Could not flush all deferred SDS writes, some data may be lost":map snd errors]
	= (Error (exception msg), iworld)
where
	flushAll [] iworld = ([],iworld)
	flushAll [(_,(_,DeferredWrite p w sds)):rest] iworld
		= case writeSDS sds p EmptyContext w iworld of
			(WriteResult notify _,iworld)
				# iworld = queueNotifyEvents (sdsIdentity sds) notify iworld
				= flushAll rest iworld
			(WriteException e,iworld)
				# (errors,iworld) = flushAll rest iworld
				= ([e:errors],iworld)

fromReadException (ReadException e) :== e
fromWriteException (WriteException e) :== e

instance Identifiable SDSSource
where
	sdsIdentity (SDSSource {SDSSourceOptions|name}) = createSDSIdentity name ?None ?None
	sdsIdentity (SDSValue done mr sds) = sdsIdentity sds

instance Readable SDSSource
where
	readSDS sds p c iworld = readSDSSource sds p c JustRead iworld

instance Writeable SDSSource
where
	writeSDS sds=:(SDSSource {SDSSourceOptions|write,name, cacheRead}) p _ w iworld
	// We have to clear the read value when writing, the new read value cannot be derived from the value written.
	# iworld = if cacheRead {iworld & readSdsSourceVals = 'DM'.del (sdsCacheKey sds p) iworld.readSdsSourceVals} iworld
	= case write p w iworld of
		(Error e, iworld) = (WriteException e, iworld)
		(Ok npred, iworld)
			# (match, iworld) = checkRequests (sdsIdentity sds) npred iworld
			= (WriteResult match sds, iworld)

	writeSDS (SDSValue False val sds) p c w iworld = case writeSDS sds p c w iworld of
		(WriteResult r ssds, iworld) = (WriteResult r (SDSValue True val ssds), iworld)
		(AsyncWrite ssds, iworld)    = (AsyncWrite (SDSValue False val ssds), iworld)
		(WriteException e, iworld)   = (WriteException e, iworld)

	writeSDS (SDSValue True val sds) p c w iworld = (WriteResult 'Set'.newSet sds, iworld)

instance Modifiable SDSSource
where
	modifySDS f sds=:(SDSSource {SDSSourceOptions|name, cacheRead}) p context iworld
	// We have to clear the read value when writing, the new read value cannot be derived from the value written.
	# iworld = if cacheRead {iworld & readSdsSourceVals = 'DM'.del (sdsCacheKey sds p) iworld.readSdsSourceVals} iworld
	= case readSDS sds p context iworld of
		(ReadResult r ssds, iworld) =  case f r of
			Error e = (ModifyException e, iworld)
			Ok (a, ?None) = (ModifyResult 'Set'.newSet r ?None a ssds, iworld)
			Ok (a, ?Just w) = case writeSDS ssds p context w iworld of
				(WriteResult n ssds, iworld) = (ModifyResult n r (?Just w) a ssds, iworld)
				(WriteException e, iworld)   = (ModifyException e, iworld)
		(ReadException e, iworld) = (ModifyException e, iworld)

	modifySDS f (SDSValue False v sds) p c iworld = case modifySDS f sds p c iworld of
		(ModifyResult notify r mw a ssds, iworld) = (ModifyResult notify r mw a (SDSValue False v ssds), iworld)
		(AsyncModify ssds f, iworld) = (AsyncModify (SDSValue True v ssds) f, iworld)
		(ModifyException e, iworld) = (ModifyException e, iworld)

	modifySDS f (SDSValue True r sds) p c iworld = case f r of
		Error e = (ModifyException e, iworld)
		Ok (a, mw) = (ModifyResult 'Set'.newSet r mw a (SDSValue True r sds), iworld)

instance Registrable SDSSource
where
	readRegisterSDS sds p c taskId iworld = readSDSSource sds p c (ReadAndRegister taskId) iworld

readSDSSource sds p c mbNotify iworld=:{readSdsSourceVals} :== case sds of
	SDSSource {SDSSourceOptions|read,name,cacheRead}
		# iworld = mbRegister p sds mbNotify c iworld
		| cacheRead
			# key = sdsCacheKey sds p
			= case 'DM'.get key readSdsSourceVals of
				?Just (val :: r^) = (ReadResult val sds, iworld)
				// We ignore value of wrong type which can occur if non-unique SDS IDs are used, such that we only pay a
				// performance cost, but things keep working.
				_ = case read p iworld of
					(Error e, iworld) = (ReadException e, iworld)
					(Ok r, iworld) =
						( ReadResult r sds
						, {iworld & readSdsSourceVals = 'DM'.put key (dynamic r :: r^) iworld.readSdsSourceVals})
		| otherwise = case read p iworld of
				(Error e, iworld) = (ReadException e, iworld)
				(Ok r, iworld) = (ReadResult r sds, iworld)
	SDSValue done v _
		= (ReadResult v sds, iworld)

instance Identifiable SDSLens
where
	sdsIdentity (SDSLens _ opts) = opts.SDSLensOptions.id

instance Readable SDSLens
where
	readSDS sds p c iworld = readSDSLens sds p c JustRead iworld

instance Writeable SDSLens
where
	writeSDS :: !(SDSLens p r w) !p !TaskContext !w !*IWorld -> *(!WriteResult p r w, !*IWorld) | TC p & TC r & TC w
	writeSDS sds=:(SDSLens sdsChild opts=:{SDSLensOptions|param,write,notify,name}) p c w iworld
	# childParam = param p
	= case write of
		// Need to read as `SDSWrite` requires read result.
		(SDSWrite writef) =
			case readSDS sdsChild childParam c iworld of
				(ReadResult readResultChild childSds, iworld)
					# mbValueToWriteForChild = writef p readResultChild w
					= case mbValueToWriteForChild of
						Error e       = (WriteException e, iworld)
						Ok ?None
							//Don't notify if we don't write to the child.
							= (WriteResult 'Set'.newSet (SDSLens childSds opts), iworld)
						Ok (?Just valueToWriteForChild) =
							case writeSDS childSds childParam c valueToWriteForChild iworld of
								(WriteResult tasksToNotifyForChild ssds, iworld)
									// Remove the registrations that we can eliminate based on the
									// current notification predicate.
									# (tasksToNotifyForChild, iworld) = case notify of
										SDSNotify pred
											# notifyPred = pred p readResultChild w
											= filterRequests (sdsIdentity sds) notifyPred tasksToNotifyForChild iworld
										SDSNotifyWithoutRead pred
											# notifyPred = pred p w
											= filterRequests (sdsIdentity sds) notifyPred tasksToNotifyForChild iworld
										SDSNotifyConst shouldAlwaysNotify =
											(if shouldAlwaysNotify tasksToNotifyForChild 'Set'.newSet, iworld)
									= (WriteResult tasksToNotifyForChild (SDSLens childSds opts), iworld)
								(AsyncWrite sds, iworld) = (AsyncWrite (SDSLens sds opts), iworld)
								(WriteException e, iworld) = (WriteException e, iworld)
				(AsyncRead sds, iworld) = (AsyncWrite (SDSLens sds opts), iworld)
				(ReadException e, iworld) = (WriteException e, iworld)
		(SDSWriteConst writef) =
			case notify of
				// Need to read as `SDSNotify` requires read result.
				SDSNotify pred =
					case readSDS sdsChild childParam c iworld of
						(ReadResult readResultChild childSds, iworld)
							# mbValueToWriteForChild = writef p w
							= case mbValueToWriteForChild of
								Error e       = (WriteException e, iworld)
								Ok ?None
									// Don't notify if we don't write to the child.
									= (WriteResult 'Set'.newSet (SDSLens childSds opts), iworld)
								Ok (?Just valueToWriteForChild) =
									case writeSDS childSds childParam c valueToWriteForChild iworld of
										(WriteResult tasksToNotifyForChild childSds, iworld)
											// Remove the registrations that we can eliminate based on
											// the current notification predicate.
											# notifyPred = pred p readResultChild w
											# (tasksToNotifyForChild, iworld) =
												filterRequests (sdsIdentity sds) notifyPred tasksToNotifyForChild iworld
											= (WriteResult tasksToNotifyForChild (SDSLens childSds opts), iworld)
										(AsyncWrite sds, iworld) = (AsyncWrite (SDSLens sds opts), iworld)
										(WriteException e, iworld) = (WriteException e, iworld)
						(AsyncRead sds, iworld) = (AsyncWrite (SDSLens sds opts), iworld)
						(ReadException e, iworld) = (WriteException e, iworld)
				// No need to read
				notify
					# mbValueToWriteForChild = writef p w
					= case mbValueToWriteForChild of
						Error e       = (WriteException e, iworld)
						Ok ?None
							// Don't notify if we don't write to the child.
							= (WriteResult 'Set'.newSet (SDSLens sdsChild opts), iworld)
						Ok (?Just valueToWriteForChild) =
							case writeSDS sdsChild childParam c valueToWriteForChild iworld of
								(WriteResult tasksToNotifyForChild sdsChild, iworld)
									// Remove the registrations that we can eliminate based on
									// the current notification predicate.
									# (tasksToNotifyForChild, iworld) =
										case notify of
											SDSNotifyWithoutRead pred
												# notifyPred = pred p w
												= filterRequests
													(sdsIdentity sds) notifyPred tasksToNotifyForChild iworld
											SDSNotifyConst shouldAlwaysNotify =
												(if shouldAlwaysNotify tasksToNotifyForChild 'Set'.newSet, iworld)
									= (WriteResult tasksToNotifyForChild (SDSLens sdsChild opts), iworld)
								(AsyncWrite sds, iworld) = (AsyncWrite (SDSLens sds opts), iworld)
								(WriteException e, iworld) = (WriteException e, iworld)

instance Modifiable SDSLens
where
	modifySDS
		f sds=:(SDSLens childSds opts=:{SDSLensOptions|param, read, write, reducer, notify, name}) p context iworld
	= case reducer of
		?None = case readSDS sds p context iworld of
			(ReadResult r ssds, iworld) = case f r of
				Error e = (ModifyException e, iworld)
				Ok (a, ?None) = (ModifyResult 'Set'.newSet r ?None a ssds, iworld)
				Ok (a, ?Just w) = case writeSDS ssds p context w iworld of
					(WriteResult notify ssds, iworld) = (ModifyResult notify r (?Just w) a ssds, iworld)
					(AsyncWrite sds, iworld)          = (AsyncModify sds f, iworld)
					(WriteException e, iworld)        = (ModifyException e, iworld)
			(AsyncRead sds, iworld)     = (AsyncModify sds f, iworld)
			(ReadException e, iworld)   = (ModifyException e, iworld)

		?Just reducer = case modifySDS sf childSds (param p) context iworld of
			(ModifyResult toNotify rs ?None a childSds`, iworld) = case doRead read p rs of
				Error e = (ModifyException e, iworld)
				Ok r = (ModifyResult 'Set'.newSet r ?None a (SDSLens childSds` opts), iworld)
			(ModifyResult toNotify rs (?Just ws) a childSds`, iworld) = case reducer p ws of
				Error e = (ModifyException e, iworld)
				Ok w    = case doRead read p rs of
					Error e = (ModifyException e, iworld)
					Ok r
						# sdsId = sdsIdentity sds
						// The registrations on which tasks to notify when
						// the child sds of the `SDSLens` is modified, are filtered based on the `SDSLensNotify`
						// predicate provided to the SDSLens`.
						# (toNotify, iworld) = case notify of
							SDSNotify f
								# notifyPred = f p rs w
								= filterRequests sdsId notifyPred toNotify iworld
							SDSNotifyWithoutRead f
								# notifyPred = f p w
								= filterRequests sdsId notifyPred toNotify iworld
							SDSNotifyConst shouldAlwaysNotify =
								(if shouldAlwaysNotify toNotify 'Set'.newSet, iworld)
						= (ModifyResult toNotify r (?Just w) a (SDSLens childSds` opts), iworld)
			(AsyncModify sds _, iworld) = (AsyncModify (SDSLens sds opts) f, iworld)
			(ModifyException e, iworld) = (ModifyException e, iworld)
		where
			sf rs
			# readV = doRead read p rs
			= case readV of
				Error e = Error e
				Ok r = case f r of
					Error e = Error e
					Ok (a, ?None) = Ok (a, ?None)
					Ok (a, ?Just w) = case doWrite write p rs w of
						Error e = Error e
						Ok (?Just ws) = Ok (a, ?Just ws)
						_ = abort "Contact not satisfied: write yields ?None while there is a reducer\n"

			doRead readf p rs = case readf of
				SDSRead rf = rf p rs
				SDSReadConst rf = Ok (rf p)

			doWrite writef p rs w = case writef of
				SDSWrite wf = wf p rs w
				SDSWriteConst wf = wf p w

instance Registrable SDSLens
where
	readRegisterSDS sds p c taskId iworld = readSDSLens sds p c (ReadAndRegister taskId) iworld

readSDSLens sds=:(SDSLens sds1 opts=:{SDSLensOptions|param,read,notify}) p c mbNotify iworld
	# iworld = if (notify=:(SDSNotifyConst _)) iworld (mbRegister p sds mbNotify c iworld)
	:== case read of
		SDSRead f =
			case readAndMbRegisterSDS sds1 (param p) c (if (notify=:(SDSNotifyConst False)) JustRead mbNotify) iworld of
				(ReadResult r ssds, iworld) = case f p r of
					Error e = (ReadException e, iworld)
					Ok r = (ReadResult r (SDSLens ssds opts), iworld)
				(AsyncRead sds, iworld) = (AsyncRead (SDSLens sds opts), iworld)
				(ReadException e, iworld) = (ReadException e, iworld)
		SDSReadConst f = (ReadResult (f p) sds, iworld)

sdsCacheKey :: !(sds p r w) !p -> SDSCacheKey | Identifiable sds & gHash{|*|} p
sdsCacheKey sds p =
	{ sdsIdHash      = (sdsIdentity sds).id_hash
	, cacheParamHash = gHash{|*|} p
	}

// SDSCache
instance Identifiable SDSCache
where
	sdsIdentity (SDSCache _ opts) = opts.SDSCacheOptions.id

instance Readable SDSCache
where
	readSDS sds p c iworld = readSDSCache sds p c JustRead iworld

instance Writeable SDSCache
where
	writeSDS sds=:(SDSCache sds1 opts=:{SDSCacheOptions|write}) p c w iworld=:{IWorld|readCache,writeCache}
	# key = sdsCacheKey sds p
	//Check cache
	# mbr = case 'DM'.get key readCache of
		?Just (val :: r^) = ?Just val
		_                 = ?None
	# mbw = case 'DM'.get key writeCache of
		?Just (val :: w^,_) = ?Just val
		_                   = ?None
	//Determine what to do
	# (mbr,policy) = write p mbr mbw w
	//Update read cache
	# iworld & readCache = case mbr of
		?Just r = 'DM'.put key (dynamic r :: r^) readCache
		?None   = 'DM'.del key readCache
	= case policy of
		NoWrite = (WriteResult 'Set'.newSet sds, iworld)
		WriteNow = case writeSDS sds1 p c w iworld of
			(WriteResult r _, iworld)  = (WriteResult r sds, iworld)
			(WriteException e, iworld) = (WriteException e, iworld)
		WriteDelayed
			//FIXME: Even though write is delayed, the notification should still happen
			# iworld & writeCache = 'DM'.put key (dynamic w :: w^, DeferredWrite p w sds1) writeCache
			= (WriteResult 'Set'.newSet sds, iworld)

instance Modifiable SDSCache
where
	modifySDS f sds=:(SDSCache _ opts) p context iworld
	= case readSDS sds p context iworld of
		(ReadResult r ssds, iworld) = case f r of
			Error e = (ModifyException e, iworld)
			Ok (a, ?None) = (ModifyResult 'Set'.newSet r ?None a sds, iworld)
			Ok (a, ?Just w) = case writeSDS ssds p context w iworld of
				(WriteResult notify ssds, iworld) = (ModifyResult notify r (?Just w) a sds, iworld)
				(WriteException e, iworld) = (ModifyException e, iworld)
		(AsyncRead sds, iworld)     = (AsyncModify sds f, iworld)
		(ReadException e, iworld) = (ModifyException e, iworld)

instance Registrable SDSCache
where
	readRegisterSDS sds p c taskId iworld = readSDSCache sds p c (ReadAndRegister taskId) iworld

readSDSCache :: !(SDSCache p r w) !p !TaskContext !ReadAndMbRegister !*IWorld
             -> *(!ReadResult p r w, !*IWorld) | TC p & TC r & TC w
readSDSCache sds=:(SDSCache sds1 opts) p c mbNotify iworld=:{readCache}
	# key = sdsCacheKey sds p
	//First check cache
	= case 'DM'.get key readCache of
		?Just (val :: r^)
			# iworld = mbRegister p sds1 mbNotify c iworld
			= (ReadResult val sds,iworld)
		?Just _ = (ReadException (exception "Cached value of wrong type"), iworld)
		?None   = case readAndMbRegisterSDS sds1 p c mbNotify iworld of
			//Read and add to cache
			(ReadResult val ssds,iworld) = (ReadResult val sds, {iworld & readCache = 'DM'.put key (dynamic val :: r^) iworld.readCache})
			(ReadException e,iworld) = (ReadException e, iworld)

// SDSSequence
instance Identifiable SDSSequence
where
	sdsIdentity (SDSSequence _ _ {SDSSequenceOptions|id}) = id

instance Readable SDSSequence
where
	readSDS sds p c iworld = readSDSSequence sds p c JustRead iworld

instance Writeable SDSSequence
where
	writeSDS sds=:(SDSSequence sds1 sds2 opts=:{SDSSequenceOptions|paraml,writel,writer,name}) p c w iworld =
		case readSDS sds1 (paraml p) c iworld of
			(AsyncRead asds, iworld) = (AsyncWrite (SDSSequence asds sds2 opts), iworld)
			(ReadResult r1 ssds, iworld)
				//Write sds1 if necessary
				# (npreds1,iworld) = case writel of
					SDSWrite f = case f p r1 w of
						Error e       = (WriteException e, iworld)
						Ok ?None      = (WriteResult 'Set'.newSet ssds, iworld)
						Ok (?Just w1) = writeSDS ssds (paraml p) c w1 iworld
					SDSWriteConst f = case f p w of
						Error e       = (WriteException e, iworld)
						Ok ?None      = (WriteResult 'Set'.newSet ssds, iworld)
						Ok (?Just w1) = writeSDS ssds (paraml p) c w1 iworld
				| npreds1=:(WriteException _) = (WriteException (fromWriteException npreds1), iworld)
				//Read/write sds2 if necessary
				# (npreds2,iworld) = case writer of
					SDSSequenceWrite paramr f
						# p2 = paramr p r1
						= case readSDS sds2 p2 c iworld of //Also read sds2
						(ReadResult r2 ssds,iworld) = case f p r1 r2 w of
							Error e = (WriteException e, iworld)
							Ok ?None = (WriteResult 'Set'.newSet ssds, iworld)
							Ok (?Just w2) = writeSDS sds2 p2 c w2 iworld
						(ReadException e, iworld) = (WriteException e, iworld)
					SDSSequenceWriteConst f = case f p r1 w of
						Error e = (WriteException e, iworld)
						Ok ?None = (WriteResult 'Set'.newSet sds2, iworld)
						Ok (?Just (p2ForWriting, w2)) = writeSDS sds2 p2ForWriting c w2 iworld
				| npreds2=:(WriteException _) = (WriteException (fromWriteException npreds2), iworld)
				= case (npreds1, npreds2) of
					(WriteResult notify1 ssds1, WriteResult notify2 ssds2) =
						(WriteResult ('Set'.union notify1 notify2) (SDSSequence ssds1 ssds2 opts), iworld)
					(WriteResult notify1 ssds1, AsyncWrite sds2) =
						(AsyncWrite (SDSSequence ssds sds2 opts), queueNotifyEvents (sdsIdentity sds1) notify1 iworld)
			(ReadException e, iworld) = (WriteException e, iworld)

instance Modifiable SDSSequence
where
	modifySDS f sds p context iworld = case readSDS sds p context iworld of
		(ReadResult r ssds, iworld) = case f r of
			Error e = (ModifyException e, iworld)
			Ok (a, ?None) = (ModifyResult 'Set'.newSet r ?None a sds, iworld)
			Ok (a, ?Just w) = case writeSDS sds p context w iworld of
				(WriteResult notify ssds, iworld) = (ModifyResult notify r (?Just w) a sds, iworld)
				(AsyncWrite _, iworld)            = (ModifyException (exception "SDSSequence cannot be modified asynchronously"), iworld)
				(WriteException e, iworld)        = (ModifyException e, iworld)
		(AsyncRead sds, iworld) = (ModifyException (exception "SDSSequence cannot be modified asynchronously in the left SDS."), iworld)
		(ReadException e, iworld) = (ModifyException e, iworld)

instance Registrable SDSSequence
where
	readRegisterSDS sds p c taskId iworld = readSDSSequence sds p c (ReadAndRegister taskId) iworld

readSDSSequence sds=:(SDSSequence sds1 sds2 opts=:{SDSSequenceOptions|paraml,read,name}) p c mbNotify iworld
	:== case readAndMbRegisterSDS sds1 (paraml p) c mbNotify iworld of
		(ReadResult r1 ssds1, iworld) = case read p r1 of
			Left r = (ReadResult r (SDSSequence ssds1 sds2 opts), iworld)
			Right (paramForR, read2) = case readAndMbRegisterSDS sds2 paramForR c mbNotify iworld of
				(ReadResult r2 ssds2, iworld) = (ReadResult (read2 r2) (SDSSequence ssds1 ssds2 opts), iworld)
				(AsyncRead sds2, iworld) = (AsyncRead (SDSSequence ssds1 sds2 opts), iworld)
				(ReadException e, iworld) = (ReadException e, iworld)
		(AsyncRead sds, iworld) = (AsyncRead (SDSSequence sds sds2 opts), iworld)
		(ReadException e, iworld) = (ReadException e, iworld)

// SDSSelect
instance Identifiable SDSSelect
where
	sdsIdentity (SDSSelect _ _ {SDSSelectOptions|id}) = id

instance Readable SDSSelect
where
	readSDS sds p c iworld = readSDSSelect sds p c JustRead iworld

instance Writeable SDSSelect
where
	writeSDS sds=:(SDSSelect sds1 sds2 opts=:{SDSSelectOptions|select,name}) p c w iworld = case select p of
		Left p1 =
			case writeSDS sds1 p1 c w iworld of
				(WriteResult notify ssds, iworld) = (WriteResult notify (SDSSelect ssds sds2 opts), iworld)
				(AsyncWrite ssds,         iworld) = (AsyncWrite (SDSSelect ssds sds2 opts),         iworld)
				(WriteException e,        iworld) = (WriteException e,                              iworld)
		Right p2 =
			case writeSDS sds2 p2 c w iworld of
				(WriteResult notify ssds, iworld) = (WriteResult notify (SDSSelect sds1 ssds opts), iworld)
				(AsyncWrite ssds,         iworld) = (AsyncWrite (SDSSelect sds1 ssds opts),         iworld)
				(WriteException e,        iworld) = (WriteException e,                              iworld)

instance Modifiable SDSSelect
where
	modifySDS f sds=:(SDSSelect sds1 sds2 opts=:{select}) p context iworld = case select p of
		Left p1 = case modifySDS f sds1 p1 context iworld of
			(ModifyResult notify r mw a ssds, iworld) = (ModifyResult notify r mw a (SDSSelect ssds sds2 opts), iworld)
			(AsyncModify sds f, iworld)            = (AsyncModify (SDSSelect sds sds2 opts) f, iworld)
			(ModifyException e, iworld)            = (ModifyException e, iworld)
		Right p2 = case modifySDS f sds2 p2 context iworld of
			(ModifyResult notify r mw a ssds, iworld) = (ModifyResult notify r mw a (SDSSelect sds1 ssds opts), iworld)
			(AsyncModify sds f, iworld)            = (AsyncModify (SDSSelect sds1 sds opts) f, iworld)
			(ModifyException e, iworld)            = (ModifyException e, iworld)

instance Registrable SDSSelect
where
	readRegisterSDS sds p c taskId iworld = readSDSSelect sds p c (ReadAndRegister taskId) iworld

readSDSSelect sds=:(SDSSelect sds1 sds2 opts=:{SDSSelectOptions|select,name}) p c mbNotify iworld
	:== case select p of
		Left p1 = case readAndMbRegisterSDS sds1 p1 c mbNotify iworld of
			(ReadResult r ssds, iworld) = (ReadResult r (SDSSelect ssds sds2 opts), iworld)
			(AsyncRead sds, iworld)     = (AsyncRead (SDSSelect sds sds2 opts), iworld)
			(ReadException e, iworld)   = (ReadException e, iworld)
		Right p2 = case readAndMbRegisterSDS sds2 p2 c mbNotify iworld of
			(ReadResult r ssds, iworld) = (ReadResult r (SDSSelect sds1 ssds opts), iworld)
			(AsyncRead sds, iworld)     = (AsyncRead (SDSSelect sds1 sds opts), iworld)
			(ReadException e, iworld)   = (ReadException e, iworld)

// SDSParallel
instance Identifiable SDSParallel
where
	sdsIdentity sds = case sds of
		SDSParallel           _ _ opts = opts.SDSParallelOptions.id
		SDSParallelWriteLeft  _ _ opts = opts.SDSParallelOptions.id
		SDSParallelWriteRight _ _ opts = opts.SDSParallelOptions.id
		SDSParallelWriteNone  _ _ opts = opts.SDSParallelOptions.id

instance Readable SDSParallel
where
	// NB: we need to pattern match to explain to the compiler that we have a gHash{|*|} instance
	readSDS sds=:(SDSParallel _ _ _)           p c iworld = readSDSParallel sds p c JustRead iworld
	readSDS sds=:(SDSParallelWriteLeft _ _ _)  p c iworld = readSDSParallel sds p c JustRead iworld
	readSDS sds=:(SDSParallelWriteRight _ _ _) p c iworld = readSDSParallel sds p c JustRead iworld
	readSDS sds=:(SDSParallelWriteNone _ _ _)  p c iworld = readSDSParallel sds p c JustRead iworld

instance Writeable SDSParallel
where
	writeSDS sds=:(SDSParallel sds1 sds2 opts=:{SDSParallelOptions|param,writel,writer,name}) p c w iworld
	# (p1,p2) = param p
	//Read/write sds1
	# (npreds1,iworld) = case writel of
		SDSWrite f = case readSDS sds1 p1 c iworld of
			(ReadResult r1 ssds,iworld) = case f p r1 w of
				Error e       = (WriteException e, iworld)
				Ok ?None      = (WriteResult 'Set'.newSet ssds, iworld)
				Ok (?Just w1) = writeSDS ssds p1 c w1 iworld
			(AsyncRead ssds, iworld) = (AsyncWrite ssds, iworld)
			(ReadException e, iworld) = (WriteException e, iworld)
		SDSWriteConst f = case f p w of
			Error e       = (WriteException e,iworld)
			Ok ?None      = (WriteResult 'Set'.newSet sds1,iworld)
			Ok (?Just w1) = writeSDS sds1 p1 c w1 iworld
	| npreds1=:(WriteException _) = (WriteException (fromWriteException npreds1), iworld)
	//Read/write sds2
	# (npreds2,iworld) = case writer of
		SDSWrite f = case readSDS sds2 p2 c iworld of
			(ReadResult r2 ssds,iworld) = case f p r2 w of
				Error e       = (WriteException e, iworld)
				Ok ?None      = (WriteResult 'Set'.newSet ssds, iworld)
				Ok (?Just w2) = writeSDS ssds p2 c w2 iworld
			(AsyncRead ssds, iworld) = (AsyncWrite ssds, iworld)
			(ReadException e, iworld)  = (WriteException e, iworld)
		SDSWriteConst f = case f p w of
			Error e       = (WriteException e,iworld)
			Ok ?None      = (WriteResult 'Set'.newSet sds2, iworld)
			Ok (?Just w2) = writeSDS sds2 p2 c w2 iworld
	| npreds2=:(WriteException _) = (WriteException (fromWriteException npreds2), iworld)
	= case (npreds1, npreds2) of
		(WriteResult n1 ssds1, WriteResult n2 ssds2) = (WriteResult ('Set'.union n1 n2) (SDSParallel ssds1 ssds2 opts), iworld)
		(WriteResult n1 ssds1, AsyncWrite sds2) = (AsyncWrite (SDSParallel ssds1 sds2 opts), queueNotifyEvents (sdsIdentity sds1) n1 iworld)
		(AsyncWrite sds1, WriteResult n2 ssds2) = (AsyncWrite (SDSParallel sds1 ssds2 opts), queueNotifyEvents (sdsIdentity sds2) n2 iworld)
		(AsyncWrite sds1, AsyncWrite sds2) = (AsyncWrite (SDSParallel sds1 sds2 opts), iworld)

	writeSDS sds=:(SDSParallelWriteLeft sds1 sds2 opts=:{SDSParallelOptions|param,writel,name}) p c w iworld
	# p1 = fst (param p)
	//Read/write sds1
	# (npreds1,iworld) = case writel of
		SDSWrite f = case readSDS sds1 p1 c iworld of
			(ReadResult r1 ssds,iworld) = case f p r1 w of
				Error e       = (WriteException e, iworld)
				Ok ?None      = (WriteResult 'Set'.newSet ssds, iworld)
				Ok (?Just w1) = writeSDS ssds p1 c w1 iworld
			(AsyncRead ssds, iworld) = (AsyncWrite ssds, iworld)
			(ReadException e, iworld) = (WriteException e, iworld)
		SDSWriteConst f = case f p w of
			Error e       = (WriteException e,iworld)
			Ok ?None      = (WriteResult 'Set'.newSet sds1,iworld)
			Ok (?Just w1) = writeSDS sds1 p1 c w1 iworld
	= case npreds1 of
		WriteResult n1 ssds1 = (WriteResult n1 (SDSParallelWriteLeft ssds1 sds2 opts), iworld)
		AsyncWrite sds1      = (AsyncWrite (SDSParallelWriteLeft sds1 sds2 opts), iworld)
		WriteException e     = (WriteException e, iworld)

	writeSDS sds=:(SDSParallelWriteRight sds1 sds2 opts=:{SDSParallelOptions|param,writer,name}) p c w iworld
	# p2 = snd (param p)
	//Read/write sds1
	# (npreds2,iworld) = case writer of
		SDSWrite f = case readSDS sds2 p2 c iworld of
			(ReadResult r2 ssds,iworld) = case f p r2 w of
				Error e       = (WriteException e, iworld)
				Ok ?None      = (WriteResult 'Set'.newSet ssds, iworld)
				Ok (?Just w2) = writeSDS ssds p2 c w2 iworld
			(AsyncRead ssds, iworld) = (AsyncWrite ssds, iworld)
			(ReadException e, iworld)  = (WriteException e, iworld)
		SDSWriteConst f = case f p w of
			Error e       = (WriteException e,iworld)
			Ok ?None      = (WriteResult 'Set'.newSet sds2,iworld)
			Ok (?Just w2) = writeSDS sds2 p2 c w2 iworld
	= case npreds2 of
		WriteResult n2 ssds2 = (WriteResult n2 (SDSParallelWriteRight sds1 ssds2 opts), iworld)
		AsyncWrite sds2      = (AsyncWrite (SDSParallelWriteRight sds1 sds2 opts), iworld)
		WriteException e     = (WriteException e, iworld)

	writeSDS sds=:(SDSParallelWriteNone sds1 sds2 opts) p c w iworld =
		(WriteResult 'Set'.newSet sds, iworld)

instance Modifiable SDSParallel
where
	modifySDS f sds p context iworld = case readSDS sds p context iworld of
		(ReadResult r ssds, iworld) = case f r of
			Error e = (ModifyException e, iworld)
			Ok (a, ?None) = (ModifyResult 'Set'.newSet r ?None a ssds, iworld)
			Ok (a, ?Just w) = case writeSDS ssds p context w iworld of
				(AsyncWrite sds, iworld)          = (AsyncModify sds f, iworld)
				(WriteResult notify ssds, iworld) = (ModifyResult notify r (?Just w) a ssds, iworld)
				(WriteException e, iworld)        = (ModifyException e, iworld)
		(AsyncRead sds, iworld)   = (AsyncModify sds f, iworld)
		(ReadException e, iworld) = (ModifyException e, iworld)

instance Registrable SDSParallel
where
	// NB: we need to pattern match to explain to the compiler that we have a gHash{|*|} instance
	readRegisterSDS sds=:(SDSParallel _ _ _)           p c taskId iworld =
		readSDSParallel sds p c (ReadAndRegister taskId) iworld
	readRegisterSDS sds=:(SDSParallelWriteLeft _ _ _)  p c taskId iworld =
		readSDSParallel sds p c (ReadAndRegister taskId) iworld
	readRegisterSDS sds=:(SDSParallelWriteRight _ _ _) p c taskId iworld =
		readSDSParallel sds p c (ReadAndRegister taskId) iworld
	readRegisterSDS sds=:(SDSParallelWriteNone _ _ _)  p c taskId iworld =
		readSDSParallel sds p c (ReadAndRegister taskId) iworld

readSDSParallel :: !(SDSParallel p r w) !p !TaskContext !ReadAndMbRegister !*IWorld
                -> *(!ReadResult p r w, !*IWorld) | TC p & TC r & TC w
readSDSParallel sds=:(SDSParallel sds1 sds2 opts=:{SDSParallelOptions|param,read,name}) p c mbNotify iworld
	# (p1,p2) = param p
	# (res1, iworld) = readAndMbRegisterSDS sds1 p1 c mbNotify iworld
	| res1=:(ReadException _) = (ReadException (fromReadException res1), iworld)
	# (res2, iworld) = readAndMbRegisterSDS sds2 p2 c mbNotify iworld
	| res2=:(ReadException _) = (ReadException (fromReadException res2), iworld)
	= case (res1, res2) of
		(ReadResult r1 ssds1, ReadResult r2 ssds2) = (ReadResult (read (r1, r2)) (SDSParallel ssds1 ssds2 opts), iworld)
		(AsyncRead sds1, ReadResult r2 ssds2)      = (AsyncRead (SDSParallel sds1 ssds2 opts), iworld)
		(ReadResult r1 ssds1, AsyncRead sds2)      = (AsyncRead (SDSParallel ssds1 sds2 opts), iworld)
		(AsyncRead sds1, AsyncRead sds2)           = (AsyncRead (SDSParallel sds1 sds2 opts), iworld)

readSDSParallel sds=:(SDSParallelWriteLeft sds1 sds2 opts=:{SDSParallelOptions|param,read,name}) p c mbNotify iworld
	# (p1,p2) = param p
	# (res1, iworld) = readAndMbRegisterSDS sds1 p1 c mbNotify iworld
	| res1=:(ReadException _) = (ReadException (fromReadException res1), iworld)
	# (res2, iworld) = readAndMbRegisterSDS sds2 p2 c mbNotify iworld
	| res2=:(ReadException _) = (ReadException (fromReadException res2), iworld)
	= case (res1, res2) of
		(ReadResult r1 ssds1, ReadResult r2 ssds2) = (ReadResult (read (r1, r2)) (SDSParallelWriteLeft ssds1 ssds2 opts), iworld)
		(AsyncRead sds1, ReadResult r2 ssds2)      = (AsyncRead (SDSParallelWriteLeft sds1 ssds2 opts), iworld)
		(ReadResult r1 ssds1, AsyncRead sds2)      = (AsyncRead (SDSParallelWriteLeft ssds1 sds2 opts), iworld)
		(AsyncRead sds1, AsyncRead sds2)           = (AsyncRead (SDSParallelWriteLeft sds1 sds2 opts), iworld)

readSDSParallel sds=:(SDSParallelWriteRight sds1 sds2 opts=:{SDSParallelOptions|param,read,name}) p c mbNotify iworld
	# (p1,p2) = param p
	# (res1, iworld) = readAndMbRegisterSDS sds1 p1 c mbNotify iworld
	| res1=:(ReadException _) = (ReadException (fromReadException res1), iworld)
	# (res2, iworld) = readAndMbRegisterSDS sds2 p2 c mbNotify iworld
	| res2=:(ReadException _) = (ReadException (fromReadException res2), iworld)
	= case (res1, res2) of
		(ReadResult r1 ssds1, ReadResult r2 ssds2) = (ReadResult (read (r1, r2)) (SDSParallelWriteRight ssds1 ssds2 opts), iworld)
		(AsyncRead sds1, ReadResult r2 ssds2)      = (AsyncRead (SDSParallelWriteRight sds1 ssds2 opts), iworld)
		(ReadResult r1 ssds1, AsyncRead sds2)      = (AsyncRead (SDSParallelWriteRight ssds1 sds2 opts), iworld)
		(AsyncRead sds1, AsyncRead sds2)           = (AsyncRead (SDSParallelWriteRight sds1 sds2 opts), iworld)

readSDSParallel sds=:(SDSParallelWriteNone sds1 sds2 opts=:{SDSParallelOptions|param,read,name}) p c mbNotify iworld
	# (p1,p2) = param p
	# (res1, iworld) = readAndMbRegisterSDS sds1 p1 c mbNotify iworld
	| res1=:(ReadException _) = (ReadException (fromReadException res1), iworld)
	# (res2, iworld) = readAndMbRegisterSDS sds2 p2 c mbNotify iworld
	| res2=:(ReadException _) = (ReadException (fromReadException res2), iworld)
	= case (res1, res2) of
		(ReadResult r1 ssds1, ReadResult r2 ssds2) = (ReadResult (read (r1, r2)) (SDSParallelWriteNone ssds1 ssds2 opts), iworld)
		(AsyncRead sds1, ReadResult r2 ssds2)      = (AsyncRead (SDSParallelWriteNone sds1 ssds2 opts), iworld)
		(ReadResult r1 ssds1, AsyncRead sds2)      = (AsyncRead (SDSParallelWriteNone ssds1 sds2 opts), iworld)
		(AsyncRead sds1, AsyncRead sds2)           = (AsyncRead (SDSParallelWriteNone sds1 sds2 opts), iworld)

optionsS :: SDSShareOptions -> String
optionsS o = o.SDSShareOptions.domain +++ ":" +++ toString o.SDSShareOptions.port

instance Identifiable SDSBox
where
	sdsIdentity (SDSBox sds) = sdsIdentity sds

instance Readable SDSBox
where
	readSDS (SDSBox sds) p c iworld = readSDS sds p c iworld

instance Writeable SDSBox
where
	writeSDS (SDSBox sds) p ctx value iworld = writeSDS sds p ctx value iworld

instance Modifiable SDSBox
where
	modifySDS f (SDSBox sds) p ctx iworld = modifySDS f sds p ctx iworld

instance Registrable SDSBox
where
	readRegisterSDS (SDSBox sds) p ctx task iworld = readRegisterSDS sds p ctx task iworld

instance Identifiable SDSRemoteSource
where
	sdsIdentity (SDSRemoteSource sds _ options) = createSDSIdentity
		(optionsS options)
		(?Just (sdsIdentity sds))
		?None

instance Readable SDSRemoteSource
where
	readSDS sds p c iworld = readSDSRemoteSource sds p c JustRead iworld

instance Writeable SDSRemoteSource
where
	writeSDS sds p EmptyContext value iworld =
		(WriteException (exception "cannot write remote SDS without task id"), iworld)

	writeSDS (SDSRemoteSource sds (?Just ioHandle) opts) p (TaskContext taskId) value iworld=:{ioStates}
		= case withIOState ioHandle ioStateFun iworld of
			(Ok (Left data), iworld)
				= (AsyncWrite (SDSRemoteSource sds (?Just ioHandle) opts), iworld)
			(Ok (Right r), iworld) = case r of
				Ok () = (WriteResult 'Set'.newSet (SDSRemoteSource sds ?None opts), iworld)
				Error e = (WriteException e, iworld)
			(Error error, iworld)
				# errorString = "SDSRemoteSourceQueued write get value\nRemote to " +++ optionsS opts +++ ": " +++ error
				= (WriteException (exception errorString), iworld)
	where
		ioStateFun :: !IOState -> MaybeErrorString SDSRequestWriteType
		ioStateFun s = getStateFromIoStatus s.ioStatus

	writeSDS sds=:(SDSRemoteSource sds1 ?None opts) p (TaskContext taskId) value iworld
		= case queueWrite value sds p taskId iworld of
			(Error (_, error), iworld)
				# errorString = "SDSRemoteSource write queue<br>Remote to " +++ optionsS opts +++ ": " +++ error
				= (WriteException (exception errorString), iworld)
			(Ok ioHandle, iworld) = (AsyncWrite (SDSRemoteSource sds (?Just ioHandle) opts), iworld)

instance Modifiable SDSRemoteSource
where
	modifySDS _ _ _ EmptyContext iworld =
		(ModifyException (exception "SDSRemoteSource modify: Cannot modify with empty context"), iworld)

	modifySDS f sds=:(SDSRemoteSource subsds (?Just ioHandle) opts) p cxt iworld
		= case withIOState ioHandle ioStateFun iworld of
			(Ok (Left data), iworld)
				= (AsyncModify sds f, iworld)
			(Ok (Right r), iworld) = case r of
				Ok (r, mw, a) = (ModifyResult 'Set'.newSet r mw a (SDSRemoteSource subsds ?None opts), iworld)
				Error e = (ModifyException e, iworld)
			(Error error, iworld)
				# errorString = "SDSRemoteSourceQueued modify get value\nRemote to " +++ optionsS opts +++ ": " +++ error
				= (ModifyException (exception errorString), iworld)
	where
		// this is a helper function to fix the type
		ioStateFun :: !IOState -> MaybeErrorString (SDSRequestModifyType r w a) | TC r & TC w & TC a
		ioStateFun s = getStateFromIoStatus s.ioStatus

	modifySDS f sds=:(SDSRemoteSource subsds ?None opts) p (TaskContext taskId) iworld =
		case queueModify f sds p taskId iworld of
			(Error (_, error), iworld)
				# errorString = "SDSRemoteSource modify queue<br>Remote to " +++ optionsS opts +++ ": " +++ error
				= (ModifyException (exception errorString), iworld)
			(Ok ioHandle, iworld) = (AsyncModify (SDSRemoteSource sds (?Just ioHandle) opts) f, iworld)

instance Registrable SDSRemoteSource
where
	readRegisterSDS sds p context taskId iworld = readSDSRemoteSource sds p context (ReadAndRegister taskId ) iworld

readSDSRemoteSource :: !(SDSRemoteSource p r w) !p !TaskContext !ReadAndMbRegister !*IWorld
                     -> *(!ReadResult p r w, !*IWorld) | TC p & TC r & TC w
readSDSRemoteSource _ _ EmptyContext _ iworld =
	(ReadException (exception "Cannot read remote SDS without task id"), iworld)

readSDSRemoteSource (SDSRemoteSource sds (?Just ioHandle) opts) p context register iworld
	= case withIOState ioHandle ioStateFun iworld of
		// reading still in progress
		(Ok (Left data), iworld)
			= (AsyncRead (SDSRemoteSource sds (?Just ioHandle) opts), iworld)
		(Ok (Right r), iworld) = case r of
			Ok r = (ReadResult r (SDSValue False r (SDSRemoteSource sds ?None opts)), iworld)
			Error e = (ReadException e, iworld)
		(Error e, iworld)
			# errorString = "SDSRemoteSourceQueued read get value\nRemote to " +++ optionsS opts +++ ": " +++ e
			= (ReadException (exception errorString), iworld)
where
	// this is a helper function to fix the type
	ioStateFun :: !IOState -> MaybeErrorString (SDSRequestReadType r) | TC r
	ioStateFun s = getStateFromIoStatus s.ioStatus
readSDSRemoteSource sds=:(SDSRemoteSource _ ?None opts) p context register iworld
	# iworld = mbRegister p sds register context iworld
	# taskId = case context of
		TaskContext taskId = taskId
		RemoteTaskContext reqTaskId currTaskId _ _ _ = currTaskId
	= case queueRead sds p taskId (register=:ReadAndRegister _) (sdsIdentity sds) iworld of
		(Error (_, error), iworld)
			# errorString = "SDSRemoteSource read queue\nRemote to " +++ optionsS opts +++ ": " +++ error
			= (ReadException (exception errorString), iworld)
		(Ok ioHandle, iworld)
			= (AsyncRead (SDSRemoteSource sds (?Just ioHandle) opts), iworld)

// Remote services
instance Identifiable SDSRemoteService
where
	sdsIdentity (SDSRemoteService mbConnId opts) = createSDSIdentity (toString opts) ?None ?None

instance Readable SDSRemoteService
where
	readSDS sds p c iworld = readSDSRemoteService sds p c JustRead iworld

instance Writeable SDSRemoteService
where
	writeSDS :: !(SDSRemoteService p r w) !p !TaskContext !w !*IWorld -> *(!WriteResult p r w, !*IWorld) | TC p & TC r & TC w
	writeSDS sds p EmptyContext value iworld =
		(WriteException (exception "cannot write remote service without task id"), iworld)

	writeSDS sds=:(SDSRemoteService (?Just ioHandle) opts=:(HTTPShareOptions _)) p cxt value iworld
		= case withIOState ioHandle (ioStateFun sds) iworld of
			(Ok (Left _), iworld) = (AsyncWrite sds, iworld)
			(Ok (Right pred), iworld)
				# (match, iworld) = checkRequests (sdsIdentity sds) pred iworld
				= (WriteResult match (SDSRemoteService ?None opts), iworld)
			(Error error, iworld)
				# errorString = "Remote service write error\nService " +++ toString opts +++ ": " +++ error
				= (WriteException $ exception errorString, iworld)
	where
		// this is a helper function to fix the type
		ioStateFun :: (sds p r w) !IOState -> MaybeErrorString (SDSServiceRequestHTTPWriteType p) | TC p
		ioStateFun _ s = getStateFromIoStatus s.ioStatus
	writeSDS sds=:(SDSRemoteService (?Just ioHandle) opts=:(TCPShareOptions _)) p cxt value iworld
		= case withIOState ioHandle (ioStateFun sds) iworld of
			(Ok (Left _), iworld) = (AsyncWrite sds, iworld)
			(Ok (Right pred), iworld)
				# (match, iworld) = checkRequests (sdsIdentity sds) pred iworld
				= (WriteResult match (SDSRemoteService ?None opts), iworld)
			(Error error, iworld)
				# errorString = "Remote service write error\nService " +++ toString opts +++ ": " +++ error
				= (WriteException $ exception errorString, iworld)
	where
		// this is a helper function to fix the type
		ioStateFun :: (sds p r w) !IOState -> MaybeErrorString (SDSServiceRequestTCPWriteType p) | TC p
		ioStateFun _ s = getStateFromIoStatus s.ioStatus

	writeSDS sds=:(SDSRemoteService ?None opts) p (TaskContext taskId) value iworld =
		case queueServiceWriteRequest sds p value taskId iworld of
			(Ok ?None, iworld) = (WriteResult 'Set'.newSet sds, iworld)
			(Ok (?Just ioHandle), iworld) = (AsyncWrite $ SDSRemoteService (?Just ioHandle) opts, iworld)
			(Error (_, error), iworld)
				# errorString = "Remote service write error\nService " +++ toString opts +++ ": " +++ error
				= (WriteException $ exception errorString, iworld)

instance Modifiable SDSRemoteService
where
	modifySDS _ _ _ _  iworld = (ModifyException (exception "modifying remote services not possible"), iworld)

/**
 * Registering a remote service consists of keeping the connection open after a response is
 * received, so that the server may send other messages. Only applicable for TCP connections.
 */
instance Registrable SDSRemoteService
where
	readRegisterSDS (SDSRemoteService _ (HTTPShareOptions _)) _ _ _ iworld =
		(ReadException (exception "registering HTTP services not possible"), iworld)
	readRegisterSDS sds p context taskId iworld = readSDSRemoteService sds p context (ReadAndRegister taskId) iworld

readSDSRemoteService :: !(SDSRemoteService p r w) !p !TaskContext !ReadAndMbRegister !*IWorld
                     -> *(!ReadResult p r w, !*IWorld) | TC p & TC r & TC w
readSDSRemoteService _ _ EmptyContext _ iworld =
	(ReadException (exception "Cannot read remote service without task id"), iworld)
readSDSRemoteService sds=:(SDSRemoteService (?Just ioHandle) opts=:(HTTPShareOptions _)) p cxt _ iworld
	= case withIOState ioHandle ioStateFun iworld of
		(Ok (Left _), iworld) = (AsyncRead sds, iworld)
		(Ok (Right r), iworld) = (ReadResult r (SDSValue False r (SDSRemoteService ?None opts)), iworld)
		(Error error, iworld)
			# errorString = "Remote service queued error\nService " +++ toString opts +++ ": " +++ error
			= (ReadException (exception errorString), iworld)
where
	// this is a helper function to fix the type
	ioStateFun :: !IOState -> MaybeErrorString (SDSServiceRequestHTTPReadType r) | TC r
	ioStateFun s = getStateFromIoStatus s.ioStatus
readSDSRemoteService sds=:(SDSRemoteService (?Just ioHandle) opts=:(TCPShareOptions _)) p cxt _ iworld
	= case withIOState ioHandle ioStateFun iworld of
		(Ok (?None, _), iworld) = (AsyncRead sds, iworld)
		(Ok (?Just r, _), iworld) = (ReadResult r (SDSValue False r (SDSRemoteService ?None opts)), iworld)
		(Error error, iworld)
			# errorString = "Remote service queued error\nService " +++ toString opts +++ ": " +++ error
			= (ReadException (exception errorString), iworld)
where
	// this is a helper function to fix the type
	ioStateFun :: !IOState -> MaybeErrorString (SDSServiceRequestTCPReadType r) | TC r
	ioStateFun s = getStateFromIoStatus s.ioStatus
readSDSRemoteService sds=:(SDSRemoteService ?None opts) p (TaskContext taskId) mbRegister iworld
	= case queueServiceRequest sds p taskId (mbRegister=:ReadAndRegister _) iworld of
		(Ok ioHandle, iworld)
			= (AsyncRead (SDSRemoteService (?Just ioHandle) opts), iworld)
		(Error (_,error), iworld)
			# errorString = "Remote service error\nService  " +++ toString opts +++ ": " +++ error
			= (ReadException (exception errorString), iworld)

instance Identifiable SDSDebug
where
	sdsIdentity (SDSDebug name sds) = sdsIdentity sds

instance Readable SDSDebug
where
	readSDS sds p c iworld = readSDSDebug sds p c JustRead iworld

instance Writeable SDSDebug
where
	writeSDS (SDSDebug name sds) p context w iworld=:{sdsNotifyRequests}
		# iworld = showWhenVerbose ['Text'.concat ["Writing to share ",name,"(identity=",toString (sdsIdentity sds),")"]] iworld
		# iworld =
			showWhenVerbose
				[ maybe
					""
					('Text'.join "\n" o map toSingleLineText o 'Set'.toList o tasksWithRequests)
					('DM'.get (sdsIdentity sds).id_hash sdsNotifyRequests)
				]
				iworld
		= db (writeSDS sds p context w iworld)
		where
			db (WriteResult notify sds, iworld) =
				( WriteResult notify (SDSDebug name sds)
				, showWhenVerbose ["WriteResult from share " + name + " notifying: " +++ 'Text'.join " " (map notifyToString ('Set'.toList notify))] iworld
				)
			db (AsyncWrite sds, iworld) =
				( AsyncWrite (SDSDebug name sds)
				, showWhenVerbose ["AsyncWrite from share " +++ name] iworld
				)
			db (WriteException e, iworld) =
				(WriteException e, showWhenVerbose [snd e] iworld)

instance Registrable SDSDebug
where
	readRegisterSDS (SDSDebug name sds) p context taskId iworld
		# iworld = showWhenVerbose [concat4 "Registering to share " name ": " (toString taskId)] iworld
		= readAndMbRegisterSDS sds p context (ReadAndRegister taskId) iworld

instance Modifiable SDSDebug
where
	modifySDS f (SDSDebug name sds) p context iworld=:{sdsNotifyRequests}
		# iworld = showWhenVerbose ['Text'.concat ["Modifying share ",name,"(identity=",toString (sdsIdentity sds),")"]] iworld
		# (regs, iworld) = listAllSDSRegistrations iworld
		# iworld = showWhenVerbose [formatRequests regs] iworld
		= db (modifySDS f sds p context iworld)
	where
		db (ModifyResult notify r mw a sds, iworld) =
			( ModifyResult notify r mw a (SDSDebug name sds)
			, showWhenVerbose ["ModifyResult from share " + name + " notifying: " + 'Text'.join ", " (map notifyToString ('Set'.toList notify))] iworld
			)
		db (AsyncModify sds f, iworld) =
			( AsyncModify (SDSDebug name sds) f
			, showWhenVerbose ["AsyncModify from share " + name] iworld
			)
		db (ModifyException e, iworld) =
			(ModifyException e, showWhenVerbose [snd e] iworld)

readSDSDebug :: !(SDSDebug p r w) !p !TaskContext !ReadAndMbRegister !*IWorld
             -> *(!ReadResult p r w, !*IWorld) | TC p & TC r & TC w
readSDSDebug (SDSDebug name sds) p context mbRegister iworld
	# iworld = showWhenVerbose ["Reading from share " +++ name] iworld
	= db (readAndMbRegisterSDS sds p context mbRegister iworld)
where
	db (ReadResult v sds, iworld) =
		( ReadResult v (SDSDebug name sds)
		, showWhenVerbose ["ReadResult from share " +++ name] iworld
		)
	db (AsyncRead sds, iworld) =
		( AsyncRead (SDSDebug name sds)
		, showWhenVerbose ["AsyncRead " +++ name] iworld
		)
	db (ReadException e, iworld) =
		(ReadException e, showWhenVerbose [snd e] iworld)

// toString instances for SDSDebug
notifyToString :: !PossiblyRemoteTaskId -> String
notifyToString {taskId, remoteNotifyOptions= ?None} = "local " +++ toString taskId
notifyToString {taskId, remoteNotifyOptions=(?Just remote)} = "remote " +++ toString taskId +++ " " +++ toString remote

instance toString RemoteNotifyOptions
where
	toString {hostToNotify, portToNotify, remoteSdsId} =
		'Text'.concat [hostToNotify,":",toString portToNotify,"@",toString remoteSdsId]

instance toString (?RemoteNotifyOptions)
where
	toString ?None = ""
	toString (?Just options) = toString options

:: ReadAndMbRegister
	= JustRead
	| ReadAndRegister !TaskId

readAndMbRegisterSDS sds p c mbRegister iworld :== case mbRegister of
	ReadAndRegister regTaskId = readRegisterSDS sds p c regTaskId iworld
	JustRead = readSDS sds p c iworld

instance Identifiable SDSNoNotify
where
	sdsIdentity (SDSNoNotify sds) = createSDSIdentity "?" (?Just (sdsIdentity sds)) ?None

instance Readable SDSNoNotify
where
	readSDS (SDSNoNotify sds) p c iworld = case readSDS sds p c iworld of
		(ReadResult r sds, iworld) = (ReadResult r sds, iworld)
		(AsyncRead sds, iworld) = (AsyncRead sds, iworld)
		(ReadException e, iworld) = (ReadException e, iworld)

instance Writeable SDSNoNotify
where
	writeSDS (SDSNoNotify sds) p c w iworld = case writeSDS sds p c w iworld of
		(WriteResult set sds, iworld) = (WriteResult set (SDSNoNotify sds), iworld)
		(AsyncWrite sds, iworld) = (AsyncWrite (SDSNoNotify sds), iworld)
		(WriteException e, iworld) = (WriteException e, iworld)

instance Registrable SDSNoNotify
where
	readRegisterSDS (SDSNoNotify sds) p c _ iworld = case readSDS sds p c iworld of
		(ReadResult r sds, iworld) = (ReadResult r sds, iworld)
		(AsyncRead sds, iworld) = (AsyncRead sds, iworld)
		(ReadException e, iworld) = (ReadException e, iworld)

instance Modifiable SDSNoNotify
where
	modifySDS mf (SDSNoNotify sds) p c iworld = case modifySDS mf sds p c iworld of
		(ModifyResult set r mw a sds, iworld) = (ModifyResult set r mw a (SDSNoNotify sds), iworld)
		(AsyncModify sds mf, iworld) = (AsyncModify (SDSNoNotify sds) mf, iworld)
		(ModifyException e, iworld) = (ModifyException e, iworld)
