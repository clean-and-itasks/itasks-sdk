definition module iTasks.Internal.Util

from Data.Error import :: MaybeErrorString, :: MaybeError
from Data.Map import :: Map
from Data.Queue import :: Queue
from StdClass import class Eq
from StdOverloaded import class <
from System.FilePath import :: FilePath
from System.OSError import :: OSError, :: OSErrorCode, :: OSErrorMessage, :: MaybeOSError
from System.Time import :: Tm

from iTasks.Extensions.DateTime import :: DateTime
from iTasks.Internal.IWorld import :: IWorld, :: IOState, :: TaskEvalState
from iTasks.Internal.TaskEval import :: TaskTime, :: TaskEvalInfo, :: TaskEvalOpts
from iTasks.UI.Definition import :: UI, :: UIChange
from iTasks.WF.Definition import :: TaskId, :: Event, :: TaskResult, :: TaskException, :: TaskNo, :: InstanceNo

showOut :: ![String] !*World -> *World
showErr :: ![String] !*World -> *World

iShowOut :: ![String] !*IWorld -> *IWorld
iShowErr :: ![String] !*IWorld -> *IWorld

showWhenVerbose :: ![String] !*IWorld -> *IWorld

tmToDateTime :: !Tm -> DateTime

//Path conversion
toCanonicalPath	:: !FilePath !*World -> (!FilePath,!*World)

//Bind a possibly failing iworld function to another
(>-=) infixl 1 :: (*env -> *(MaybeError e a, *env)) (a -> *(*env -> (MaybeError e b, *env))) *env -> (MaybeError e b, *env)

//Lift a world function to an iworld function
liftIWorld :: .(*World -> *(.a, *World)) *IWorld -> *(.a, *IWorld)

//Apply an IWorld transformer and transform the result to a taskresult
apIWTransformer :: *env (*env -> *(MaybeError TaskException (TaskResult a), *env)) -> *(TaskResult a, *env)

generateRandomString :: !Int !*IWorld -> (!String, !*IWorld)

isRefreshForTask :: !Event !TaskId -> Bool

isDestroyOrInterrupt :: !Event -> Bool

//* Make a ...TaskEvalInfo... record.
mkTaskEvalInfo :: TaskEvalInfo

/**
 * Make a ...TaskEvalOpts... record.
 *
 * @param The id of the task.
 * @param The last evaluation.
 * @param If we are evaluating a task in response to an event from a session.
 * @param The current way the evaluated task instance is attached to other instances.
 * @result Task evaluation options.
 */
mkTaskEvalOpts :: !TaskId !TaskTime !(?InstanceNo) ![TaskId] -> TaskEvalOpts

/**
 * Make a `TaskEvalState` record.
 *
 * @param nextTaskNo
 * @result task eval state
 */
mkTaskEvalState :: !TaskNo -> .TaskEvalState

mkUIIfReset :: !Event !UI -> UIChange

mkEmptyUI :: !Event -> UIChange

//* Determines the app name for a given executable path.
appNameFor :: !FilePath -> String

//* Safe dequeue to use with `amend.
safeDequeue :: !(Queue a) -> (!?a, !?(Queue a))

//* Debug function to trace the IOstates of the IWorld
traceIOStates :: !String !*IWorld -> IWorld

instance toString Dynamic
instance toString IOState

//* An unsafe variant of base64URLDecode
base64URLDecodeUnsafe :: !.String -> .String
//* An unsafe variant of base64Decode
base64DecodeUnsafe :: !.String -> .String
