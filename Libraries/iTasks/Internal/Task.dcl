definition module iTasks.Internal.Task
/**
* This module provides types for the definition of tasks.
*/

import iTasks.WF.Definition

import  iTasks.SDS.Definition
from iTasks.UI.Definition import :: UIChange
from Data.Map			import :: Map
from Data.CircularStack import :: CircularStack
from Data.Error         import :: MaybeError, :: MaybeErrorString
from System.OSError		import :: MaybeOSError, :: OSError, :: OSErrorCode, :: OSErrorMessage

derive JSONEncode		Task
derive JSONDecode		Task
derive gDefault			Task
derive gText	        Task
derive gEditor			Task
derive gEq				Task

/**
* Create a task that finishes instantly
*/
mkInstantTask :: !(TaskEvalOpts *IWorld -> (MaybeError TaskException a,*IWorld)) -> Task a

/**
 * Apply a function on the task continuation of the task result
 * @type ((Task a) -> (Event TaskEvalOpts !*IWorld -> *(TaskResult a, !*IWorld))) !(TaskResult a) -> TaskResult a
 */
wrapTaskContinuation tf val :== case val of
	(ValueResult val tei ui newtask) = ValueResult val tei ui (Task (tf newtask))
	a = a

nopTask :: Task a | iTask a
