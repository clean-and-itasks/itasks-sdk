definition module iTasks.Internal.TaskServer

from iTasks.Internal.SDS import :: SDSSource

from System.FilePath          import :: FilePath

from Data.Error               import :: MaybeError, :: MaybeErrorString
from Data.Map                 import :: Map
from iTasks.WF.Definition     import :: TaskId
from iTasks.Internal.IWorld   import :: IWorld, :: IOHandle
from iTasks.Internal.Task     import :: TaskException
from iTasks.Internal.AsyncIO  import :: ConnectionContext
from iTasks.Engine            import :: StartupTask
from iTasks.SDS.Definition    import :: Shared, class Readable, class Identifiable
from AsyncIO                  import :: Port, :: ConnectionId

/**
 * Makes sure events for the tasks which should be evaluated on startup
 * are queued.
 * Sets up the HTTP server and other services which require listening for TCP connections.
 * Starts the iTasks event loop.
 * Performing the above steps results in serving the iTasks application.
 *
 * @param List of tasks to be evaluated on startup
 * @param List of TCP listeners to setup alongside their port
 * @param Timeout function which determines the maximum amount of time the event loop may block for.
 * @param IWorld
 * @result IWorld
 */
serve :: ![StartupTask] ![(Port,ConnectionContext)] !(*IWorld -> (?Int,*IWorld)) !*IWorld -> *IWorld

//Core task server loop
loop :: !(*IWorld -> (?Int,*IWorld)) !*IWorld -> *IWorld

/**
 * Creates a TCP listener which listens for connections.
 *
 * @param The TaskId of the task which uses this function to establish a connection (or ?None if it is system).
 * @param The port to listen on
 * @param Whether a connectionstate of a client should be removed when the client disconnects.
 * @param The ConnectionContext which defines how the TCP listener should communicate with clients that connect.
 * @param IWorld
 * @result Failure: TaskException
 * @result Success: the connection id of the listener.
 * @result IWorld
 */
addListener :: !(?TaskId) !Port !Bool !ConnectionContext !*IWorld -> (!MaybeError TaskException IOHandle,!*IWorld)

/**
 * Close a listener. This closes the clients that are still connected and removes the IOState.
 *
 * @param The IOHandle obtained from addListener
 */
closeListener :: !IOHandle !*IWorld -> (!MaybeError TaskException (), !*IWorld)

/**
 * Establishes a TCP connection to a TCP listener.
 *
 * @param The TaskId of the task which uses this function to establish a connection (or ?None if it is system).
 * @param The hostname of the TCP listener to which a connection should be established.
 * @param The port to use when establishing a connection
 * @param The ConnectionContext which defines how the TCP listener should be communicated with.
 * @param IWorld
 * @result Failure: A task exception.
 * @result Success: A connectionstate
 * @result IWorld
 */
addConnection :: !(?TaskId) !String !Port !ConnectionContext !*IWorld -> (!MaybeError TaskException IOHandle, !*IWorld)

/**
 * Close a connection (if it has been set up) and delete the IO state.
 */
closeConnection :: !IOHandle !*IWorld -> *IWorld

//Ticks every time the server loops once
tick :: SDSSource () () ()
