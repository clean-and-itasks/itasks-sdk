implementation module iTasks.Internal.TaskServer

import qualified AsyncIO
from AsyncIO import instance < ConnectionId
import Data.Bifunctor
import Data.Error
import Data.Func
import Data.Functor
from Data.Foldable import class Foldable
import Data.GenHash
import qualified Data.Map
import qualified Data.Set
from Data.Set import instance Foldable Set
import Data.Set.GenJSON
import Data.Tuple
import StdEnv
import System.CommandLine
import System.OS
import System.Signal
import System.SysCall
import System.Time
import Text

import iTasks.Engine
import iTasks.Internal.AsyncIO
import iTasks.Internal.IWorld
import iTasks.Internal.SDS
import iTasks.Internal.TaskEval
import iTasks.Internal.TaskIO
import iTasks.Internal.TaskState
import iTasks.Internal.Util
import iTasks.SDS.Combinators.Common
import iTasks.SDS.Definition
import iTasks.SDS.Sources.System
import iTasks.WF.Definition
import iTasks.WF.Derives

serve :: ![StartupTask] ![(Port,ConnectionContext)] !(*IWorld -> (?Int,*IWorld)) !*IWorld -> *IWorld
serve its cts determineTimeout iworld
	= loop determineTimeout (init its cts iworld)

init :: ![StartupTask] ![(Port,ConnectionContext)] !*IWorld -> *IWorld
init initialTasks connectionTasks iworld
	# iworld = installSignalHandlers iworld
	// Check if the initial tasks have been added already
	# iworld = createInitialInstances initialTasks iworld
	// All persistent task instances should receive a reset event to continue their work
	# iworld=:{IWorld|world} = queueAll iworld
	# iworld = connectAll connectionTasks iworld
	= iworld
where
	createInitialInstances :: [StartupTask] !*IWorld -> *IWorld
	createInitialInstances [] iworld = iworld
	createInitialInstances [{StartupTask|task=TaskWrapper task,attributes}:ts] iworld
		= case createStartupTaskInstance task attributes iworld of
			(Ok _,iworld) = createInitialInstances ts iworld
			(Error (_,e),iworld) = abort e

	queueAll :: !*IWorld -> *IWorld
	queueAll iworld
		# param = (TaskId 0 0, TaskId 0 0, fullTaskListFilter, fullExtendedTaskListFilter)
		# (mbIndex,iworld) = read (sdsFocus param taskListMetaData) EmptyContext iworld
		= case mbIndex of
			Ok (ReadingDone (_,index)) =
				// Queue a ResetEvent for every task instance.
				foldl (\w {TaskMeta|taskId=(TaskId instanceNo _)}-> queueEvent instanceNo ResetEvent w) iworld index
			_           = iworld

	connectAll :: ![(Port,ConnectionContext)] !*IWorld -> *IWorld
	connectAll portCts iworld = foldr connect iworld portCts

	connect :: (!Port, !ConnectionContext) !*IWorld -> *IWorld
	connect (port, cxt) iworld = case addListener ?None port True cxt iworld of
		(Ok _, iworld) = iworld
		(Error e, iworld) = abort (concat4 "Error creating listener on port " (toString port) ", error message: " (snd e))

	installSignalHandlers iworld=:{signalHandlers,world}
		= case signalInstall SIGTERM world of
			(Error (_, e), world) = abort $ concat ["Couldn't install SIGTERM: ", e, "\n"]
			(Ok h1, world) = case signalInstall SIGINT world of
				(Error (_, e), world) = abort $ concat ["Couldn't install SIGINT: ", e, "\n"]
				// Windows doesn't use SIGPIPE
				(Ok h2, world) = case IF_WINDOWS (Ok (), world) (signalIgnore SIGPIPE world) of
					(Error (_, e), world) = abort $ concat ["Couldn't ignore SIGPIPE: ", e, "\n"]
					(Ok _, world) = {iworld & signalHandlers=[h1,h2:signalHandlers], world=world}

loop :: !(*IWorld -> (?Int,*IWorld)) !*IWorld -> *IWorld
loop determineTimeout iworld=:{signalHandlers}
	// Process the onShareChange and onDestroy callbacks for TCP clients.
	# iworld = processOnShareChangeOnDestroy iworld
	// Determine timeout
	# (mbTimeout,iworld=:{IWorld|world}) = determineTimeout iworld
	/* There can be one I/O event per file descriptor that is monitored per event loop iteration.
	 * It is expected that the applications using iTasks for which performance matters will have around 50 users.
	 * Therefore, the maximum number of I/O events to process per loop iteration is set to 50.
	 */
	# iworld = 'AsyncIO'.tick
		mbTimeout 50
		(\iworld
			# (mbErr, iworld) = updateClock iworld
			= if (isError mbErr) (abort "Error updating clock.\n") iworld)
		iworld
	# (mbErr, iworld) = updateClock iworld
	# (hadsig, iworld) = hadSignal iworld
	| hadsig = halt 0 iworld
	# iworld = {iworld & readSdsSourceVals = 'Data.Map'.newMap} // Clear read SDS source values.
	// Write ticker
	# (merr, iworld=:{options}) = write () tick EmptyContext iworld
	| isError merr = abort "Error writing ticker\n"
	//Process the events it created
	# (merr, iworld) = processEvents options.maxEvents {iworld & options = options}
	| isError merr = abort "Error processing events\n"
	# (shutdown, iworld) = iworld!shutdown
	//Everything needs to be re-evaluated
	= case shutdown of
		(?Just exitCode)
			= halt exitCode iworld
		_
			// Empty the writeQueues
			# iworld = emptyAsyncIOWriteQueues iworld
			// Recurse
			= loop determineTimeout iworld
where
	pollSignalHandlers [] world = (False, [], world)
	pollSignalHandlers [h:hs] world = case signalPoll h world of
		(Error e, h, world) = abort "Error polling signal handler" //Shouldn't occur
		(Ok s, h, world)
			# (stop, hs, world) = pollSignalHandlers hs world
			= (stop || s, [h:hs], world)

	hadSignal iworld=:{signalHandlers,world}
		# (hadsig, signalHandlers, world) = pollSignalHandlers signalHandlers world
		# iworld = {iworld & signalHandlers=signalHandlers, world=world}
		= (hadsig, iworld)

addListener :: !(?TaskId)
               !Port
               !Bool
               !ConnectionContext
               !*IWorld
            -> (!MaybeError TaskException IOHandle,!*IWorld)
addListener taskId port removeOnClose {handlers, sdsModifiedByHandlers} iworld=:{nextIOHandle}
	# iworld & nextIOHandle = nextIOHandle + 1
	# (mbId, iworld) = 'AsyncIO'.addListener (translatedHandlers removeOnClose nextIOHandle) port iworld
	| isError mbId = (liftError $ mapError osErrorToTaskException mbId, iworld)
	# iworld & ioStates = 'Data.Map'.put nextIOHandle
		{ IOState
		| onShareChangeHandler=handlers.ConnectionHandlersIWorld.onShareChange
		, onDestroyHandler=handlers.ConnectionHandlersIWorld.onDestroy
		, sdsModifiedByHandlers=sdsModifiedByHandlers
		, taskId=taskId
		, ioStatus=IOListener (fromOk mbId)
		, listenerMap='Data.Set'.newSet
		, clientInstances='Data.Set'.newSet
		, listener=True
		} iworld.ioStates
	// We cannot store the ioHandle in the handlemap because there are no connections yet
	= (Ok nextIOHandle, iworld)
where
	translatedHandlers :: !Bool !IOHandle -> 'AsyncIO'.ConnectionHandlers IWorld
	translatedHandlers removeOnClose ioHandle = tslConnectionHandlersIWorld (HSListener removeOnClose ioHandle) handlers taskId sdsModifiedByHandlers

closeListener :: !IOHandle !*IWorld -> (!MaybeError TaskException (), !*IWorld)
closeListener ioHandle iworld = case getIOState ioHandle iworld of
	(Ok ioState, iworld) = case ioState.ioStatus of
		IOListener cId
			# iworld = foldr closeConnection iworld ('Data.Set'.toList ioState.listenerMap)
			# iworld = 'AsyncIO'.closeListener cId iworld
			= case delIOState ioHandle iworld of
				(Ok _, iworld) = (Ok (), iworld)
				(Error e, iworld) = (Error (exception e), iworld)
		IOException e = (Error (exception e), iworld)
	(Error e, iworld) = (Error (exception e), iworld)

addConnection :: !(?TaskId)
                 !String
                 !Port
                 !ConnectionContext
                 !*IWorld
              -> (!MaybeError TaskException IOHandle,!*IWorld)
addConnection taskId host port {handlers, sdsModifiedByHandlers} iworld=:{nextIOHandle}
	# iworld & nextIOHandle = nextIOHandle + 1
	# (mbId, iworld) = 'AsyncIO'.addConnection (translatedHandlers nextIOHandle) host port iworld
	| isError mbId = (liftError $ mapError osErrorToTaskException mbId, iworld)
	# iworld & ioStates = 'Data.Map'.put nextIOHandle
		{ IOState
		| onShareChangeHandler=handlers.ConnectionHandlersIWorld.onShareChange
		, onDestroyHandler=handlers.ConnectionHandlersIWorld.onDestroy
		, sdsModifiedByHandlers=sdsModifiedByHandlers
		, taskId=taskId
		, ioStatus=IOConnecting (fromOk mbId)
		, listenerMap='Data.Set'.newSet
		, clientInstances='Data.Set'.newSet
		, listener=False
		} iworld.ioStates
	// Store the ioHandle in the IoHandleMap
	# iworld & ioHandleMap = 'Data.Map'.put (fromOk mbId) nextIOHandle iworld.ioHandleMap
	= (Ok nextIOHandle, iworld)
where
	translatedHandlers :: !IOHandle -> 'AsyncIO'.ConnectionHandlers IWorld
	translatedHandlers ioHandle = tslConnectionHandlersIWorld (HSClient ioHandle) handlers taskId sdsModifiedByHandlers

closeConnection :: !IOHandle !*IWorld -> *IWorld
closeConnection ioHandle iworld=:{ioStates} = case 'Data.Map'.get ioHandle ioStates of
	?None = iworld
	?Just ioState
		# (_, iworld) = delIOState ioHandle iworld
		= case ioState.ioStatus of
			IOConnecting cId
				# iworld = 'AsyncIO'.closeConnection cId iworld
				# iworld & ioHandleMap = 'Data.Map'.del cId iworld.ioHandleMap
				= iworld
			IOActive _ cId
				# iworld = 'AsyncIO'.closeConnection cId iworld
				# iworld & ioHandleMap = 'Data.Map'.del cId iworld.ioHandleMap
				= iworld
			IODestroyed _ cId
				# iworld = 'AsyncIO'.closeConnection cId iworld
				# iworld & ioHandleMap = 'Data.Map'.del cId iworld.ioHandleMap
				= iworld
			_ = iworld

halt :: !Int !*IWorld -> *IWorld
halt exitCode iworld=:{world}
	# iworld = {iworld & world = setReturnCode exitCode world}
	# (merr, iworld) = read allTaskInstances EmptyContext iworld
	| isError merr = iShowErr [snd (fromError merr)] ('AsyncIO'.closeAllConnections iworld)
	# iworld = foldr destroy iworld [i.TaskInstance.instanceNo \\ i <- directResult (fromOk merr)]
	= 'AsyncIO'.closeAllConnections iworld
where
	destroy :: !InstanceNo !*IWorld -> *IWorld
	destroy ino iworld=:{options={persistTasks}}
		= case evalTaskInstance ino (if persistTasks ServerInterruptedEvent DestroyEvent) iworld of
			(Error e, iworld) = iShowErr [e] iworld
			(Ok _, iworld) = iworld

tick :: SDSSource () () ()
tick = createReadWriteSDS False "system" "_ticker"
	(\p iw->(Ok (), iw))
	(\p w iw->(Ok \_ _->True, iw))

updateClock :: !*IWorld -> *(!MaybeError TaskException (), !*IWorld)
updateClock iworld=:{IWorld|clock,world}
	//Determine current date and time
	# (timespec,world) = nsTime world
	# iworld & world   = world, clock = timespec
	//Write SDS if necessary
	# (mbe,iworld)     = write timespec focusedTimeSpec EmptyContext iworld
	= (() <$ mbe, iworld)

/* CAF for efficiency */
focusedTimeSpec :: SimpleSDSLens Timespec
focusedTimeSpec =: sdsFocus {start=zero,interval=zero} iworldTimespec

osErrorToTaskException :: !OSError -> TaskException
osErrorToTaskException err =
	(dynamic err
	, toString $
		bifmap (\errCode -> "error code: " +++ toString errCode) (\errMsg -> "error message: " +++ errMsg) err
	)
