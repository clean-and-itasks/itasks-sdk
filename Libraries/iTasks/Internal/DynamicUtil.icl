implementation module iTasks.Internal.DynamicUtil

import _SystemDynamic
import Data.GenHash
import iTasks.WF.Definition

:: MyTypeCodeConstructor = E.a: { my_tcc_cons :: !a }

:: MyDynamic = E.a:
	{	mydynamic_value :: a
	,	mydynamic_type	:: TypeCode
	}
	
unpackType :: !Dynamic -> TypeCode
unpackType dyn = (toMyDynamic dyn).mydynamic_type
where
	toMyDynamic :: !Dynamic -> MyDynamic
	toMyDynamic _ = code {
		pop_a 0
	}

toDyn :: a -> Dynamic | TC a
toDyn x = dynamic x

cast :: a -> b | TC a & TC b
cast a = case toDyn a of (a::b^) -> a

cast_to_TaskValue :: a -> TaskValue b | TC a & TC b
cast_to_TaskValue a = case toDyn a of (a::TaskValue b^) -> a

unwrapTask :: Dynamic -> Task a | TC a
unwrapTask (task :: Task a^) = task
