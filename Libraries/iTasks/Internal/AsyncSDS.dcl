definition module iTasks.Internal.AsyncSDS

import iTasks.SDS.Definition
from iTasks.WF.Definition import :: TaskId, :: TaskValue, :: Event, :: TaskEvalOpts, :: TaskResult
from iTasks.Internal.IWorld import :: IOState, :: IOHandle
from iTasks.Internal.SDS import :: SDSIdentity, :: SDSNotifyRequest

:: AsyncAction = Read | Write | Modify

/**
 * The type containing the asynchronous SDS operations
 *
 * @var parameter type
 * @var read type
 * @var write type
 * @var auxiliary type (extra result of modification)
 */
:: SDSRequest p r w a = E. sds: SDSReadRequest !(sds p r w) !p & TC p & TC r & TC w & Readable sds
	/*
	 * sds: SDS to read
	 * p: parameter with which to read the SDS
	 * reqSDSId: id of the original SDS read
	 * remoteSDSId: id of the current remote sds, so that refresh events can be sent using this identity.
	 * taskId: taskId of the task on the current instance
	 * port: Port which to send refresh messages on
	 */
	| E. sds: SDSRegisterRequest !(sds p r w) !p !SDSIdentity !SDSIdentity !TaskId !Port & TC p & TC r & TC w & Registrable sds & Readable sds
	| E. sds: SDSWriteRequest    !(sds p r w) !p !w & TC p & TC r & TC w & Writeable sds
	| E. sds: SDSModifyRequest   !(sds p r w) !p !(r -> MaybeError TaskException (a, ?w)) & TC p & TC r & TC w & TC a & Modifiable sds
	| SDSRefreshRequest !TaskId !SDSIdentity

//* The type that is stored in the dynamic connection state for a read operation
:: SDSRequestReadType r :== Either [String] (MaybeError TaskException r)
//* The type that is stored in the dynamic connection state for a write operation
:: SDSRequestWriteType :== Either [String] (MaybeError TaskException ())
//* The type that is stored in the dynamic connection state for a amend operation
:: SDSRequestModifyType r w a :== Either [String] (MaybeError TaskException (r, ?w, a))
//* The type that is stored in the dynamic connection state for a HTTP sds service read operation
:: SDSServiceRequestHTTPReadType r :== Either [String] r
//* The type that is stored in the dynamic connection state for a TCP sds service read operation
:: SDSServiceRequestTCPReadType r :== (?r, [String])
//* The type that is stored in the dynamic connection state for a HTTP sds service write operation
:: SDSServiceRequestHTTPWriteType p :== Either [String] (SDSNotifyPred p)
//* The type that is stored in the dynamic connection state for a TCP sds service write operation
:: SDSServiceRequestTCPWriteType p :== Either String (SDSNotifyPred p)

/**
 * Queue a read or register operation.
 * @param the sds
 * @param the parameter
 * @param the taskId which queues the operation
 * @param should we register?
 * @param the identity of the original sds
 */
queueRead :: !(SDSRemoteSource p r w) p !TaskId !Bool !SDSIdentity !*IWorld -> (!MaybeError TaskException IOHandle, !*IWorld) | TC p & TC r & TC w

/**
 * Queue a read to an external service.
 * @param the sds
 * @param the parameter
 * @param the taskId which queues the operation
 */
queueServiceRequest :: !(SDSRemoteService p r w) p !TaskId !Bool !*IWorld -> (!MaybeError TaskException IOHandle, !*IWorld) | TC p & TC r

queueServiceWriteRequest :: !(SDSRemoteService p r w) !p !w !TaskId !*IWorld -> (MaybeError TaskException (?IOHandle), !*IWorld) | TC p & TC w

/**
 * Queue that a task on a remote service should refresh itself.
 * @param Remote notify requests
 */
queueRemoteRefresh :: ![(TaskId, RemoteNotifyOptions)] !*IWorld -> *IWorld

/**
 * Queue a write operation to a remote sds.
 * @param the value to be written
 * @param the sds
 * @param the parameter
 * @param the taskId which queues the operation
 */
queueWrite :: !w !(SDSRemoteSource p r w) p !TaskId !*IWorld -> (!MaybeError TaskException IOHandle, !*IWorld) | TC p & TC r & TC w

/**
 * Queue a modify operation to a remote sds.
 * @param the modify function
 * @param the sds
 * @param the parameter
 * @param the taskId which queues the operation
 */
queueModify :: !(r -> MaybeError TaskException (a, ?w)) !(SDSRemoteSource p r w) p !TaskId !*IWorld -> (!MaybeError TaskException IOHandle, !*IWorld) | TC p & TC r & TC w & TC a

/**
 * The default UI during the loading of an asynchronous SDS
 */
asyncSDSLoaderUI :: !AsyncAction -> UI

/**
 * Completely load an sds and continue with the continuation
 *
 * @param sds
 * @param value during loading
 * @param ui during loading
 * @param continuation
 * @param event
 * @param taskevalopts
 * @param iworld
 * @result taskresult and iworld with the continuation embedded
 */
readCompletely :: (sds () r w) (TaskValue a) (Event -> UIChange) (r Event TaskEvalOpts *IWorld -> *(TaskResult a, *IWorld)) Event TaskEvalOpts !*IWorld
	-> *(TaskResult a, *IWorld) | Readable sds & TC r & TC w

/**
 * Completely write an sds and continue with the continuation
 *
 * @param value to write
 * @param sds
 * @param value during loading
 * @param ui during loading
 * @param continuation
 * @param event
 * @param taskevalopts
 * @param iworld
 * @result taskresult and iworld with the continuation embedded
 */
writeCompletely :: w (sds () r w) (TaskValue a) (Event -> UIChange) (Event TaskEvalOpts *IWorld -> *(TaskResult a, *IWorld)) Event TaskEvalOpts !*IWorld
	-> *(TaskResult a, *IWorld) | Writeable sds & TC r & TC w

/**
 * Completely modify an sds and continue with the continuation
 *
 * @param modification function
 * @param sds
 * @param value during loading
 * @param ui during loading
 * @param continuation
 * @param event
 * @param taskevalopts
 * @param iworld
 * @result taskresult and iworld with the continuation embedded
 */
modifyCompletely :: (r -> w) (sds () r w) (TaskValue a) (Event -> UIChange) (w Event TaskEvalOpts *IWorld -> *(TaskResult a, *IWorld)) Event TaskEvalOpts !*IWorld -> *(TaskResult a, *IWorld) | TC r & TC w & Modifiable sds

/**
 * Completely amend an sds and continue with the continuation
 *
 * @param amend function
 * @param sds
 * @param value during loading
 * @param ui during loading
 * @param continuation
 * @param event
 * @param taskevalopts
 * @param iworld
 * @result taskresult and iworld with the continuation embedded
 */
amendCompletely :: !(r -> (a, ?w)) !(sds () r w) !(TaskValue b) !(Event -> UIChange) !((?w) a Event TaskEvalOpts *IWorld -> *(TaskResult b, *IWorld)) !Event !TaskEvalOpts !*IWorld -> *(!TaskResult b, !*IWorld) | TC r & TC w & TC a & Modifiable sds

/**
 * Completely readRegister an sds and continue with the continuation
 *
 * @param sds
 * @param value during loading
 * @param ui during loading
 * @param continuation
 * @param event
 * @param taskevalopts
 * @param iworld
 * @result taskresult and iworld with the continuation embedded
 */
readRegisterCompletely :: (sds () r w) (TaskValue a) (Event -> UIChange) (r Event TaskEvalOpts *IWorld -> *(TaskResult a, *IWorld)) Event TaskEvalOpts !*IWorld
	-> *(TaskResult a, *IWorld) | TC r & TC w & Registrable sds
