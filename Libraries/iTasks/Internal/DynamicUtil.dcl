definition module iTasks.Internal.DynamicUtil

from iTasks.WF.Definition import :: Task, :: TaskValue

unpackType :: !Dynamic -> TypeCode

toDyn :: a -> Dynamic | TC a

cast :: a -> b | TC a & TC b
cast_to_TaskValue :: a -> TaskValue b | TC a & TC b

unwrapTask :: Dynamic -> Task a | TC a
