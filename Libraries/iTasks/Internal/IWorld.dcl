definition module iTasks.Internal.IWorld

/**
* @property-bootstrap
* import StdEnv
* import System.Time
* import System.Time.Gast
* import StdOverloadedList
* import Text.GenPrint
* from Gast.Gen import diagBent
*
* derive gPrint ComputeNextFireTimespec
* derive genShow ComputeNextFireTimespec
*
* :: ComputeNextFireTimespec = ComputeNextFireTimespec Timespec
* // See ggen{|Timespec|} in {{System.Time.Gast}}
* // Timespec 1920000000 999999999 is added as it is an exception case within the next 20 years which results in a
* // large positive overflow which is related to iTasks-sdk issue 504.
* // Timespec 0 100000000 is added as it is the interval for which the exceptional case may occur.
* ggen{|ComputeNextFireTimespec|} s =
* 	[! (ComputeNextFireTimespec {tv_sec=1920000000, tv_nsec=999999999}) :
* 	   (ComputeNextFireTimespec {tv_sec=0, tv_nsec=100000000}) :
* 	   [! ComputeNextFireTimespec x \\ x <|- ggen {|*|} s]
* 	]
* unwrapToTimespec (ComputeNextFireTimespec ts) = ts
*/

from symbols_in_program         import :: Symbol
from System.FilePath            import :: FilePath
from Data.Map                   import :: Map
from Data.Error                 import :: MaybeError(..), :: MaybeErrorString(..)
from Data.Set                   import :: Set
from Data.Queue                 import :: Queue
from Data.Either                import :: Either
from StdFile                    import class FileSystem, class FileEnv
from System.Time                import :: Timestamp, :: Timespec
from System.SysCall             import class SysCallEnv, instance SysCallEnv World
from Text.GenJSON               import :: JSONNode
from iTasks.Engine              import :: EngineOptions
from iTasks.UI.Definition       import :: UI, :: UIType
from iTasks.Internal.TaskState  import :: TaskMeta
from iTasks.Internal.TaskEval   import :: TaskTime
from iTasks.Util.DeferredJSON   import :: DeferredJSON

from iTasks.WF.Definition import:: TaskValue, :: Event, :: TaskId, :: InstanceNo, :: TaskNo, :: TaskException
from iTasks.WF.Combinators.Core import :: ParallelTaskType, :: TaskListItem
from iTasks.Internal.SDS import :: DeferredWrite
from iTasks.SDS.Definition import :: SDSIdentity, :: SDSIdentityHash, :: SDSNotifyRequest,
	:: SDSSource, :: SDSLens, :: SDSParallel, :: SDSBox, :: SDSCacheKey,
	class RWShared, class Registrable, class Modifiable, class Identifiable, class Readable, class Writeable,
	:: PossiblyRemoteTaskId, :: SdsNotifyRequests
from iTasks.Extensions.DateTime import :: Time, :: Date, :: DateTime
from iTasks.Internal.AsyncIO import :: ConnectionContext

from System.Signal import :: SigHandler
from AsyncIO import :: AIOState, :: ConnectionId
from ABC.Interpreter import :: PrelinkedInterpretationEnvironment
from Gast import generic gPrint, generic genShow, generic ggen, :: GenState, :: PrintState, class PrintOutput

CLEAN_HOME_VAR	:== "CLEAN_HOME"

:: *IWorld = !
	{ options             :: !EngineOptions                           //* Engine configuration
	, clock               :: !Timespec                                //* Server side clock
	, clockDependencies   :: ![#ClockDependency!]                     //* Dependencies to the clock, sorted from first to last next event
	, current             :: !*TaskEvalState                          //* Shared state during task evaluation
	, currentInstance     :: !InstanceNo                              //* Instance number of the task currently being evaluated
	, random              :: [Int]                                    //* Infinite random stream
	, symbols             :: !{#Symbol}                               //* Symbols of the program

	, sdsNotifyRequests   :: !Map SDSIdentityHash SdsNotifyRequests   //* Notification requests from previously read sds's
	, sdsNotifyReqsByTask :: !Map TaskId (Set SDSIdentityHash)        //* Allows to efficiently find notification by taskID for clearing notifications
	, memoryShares        :: !Map String Dynamic                      //* Run-time memory shares
	, readCache           :: !Map SDSCacheKey Dynamic                 //* Cached share reads
	, writeCache          :: !Map SDSCacheKey (Dynamic,DeferredWrite) //* Cached deferred writes
	, readSdsSourceVals   :: !Map SDSCacheKey Dynamic
	  //* SDS source values read during the current task instance evaluation. Re-used such that SDS sources are only
	  //* read once during an evaluation to provide a consistent view and improve performance.
	, abcInterpreterEnv   :: !PrelinkedInterpretationEnvironment      //* Used to serialize expressions for the client
	, ioStates            :: !Map IOHandle IOState                    //* Results of low-level io tasks, indexed by the high-level taskid that it is linked to
	, ioHandleMap         :: !Map ConnectionId IOHandle               //* Quickly find the io handle belonging to the connection Id
	, nextIOHandle        :: !Int                                     //* Stores the next IO handle to be used for a connection.
	, lastOnDataCId       :: !?IOHandle                               //* Last ConnectionId on which data was received, used by WebService.
	, webServiceInstances :: ![WebServiceInstance]                    //* Stores client instances for WebService.
	, signalHandlers      :: *[*SigHandler]                           //* Signal handlers
	, world               :: !*World                                  //* The outside world
	, aioState            :: !(AIOState IWorld)
	, resources           :: *[*Resource]                             //* Experimental database connection cache
	, onClient            :: !Bool                                    //* `False` on the server, `True` on the client
	, shutdown            :: !?Int                                    //* Signals the server function to shut down, the int will be set as exit code
	}

instance SysCallEnv IWorld

:: WebServiceInstance =
	{ instanceNo   :: !InstanceNo, // Instance number of the Task which the connected client is working on.
	  connectionId :: !ConnectionId, // ConnectionId of the connected client.
	  instanceKey  :: !String
	  	/* Key used to make sure that a client can only work
		 * on an instance of a Task through one viewport (browser window).
		 * Seperate viewports lead to seperate instances,
		 * The key leads to the client having access to the instance of the task.
		 */
	}

//* A task's dependency to the clock, it's only used internally in the {{IWorld}}
:: ClockDependency =
	{ nextFire :: !Timespec //* The time when the next event is desired
	, taskId   :: !TaskId   //* The task id of the registrant
	}

/**
 * Stateful information regarding task evaluation.
 * Always create this with `mkTaskEvalState` to maintain (some) forwards compatibility.
 */
:: TaskEvalState =
	{ nextTaskNo      :: !TaskNo      //* The next task number to assign
	}

:: IOHandle :== Int

/**
 * The IOState contains the state
 * that is related to performing network I/O in an iTasks specific context.
 */
:: IOState = !
	{/**
	  * The onShareChangeHandler callback is currently evaluated every during every tick of the event loop.
	  *
	  * @param The ConnectionState of the connection as a Dynamic.
	  * @param The read value of the SDS which is being monitored as a dynamic
	  * @param IWorld
	  * @result An error or a new connection state
	  * @result Possibly a write value which is written to the SDS.
	  * @result Output to be sent to the peer over the connection.
	  * @result Whether the connection should be closed.
	  * @result IWorld
	  */
	  onShareChangeHandler  :: !(Dynamic Dynamic *IWorld -> *(MaybeErrorString (?Dynamic), ?Dynamic, [String], Bool, *IWorld))
	 /**
	  * The onDestroyHandler callback is evaluated when the task (taskId) receives an OnDestroy event.
	  *
	  * @param The connection state of the connection as a Dynamic
	  * @param IWorld
	  * @result An error or a new connection state
	  * @result Output to be sent to the peer over the connection
	  * @result IWorld
	  */
	, onDestroyHandler      :: !(Dynamic *IWorld -> *(MaybeErrorString (?Dynamic), [String], *IWorld))
	 /** The SDS whose read and write value is passed on to the callbacks
	  * onShareChange, onDestroy and onConnect, onData, onTick and onDisconnect.
	  */
	, sdsModifiedByHandlers :: !SDSLens () Dynamic Dynamic
	// The taskId of the Task which is making use of Network I/O.
	, taskId                :: !?TaskId
	// The status of the connection (storing the dynamic state also)
	, ioStatus              :: !IOStatus
	// Stores the clients in case the IOHandle is a listener
	, listenerMap           :: !Set IOHandle
	// Stores the instances in case the IOHandle is a listener and a web service
	, clientInstances       :: !Set InstanceNo
	// Whether the client is a listener
	, listener              :: !Bool
	}

//* Status of an IO port
:: IOStatus
	= IOConnecting          !ConnectionId //* connection is being established
	| IOListener            !ConnectionId //* connection is a listener
	| IOActive     !Dynamic !ConnectionId //* Task is active, connection is active
	| IODestroyed  !Dynamic !ConnectionId //* Task is destroyed, connection is active
	| IOClosed     !Dynamic               //* Task is active, connection is closed
	| IOException  !String                //* Exception in IO occurred
instance toString IOStatus

/**
 * Apply a function to the IOState for a given handle.
 *
 * @param IOHandle to update.
 * @param Function to apply on the IOState of the IOHandle.
 * @throws an error if the IOHandle is unknown or the function throws an error.
 */
updIOState :: !IOHandle !.(IOState -> MaybeErrorString IOState) !*IWorld -> (!MaybeErrorString (), !*IWorld)
/**
 * Delete the IOState for a given handle.
 *
 * @param IOHandle to retrieve the IOState for.
 * @throws an error if the IOHandle is unknown.
 */
delIOState :: !IOHandle !*IWorld -> (!MaybeErrorString (), !*IWorld)
/**
 * Retrieve the IOState for a given handle.
 *
 * @param IOHandle to retrieve the IOState for.
 * @throws an error if the IOHandle is unknown.
 */
getIOState :: !IOHandle !*IWorld -> (!MaybeErrorString IOState, !*IWorld)

/**
 * Apply a function that requires an IOState as an argument for a given handle.
 *
 * @param IOHandle to use.
 * @param Function to apply.
 * @result An error if the IOHandle is unknown or the function throws an error.
 */
withIOState :: !IOHandle !.(IOState -> MaybeErrorString a) !*IWorld -> (!MaybeErrorString a, !*IWorld)

/**
 * Replace the dynamic state in an IOStatus, this conserves the constructor.
 * If there is no dynamic state, nothing is updated.
 */
replaceDynStateInIoStatus :: !Dynamic !IOStatus -> IOStatus
/**
 * Get the dynamic state from the IOStatus if there is one.
 *
 * @param IOHandle to retrieve the IOState for.
 * @result An error if there is no dynamic state (e.g. when the IOStatus is IOConnecting, IOException or IOListener). Otherwise, returns the dynamic state.
 */
getDynStateFromIoStatus :: !IOStatus -> MaybeErrorString Dynamic
/**
 * Get the dynamic state from the IOStatus if there is one.
 *
 * @param IOHandle to retrieve the IOState for.
 * @result An error if there is no dynamic state (e.g. when the IOStatus is IOConnecting, IOException or IOListener) or when there is a type mismatch. otherwise, returns the connection state.
 */
getStateFromIoStatus :: !IOStatus -> MaybeErrorString l | TC l

:: *Resource = Resource | .. //Extensible resource type for caching database connections etc...

//Creation and destruction of the iworld
/**
* Creates and initializes the IWorld state
*
* @param The engine options
* @param The world
*
* @result An initialized iworld or world together with an error string on failure
*/
createIWorld :: !EngineOptions !*World -> Either (!String, !*World) *IWorld

/**
* Destroys the iworld state
*/
destroyIWorld :: !*IWorld -> *World

//*Internally used for the clock share, see {{computeNextFire}} for details
:: ClockParameter a =
	{ start    :: !a
	, interval :: !a
	}

/**
 * Determines when the next fire should be according to the parameter and the time of registration
 * This function is only used internally to determine notifications for {{iworldTimestamp}} and {{iworldTimespec}}.
 * It is exported solely for unit tests.
 *
 * If the interval is zero, the next fire is start.
 * If the interval is non-zero, the next fire is the earliest timespec after regTime and start that is equal to start modulo the interval.
 *
 * Or: nextfire = start + n*interval where nextfire > regTime, n >= 0 and minimal.
 *
 * This next timeout may very well be in the past.
 *
 * @param regTime          Time of registration
 * @param {start,interval} Clock Parameter
 *
 * @result the time of the next fire
 * The precondition is added to prevent unavoidable overflows that should not realistically occur.
 * @property
 *      always_greater_or_equal_to_start: A.a :: ComputeNextFireTimespec; b :: ClockParameter ComputeNextFireTimespec:
 *      let
 *      	computeNextFireResult =
 *      		computeNextFire
 *      		(unwrapToTimespec a)
 *      		{start=unwrapToTimespec b.start, interval=unwrapToTimespec b.interval}
 *      	start = unwrapToTimespec b.start
 *      	interval = unwrapToTimespec b.interval
 *      	//The test assumption is added to prevent unavoidable overflows that should not realistically occur.
 *      	testAssumption = start <=. {tv_sec=1000, tv_nsec=0} /\ interval.tv_sec <=. 1000
 *      in
 *      testAssumption ==>
 *      computeNextFireResult
 *      >=. start
 * @property
 *      never_more_than_now_plus_interval: A.a :: ComputeNextFireTimespec; b :: ClockParameter ComputeNextFireTimespec:
 *      let
 *      	computeNextFireResult =
 *      		computeNextFire
 *      		(unwrapToTimespec a)
 *      		{start=unwrapToTimespec b.start, interval=unwrapToTimespec b.interval}
 *      	regTime = unwrapToTimespec a
 *      	start = unwrapToTimespec b.start
 *      	interval = unwrapToTimespec b.interval
 *      	//The test assumption is added to prevent unavoidable overflows that should not realistically occur.
 *      	testAssumption = start <=. {tv_sec=1000, tv_nsec=0} /\ interval.tv_sec <=. 1000
 *      in
 *      testAssumption ==>
 *      computeNextFireResult
 *      <=. start + regTime + interval
 * @property
 *      never_in_the_past_but_after_start: A.a :: ComputeNextFireTimespec; b :: ClockParameter ComputeNextFireTimespec:
 *      let
 *      	computeNextFireResult =
 *      		computeNextFire
 *      		(unwrapToTimespec a)
 *      		{start=unwrapToTimespec b.start, interval=unwrapToTimespec b.interval}
 *      	start = unwrapToTimespec b.start
 *      	regTime = unwrapToTimespec a
 *      	interval = unwrapToTimespec b.interval
 *      	//The test assumption is added to prevent unavoidable overflows that should not realistically occur.
 *      	testAssumption = start.tv_nsec <=. 1000 /\ start.tv_sec <=. 1000 /\ interval.tv_sec <=. 1000
 *      in
 *      testAssumption ==>
 *      start =.=
 *      computeNextFireResult
 *      \/
 *      computeNextFireResult >=.
 *      regTime
 */
computeNextFire :: !Timespec !(ClockParameter Timespec) -> Timespec

/**
 * Represents the current timespec (seconds and nanoseconds since the unix epoch).
 * The parameter determines the notification rate (see {{computeNextFire}})
 */
iworldTimespec :: SDSBox (ClockParameter Timespec) Timespec Timespec

/**
 * Represents the current timestamp (seconds since the unix epoch).
 * The parameter determines the notification rate (See {{computeNextFire}})
 */
iworldTimestamp :: SDSLens (ClockParameter Timestamp) Timestamp ()

/**
 * Represents the datetime in the local timezone.
 * The notification rate is once a second.
 */
iworldLocalDateTime :: SDSParallel () DateTime ()

/*
 * Gives you possibly a matching resource while adhering to the uniqueness
 * constraints. Note that this does remove it from the IWorld
 *
 * @param Function that classifies the resource whether it matches
 */
iworldResource :: (*Resource -> (Bool, *Resource)) *IWorld -> (*[*Resource], *IWorld)

instance FileSystem IWorld
instance FileEnv IWorld

derive gPrint ClockParameter
derive genShow ClockParameter
derive ggen ClockParameter
