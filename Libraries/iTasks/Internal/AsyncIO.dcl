definition module iTasks.Internal.AsyncIO

/**
 * This module provides functions that allow to make use of the AsyncIO package while making use of iTasks.
 * This means that this module allows to perform Async(hronous)IO operations on communication channels
 * , while making use of iTasks specific functionality (most notably SDSs (Shared Data Sources)).
 * The communication channels that may be used are TCP connections.
 *
 * The user of the module makes use of callback functions to react to I/O events occurring in an asynchronous manner.
 * For more information on the callback functions that may be provided, see :: ConnectionHandlers.
 *
 * For more information on performing asynchronous I/O, read the definition module of the AsyncIO module.
 */

from Data.Error import :: MaybeErrorString, :: MaybeError
from Data.Map import :: Map (..)
from System.SysCall import class SysCallEnv
from iTasks.SDS.Definition import :: SDSLens, class Readable, class Writeable, class Identifiable, class RWShared
	, class Registrable, class Modifiable
from iTasks.Internal.IWorld import :: IWorld, :: IOHandle
from iTasks.WF.Definition import :: TaskId, class iTask, generic gEditor, generic JSONEncode, generic JSONDecode
	, generic gEq, :: Editor, :: EditorPurpose, :: JSONNode, :: EditorReport
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from AsyncIO import class containsAIOState, :: Port (..),
	instance toString Port, instance == Port, class <, instance < Port
from AsyncIO import qualified :: ConnectionHandlers

/**
* Transforms a record containing connection handlers and a shared source which is modified by the handlers
* to a ConnectionContext.
*
* @param Record containing connection handlers
* @param The SDS which is modified by the handlers
* @result A ConnectionContext.
*/
wrapConnectionContext :: !(ConnectionHandlers l r w)
                         !(sds () r w)
                      -> ConnectionContext | TC l & TC r & TC w & RWShared sds

/**
* Transforms a record containing connection handlers which pass on the IWorld
* and a shared source which is modified by the handlers to a ConnectionContext.
*
* @param Record containing connection handlers which make use of the IWorld
* @param The SDS which is modified by the handlers
* @result A ConnectionContext.
*/
wrapIWorldConnectionContext :: !(ConnectionHandlersIWorld l r w)
                               !(sds () r w)
                            -> ConnectionContext | TC l & TC r & TC w & RWShared sds

//* Type describing the source of the handler
:: HandlerSource
	= HSClient !IOHandle
	//* Handler is a client, therefore, the given IOHandle is the handler of the client.
	| HSListener !Bool !IOHandle
	//* Handler is a listener, the boolean states whether to remove the clients after closing, the given IOHandle is the handler of the listener.

/**
 * Translates a ConnectionHandlersIWorld record containing connection handlers
 * to a ConnectionHandlers record, to be able to make use of AsyncIO.
 *
 * @param Source of the handler
 * @param The IO handle
 * @param Connection handlers
 * @param Task id of the task that started the connection (or none if it is system)
 * @param SDS used to store read and write values
 * @result AsyncIO handlers
 */
tslConnectionHandlersIWorld :: !HandlerSource !(ConnectionHandlersIWorld Dynamic Dynamic Dynamic)
                               !(?TaskId)
                               !(SDSLens () Dynamic Dynamic)
                            -> 'AsyncIO'.ConnectionHandlers IWorld

/**
 * Handles an IO exception by generating a Task Exception for the task which is performing the IO operations.
 *
 * @param The exception
 * @param The connection Id for the connection on which the exception occurred.
 * @param The task id to notify (if there is one)
 * @param IWorld
 * @result IWorld
 */
handleIOException ::!e !IOHandle !(?TaskId) !*IWorld -> *IWorld | toString e

/**
 * Context which is needed for handling connections.
 */
:: ConnectionContext =
	{ handlers :: !(ConnectionHandlersIWorld Dynamic Dynamic Dynamic)
	, sdsModifiedByHandlers :: !(SDSLens () Dynamic Dynamic)
	}

/**
 * The ConnectionHandlers record contains handlers which make it possible to react to
 * I/O events occuring on a connection. The appropriate handler is evaluated when a certain I/O event occurs.
 *
 * @var The type of the connection state which is altered by the handlers
 * @var The type of the read value of the SDS which is read by the handlers
 * @var The type of the write value of the SDS which the handlers may write to.
 */
:: ConnectionHandlers l r w =
    {
	/**
	 * This handler is evaluated when a new connection is established.
	 *
	 * @param The IOHandle contains an ID which uniquely identifies the connection.
	 * @param The IPAddr contains the IP Address of the client that established the connection.
	 * @param The r argument contains the read value of the SDS that is being modified
	 * @result The first return value may contain an Error or an initial value for the ConnectionState
	 * @result The second return value may contain a write value that is written to the SDS being modified
	 * @result The third return value contains the output to be written over the connection
	 * @result The fourth return value indicates whether the connection should be closed after sending the output.
	 */
	 onConnect :: !(IOHandle String r -> (MaybeErrorString l, ?w, [String], Bool))
	/**
	 * This handler is evaluated when data that was sent over the connection is received.
	 *
	 * @param The IOHandle contains an ID which uniquely identifies the connection.
	 * @param The String argument contains the data that was received.
	 * @param The l argument contains the ConnectionState of the connection
	 * @param The r argument contains the read value of the SDS that is being modified
	 * @result The first return value may contain an Error, a ?None, or a new value for the ConnectionState
	 * @result The second return value may contain a write value that is written to the SDS being modified
	 * @result The third return value contains the output to be written over the connection
	 * @result The fourth return value indicates whether the connection should be closed after sending the output.
	 */
	, onData :: !(IOHandle String l r -> (MaybeErrorString (?l), ?w, [String], Bool))
	/**
	 * This handler is evaluated every event loop iteration.
	 *
	 * @param The l argument contains the ConnectionState of the connection
	 * @param The r argument contains the read value of the SDS that is being modified
	 * @result The first return value may contain an Error, a ?None, or a new value for the ConnectionState
	 * @result The second return value may contain a write value that is written to the SDS being modified
	 * @result The third return value contains the output to be written over the connection
	 * @result The fourth return value indicates whether the connection should be closed after sending the output.
	 */
	, onShareChange :: !(l r -> (MaybeErrorString (?l), ?w, [String], Bool))
	/**
	 * The onDisconnect handler is evaluated when the peer closes the connection.
	 * Note that this handler is not evaluated when the connection is closed out of ones
	 * own initiative, e.g when the `Bool` close connection indicator of another handler
	 * is set to `True`.
	 *
	 * @param The IOHandle contains an ID which uniquely identifies the connection.
	 * @param The l argument contains the ConnectionState of the connection
	 * @param The r argument contains the read value of the SDS that is being modified
	 * @result The first return value may contain an Error, a ?None, or a new value for the ConnectionState
	 * @result The second return value may contain a write value that is written to the SDS being modified
	 */
	, onDisconnect :: !(IOHandle l r -> (MaybeErrorString (?l), ?w))
	/**
	 * This handler is evaluated when the task that
	 * lead to establishing the connection is destroyed.
	 *
	 * @param The l argument contains the ConnectionState of the connection
	 * @result The first return value may contain an Error, a ?None, or a new value for the ConnectionState
	 * @result The second return value contains the output to be written over the connection
	 */
	, onDestroy :: !(l -> (MaybeErrorString (?l), [String]))
	}

/**
 * Version of connection handlers with IWorld side-effects that is used by e.g the HTTP server.
 * For more information, see :: ConnectionHandlers.
 */
:: ConnectionHandlersIWorld l r w =
	{ onConnect     :: !(IOHandle String   r *IWorld -> *(MaybeErrorString l,    ?w, [String], Bool, *IWorld))
	, onData        :: !(IOHandle String l r *IWorld -> *(MaybeErrorString (?l), ?w, [String], Bool, *IWorld))
	, onShareChange :: !(                l r *IWorld -> *(MaybeErrorString (?l), ?w, [String], Bool, *IWorld))
	, onTick        :: !(                l r *IWorld -> *(MaybeErrorString (?l), ?w, [String], Bool, *IWorld))
	, onDisconnect  :: !(IOHandle        l r *IWorld -> *(MaybeErrorString (?l), ?w,                 *IWorld))
	, onDestroy     :: !(                l   *IWorld -> *(MaybeErrorString (?l),     [String],       *IWorld))
	}

/**
 * Process the I/O handlers for `onShareChange` and `onDestroy`.
 * Errors are reported by setting the I/O state to `IOException` so that the tasks observe this in the next evaluation.
 */
processOnShareChangeOnDestroy :: !*IWorld -> *IWorld

//* Empty the write AsyncIO queues
emptyAsyncIOWriteQueues :: !*IWorld -> *IWorld

derive class iTask Port

instance containsAIOState IWorld
