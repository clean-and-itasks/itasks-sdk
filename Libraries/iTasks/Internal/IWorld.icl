implementation module iTasks.Internal.IWorld

import StdEnv
from StdFunc import seqList, :: St

from ABC.Interpreter import prepare_prelinked_interpretation, :: PrelinkedInterpretationEnvironment

import Data.Func
import Data.GenHash
import Data.Integer
from Data.Map import :: Map
import qualified Data.Map as DM
import Data.Maybe
import Gast => qualified <.>
import Gast.Gen
import Math.Random
import StdOverloadedList
import System.CommandLine
import System.Directory
import System.File
import System.FilePath
import System.OS
import System.Signal
import System.SysCall
import Text, Text.GenPrint
from AsyncIO import :: AIOState, emptyAIOState, instance toString ConnectionId
import symbols_in_program

import iTasks.Engine
import iTasks.Extensions.DateTime
import iTasks.Internal.Task
import iTasks.Internal.TaskEval
import iTasks.Internal.Util
import iTasks.SDS.Combinators.Common
import iTasks.SDS.Combinators.Core
import iTasks.WF.Definition
import iTasks.WF.Derives
import iTasks.Internal.TaskServer
from Gast import generic ggen, generic genShow, generic gPrint, :: GenState

instance SysCallEnv IWorld where
	accWorld f iworld=:{IWorld|world}
		# (a, world) = f world
		= (a, {IWorld | iworld & world=world})
	appWorld f iworld = {iworld & world = f iworld.world}

createIWorld :: !EngineOptions !*World -> Either (!String, !*World) *IWorld
createIWorld options world
	# (ts=:{tv_nsec=seed}, world) = nsTime world
	# (mbAbcEnv,           world) = prepare_prelinked_interpretation options.appPath options.byteCodePath world
	# (symbols,            world) = if (isNone options.distributed) ({}, world) (initSymbols options world)
	= case mbAbcEnv of
		?Just abcEnv
			# (mbAioState, world) = emptyAIOState world
			| isError mbAioState = Left (snd $ fromError mbAioState, world)
			# aioState = fromOk mbAioState
			= Right
				{ IWorld
				| options = options
				, clock   = ts
				, clockDependencies = [|]
				// This is always set before evaluation so it doesn't matter wat current is here
				, current             = mkTaskEvalState 0
				, currentInstance     = 0
				, sdsNotifyRequests   = 'DM'.newMap
				, sdsNotifyReqsByTask = 'DM'.newMap
				, memoryShares        = 'DM'.newMap
				, readCache           = 'DM'.newMap
				, writeCache          = 'DM'.newMap
				, readSdsSourceVals   = 'DM'.newMap
				, abcInterpreterEnv   = abcEnv
				, shutdown            = ?None
				, ioStates            = 'DM'.newMap
				, ioHandleMap         = 'DM'.newMap
				, nextIOHandle        = 0
				, lastOnDataCId       = ?None
				, webServiceInstances = []
				, aioState            = aioState
				, world               = world
				, signalHandlers      = []
				, resources           = []
				// genRandInt aborts if the seed is zero and it may happen that seed (tv_nsec) is exactly zero.
				, random              = genRandInt (if (seed == 0) 42 seed)
				, symbols             = symbols
				, onClient            = False
				}
		?None =
			Left ("Failed to parse bytecode, is ByteCode set in the project file?", world)
where
	initSymbols {appPath} world
		# (symbols, world) = accFiles (read_symbols appPath) world
		# world = if (size symbols == 0) (showErr ["No symbols found, did you compile with the clm_option strip: false?"] world) world
		= (symbols, world)

// Determines the server executables path
determineAppPath :: !*World -> (!FilePath, !*World)
determineAppPath world
	# ([arg:_],world) = getCommandLine world
	| dropDirectory arg <> "ConsoleClient.exe" = toCanonicalPath arg world
	//Using dynamic linker:
	# (res, world)              = getCurrentDirectory world
	| isError res               = abort "Cannot get current directory."
	# currentDirectory          = fromOk res
	# (res, world)              = readDirectory currentDirectory world
	| isError res               = abort "Cannot read current directory."
	# batchfiles                = [f \\ f <- fromOk res | takeExtension f == "bat" ]
	| isEmpty batchfiles        = abort "No dynamic linker batch file found."
	# (infos, world)            = seqList (map getFileInfo batchfiles) world
	| any isError infos         = abort "Cannot get file information."
	= (currentDirectory </> (fst o hd o sortBy cmpFileTime) (zip2 batchfiles infos), world)
	where
		cmpFileTime (_,Ok {FileInfo | lastModifiedTime = x})
					(_,Ok {FileInfo | lastModifiedTime = y}) = x > y

destroyIWorld :: !*IWorld -> *World
destroyIWorld iworld=:{IWorld|world} = world

computeNextFire :: !Timespec !(ClockParameter Timespec) -> Timespec
computeNextFire regTime p=:{start,interval}
	// If interval is zero, just wait for start
	| interval == zero
		= start
	// Start hasn't passed, just wait for start
	| start > regTime
		= start
	// Computation is done using integers on 32 bit since otherwise the computation might fail due to overflows.
	= IF_INT_64_OR_32 (computeNext64 (regTime - start) interval) (computeNext32 (regTime - start) interval)
where
	computeNext64 :: !Timespec !Timespec -> Timespec
	computeNext64 rtSinceStart interval
		#! intervalsPassed = divTs64 rtSinceStart interval
		= start + scaleTs64 (intervalsPassed + 1) interval
	where
		//* Floor division of two timespecs
		divTs64 :: !Timespec !Timespec -> Int
		divTs64 rtSinceStart interval = max zero $ (toNS rtSinceStart / toNS interval)
		where
			toNS x = x.tv_sec*nanofactorInt+x.tv_nsec

		scaleTs64 :: !Int !Timespec -> Timespec
		scaleTs64 intervalsPassedInc interval
			# nsec = interval.tv_nsec*intervalsPassedInc
			// Convert ns to seconds by dividing by nanofactorInt, put remainder of division in tv_nsec.
			= { tv_sec  = toInt $ interval.tv_sec*intervalsPassedInc+nsec/nanofactorInt
			  , tv_nsec = toInt $ nsec rem nanofactorInt
			  }

	computeNext32 :: !Timespec !Timespec -> Timespec
	computeNext32 rtSinceStart interval
		#! intervalsPassed = divTs32 rtSinceStart interval
		= start + scaleTs32 (intervalsPassed + toInteger 1) interval
	where
		//* Floor division of two timespecs
		divTs32 :: !Timespec !Timespec -> Integer
		divTs32 rtSinceStart interval = max zero $ (toNS rtSinceStart / toNS interval)
		where
			toNS x = toInteger x.tv_sec*nanofactorInteger+toInteger x.tv_nsec

		scaleTs32 :: !Integer !Timespec -> Timespec
		scaleTs32 intervalsPassedInc interval
			# nsec = toInteger interval.tv_nsec*intervalsPassedInc
			// Convert ns to seconds by dividing by nanofactorInt, put remainder of division in tv_nsec.
			= { tv_sec  = toInt $ toInteger interval.tv_sec*intervalsPassedInc+nsec/nanofactorInteger
			  , tv_nsec = toInt $ nsec rem nanofactorInteger
			  }

nanofactorInt :: Int
nanofactorInt = 1000000000

nanofactorInteger :: Integer
nanofactorInteger =: toInteger 1000000000

:: SDSWithRegisterHook p r w =
	{ hook   :: !p TaskId *IWorld -> *(MaybeError TaskException (), *IWorld)
	, source :: !SDSSource p r w
	}

instance Identifiable SDSWithRegisterHook
where
	sdsIdentity {source} = sdsIdentity source

instance Readable SDSWithRegisterHook
where
	readSDS {source} p c iworld = readSDS source p c iworld

instance Writeable SDSWithRegisterHook
where
	writeSDS {source} p c w iworld = writeSDS source p c w iworld

instance Modifiable SDSWithRegisterHook
where
	modifySDS f {source} p c iworld = modifySDS f source p c iworld

instance Registrable SDSWithRegisterHook
where
	readRegisterSDS {hook,source} p c t iworld
		= case hook p t iworld of
			(Ok _, iworld) = readRegisterSDS source p c t iworld
			(Error e, iworld) = (ReadException e, iworld)

iworldTimespec :: SDSBox (ClockParameter Timespec) Timespec Timespec
iworldTimespec =: SDSBox
	{ hook   = register
	, source = createReadWriteSDS False "IWorld" "timespec" read write
	}
where
	read :: !(ClockParameter Timespec) !*IWorld -> (!MaybeError TaskException Timespec, !*IWorld)
	read _ iworld=:{IWorld|clock} = (Ok clock, iworld)

	write :: !(ClockParameter Timespec) !Timespec !*IWorld -> (!MaybeError TaskException (SDSNotifyPred (ClockParameter Timespec)), !*IWorld)
	write _ ts iworld = (Ok (pred ts), {iworld & clock=ts})

	/**
	 * @param ts Timestamp that is written
	 * @param reg Timestamp of registration
	 * @param p Clock parameter
	 */
	pred :: !Timespec !Timespec !(ClockParameter Timespec) -> Bool
	pred ts reg p = ts >= computeNextFire reg p

	register :: !(ClockParameter Timespec) !TaskId !*IWorld -> (!MaybeError TaskException (), !*IWorld)
	register p tid iworld
		# iworld & clockDependencies = Insert
			(\new old->new.nextFire < old.nextFire)
			{nextFire=computeNextFire iworld.clock p, taskId=tid}
			iworld.clockDependencies
		= (Ok (), iworld)

iworldTimestamp :: SDSLens (ClockParameter Timestamp) Timestamp ()
iworldTimestamp =: mapReadWrite (timespecToStamp, \w r -> ?None) ?None
	$ sdsTranslate "iworldTimestamp translation" (\{start,interval}->{start=timestampToSpec start,interval=timestampToSpec interval}) iworldTimespec

iworldLocalDateTime :: SDSParallel () DateTime ()
iworldLocalDateTime =: sdsParallel "iTasks.Internal.IWorld.iworldLocalDateTime"
	// ignore value, but use notifications for 'iworldTimestamp'
	(\p -> (p,p)) fst
	(SDSWriteConst \_ _ -> Ok ?None) (SDSWriteConst \_ _ -> Ok ?None)
	(createInternalReadOnlySDS False "iworldLocalDateTime" \_ -> iworldLocalDateTime`)
	(sdsFocus {start=Timestamp 0,interval=Timestamp 1} iworldTimestamp)
where
	iworldLocalDateTime` :: !*IWorld -> (!MaybeError a DateTime, !*IWorld)
	iworldLocalDateTime` iworld=:{clock={tv_sec}, world}
		# (tm, world) = toLocalTime (Timestamp tv_sec) world
		= (Ok $ tmToDateTime tm, {iworld & world = world})

iworldResource :: (*Resource -> (Bool, *Resource)) *IWorld -> (*[*Resource], *IWorld)
iworldResource f iworld=:{IWorld|resources}
# (matches, resources) = splitWithUnique f resources
= (matches, {iworld & resources=resources})
where
	splitWithUnique f [] = ([], [])
	splitWithUnique f [r:rs]
	# (ok, r) = f r
	| ok = let (ms, xs) = splitWithUnique f rs in ([r:ms], xs)
	= let (ms, xs) = splitWithUnique f rs in (ms, [r:xs])

instance toString IOStatus where
	toString (IOListener id) = "IOListener (" +++ toString id +++ ")"
	toString (IOConnecting id) = "IOConnecting (" +++ toString id +++ ")"
	toString (IOActive m id) = "IOActive: " +++ toString m +++ " (" +++ toString id +++ ")"
	toString (IODestroyed m id) = "IODestroyed: " +++ toString m +++ " (" +++ toString id +++ ")"
	toString (IOClosed m) = "IOClosed: " +++ toString m
	toString (IOException m) = "IOException: " +++ m

getIOState :: !IOHandle !*IWorld -> (!MaybeErrorString IOState, !*IWorld)
getIOState cId iworld = withIOState cId Ok iworld

withIOState :: !IOHandle !.(IOState -> MaybeErrorString a) !*IWorld -> (!MaybeErrorString a, !*IWorld)
withIOState cId f iworld=:{ioStates} = case 'DM'.get cId ioStates of
	?Just ioState = (f ioState, iworld)
	?None = (Error (concat3 "getIOState " (toString cId) " not found"), iworld)

delIOState :: !IOHandle !*IWorld -> (!MaybeErrorString (), !*IWorld)
delIOState cId iworld = case 'DM'.get cId iworld.ioStates of
	?None   = (Error (concat3 "delIOState " (toString cId) " didn't exist in the first place"), iworld)
	?Just _ = (Ok (), {iworld & ioStates='DM'.del cId iworld.ioStates})

updIOState :: !IOHandle !.(IOState -> MaybeErrorString IOState) !*IWorld -> (!MaybeErrorString (), !*IWorld)
updIOState ioHandle f iworld = case withIOState ioHandle f iworld of
	(Ok ioState, iworld) = (Ok (), {iworld & ioStates='DM'.put ioHandle ioState iworld.ioStates})
	(Error e, iworld) = (Error e, iworld)

getDynStateFromIoStatus :: !IOStatus -> MaybeErrorString Dynamic
getDynStateFromIoStatus (IOActive    d _) = Ok d
getDynStateFromIoStatus (IODestroyed d _) = Ok d
getDynStateFromIoStatus (IOClosed    d) = Ok d
getDynStateFromIoStatus (IOConnecting _) = Error ("No IO state yet, status is IOConnecting")
getDynStateFromIoStatus (IOException m) = Error m

getStateFromIoStatus :: !IOStatus -> MaybeErrorString l | TC l
getStateFromIoStatus s = case getDynStateFromIoStatus s of
	Ok (l :: l^) = Ok l
	Ok d = Error (concat4 "Type mismatch in IOState, got: " (toString (typeCodeOfDynamic d)) " expect: " (toString (typeCodeOfDynamic (dynamic undef :: l^))))
	Error e = Error e

replaceDynStateInIoStatus :: !Dynamic !IOStatus -> IOStatus
replaceDynStateInIoStatus d (IOActive    _ cId) = IOActive d cId
replaceDynStateInIoStatus d (IODestroyed _ cId) = IODestroyed d cId
replaceDynStateInIoStatus d (IOClosed    _) = IOClosed d
replaceDynStateInIoStatus _ a = a

//Wrapper instance for file access
instance FileSystem IWorld
where
	fopen filename mode iworld=:{IWorld|world}
		# (ok,file,world) = fopen filename mode world
		= (ok,file,{IWorld|iworld & world = world})
	fclose file iworld=:{IWorld|world}
		# (ok,world) = fclose file world
		= (ok,{IWorld|iworld & world = world})
	stdio iworld=:{IWorld|world}
		# (io,world) = stdio world
		= (io,{IWorld|iworld & world = world})
	sfopen filename mode iworld=:{IWorld|world}
		# (ok,file,world) = sfopen filename mode world
		= (ok,file,{IWorld|iworld & world = world})

instance FileEnv IWorld
where
	accFiles accfun iworld=:{IWorld|world}
		# (x, world) = accFiles accfun world
		= (x, {IWorld | iworld & world=world})
	appFiles appfun iworld=:{IWorld|world}
		# world = appFiles appfun world
		= {IWorld | iworld & world=world}

ggen{|ClockParameter|} f s = [! {start=a,interval=b} \\ (a,b) <|- ggen{|*->*->*|} f f s]
derive gPrint ClockParameter
derive genShow ClockParameter
