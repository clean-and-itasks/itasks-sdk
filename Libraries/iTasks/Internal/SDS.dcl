definition module iTasks.Internal.SDS

import Data.GenEq
import System.FilePath, Data.Either, Data.Error, System.Time, Text.GenJSON
from Data.Set import :: Set
from iTasks.Internal.IWorld import :: IWorld
from iTasks.Internal.Generic.Visualization import :: TextFormat

from iTasks.WF.Definition import class iTask
from iTasks.WF.Definition import :: TaskException, :: TaskId, :: InstanceNo
import iTasks.SDS.Definition

instance Identifiable SDSSource
instance Readable SDSSource
instance Writeable SDSSource
instance Modifiable SDSSource
instance Registrable SDSSource

instance Identifiable SDSLens
instance Readable SDSLens
instance Writeable SDSLens
instance Modifiable SDSLens
instance Registrable SDSLens

instance Identifiable SDSCache
instance Readable SDSCache
instance Writeable SDSCache
instance Modifiable SDSCache
instance Registrable SDSCache

instance Identifiable SDSSequence
instance Readable SDSSequence
instance Writeable SDSSequence
instance Modifiable SDSSequence
instance Registrable SDSSequence

instance Identifiable SDSSelect
instance Readable SDSSelect
instance Writeable SDSSelect
instance Modifiable SDSSelect
instance Registrable SDSSelect

instance Identifiable SDSParallel
instance Readable SDSParallel
instance Writeable SDSParallel
instance Modifiable SDSParallel
instance Registrable SDSParallel

instance Identifiable SDSBox
instance Readable SDSBox
instance Writeable SDSBox
instance Modifiable SDSBox
instance Registrable SDSBox

instance Identifiable SDSRemoteService
instance Readable SDSRemoteService
instance Writeable SDSRemoteService
instance Modifiable SDSRemoteService
instance Registrable SDSRemoteService

instance Identifiable SDSRemoteSource
instance Readable SDSRemoteSource
instance Writeable SDSRemoteSource
instance Modifiable SDSRemoteSource
instance Registrable SDSRemoteSource

instance Identifiable SDSDebug
instance Readable SDSDebug
instance Writeable SDSDebug
instance Modifiable SDSDebug
instance Registrable SDSDebug

instance Identifiable SDSNoNotify
instance Readable SDSNoNotify
instance Writeable SDSNoNotify
instance Modifiable SDSNoNotify
instance Registrable SDSNoNotify

:: DeferredWrite = E. p r w sds: DeferredWrite !p !w !(sds p r w) & TC p & TC r & TC w & RWShared sds

//Internal creation functions:

/**
 * Create an SDS with a read/write function.
 *
 * @param Whether reads should be cached (`True`) or not (`False`).
 * @param The namespace (used to differentiate between sds which have the same id).
 * @param The id, which should uniquely identity the sds within the namespace of the sds.
 * @param The read function, used for retuning a read value for the SDS depending on the param/IWorld.
 * @param The write function, used for writing to the SDS.
 * @result SDS.
 */
createReadWriteSDS ::
	!Bool
	!String
	!String
	!(p *IWorld -> *(MaybeError TaskException r, *IWorld))
	!(p w *IWorld -> *(MaybeError TaskException (SDSNotifyPred p), *IWorld))
	-> SDSSource p r w
	| gHash{|*|} p

/**
 * Create an SDS with a read/write function and a custom amend function.
 *
 * @param Whether reads should be cached (`True`) or not (`False`).
 * @param The namespace (used to differentiate between sds which have the same id).
 * @param The id, which should uniquely identity the sds within the namespace of the sds.
 * @param The read function, used for retuning a read value for the SDS depending on the param/IWorld.
 * @param The write function, used for writing to the SDS.
 * @param The amend function, used for performing a read/write in a single operation.
 * @result SDS.
 */
createReadWriteAmendSDS ::
	!Bool
	!String
	!String
	!(p *IWorld -> *(MaybeError TaskException r, *IWorld))
	!(p w *IWorld -> *(MaybeError TaskException (SDSNotifyPred p), *IWorld))
	!(A.a: p -> (r -> MaybeError TaskException (a, ?w)) -> *IWorld -> *(MaybeError TaskException (a, ?(w, SDSNotifyPred p)), *IWorld))
	-> SDSSource p r w
	| gHash{|*|} p

/**
 * Create a read only sds, where reading the value may return an error.
 *
 * @param Whether reads should be cached (`True`) or not (`False`).
 * @param The namespace (used to differentiate between sds which have the same id).
 * @param The id, which should uniquely identity the sds within the namespace of the sds.
 * @param The read function, used for retuning a read value for the SDS depending on the param/IWorld.
 * @result SDS.
 */
createReadOnlySDS ::
	!Bool !String !String !(p *IWorld -> *(MaybeError TaskException r, *IWorld)) -> SDSSource p r () | gHash{|*|} p

//* Same as {{`createReadOnlySDS`}}, creates the SDS within the iTasks internal SDS namespace.
createInternalReadOnlySDS ::
	!Bool !String !(p *IWorld -> *(MaybeError TaskException r, *IWorld)) -> SDSSource p r () | gHash{|*|} p

// Helper to immediately get the read value from a share. Use only when reading in an EmptyContext.
directResult :: (AsyncRead r w) -> r

:: AsyncRead r w = ReadingDone r
	| E. sds: Reading (sds () r w) & TC r & TC w & Readable sds & Registrable sds

/*
 * Read the SDS. TaskContext is used to determine whether a read is done in the
 * context of a task. The read is performed asynchronously when there is a task
 * context and the share is a asynchronous share.
 *
 * @return A ReadResult, either Queued (a asynchronous read is queued and the
 *	task will be notified when it is ready), or a direct result in the case of
 *	a blocking read.
 */
read 			:: !(sds () r w) 			!TaskContext !*IWorld -> (!MaybeError TaskException (AsyncRead r w), !*IWorld) | TC r & TC w & Readable sds

/*
 * Read an SDS and register a taskId to be notified when it is written.
 *
 * @param Task context (task that issues the request).
 * @param Task id to notify.
 * @param The SDS to register to.
 * @result Read result.
 */
readRegister :: !TaskContext !TaskId !(sds () r w) !*IWorld -> (!MaybeError TaskException (AsyncRead r w), !*IWorld) | TC r & TC w & Readable, Registrable sds

:: AsyncWrite r w = WritingDone
	| E. sds: Writing (sds () r w) & Writeable sds & TC r & TC w

//Write an SDS (and queue evaluation of those task instances which contained tasks that registered for notification)
write			:: !w					    !(sds () r w) !TaskContext !*IWorld -> (!MaybeError TaskException (AsyncWrite r w), !*IWorld)	| TC r & TC w & Writeable sds

/**
 * Util function for writing a Maybe value to a share.
 * If the value provided is ?None, nothing is written.
 * If the value provided is ?Just value, value is written to the SDS.
 *
 * N.B The share that is written to must be synchronous.
 *
 * @param The SDS to write to
 * @param The value to write to the share, ?None = do nothing. ?Just value = write value.
 * @param IWorld
 * @result Either a TaskException or void.
 * @result The IWorld
 */
writeShareIfNeeded :: !(sds () r w) !(?w) !*IWorld -> (!MaybeError TaskException (), !*IWorld) | TC r & TC w & Writeable sds

:: AsyncModify r w a = ModifyingDone (?w) a & TC w & TC a
	| E. sds: Modifying (sds () r w) (r -> (a, ?w)) & Modifiable sds & TC r & TC w & TC a

/**
 * Modify an SDS atomically.
 *
 * @param modification function
 * @param sds
 * @param context
 * @param iworld
 * @result modification result
 */
modify :: !(r -> w) !(sds () r w) !TaskContext !*IWorld -> (!MaybeError TaskException (AsyncModify r w ()), !*IWorld) | TC r & TC w & Modifiable sds

/**
 * Amend an SDS atomically, return an auxiliary and possibly write a new value.
 *
 * @param modification function
 * @param sds
 * @param context
 * @param iworld
 * @result modification result
 */
amend` :: !(r -> (a, ?w)) !(sds () r w) !TaskContext !*IWorld -> (!MaybeError TaskException (AsyncModify r w a), !*IWorld) | TC r & TC w & TC a & Modifiable sds

//Clear all registrations for the given tasks.
//This is normally called by the queueRefresh functions, because once a task is queued
//for evaluation anyway, it no longer make sense to notify it again.
clearTaskSDSRegistrations :: !(Set TaskId) !*IWorld -> *IWorld

queueNotifyEvents :: !SDSIdentity !(Set PossiblyRemoteTaskId) !*IWorld -> *IWorld

//List all current registrations (for debugging purposes)
listAllSDSRegistrations :: !*IWorld -> (![(InstanceNo,[(TaskId,SDSIdentityHash)])], !*IWorld)

//Flush all deffered/cached writes of
flushDeferredSDSWrites :: !*IWorld -> (!MaybeError TaskException (), !*IWorld)

/**
 * Check requests, useful for implementing writeSDS for your own SDS.
 * WriteResult has a set of task Ids as a field, this function can be used to generate this set.
 *
 * @param sds identity
 * @param notification predicate
 * @param iworld
 * @result task id's to notify
 * @result new iworld
 */
checkRequests :: !SDSIdentity !(SDSNotifyPred p) !*IWorld -> (!Set PossiblyRemoteTaskId, !*IWorld) | TC p

/**
 * Register to a sds with a parameter, useful for implementing readRegisterSDS for your own SDS.
 * I.e.
 *
 * @param parameter
 * @param sds
 * @param context
 * @param task id of the task to be notified
 * @param iworld
 * @result new iworld
 */
register :: !p !(sds p r w) !TaskContext !TaskId !*IWorld -> *IWorld | TC, gHash{|*|} p & Registrable sds
