implementation module iTasks.Internal.Test.Stubs

import Data.Either

import iTasks
import iTasks.Internal.IWorld

//TEST STUBS
toStubIWorld :: !*World -> *IWorld
toStubIWorld world
	# (opts, world) = defaultEngineOptions world
	# (Right iworld) = createIWorld {opts & autoLayout = False} world
	= iworld

fromStubIWorld :: !*IWorld -> *World
fromStubIWorld iworld=:{IWorld|world} = world
