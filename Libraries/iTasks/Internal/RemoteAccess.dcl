definition module iTasks.Internal.RemoteAccess

import StdString
from iTasks.Internal.IWorld import :: IWorld
import Internet.HTTP, Text.URI

httpRequest :: !HTTPMethod !URI !(?String) !IWorld -> (!HTTPResponse, !IWorld)
