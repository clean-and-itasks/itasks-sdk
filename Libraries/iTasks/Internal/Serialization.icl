implementation module iTasks.Internal.Serialization

import StdEnv
import dynamic_string
import graph_copy_with_names

import Base64
import Data.Error
import Text.GenJSON

import iTasks.Internal.Util

serialize :: !a -> *String
serialize value = copy_to_string value

deserialize	:: !*String -> MaybeErrorString a
deserialize str = let (a,_) = (copy_from_string str) in (Ok a)

serializeDynamic :: !Dynamic -> *String
serializeDynamic dyn = dynamic_to_string dyn

deserializeDynamic :: !*String -> Dynamic
deserializeDynamic str = string_to_dynamic str

JSONEncode{|Dynamic|} _ dyn = [JSONArray [JSONString "_DYNAMIC_", JSONString (base64URLEncode (serializeDynamic dyn))]]

JSONEncode{|(->)|} _ _ _ f = [JSONArray [JSONString "_FUNCTION_", JSONString (base64URLEncode (serialize f))]]

JSONDecode{|Dynamic|} _ [JSONArray [JSONString "_DYNAMIC_",JSONString string]:c] = (?Just (deserializeDynamic (base64URLDecodeUnsafe string)), c)
JSONDecode{|Dynamic|} _ c = (?None, c)

JSONDecode{|(->)|} _ _ _ [JSONArray [JSONString "_FUNCTION_",JSONString string]:c] = (?Just (fst(copy_from_string {s` \\ s` <-: base64URLDecodeUnsafe string})) ,c)
JSONDecode{|(->)|} _ _ _ c = (?None,c)

functionFree :: !JSONNode -> Bool
functionFree (JSONString "_FUNCTION_") = False
functionFree (JSONString "_DYNAMIC_") = False
functionFree (JSONString "_DYNAMICENCODE_") = False
functionFree (JSONArray items) = and (map functionFree items)
functionFree (JSONObject fields) = and (map (functionFree o snd) fields)
functionFree _ = True

dynamicJSONEncode :: !a -> JSONNode
dynamicJSONEncode f = JSONArray [JSONString "_DYNAMICENCODE_",JSONString (base64URLEncode (copy_to_string f))]

dynamicJSONDecode :: !JSONNode -> ?a
dynamicJSONDecode (JSONArray [JSONString "_DYNAMICENCODE_",JSONString str]) = ?Just (fst (copy_from_string (base64URLDecodeUnsafe str)))
dynamicJSONDecode _ = ?None

deserializeFromBase64 :: !String !{#Symbol} -> a
deserializeFromBase64 input symbols
# decoded = base64DecodeUnsafe input
# string = toString decoded
# json = fromJSON (fromString string)
= case json of
	(?Just data)
		# (x, y, z) = deserializeFromString data
		= fst (copy_from_string_with_names x y z symbols)
	?None
		= abort ("Could not deserialize.\nInput:\n" +++ input +++ "\n\nString:\n" +++ string)

// We evaluate the argument to normal form due to some unknown laziness which creates dependency on the whole iTasks library.
eval :: !a -> Bool
eval a = code {
	.d 1 0
	jsr	_eval_to_nf
	.o 0 0
	pushB TRUE
}

serializeToBase64 :: !a -> String
serializeToBase64 item
| eval item = base64Encode (toString (toJSON (serializeToString (copy_to_string_with_names item))))

serializeToString :: !(!String,!*{#DescInfo},!*{#String}) -> [String]
serializeToString (state, descInfo, module) = ["State", base64Encode state : packDescInfo [e \\ e <-: descInfo]] ++ packModule module
where
	packDescInfo :: [DescInfo] -> [String]
	packDescInfo [] = []
	packDescInfo [x:xs] = ["DescInfo", toString (x.di_prefix_arity_and_mod), toString (base64Encode x.di_name) : packDescInfo xs]

	packModule :: {#String} -> [String]
	packModule array = [base64Encode e \\ e <-: array]

deserializeFromString :: [String] -> (!*{#Char},!*{#DescInfo},!*{#String})
deserializeFromString ["State", state : xs] = (base64DecodeUnsafe state, {e \\ e <- desc}, {base64DecodeUnsafe e \\ e <- rest})
where
	(desc, rest) = unpackDescInfo xs

	unpackDescInfo :: [String] -> ([DescInfo], [String])
	unpackDescInfo [] = ([], [])
	unpackDescInfo ["DescInfo", di_prefix_arity_and_mod, di_name : xs]
		= let (x, rest) = unpackDescInfo xs in ([{di_prefix_arity_and_mod = (toInt di_prefix_arity_and_mod), di_name = base64DecodeUnsafe di_name} : x], rest)
	unpackDescInfo xs = ([], xs)
