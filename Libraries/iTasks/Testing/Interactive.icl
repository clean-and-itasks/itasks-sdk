implementation module iTasks.Testing.Interactive

import Data.Func

import iTasks

testEditor :: !(Editor a (EditorReport a)) !(?a) !Bool -> Task a | iTask a
testEditor editor mbval readonly
	= withShared mbval \sds ->
		((if readonly (interactR editor` sds) (interactRW editor` sds))
		>&>
			\s -> Title "Editor value" @>> viewSharedInformation [ViewAs (toString o toJSON)] s @? tvFromMaybe
		) <<@ ArrangeHorizontal
where
	editor` = mapEditorWrite editorReportToMaybe editor

testCommonInteractions :: !String -> Task a | iTask, gDefault{|*|} a
testCommonInteractions typeName
	= 	 (Title "Enter" @>> Hint ("Enter information of type " +++ typeName) @>> enterInformation [] >>! viewInformation [])
	-||- (Title "Update" @>> Hint ("Update default value of type " +++ typeName) @>> updateInformation  [] defaultValue >>! viewInformation [])
	-||- (withShared defaultValue
			\s -> ((Title "Update shared" @>> Hint ("Update shared value of type " +++ typeName) @>> updateSharedInformation [] s)
				   -||
				   (Title "View shared" @>> Hint ("View shared value of type " +++ typeName) @>> viewSharedInformation  [] s)
				  )
		 )
