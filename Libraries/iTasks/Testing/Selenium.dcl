definition module iTasks.Testing.Selenium

from Data.GenEq import generic gEq
from Data.Queue import :: Queue
from System.FilePath import :: FilePath
from Testing.TestEvents import :: TestEvent
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode

from ABC.Interpreter.JavaScript.Monad import :: JS, :: JSVal

from iTasks import :: Task, class iTask
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from iTasks.SDS.Definition import class RWShared, class Registrable,
	class Modifiable, class Readable, class Writeable, class Identifiable
from iTasks.Testing.Selenium.Interface import :: TestState
from iTasks.UI.Definition import :: UIAttribute
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorReport, :: EditorPurpose
from iTasks.UI.Tune import class tune

:: Browser
	= Firefox
	| Chrome
	| Safari
	| Edge

:: ClientTestOptions =
	{ headless                  :: !Bool //* Whether the browser is run in headless mode
	, windowSize                :: !(!Int,!Int) //* Width and height of the window in headless mode
	, timeout                   :: !Int  //* Timeout in ms to use when waiting for elements/events
	, screenshotsForFailedTests :: !Bool //* Whether to automatically make screenshots when tests fail
	}

:: TestOptions =
	{ clientOptions  :: !ClientTestOptions
	, browser        :: !Browser
	, screenshotsDir :: !FilePath //* Path for screenshots
	}

/**
 * Specification of an end-to-end test. The argument is of the format
 * "http://hostname:port". The result should contain the steps to perform,
 * possibly resulting in a promise.
 */
:: TestSpec :== String -> JS TestState JSVal

//* A test specification including the task to test.
:: TestedTask = E.a: TestedTask !(Task a) !TestSpec & iTask a

//* Tasks can be `tune`d with values of this type.
:: TestProperty
	= Name !String //* Gives a name to a task so that it can be found with `ByTestName`.

derive class iTask \ JSONEncode, JSONDecode TestEvent

instance tune TestProperty (Task a)

//* For internal use only.
testPropertyToUIAttribute :: !TestProperty -> UIAttribute

/**
 * Connect to a test driver and run a test.
 * @param The test options.
 * @param The test specification.
 */
runTest :: !TestOptions !TestSpec !(sds () (Queue TestEvent) (Queue TestEvent)) -> Task [TestEvent] | RWShared sds

//* Run a number of test specifications and output test events to `stdout`.
runTestSuite :: !TestOptions ![TestedTask] !*World -> *World

//* Like {{`runTestSuite`}}, but uses a simple CLI.
runTestSuiteWithCLI :: ![TestedTask] !*World -> *World
