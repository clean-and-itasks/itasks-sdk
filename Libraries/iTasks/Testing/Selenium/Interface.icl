implementation module iTasks.Testing.Selenium.Interface

import StdEnv
import StdGeneric

import Control.Monad => qualified join
from Data.Func import $
import Data.Functor
import Data.List
import Data.Maybe
import Testing.TestEvents
from Text import class Text(concat,join,split), instance Text String, concat3, concat4
import Text.GenJSON

import ABC.Interpreter.JavaScript.Monad

from iTasks.UI.Editor.Generic import toLabelText
import iTasks.UI.Layout.Common
import iTasks.Testing.Selenium

/* Workarounds in this file:
 *
 * 1. Most of the functions here do not have continuations, because
 *    `seleniumTest` uses promises. In `gInput` we still need a continuation,
 *    because of the PAIR instance. In `gInput` we keep a list of input fields,
 *    which is updated in the recursion after some fields have been filled in:
 *    after the left side of the PAIR has been evaluated, the right side must
 *    continue with the remaining unfilled fields. This can in principle be
 *    done in a promise monad because the fields are just `JSVal`s, but then
 *    we would have to unpack the JavaScript list that the left side resolved
 *    to in every call to the PAIR instance; a continuation is faster because
 *    we do not need to convert the field list in every step.
 */

instance toString Browser
where
	toString b = case b of
		Firefox -> "firefox"
		Chrome  -> "chrome"
		Safari  -> "safari"
		Edge    -> "MicrosoftEdge"

buttonToString :: !Button -> String
buttonToString LeftButton   = "LEFT"
buttonToString MiddleButton = "MIDDLE"
buttonToString RightButton  = "RIGHT"

keyToString :: !Key -> String
keyToString ArrowDownKey  = "ARROW_DOWN"
keyToString ArrowLeftKey  = "ARROW_LEFT"
keyToString ArrowRightKey = "ARROW_RIGHT"
keyToString ArrowUpKey    = "ARROW_UP"
keyToString TabKey        = "TAB"

getComponent :: JS st JSVal
getComponent = gets \s -> s.component

getDriver :: JS st JSVal
getDriver = gets \s -> s.component .# "driver"

withComponent :: !(JSVal -> JS st a) -> JS st a
withComponent f = getComponent >>= f

withDriver :: !(JSVal -> JS st a) -> JS st a
withDriver f = getDriver >>= f

newTestState :: !ClientTestOptions -> TestState
newTestState clientOptions =
	{ testNamePrefix = ""
	, clientOptions  = clientOptions
	}

seleniumTest :: !String !(JS TestState JSPromise) -> JS TestState JSPromise
seleniumTest desc eval =
	gets (\st -> st.state.testNamePrefix) >>= \prefix ->
	withComponent \comp ->
	fromJS -1 <$>
	accJS (comp .# "startTest" .$ (prefix+++desc)) >>= \id ->
	tryPromise (
		eval `then` \_ ->
		finishTest id Passed
	)
		\err
			| jsIsUndefined err || jsIsNull err ->
				finishTest id (Failed ?None)
			| otherwise ->
				accJS (jsNew "String" err) >>= \err ->
				finishTest id $ Failed $ ?Just $ CustomFailReason $ fromJS "unknown error" err

finishTest :: !Int !EndEventType -> JS TestState JSVal
finishTest id Passed =
	withComponent \comp ->
	accJS (comp .# "finishTest" .$ (id,True))
finishTest id (Failed reason) =
	withComponent \comp ->
	mbTakeScreenshot comp `then` \_ ->
	case reason of
		?None ->
			appJS (comp .# "finishTest" .$! (id,False))
		?Just (CustomFailReason reason) ->
			appJS (comp .# "finishTest" .$! (id,False,reason))
		?Just _ ->
			abort "selenium tests do not support this fail reason\n"
where
	mbTakeScreenshot comp =
		gets (\st -> st.state.TestState.clientOptions.screenshotsForFailedTests) >>= \make_screenshot
			| make_screenshot ->
				accJS ((.?) (comp .# "tests" .# id .# "name")) >>= \name ->
				takeScreenshot ("failed-"+++fromJS (toString id) name+++".png") NoElement
			| otherwise ->
				resolvePromise ()
finishTest _ Skipped =
	abort "selenium tests do not support skipped tests\n"

prefixTest :: !String !(JS TestState a) -> JS TestState a
prefixTest s f =
	gets (\st -> st.state.testNamePrefix) >>= \old ->
	modState (\st -> {st & state.testNamePrefix=s+++old}) >>|
	f >>= \r ->
	modState (\st -> {st & state.testNamePrefix=old}) $>
	r

fail :: !String -> JS TestState JSPromise
fail reason = rejectPromise reason

expect :: !String !Bool -> JS TestState JSPromise
expect desc ok = if ok (resolvePromise ()) (fail desc)

createWebDriver :: !String !ClientTestOptions -> JS TestState ()
createWebDriver browser clientTestOptions =
	gets (\st -> st.state.TestState.clientOptions.headless) >>= \headless ->

	accJS (jsNew "Builder" ()) >>= \builder ->

	accJS (jsNew "firefox.Options" ()) >>= \firefox_options ->
	if headless (accJS (firefox_options .# "addArguments" .$ "-headless")) (pure jsNull) >>|
	if headless (accJS (firefox_options .# "windowSize" .$ window_size)) (pure firefox_options) >>= \firefox_options ->
	accJS (builder .# "setFirefoxOptions" .$ firefox_options) >>|

	accJS (jsNew "chrome.Options" ()) >>= \chrome_options ->
	if headless (accJS (chrome_options .# "addArguments" .$ "headless")) (pure jsNull) >>|
	if headless (accJS (chrome_options .# "windowSize" .$ window_size)) (pure chrome_options) >>= \chrome_options ->
	accJS (builder .# "setChromeOptions" .$ chrome_options) >>|

	accJS (builder .# "forBrowser" .$ browser) >>|
	accJS (builder .# "build" .$ ()) >>= \the_driver ->
	withComponent \comp ->
	accJS \w -> ((),(comp .# "driver" .= the_driver) w)
where
	window_size = jsRecord ["width" :> w, "height" :> h]
	where
		(w,h) = clientTestOptions.windowSize

quitWebDriver :: JS TestState ()
quitWebDriver =
	withComponent \comp ->
	accJS (comp .# "destroy" .$ ()) >>| pure ()

withWebDriver :: !Browser !ClientTestOptions !String !String !(JS TestState JSVal) -> JS TestState JSVal
withWebDriver browser clientTestOptions name url tests =
	appJS (jsTrace (concat ["Testing ",name," at ",url,"..."])) >>|
	createWebDriver (toString browser) clientTestOptions >>|
	tests `then` \_ ->
	quitWebDriver >>|
	withComponent \comp ->
	accJS (comp .# "close" .$ ())

sleep :: !Int -> JS TestState JSVal
sleep ms = withDriver \driver -> accJS (driver .# "sleep" .$ ms)

takeScreenshot :: !String !MaybeElement -> JS TestState JSVal
takeScreenshot filename elem =
	withDriver \driver ->
	accJS (root driver .# "takeScreenshot" .$ ()) `then` \png ->
	withComponent \comp ->
	accJS (comp .# "send" .$ ["Screenshot",filename,fromJS "" png])
where
	root driver = case elem of
		Element e -> e
		NoElement -> driver

getURL :: !String -> JS TestState JSVal
getURL url =
	withDriver \driver ->
	accJS (driver .# "get" .$ url) `then` \_ ->
	executeScript installWebSocketSniffer () `then` \_ ->
	executeScript installTimeoutHandlers () `then` \_ ->
	gets (\st -> st.state.TestState.clientOptions.headless) >>= \headless ->
	if headless (executeScript addHeadlessCSS ()) (resolvePromise ())
where
	installWebSocketSniffer = concat
		[ "const conn=itasks.components.get(document.body).connection;"
		, "itasks.messages=[];"
		, "const old_send=conn._send;"
		, "conn._send=function (msg) {"
			, "console.info('-->',msg);"
			, "itasks.messages.push({sent: true, time: new Date().getTime(), msg: msg});"
			, "old_send.apply(conn,arguments);"
		, "};"
		, "const old_onMessage=conn.onMessage_;"
		, "conn.wsock.onmessage=function (ev) {"
			, "console.info('<--',JSON.parse(ev.data));"
			, "itasks.messages.push({sent: false, time: new Date().getTime(), msg: JSON.parse(ev.data)});"
			, "old_onMessage.apply(conn,arguments);"
		, "};"
		]
	installTimeoutHandlers = concat
		[ "let _setTimeout=window.setTimeout;"
		, "let _clearTimeout=window.clearTimeout;"
		, "window.timeouts=new Map();"
		, "window.timeout_id=0;"
		, "window.setTimeout=function(_fun,ms){"
			, "let i=window.timeout_id++;"
			, "let fun=function(){ window.timeouts.delete(i); _fun(); };"
			, "let id=_setTimeout(fun,ms);"
			, "window.timeouts.set(i, {id: id, fun: fun});"
			, "return i;"
		, "};"
		, "window.clearTimeout=function(id){"
			, "if (!window.timeouts.has(id)) return;"
			, "_clearTimeout(window.timeouts.get(id).id);"
			, "window.timeouts.delete(id);"
		, "};"
		, "window.triggerTimeout=function(id){"
			, "if (!window.timeouts.has(id)) return;"
			, "let to=window.timeouts.get(id);"
			, "_clearTimeout(to.id);"
			, "to.fun();"
		, "};"
		, "window.triggerAllTimeouts=function(){"
			, "Array.from(window.timeouts.keys()).forEach(window.triggerTimeout);"
		, "};"
		]
	addHeadlessCSS = concat
		[ "var css=document.createElement('link');"
		, "css.rel='stylesheet';"
		, "css.type='text/css';"
		, "css.async=false;"
		, "document.head.appendChild(css);"
		, "css.href='/css/selenium-headless.css';"
		]

waitForIdle :: !JSVal !(JS TestState JSVal) -> JS TestState JSVal
waitForIdle task continuation =
	executeScript find_instance_no task `then` \instance_no ->
	withComponent \comp ->
	accJS (comp .# "waitForIdle" .$ instance_no) `then` \_ -> continuation
where
	find_instance_no = concat
		[ "var comp=itasks.components.get(arguments[0]);"
		, "while (!('instanceNo' in comp.attributes)) comp=comp.parentCmp;"
		, "return comp.attributes.instanceNo;"
		]

doAndWaitForUIEvent :: !(JS TestState JSVal) -> JS TestState JSVal
doAndWaitForUIEvent eval = doAndWaitForMessage 100 "ui-event" eval

doAndWaitForUIChange :: !(JS TestState JSVal) -> JS TestState JSVal
doAndWaitForUIChange eval = doAndWaitForMessage 100 "ui-change" eval

expectElementToEnable :: !String !JSVal -> JS TestState JSVal
expectElementToEnable desc elem = expectElementTo desc elem "elementIsEnabled"

expectElementToDisable :: !String !JSVal -> JS TestState JSVal
expectElementToDisable desc elem = expectElementTo desc elem "elementIsDisabled"

expectElementToBecomeVisible :: !String !JSVal -> JS TestState JSVal
expectElementToBecomeVisible desc elem = expectElementTo desc elem "elementIsVisible"

expectElementTo :: !String !JSVal !String -> JS TestState JSVal
expectElementTo desc elem what =
	withDriver \driver ->
	gets (\s -> s.state.TestState.clientOptions.timeout) >>= \timeout ->
	accJS (jsGlobal "until" .# what .$ elem) >>= \until ->
	tryPromise (
		accJS (driver .# "wait" .$ (until, timeout))
	) \_ -> fail desc

doAndWaitForMessage :: !Int !String !(JS TestState JSVal) -> JS TestState JSVal
doAndWaitForMessage min_time_since msg_type eval =
	withDriver \driver ->
	executeScript "window.triggerAllTimeouts();" () `then` \_ ->
	executeScript "return itasks.messages.length;" () `then` \n_messages ->
	case jsValToInt n_messages of
		?None   -> abort "failed to get itasks.messages.length\n"
		?Just n ->
			eval `then` \_ ->
			gets (\s -> s.state.TestState.clientOptions.timeout) >>= \timeout ->
			accJS (driver .# "wait" .$ (jsGlobal ("Function('driver',\""+++condition n+++"\")"), timeout))
where
	condition nmsg = concat
		[ "return driver.executeScript(function(){"
			, "if (window.timeouts.size > 0) return false;"
			, "var without_reply=new Set();"
			, "var message_seen=false;"
			, "for (var i=",toString nmsg,"; i<itasks.messages.length; i++) {"
				, "if (itasks.messages[i].sent){"
					, "without_reply.add(itasks.messages[i].msg[0]);"
				, "} else {"
					, "without_reply.delete(itasks.messages[i].msg[0]);"
					, "if (itasks.messages[i].msg[1]=='",msg_type,"')"
						, "message_seen|=new Date().getTime()-itasks.messages[i].time>",toString min_time_since,";"
				, "}"
			, "}"
			, "return message_seen && without_reply.size==0;"
		, "});"
		]

findElement :: !ElementLocator !MaybeElement -> JS TestState JSVal
findElement loc root = findElementOrElements False loc root

findElements :: !ElementLocator !MaybeElement -> JS TestState JSVal
findElements loc root = findElementOrElements True loc root

/* NB: we need some hacks here due to a bug in selenium:
 * https://github.com/SeleniumHQ/selenium/issues/7559
 * This bug means we cannot use a JavaScript locator on a specific element -
 * so, in the ByTestName and ByLabel (=ByDefaultLabel) case, we set
 * itasks._tmp_root with the root element and use a JavaScript locator on the
 * driver instead of the element; this locator then uses itasks._tmp_root.
 */
findElementOrElements :: !Bool !ElementLocator !MaybeElement -> JS TestState JSVal
findElementOrElements list (ByDefaultLabel l) root =
	findElementOrElements list (ByLabel (toLabelText l)) root

findElementOrElements list loc root =
	installRootElement loc `then` \_ ->
	accJS (jsGlobal "By" .# byFunction .$ byArg) >>= \locator ->
	case root of
		Element root | byFunction <> "js" ->
			accJS (root .# if list "findElements" "findElement" .$ locator)
		_
		| list ->
			withDriver \driver ->
			accJS (driver .# "findElements" .$ locator)
		| otherwise ->
			gets (\s -> s.state.TestState.clientOptions.timeout) >>= \timeout ->
			accJS (jsGlobal "until" .# "elementLocated" .$ locator) >>= \until ->
			withDriver \driver ->
			accJS (driver .# "wait" .$ (until, timeout))
where
	(byFunction,byArg) = by loc
	by loc = case loc of
		ByClassName c -> ("className",c)
		ByCSS s       -> ("css",s)
		ByXPath p     -> ("xpath",p)
		ById id       -> ("id",id)
		ByTestName n
			-> ("js",script (testPropertyToUIAttribute (Name n)))
			with
				script (prop,JSONString name) = recursiveSearch
					["elem.attributes['",prop,"']==\"",jsonEscape name,"\""]
					"[elem.domEl]"
		ByLabel l
			-> ("js",recursiveSearch
				["elem.type=='Label' && elem.attributes.text.match(/",l,"\\*?:?/)"] // TODO: escape special characters
				input_field_description)
		ButtonByLabel l
			-> ("js",recursiveSearch
				["elem.type=='Button' && elem.attributes.text==\"",jsonEscape l,"\""]
				"[elem.domEl]")
	installRootElement loc
		| byFunction == "js" = executeScript
			"itasks._tmp_root=arguments[0]===null ? document.body : arguments[0];"
			(case root of Element r -> r; NoElement -> jsNull)
		| otherwise =
			resolvePromise ()

	input_field_description = foldr (\a b -> a+++".concat("+++b+++")") "[]" $
		["Array.from(elem.parentCmp.domEl.getElementsByTagName('"+++tag+++"'))" \\ tag <- ["input","select"]] ++
		["Array.from(elem.parentCmp.domEl.getElementsByClassName('"+++cls+++"'))" \\ cls <- ["itasks-textview"]]

	recursiveSearch :: ![String] !String -> String
	recursiveSearch condition returns = concat $
		[ "let sortfun=(a,b) => a.compareDocumentPosition(b) & 2 ? 1 : -1;"
		, "var elems=[];"
		, "var results=[];"
		, "elems.push (itasks.components.get (itasks._tmp_root));"
		, "console.log (itasks._tmp_root);"
		, "while (elems.length) {"
			, "var elem=elems.shift();"
			, "console.log(elem, elem.type, elem.attributes);"
			, "if ("]++condition++[") {"
				, "results = results.concat(", returns, ");"
				, if list "" "if (results.length) return results.sort(sortfun)[0];"
			, "}"
			, "if (elem.children) elems=elems.concat (elem.children);"
		, "}"
		, if list "return results.sort(sortfun);" "return false;"
		]

getFocusedElement :: JS TestState JSVal
getFocusedElement = executeScript "return document.activeElement" ()

elementsAreEqual :: !JSVal !JSVal -> JS TestState JSVal
elementsAreEqual a b = accJS (jsGlobal "WebElement.equals" .$ (a, b))

execOnElem :: !String !a !JSVal !(JSVal -> b) !(b -> JS TestState JSVal) -> JS TestState JSVal | toJSArgs a
execOnElem fun args elem transform continuation =
	accJS (elem .# fun .$ args) `then` continuation o transform

clear :: !JSVal -> JS TestState JSVal
clear elem = accJS (elem .# "clear" .$ ())

click :: !JSVal -> JS TestState JSVal
click elem = accJS (elem .# "click" .$ ())

press :: !Button !JSVal -> JS TestState JSVal
press button elem =
	withDriver \driver ->
	accJS (driver .# "actions" .$ ()) >>= \actions ->
	accJS (actions .# "move" .$ jsRecord ["origin" :> elem]) >>= \actions ->
	let jsButton = jsGlobal "Button" .# buttonToString button in
	accJS (actions .# "press" .$ jsButton) >>= \actions ->
	accJS (actions .# "perform" .$ ())

release :: !Button -> JS TestState JSVal
release button =
	withDriver \driver ->
	accJS (driver .# "actions" .$ ()) >>= \actions ->
	let jsButton = jsGlobal "Button" .# buttonToString button in
	accJS (actions .# "release" .$ jsButton) >>= \actions ->
	accJS (actions .# "perform" .$ ())

pressKey :: !Key -> JS TestState JSVal
pressKey key =
	withDriver \driver ->
	accJS (driver .# "actions" .$ ()) >>= \actions ->
	let jsKey = jsGlobal "Key" .# keyToString key in
	accJS (actions .# "keyDown" .$ jsKey) >>= \actions ->
	accJS (actions .# "keyUp" .$ jsKey) >>= \actions ->
	accJS (actions .# "perform" .$ ())

getAttribute :: !String !JSVal !(String -> JS TestState JSVal) -> JS TestState JSVal
getAttribute attr elem continuation =
	execOnElem "getAttribute" attr elem (fromJS "") continuation

getClasses :: !JSVal !([String] -> JS TestState JSVal) -> JS TestState JSVal
getClasses elem continuation =
	getAttribute "class" elem (continuation o split " ")

getCssValue :: !String !JSVal !(String -> JS TestState JSVal) -> JS TestState JSVal
getCssValue prop elem continuation =
	execOnElem "getCssValue" prop elem (fromJS "") continuation

expectCssValue :: !String !String !JSVal -> JS TestState JSPromise
expectCssValue cssProperty expectedValue element =
	getCssValue
		cssProperty
		element
		(	\value ->
			if (value == expectedValue)
				(resolvePromise ())
				(	fail $
					concat4
						"Expected CSS value did not match actual value. Expected: "
						expectedValue
						" Actual: "
						value
				)
		)

getTagName :: !JSVal !(String -> JS TestState JSVal) -> JS TestState JSVal
getTagName elem continuation =
	execOnElem "getTagName" () elem (fromJS "") continuation

getText :: !JSVal !(String -> JS TestState JSVal) -> JS TestState JSVal
getText elem continuation =
	execOnElem "getText" () elem (fromJS "") continuation

getValue :: !JSVal !(JSVal -> a) !(a -> JS TestState JSVal) -> JS TestState JSVal
getValue elem transform continuation =
	execOnElem "getAttribute" "value" elem transform continuation

isDisplayed :: !Bool !JSVal !(Bool -> JS TestState JSVal) -> JS TestState JSVal
isDisplayed default elem continuation =
	execOnElem "isDisplayed" () elem (fromJS default) continuation

isEnabled :: !Bool !JSVal !(Bool -> JS TestState JSVal) -> JS TestState JSVal
isEnabled default elem continuation =
	execOnElem "isEnabled" () elem (fromJS default) continuation

isSelected :: !Bool !JSVal !(Bool -> JS TestState JSVal) -> JS TestState JSVal
isSelected default elem continuation =
	execOnElem "isSelected" () elem (fromJS default) continuation

focusElement :: !JSVal -> JS TestState JSVal
focusElement elem = executeScript "arguments[0].focus();" elem

sendKeys :: !Keys !JSVal -> JS TestState JSVal
sendKeys (TextKeys keys) elem = accJS (elem .# "sendKeys" .$ keys)

submit :: !JSVal -> JS TestState JSVal
submit elem = accJS (elem .# "submit" .$ ())

input :: !(String -> String) !a !MaybeElement -> JS TestState JSVal | gInput{|*|} a
input labelName val root =
	gInput{|*|} labelName GIM_Enter val root (pure jsNull) [] [] (const (resolvePromise ()))

check :: !(String -> String) !a !MaybeElement -> JS TestState JSVal | gInput{|*|} a
check labelName val root =
	gInput{|*|} labelName GIM_Check val root (pure jsNull) [] [] (const (resolvePromise ()))

generic gInput a
	::
		!(String -> String)              // To convert field names to labels
		!GInputMode
		!a
		!MaybeElement                    // root element
		!(JS TestState JSVal)            // to find fields (used in CONS because the view changes)
		![JSVal]                         // input/view fields
		![String]                        // field names
		!([JSVal] -> JS TestState JSVal) // continuation (see workaround 1)
	-> JS TestState JSVal

gInput{|String|} _ GIM_Enter s _ _ [field:fields] _ next =
	clear field `then` \_ ->
	sendKeys (TextKeys s) field `then` const (next fields)
gInput{|String|} _ GIM_Check s _ _ [field:fields] field_names next =
	getValue field jsValToString \val ->
	expect (input_error field_names s (fromMaybe "?None" val)) (val == ?Just s) `then` \_ ->
	next fields
gInput{|String|} _ _ _ _ _ _ _ _ = abort "gInput{|String|} failed\n"

gInput{|Bool|} _ GIM_Enter b _ _ [field:fields] _ next =
	let next` = next fields in
	getAttribute "checked" field \val -> case val of
		"" -> if b (click field `then` const next`) next`
		_  -> if b next` (click field `then` const next`)
gInput{|Bool|} _ GIM_Check b _ _ [field:fields] field_names next =
	getAttribute "checked" field \val ->
	expect (input_error field_names (toString b) (toString (not b))) (if b (<>) (==) "" val) `then` \_ ->
	next fields
gInput{|Bool|} _ _ _ _ _ _ _ _ = abort "gInput{|Bool|} failed\n"

gInput{|Int|}  labelName mode i root fetchFields fields field_names next =
	gInput{|*|} labelName mode (toString i) root fetchFields fields field_names next
gInput{|Char|} labelName mode c root fetchFields fields field_names next =
	gInput{|*|} labelName mode (toString c) root fetchFields fields field_names next
gInput{|Real|} labelName mode r root fetchFields fields field_names next =
	gInput{|*|} labelName mode (toString r) root fetchFields fields field_names next

gInput{|UNIT|} _ _ _ _ _ fields _ next = next fields

gInput{|EITHER|} fl fr labelName mode x root fetchFields fields field_names next =
	case x of
		LEFT  l -> fl labelName mode l root fetchFields fields field_names next
		RIGHT r -> fr labelName mode r root fetchFields fields field_names next

gInput{|PAIR|} fx fy labelName mode (PAIR x y) root fetchFields fields field_names next =
	fx labelName mode x root fetchFields fields field_names \fields ->
	fy labelName mode y root fetchFields fields field_names next

gInput{|FIELD of {gfd_name}|} fx labelName mode (FIELD x) root _ fields field_names next =
	let fetchFields = findElements (ByLabel (labelName gfd_name)) root in
	fetchFields `then` \new_fields ->
	accJS (jsValToList` new_fields id) >>= \new_fields ->
	fx labelName mode x root fetchFields (new_fields ++ fields) [gfd_name:field_names] next

gInput{|CONS of {gcd_name,gcd_arity,gcd_index}|} fx labelName GIM_Enter (CONS x) root fetchFields [field:fields] field_names next =
	findElements (ByCSS "option") (Element field) `then` \options ->
	accJS (jsValToList` options id) >>= \options
	| gcd_arity == 0 ->
		click (options !! (gcd_index+1)) `then` \_ ->
		next fields
	| otherwise ->
		doAndWaitForUIEvent (click (options !! (gcd_index+1))) `then` \_ ->
		fetchFields `then` \fields ->
		accJS (jsValToList` fields id) >>= \fields -> case fields of
			[_:fields] -> fx labelName GIM_Enter x root fetchFields fields field_names next
			_          -> abort ("gInput{|CONS of "+++gcd_name+++"|} failed while finding new input fields\n")
gInput{|CONS of {gcd_name}|} fx labelName mode=:GIM_Check (CONS x) root fetchFields [field:fields] field_names next =
	gInput{|*|} labelName mode gcd_name root fetchFields [field] field_names \[] ->
	fx labelName mode x root fetchFields fields field_names next
gInput{|CONS of {gcd_name}|} _ _ _ _ _ _ _ _ _ =
	abort ("gInput{|CONS of "+++gcd_name+++"|} failed\n")

gInput{|RECORD|} fx labelName mode (RECORD x) root fetchFields fields field_names next =
	fx labelName mode x root fetchFields fields field_names next
gInput{|OBJECT|} fx labelName mode (OBJECT x) root fetchFields fields field_names next =
	fx labelName mode x root fetchFields fields field_names next

input_error :: ![String] !String !String -> String
input_error field_names expected actual = concat
	[ "Value of "
	, join "." field_names
	, " was "
	, actual
	, " (expected "
	, expected
	, ")"
	]

executeScript :: !String !a -> JS TestState JSVal | toJSArgs a
executeScript script args =
	withDriver \driver ->
	accJS (toJS [script] .# "concat" .$ args) >>= \args ->
	accJS (driver .# "executeScript" .# "apply" .$ (driver, args))

selectTab :: !String !JSVal -> JS TestState JSVal
selectTab title task =
	findElement (ByCSS "ul.itasks-tabbar") (Element task) `then` \bar ->
	executeScript "return itasks.components.get(arguments[0]).attributes.options.map(o => o.text)" bar `then` \options ->
	accJS (jsValToList` options (fromJS "")) >>= \options ->
	case elemIndex title options of
		?None ->
			rejectPromise (concat3 "selectTab: could not find '" title "'")
		?Just i ->
			findElement (ByCSS ("li:nth-child("+++toString (i+1)+++") a")) (Element bar) `then`
			click `cont`
			findElement (ByXPath ("../div[1]/div[1]/div["+++toString (i+2)+++"]")) (Element bar) `then`
			expectElementToBecomeVisible "selectTab: the tab did not become visible"
