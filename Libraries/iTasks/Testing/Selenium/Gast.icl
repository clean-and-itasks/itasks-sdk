implementation module iTasks.Testing.Selenium.Gast

import ABC.Interpreter.JavaScript
import ABC.Interpreter.JavaScript.Monad
import Control.Monad
import Data.Func
import Gast.Gen
import StdEnv, StdOverloadedList
from Text import class Text(concat), instance Text String

import iTasks.Internal.Generic.Visualization
import iTasks.Testing.Selenium.Interface

withGast ::
	!Int !GenState !(a -> Bool)
	!(a -> JS TestState JSVal)
	-> JS TestState JSVal
	| ggen{|*|}, gText{|*|} a
withGast n genState p testf = Foldl
	(\p val -> p `then` \_ -> testf val)
	(resolvePromise ())
	(Take n $ Filter p values)
where
	values = ggen{|*|} genState
