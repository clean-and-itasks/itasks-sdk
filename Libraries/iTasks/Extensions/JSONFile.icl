implementation module iTasks.Extensions.JSONFile

import Data.Either, Data.Func
import StdBool, StdList, StdFile, StdArray, System.FilePath, System.File, Data.Error, Text.GenJSON, StdString

import iTasks.Internal.IWorld, iTasks.Internal.Task, iTasks.Internal.TaskState
import iTasks.Extensions.Document

:: JSONParseException = CannotParse !String
instance toString JSONParseException
where
	toString (CannotParse msg) = msg

CHUNK_SIZE :== 1048576 // 1M

importJSONFile :: !FilePath -> Task a | iTask a
importJSONFile filename = mkInstantTask eval
where
	eval _ iworld = readJSON filename fromJSON iworld

importJSONDocument  :: !Document -> Task a | iTask a
importJSONDocument {Document|documentId} = mkInstantTask eval
where
	eval _ iworld
		# (filename, iworld) = documentLocation documentId iworld
		# (mbstr, iworld)    = loadDocumentContent documentId iworld
		= case mbstr of
			?Just str -> case fromJSON (fromString str) of
				?Just a -> (Ok a, iworld)
				_       -> (parseException filename, iworld)
			_ -> (openException filename, iworld)

importJSONFileWith :: !(JSONNode -> ?a) !FilePath -> Task a | iTask a
importJSONFileWith parsefun filename = mkInstantTask eval
where
	eval _ iworld = readJSON filename parsefun iworld

createJSONFile :: !String a -> Task Document | iTask a
createJSONFile filename content =
	mkInstantTask \_ = createDocument filename "text/json" (Left $ toString (toJSON content))

exportJSONFile :: !FilePath a -> Task a | iTask a
exportJSONFile filename content = exportJSONFileWith toJSON filename content

exportJSONFileWith :: !(a -> JSONNode) !FilePath a -> Task a | iTask a
exportJSONFileWith encoder filename content = mkInstantTask eval
where
	eval _ iworld = fileTask filename content (writeJSON encoder) iworld

fileTask filename content f iworld=:{IWorld|world}
	# (ok,file,world)	= fopen filename FWriteData world
	| not ok			= (openException filename,{IWorld|iworld & world = world})
	# file				= f content file
	# (ok,world)		= fclose file world
	| not ok			= (closeException filename,{IWorld|iworld & world = world})
	= (Ok content, {IWorld|iworld & world = world})

readJSON filename parsefun iworld=:{IWorld|world}
	# (ok,file,world)	= fopen filename FReadData world
	| not ok			= (openException filename,{IWorld|iworld & world = world})
	# (content,file)	= readAll file
	# (ok,world)		= fclose file world
	| not ok			= (closeException filename,{IWorld|iworld & world = world})
	= case (parsefun (fromString content)) of
		?Just a
			= (Ok a, {IWorld|iworld & world = world})
		?None
			= (parseException filename, {IWorld|iworld & world = world})

readAll file
	# (chunk,file) = freads file CHUNK_SIZE
	| size chunk < CHUNK_SIZE
		= (chunk,file)
	| otherwise
		# (rest,file) = readAll file
		= (chunk +++ rest,file)

writeAll content file
	= fwrites content file

writeJSON encoder content file
	= fwrites (toString (encoder content)) file

ioException s
	# e = FileException s IOError
	= Error (dynamic e, toString e)
openException s
	# e = FileException s CannotOpen
	= Error (dynamic e, toString e)
closeException s
	# e = FileException s CannotClose
	= Error (dynamic e, toString e)

parseException s
	# e = CannotParse ("Cannot parse JSON file " +++ s)
	= Error (dynamic e, toString e)

