implementation module iTasks.Extensions.Contact
import iTasks
import Text.HTML

derive class iTask PhoneNumber

derive gDefault PhoneNumber

instance toString PhoneNumber
where
	toString (PhoneNumber num) = num

instance html PhoneNumber
where
	html (PhoneNumber num) = Text num
