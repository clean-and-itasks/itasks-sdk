implementation module iTasks.Extensions.Form.Pikaday

import StdEnv
import iTasks, Data.Func, Data.Functor
import iTasks.UI.Definition, iTasks.UI.Editor
import iTasks.UI.Editor.Modifiers, iTasks.UI.Editor.Controls
import iTasks.UI.JavaScript
import iTasks.Extensions.DateTime
import qualified Data.Map as DM
import ABC.Interpreter.JavaScript

PIKADAY_JS_URL :== "pikaday/pikaday.js"
PIKADAY_CSS_URL :== "pikaday/css/pikaday.css"
MOMENT_JS_URL :== "momentjs/moment.min.js"

pikadayField :: Editor String String
pikadayField =
	JavaScriptInit initUI @>> leafEditorToEditor
	{LeafEditor|onReset = onReset, onEdit = onEdit, onRefresh = onRefresh, writeValue = writeValue}
where
	onReset attr mbval vst=:{VSt|optional}
		# valAttr = maybe JSONNull JSONString mbval
		# attr = 'DM'.unions [optionalAttr optional, valueAttr valAttr, attr]
		= (Ok (uia UITextField attr, mbval, ?None),vst)

	initUI {FrontendEngineOptions|serverDirectory} me world
		//Load css
		# world      = addCSSFromUrl (serverDirectory+++PIKADAY_CSS_URL) ?None world
		//Defer further action till after the field is created...
		# (cb,world) = jsWrapFun (initDOMEl serverDirectory me) me world
		# world      = (me .# "initDOMEl" .= cb) world
		= world

	beforeRemove me args = snd o (me .# "picker" .# "destroy" .$ ())

	initDOMEl :: !String !JSVal !{!JSVal} !*JSWorld -> *JSWorld
	initDOMEl serverDirectory me args world
		# (cb,world) = jsWrapFun (initDOMEl` me) me world
		# world =
			addJSFromUrls
				[(True,serverDirectory+++MOMENT_JS_URL), (True, serverDirectory+++PIKADAY_JS_URL)]
				(?Just cb)
				me
				world
		= world

	initDOMEl` :: !JSVal !{!JSVal} !*JSWorld -> *JSWorld
	initDOMEl` me args world
		# (cb,world) = jsWrapFun (beforeRemove me) me world
		# world      = (me .# "beforeRemove" .= cb) world
		//Create pikaday object
		# (value,world)     = me .# "attributes.value" .? world
		# (domEl,world)     = me .# "domEl" .? world
		# world             = (domEl .# "value" .= value) world
		//Create onselect/keyup
		# (onSelectCb,world) = jsWrapFun (onSelect me) me world
		# (onKeyupCb,world)  = jsWrapFun (onKeyup me) me world
		# (cfg,world)        = jsEmptyObject world
		# world              = (cfg .# "field" .= domEl) world
		# world              = (cfg .# "format" .= "YYYY-MM-DD") world
		# world              = (cfg .# "firstDay" .= 1) world
		# world              = (cfg .# "onSelect" .= onSelectCb) world
		# world              = (domEl .# "addEventListener" .$! ("keyup", onKeyupCb)) world
		# (picker,world)     = jsNew "Pikaday" cfg world
		# world              = (me .# "picker" .= picker) world
		//Handle attribute changes
		# (cb,world)         = jsWrapFun (onAttributeChange picker me) me world
		# world              = (me .# "onAttributeChange" .= cb) world
		//React to selects
		= world

	onAttributeChange picker me {[0]=name,[1]=value} world
		| jsValToString name <> ?Just "value"
			= world
		# world = (me.# "noEvents" .= True ) world
		# world = (picker .# "setDate" .$! value) world
		# world = (me.# "noEvents" .= False) world
		= world

	onSelect me args world
		# (noEvents,world)  = me .# "noEvents" .?? (False, world)
		| noEvents
			= world
		# (value,world)     = (me .# "picker.toString" .$ "YYYY-MM-DD") world
		# value             = jsValToString value
		# world             = (me .# "doEditEvent" .$! toJSON value) world
		= world

	onKeyup me args world
		# (value,world)    = me .# "domEl.value" .? world
		# value            = jsValToString value
		# world            = (me .# "doEditEvent" .$! toJSON value) world
		= world

	onEdit e _ vst=:{VSt|taskId}
		= (Ok (ChangeUI [SetAttribute "value" (JSONString (fromMaybe "" e))] [], e,?Just (fromMaybe "" e)),vst)

	onRefresh mbnew st vst=:{VSt| optional,taskId}
		| st === mbnew = (Ok (NoChange, st, ?None), vst)
		| otherwise    = (Ok (ChangeUI [SetAttribute "value" (maybe JSONNull JSONString mbnew)] [], mbnew, ?None), vst)

	writeValue mbs = Ok (fromMaybe "" mbs)

pikadayDateField :: Editor Date (EditorReport Date)
pikadayDateField
	= withDynamicHintAttributes "date (yyyy-mm-dd)"
	$ mapEditorRead toString
	$ mapEditorWrite
		(\str
			| str == "" = EmptyEditor
			= case parseDate str of (Ok x) = ValidEditor x ; (Error e) = InvalidEditor [e]
		)
	$ pikadayField
