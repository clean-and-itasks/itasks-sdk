definition module iTasks.Extensions.Form.Pikaday
import iTasks, iTasks.UI.Editor
/**
* Integration of the Pikaday datefield library
*/
pikadayField :: Editor String String
/**
* Editor that is the same as `pikadayField`, but prints and parses `Date` values to strings
* in the `yyyy-mm-dd` format expected by the pikaday field.
* If a string is entered but is not parsable it writes `InvalidEditor` values.
*/
pikadayDateField :: Editor Date (EditorReport Date)
