implementation module iTasks.Extensions.Process

import Data.Error, Data.Func, Data.GenHash
import StdString, StdList
import System.Time
import qualified System.Process
import Text

import iTasks
import iTasks.WF.Definition
import iTasks.WF.Tasks.Core
import iTasks.WF.Tasks.IO
import iTasks.WF.Tasks.Interaction
import iTasks.SDS.Sources.Core
import iTasks.UI.Editor.Modifiers

derive class iTask ProcessInformation, ProcessStatus, CallException

instance toString CallException
where
	toString (CallFailed (_,err)) = "Error calling external process: " +++ err

callProcess ::
	![ViewOption ProcessInformation] !FilePath ![ProcessOption] -> Task ProcessInformation
callProcess vopts fp processOptions
	= withShared [] \stdin->withShared ([], []) \out->
		let s = (mapRead (\(stdout,stderr)->
				{ executable=fp
				, arguments=args
				, stdout=concat stdout
				, stderr=concat stderr
				, status=RunningProcess}) out) in
		(externalProcess {tv_sec=0,tv_nsec=100000000} fp processOptions stdin out -|| viewSharedInformation vopts s)
		>>- \c-> get s @ \s->{s & status=CompletedProcess c}
where
	args = checkForArgs processOptions
	where
		checkForArgs :: ![ProcessOption] -> [String]
		checkForArgs [Arguments args:_] = args
		checkForArgs [_:rest] = checkForArgs rest
		checkForArgs [] = []

instance toString ProcessException
where
	toString (ProcessException pinfo) =
		"Process terminated with non-zero return code. Details: " +++ toSingleLineText pinfo

callProcessAndCheckReturnCode ::
    ![ViewOption ProcessInformation] !FilePath ![ProcessOption] -> Task ProcessInformation
callProcessAndCheckReturnCode vopts fp processOptions =
	callProcess vopts fp processOptions >>- \pinfo=:{ProcessInformation| status} ->
	if (status =: (CompletedProcess 0)) (return pinfo) (throw $ ProcessException pinfo)

callInstantProcess :: !FilePath ![String] !(?FilePath) -> Task Int
callInstantProcess cmd args dir = accWorldError (\world -> 'System.Process'.callProcess cmd args dir world) CallFailed
