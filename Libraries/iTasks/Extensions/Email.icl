implementation module iTasks.Extensions.Email

import Base64
import Data.Func
import Data.Functor
import Data.GenHash
from Data.List import lookup
import StdEnv
import System.Time, System.Time.GenJSON
import Text
import Text.GenPrint
import Text.HTML

import iTasks

sendEmail :: ![EmailOpt] !String ![String] !String !String ![Attachment] -> Task ()
sendEmail opts sender recipients subject body attachments
	= ensureHasDateHeader headers >>- \headersWithDateHeader =
		tcpconnect server port (constShare ())
		{ConnectionHandlers|onConnect=onConnect headersWithDateHeader,onData=onData,onDisconnect=onDisconnect
		,onShareChange = \_ _ = (Ok ?None, ?None, [], False), onDestroy = \_ = (Ok ?None, [])}
		@! ()
where
	server 	= getServerOpt opts
	port	= getPortOpt opts
	headers = getHeadersOpt opts

	// If a date header is already provided as an `EmailOpt`, it is left unchanged.
	ensureHasDateHeader :: ![(String, String)] -> Task [(String, String)]
	ensureHasDateHeader headers
		| isJust (lookup dateHeader headers) = return headers
		= accWorld gmTime >>- \tm = return [(dateHeader, toRfc2822CompliantDateString tm):headers]
	where
		toRfc2822CompliantDateString :: !Tm -> String
		toRfc2822CompliantDateString tm = strfTime "%a, %d %b %Y %H:%M:%S -0000" tm

	//Sending the message with SMTP is essentially one-way communication
	//but we send it in parts. After each part we get a response with a status code.
	//After each message we check if it is a status code we expect.
	messages :: ![(String, String)] -> [(String, Int)]
	messages headers =
			[("",220) //Initially we don't send anything, but wait for the welcome message from the server
			,(smtpHelo, 250)
			,(smtpFrom sender, 250)
			]
		++
			((\recipient -> (smtpTo recipient, 250)) <$> recipients)
		++
			[(smtpData, 354)
			,(smtpBody sender recipients headers subject body attachments, 250)
			,(smtpQuit, 221)
			]

	//Send the first message
	onConnect ::
		![(String, String)] !IOHandle !String !() -> (!MaybeErrorString [(String, Int)], !?(), ![String], !Bool)
	onConnect headersWithDateHeader _ _ _
		= (Ok $ messages headersWithDateHeader,?None,[],False)
	//Response to last message: if ok, close connection
	onData :: !IOHandle !String ![(String, Int)] !() -> (!MaybeErrorString (?[(String, Int)]), !?(), ![String], !Bool)
	onData _ data [(_,expectedCode)] _
		| statusCode data == expectedCode
			= (Ok (?Just []),?None,[],True)
			= (Error data,?None,[],False)
	//Response to other messages: if ok, send next message
	onData _ data [(_,expectedCode):ms] _
		| statusCode data == expectedCode
			= (Ok (?Just ms),?None,[fst (hd ms)],False)
			= (Error data,?None,[],False)

	//We don't expect the server to disconnect before we close
	//the connection ourselves
	onDisconnect _ _ _
		= (Error "SMTP server disconnected unexpectedly",?None)

sendHtmlEmail :: ![EmailOpt] !String ![String] !String !HtmlTag ![Attachment] -> Task ()
sendHtmlEmail opts sender recipients subject body attachments =
	sendEmail
		[EmailOptExtraHeaders [("content-type", "text/html; charset=UTF8"), ("content-disposition", "inline")]: opts]
		sender
		recipients
		subject
		(toString body)
		attachments

// SMTP messages
smtpHelo = "HELO localhost\r\n"
smtpFrom email_from = "MAIL FROM:<" +++ (cleanupEmailString email_from) +++ ">\r\n"
smtpTo email_to = "RCPT TO:<" +++ (cleanupEmailString email_to) +++ ">\r\n"
smtpData = "DATA\r\n"
smtpBody :: !String ![String] ![(String, String)] !String !String ![Attachment] -> String
smtpBody email_from email_to bodyHeaders email_subject email_body attachments =
	concat $ flatten $
			[ headerFor header
			\\ header <-
					[ ("From", cleanupEmailString email_from)
					: (\email_to -> ("To", cleanupEmailString email_to)) <$> email_to
					]
				++
					[ ("Subject", cleanupEmailString email_subject)
					// Make sure `Date` header is not placed in the first part for multipart mails.
					: [header \\ header=:(k, _) <- bodyHeaders | k == dateHeader]]]
		++
			if (isEmpty attachments) [] [["Content-Type: multipart/mixed; boundary=sep\r\n\r\n--sep\r\n"]]
		++
			[headerFor header \\ header=:(k, _) <- bodyHeaders | k <> dateHeader]
		++
			[if (isNone $ lookup "content-type" bodyHeaders)
				["content-type: text/plain; charset=UTF-8\r\n"] [], ["content-transfer-encoding: base64\r\n"]]
		++
			// Need to encode the body as base64 such that html mails do not break because we need to restrict the
			// line length to comply with RFC 2822 (to avoid permanent SMTP errors because lines are too long).
			[["\r\n", concat (withRestrictedLineLength $ base64Encode email_body)]]
		++
			[ flatten $
				[	[ "\r\n--sep\r\n"
					, "content-type: application/octet-stream; name=\"", attachment.Attachment.name, "\"\r\n"
					, "content-disposition: attachment; filename=\"", attachment.Attachment.name, "\"\r\n"
					, "content-transfer-encoding: base64\r\n\r\n"]
				,	withRestrictedLineLength (base64Encode attachment.content)]
			\\ attachment <- attachments]
		++
			if (isEmpty attachments) [["\r\n.\r\n"]] [["\r\n--sep--\r\n.\r\n"]]

headerFor :: !(!String, !String) -> [String]
headerFor (k, v) = [k, ":", v, "\r\n"]

dateHeader :== "Date"

smtpQuit = "QUIT\r\n"

//Utility functions

//Parse the reply of the server into a status code
statusCode :: String -> Int
statusCode msg = toInt (msg % (0,2))

//Strip any newline chars and tabs from a string.
cleanupEmailString :: String -> String
cleanupEmailString s = toString (filter (\x -> not (isMember x ['\r\n\t'])) (fromString s))

getServerOpt [] 						= "localhost"
getServerOpt [EmailOptSMTPServer s:xs]	= s
getServerOpt [x:xs] 					= getServerOpt xs

getPortOpt [] 						= Port 25
getPortOpt [EmailOptSMTPServerPort s:xs]	= s
getPortOpt [x:xs] 					= getPortOpt xs

getHeadersOpt [] 							= []
getHeadersOpt [EmailOptExtraHeaders s:xs]	= s ++ getHeadersOpt xs
getHeadersOpt [x:xs] 						= getHeadersOpt xs

//* Cut into lines of 1000 character (including "\r\n"), to fulfil SMTP standard.
withRestrictedLineLength :: !String -> [String]
withRestrictedLineLength str = reverse $ withRestrictedLineLength` 0 []
where
	withRestrictedLineLength` :: !Int ![String] -> [String]
	withRestrictedLineLength` i acc
		| strSize - i <= 998 = [str % (i, strSize - 1): acc]
		| otherwise          = withRestrictedLineLength` (i + 998) ["\r\n", str % (i, i + 997): acc]

	strSize = size str

instance == Attachment derive gEq

derive class iTask Attachment
derive gPrint Attachment
