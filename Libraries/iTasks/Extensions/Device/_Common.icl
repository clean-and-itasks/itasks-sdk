implementation module iTasks.Extensions.Device._Common

import Data.GenHash
from Text import class Text, instance Text String
import qualified Text as T

import iTasks

:: DeviceRequestState  = { buffer :: String, result :: String }

derive class iTask DeviceRequestState

deviceRequest :: String (String -> Bool) -> Task String
deviceRequest request close
	= tcpconnect "127.0.0.1" (Port 20097) (constShare ())
		{ ConnectionHandlers
		| onConnect      = onConnect
		, onData	 = onData
		, onShareChange  = onShareChange
		, onDisconnect   = onDisconnect
		, onDestroy= \_->(Ok ?None, [])
		}
	>>- \{DeviceRequestState|result} -> return result
where
	onConnect :: !IOHandle !String !() -> (!MaybeErrorString DeviceRequestState, !?(), ![String], !Bool)
	onConnect connId host _
		= ( Ok { buffer = "", result = "" }
		  , ?None
		  , [request +++ "\n"]
		  , False
		  )

	onData :: !IOHandle !String !DeviceRequestState !()
		-> (!MaybeErrorString (?DeviceRequestState), !?(), ![String], !Bool)
	onData _ newData state=:{buffer} _
		#! buffer = buffer +++ newData
		# splitpoint = 'T'.indexOf "\n" buffer
		| splitpoint <> -1
			# result = 'T'.subString 0 splitpoint buffer
			# incomplete = 'T'.dropChars (splitpoint + 1) buffer
			= (Ok (?Just {state & buffer = incomplete, result = result})
			  , ?None
			  , []
			  , close result
			  )
		= (Ok (?Just {state & buffer = buffer}), ?None, [], False)

	onShareChange _ _
		= (Ok ?None, ?None, [], False)

	onDisconnect :: !IOHandle !DeviceRequestState !() -> (!MaybeErrorString (?DeviceRequestState), !?())
	onDisconnect _ _ _
		= (Ok ?None, ?None)
