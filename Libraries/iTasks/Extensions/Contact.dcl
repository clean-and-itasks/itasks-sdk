definition module iTasks.Extensions.Contact
import iTasks
import iTasks.Extensions.EmailAddress
import Text.HTML

//* Phone number
:: PhoneNumber = PhoneNumber !String
instance toString	PhoneNumber
instance html		PhoneNumber

derive class iTask PhoneNumber

derive gDefault PhoneNumber
