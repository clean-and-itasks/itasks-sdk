implementation module iTasks.Extensions.JavaScript

import ABC.Interpreter.JavaScript
import ABC.Interpreter.JavaScript.Monad
import Data.Func
import iTasks
import iTasks.UI.JavaScript

appJSWorld :: !(*JSWorld -> *JSWorld) -> Task ()
appJSWorld f = enterInformation [EnterUsing id $ accJSEditor (\v w->((), f w))] @? \tv->case tv of
	Value (?Just a) s = Value () True
	_ = NoValue

accJSWorld :: !(*JSWorld -> (a, *JSWorld)) -> Task a | iTask a
accJSWorld f = enterInformation [EnterUsing id $ accJSEditor (const f)] @? \tv->case tv of
	Value (?Just a) s = Value a True
	_ = NoValue

runJSMonad :: st !(JS st a) -> Task a | iTask a
runJSMonad st m = enterInformation [EnterUsing id $ accJSEditor (\me -> runJS st me m)] @? \tv->case tv of
	Value (?Just a) s = Value a True
	_ = NoValue

accJSEditor :: !(JSVal *JSWorld -> (a, *JSWorld)) -> Editor (?a) (EditorReport (?a)) | JSONEncode{|*|}, JSONDecode{|*|}, TC a
accJSEditor f = JavaScriptInit initUI @>> leafEditorToEditor
	{LeafEditor | onReset = onReset, onEdit = onEdit, onRefresh = onRefresh, writeValue = writeValue}
where
	onReset attr mbval vst = (Ok (uia UIComponent attr, mbval, ?None),vst)
	initUI _ me world
		# (value, world) = f me world
		# world          = (me .# "doEditEvent" .$! toJSON (?Just (?Just value))) world
		= world
	onEdit e _ vst = (Ok (ChangeUI [SetAttribute "value" (toJSON (fromJust e))] [], e, maybe ?None (?Just o ValidEditor) e),vst)
	onRefresh mbVal _ vst = (Ok (NoChange, mbVal, ?None),vst)
	writeValue _ = Ok EmptyEditor
