definition module iTasks.Extensions.Editors.Select2

/**
 * Allows you to replace standard HTML `select` boxes in editors with Select2
 * input fields (https://select2.org/). This lets the user search for options.
 */

from iTasks.UI.Editor import :: Editor, :: EditorReport
from iTasks.UI.Editor.Controls import :: ChoiceText

/**
 * Replaces HTML `select` boxes in the editor with Select2 input fields
 * (https://select2.org/).
 *
 * @param When `True`, also the `select` boxes of child editors, which are
 *   added dynamically, are modified to use Select2.
 */
withSelect2 :: !Bool !(Editor a w) -> Editor a w

/**
 * A replacement for `dropdown` (iTasks.UI.Editor.Controls) using Select2 input
 * fields (https://select2.org/).
 */
select2Dropdown :: Editor ([ChoiceText], [Int]) [Int]

/**
 * A replacement for `dropdownWithGroups` (iTasks.UI.Editor.Controls) using
 * Select2 input fields (https://select2.org/).
 */
select2DropdownWithGroups :: Editor ([(ChoiceText, ?String)], [Int]) [Int]

/**
 * A replacement for `chooseWithDropdown` (iTasks.UI.Editor.Common) using
 * Select2 input fields (https://select2.org/).
 */
chooseWithSelect2Dropdown :: ![String] -> Editor Int (EditorReport Int)
