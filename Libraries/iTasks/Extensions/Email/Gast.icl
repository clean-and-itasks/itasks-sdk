implementation module iTasks.Extensions.Email.Gast

import Control.GenBimap
import Gast => qualified label
import Gast.Gen
import StdOverloadedList

import iTasks.Extensions.Email

derive genShow Attachment
derive ggen Attachment

