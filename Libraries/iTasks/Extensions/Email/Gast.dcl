definition module iTasks.Extensions.Email.Gast

from iTasks.Extensions.Email import :: Attachment

from Gast import generic genShow, generic ggen, :: GenState

derive genShow Attachment
derive ggen Attachment

