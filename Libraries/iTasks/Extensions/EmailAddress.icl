implementation module iTasks.Extensions.EmailAddress

import StdEnv

import Data.Func
import Data.Functor
import Text.HTML

import iTasks

instance toString EmailAddress
where
	toString (EmailAddress email) = email

instance html EmailAddress
where
	html (EmailAddress email) = ATag [HrefAttr ("mailto:" +++ email)] [Text email]

instance == EmailAddress
where
	(==) (EmailAddress e1) (EmailAddress e2) = e1 == e2

derive class iTask \ gText, gEditor EmailAddress

gText{|EmailAddress|} _ x = gTextWithToString x

gEditor{|EmailAddress|} purpose = mapEditorRead toString (editor purpose)
where
	editor ViewValue = mapEditorWrite (const EmptyEditor) textView
	editor EditValue
		= withDynamicHintAttributes "email address"
		$ mapEditorWrite checkEmailAddress textField

	// Very crude checker
	checkEmailAddress :: !(EditorReport String) -> EditorReport EmailAddress
	checkEmailAddress EmptyEditor = EmptyEditor
	checkEmailAddress (InvalidEditor r) = InvalidEditor r
	checkEmailAddress (ValidEditor address) = checkUserName 0
	where
		checkUserName i
			| i >= size address
				= InvalidEditor ["email addresses must contain the '@' symbol"]
			| address.[i] == '@'
				| i == 0
					= InvalidEditor ["this email address does not contain a user name"]
					= checkDomainName True (i+1)
			| isValid address.[i]
				= checkUserName (i+1)
				= InvalidEditor ["the character '"+++{#address.[i]}+++"' is not allowed"]
		checkDomainName first_call i
			| i >= size address
				= InvalidEditor ["the domain name must contain a period ('.')"]
			| address.[i] == '.'
				| first_call
					= InvalidEditor ["the domain name may not start with a period"]
					= checkTLD True (i+1)
			| isValid address.[i]
				= checkDomainName False (i+1)
				= InvalidEditor ["the character '"+++{#address.[i]}+++"' is not allowed"]
		checkTLD first_call i
			| i >= size address
				| first_call
					= InvalidEditor ["the domain name may not end with a period"]
					= ValidEditor (EmailAddress address)
			| address.[i] == '.'
				| first_call
					= InvalidEditor ["the domain name may not contain two consecutive periods"]
					= checkTLD True (i+1)
			| isValid address.[i]
				= checkTLD False (i+1)
				= InvalidEditor ["the character '"+++{#address.[i]}+++"' is not allowed"]
		isValid c = ' ' < c && c <= '~'

gDefault{|EmailAddress|} = EmailAddress "example@example.org"