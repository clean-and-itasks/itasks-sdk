implementation module iTasks.Extensions.Admin.UserAdmin

import Crypto.Hash.SHA1
import Data.Func, Data.Functor
import Data.GenHash
from Data.List import intersperse, findIndex, getItems, zipWith
import qualified Data.Map as DM
import qualified Data.Set as Set
import Data.Tuple
import Text
import StdEnv
import iTasks, iTasks.Internal.Store
import iTasks.Extensions.CSVFile
import iTasks.UI.Editor
from iTasks.UI.Editor.Common import choiceEditor


derive class iTask UserAccount, StoredUserAccount, StoredCredentials

//Initial root user
ROOT_USER :==
	{ StoredUserAccount
	| displayName = ?Just "Root user"
	, roles       = ["admin","manager"]
	, token       = ?None
	, credentials =
		{ StoredCredentials
		| username           = Username "root"
		, saltedPasswordHash = "1e27e41d50caf2b0516e77c60fc377e8ae5a32ee" // password is "root"
		, salt               = "25wAdtdQcJZOQLETvki7eJFcI7u5XoSO"
		}
	}

AUTHENTICATION_COOKIE_TTL :== 3600 * 24 * 7 //One week validity

userAccounts :: SDSLens () [StoredUserAccount] [StoredUserAccount]
userAccounts =: sdsFocus "UserAccounts" (storeShare NS_APPLICATION_SHARES False InJSONFile (?Just [ROOT_USER]))

users :: SDSLens () [User] ()
users =:
	mapReadWrite
		(\accounts =
			[AuthenticatedUser
				(toString a.StoredUserAccount.credentials.StoredCredentials.username)
				a.StoredUserAccount.roles a.StoredUserAccount.displayName
			\\ a <- accounts
			]
		, \() _ -> ?None
		) (?Just \_ _ -> Ok ()) userAccounts

usersWithRole :: !Role -> SDSLens () [User] ()
usersWithRole role = mapRead (filter (hasRole role)) users
where
	hasRole role (AuthenticatedUser _ roles _) = isMember role roles
	hasRole _ _ = False

userAccountsFile :: SDSLens FilePath [StoredUserAccount] [StoredUserAccount]
userAccountsFile = mapReadWrite (parseUsers,printUsers) ?None fileShare
where
	parseUsers mbText
		= [user \\ ?Just user <- map (parseUser o (split ";")) $ maybe [] (split OS_NEWLINE) mbText]
	parseUser [a,b,c,d,e:rest]
		# token  = case rest of
			[f,g,h] = ?Just {username=Username f,saltedPasswordHash=g,salt=h}
			_ = ?None
		= ?Just {credentials={username=Username a,saltedPasswordHash=d,salt=e},displayName= ?Just b,roles = map trim $ split "," c,token=token}
	parseUser _ = ?None
	printUsers [] _ = ?Just $ ?None
	printUsers users _ = ?Just $ ?Just $ join OS_NEWLINE $ map (join ";" o printUser) users
	printUser {credentials={username=Username username,saltedPasswordHash,salt},displayName,roles,token}
		# ptoken = maybe [] (\{username=Username username,saltedPasswordHash,salt} -> [username,saltedPasswordHash,salt]) token
		= [username, fromMaybe "-" displayName, join "," roles,saltedPasswordHash,salt:ptoken]

userAccountIn :: !(sds () [StoredUserAccount] [StoredUserAccount]) -> SDSLens UserId (?StoredUserAccount) (?StoredUserAccount) | RWShared sds
userAccountIn userAccounts =
	sdsLens "userAccount" (const ()) (SDSRead read) (SDSWrite write) (SDSNotifyWithoutRead notify) ?None userAccounts
where
	read userId accounts = Ok $ listToMaybe [a \\ a <- accounts | identifyUserAccount a == userId]
	write userId accounts ?None = Ok ?None
	write userId accounts (?Just account) = Ok $ ?Just [if (identifyUserAccount a == userId) account a \\ a <- accounts]
	notify p _ t q = p == q

userTokenIn :: !(sds () [StoredUserAccount] [StoredUserAccount]) -> SDSLens String (?StoredUserAccount) () | RWShared sds
userTokenIn userAccounts =
	sdsLens "userToken" (const ()) (SDSRead read) (SDSWriteConst write) (SDSNotifyConst False) ?None userAccounts
where
	read token accounts = Ok $ listToMaybe [a \\ a <- accounts | maybe False (checkToken (split ":" token)) a.StoredUserAccount.token]

	write token () = Ok ?None

activeTokensIn ::
	!(sds () [StoredUserAccount] [StoredUserAccount]) -> SDSLens (Set String) (Set String) () | RWShared sds
activeTokensIn userAccounts =
	sdsLens "activeTokens" (const ()) (SDSRead read) (SDSWriteConst write) (SDSNotifyConst False) ?None userAccounts
where
	read :: !(Set String) ![StoredUserAccount] -> MaybeError TaskException (Set String)
	read tokensToFilter accounts = Ok $
		'Set'.filter
			(\tokenToFilter =
				any (\{StoredUserAccount|token} = maybe False (checkToken (split ":" tokenToFilter)) token) accounts)
			tokensToFilter

	write :: !idc0 !idc1 -> MaybeError TaskException (?[StoredUserAccount])
	write _ _ = Ok ?None

checkToken :: ![String] !StoredCredentials -> Bool
checkToken [tokenpublic,tokenprivate:_] {StoredCredentials|username=(Username uname),saltedPasswordHash,salt}
	= uname == tokenpublic && saltedPasswordHash == sha1 (tokenprivate +++ salt)

identifyUserAccount :: StoredUserAccount -> UserId
identifyUserAccount {StoredUserAccount|credentials={StoredCredentials|username}} = toString username

accountToUser :: !StoredUserAccount -> User
accountToUser {StoredUserAccount|credentials={StoredCredentials|username},displayName,roles} = AuthenticatedUser (toString username) roles displayName

accountDisplayName :: !StoredUserAccount -> String
accountDisplayName {StoredUserAccount|credentials={StoredCredentials|username},displayName= ?Just displayName} = concat4 displayName " (" (toString username) ")"
accountDisplayName {StoredUserAccount|credentials={StoredCredentials|username}} = concat3 "Unnamed (" (toString username) ")"

isValidPassword :: !Password !StoredCredentials -> Bool
isValidPassword (Password password) credentials =
	sha1 (password +++ credentials.salt) == credentials.saltedPasswordHash

authenticateUser :: !Username !Password	!Bool -> Task (?User)
authenticateUser username password persistent = authenticateUserWith username password persistent (userAccountIn userAccounts)

authenticateUserWith :: !Username !Password !Bool (sds UserId (?StoredUserAccount) (?StoredUserAccount)) -> Task (?User) | RWShared sds
authenticateUserWith (Username username) password persistent accountStore
	| persistent
		= checkUserAccount username password
		>>- maybe (return ?None) (\user -> storeUserToken username >-| return (?Just user))
	| otherwise
		= checkUserAccount username password
where
	checkUserAccount username password
		=	get (sdsFocus username accountStore)
		@	(maybe ?None (\a -> if (isValidPassword password a.StoredUserAccount.credentials) (?Just (accountToUser a)) ?None))

	storeUserToken :: String -> Task ()
	storeUserToken username
		= getRandom -&&- getRandom -&&- getRandom
		>>- \(salt,(tokenpublic,tokenprivate)) ->
			set ("auth-token",tokenpublic +++ ":" +++ tokenprivate, ?Just AUTHENTICATION_COOKIE_TTL) currentTaskInstanceCookies
			-&&-
			upd (fmap $ setToken salt tokenpublic tokenprivate) (sdsFocus username accountStore)
		@! ()
	where
		setToken salt tokenpublic tokenprivate account =
			{account & token = ?Just {username=Username tokenpublic,saltedPasswordHash = sha1 (tokenprivate +++ salt), salt = salt}}
		getRandom = get (sdsFocus 32 randomString)

authenticateUsingToken :: Task (?User)
authenticateUsingToken =: authenticateUsingTokenWith (userTokenIn userAccounts)

authenticateUsingTokenWith :: (sds String (?StoredUserAccount) ()) -> Task (?User) | RWShared sds
authenticateUsingTokenWith accountStore
	= watch (mapRead ('DM'.get "auth-token") currentTaskInstanceCookies)
	>>* [OnValue (ifValue isJust (checkToken o fromJust))]
where
	checkToken :: String -> Task (?User)
	checkToken token = get (sdsFocus token accountStore)
		>>- maybe (clearAuthenticationToken @! ?None) (\a -> return (?Just (accountToUser a)))

clearAuthenticationToken :: Task ()
clearAuthenticationToken =: set ("auth-token","",?Just 0) currentTaskInstanceCookies @! ()

doAuthenticated :: (Task a) -> Task a | iTask a
doAuthenticated task = doAuthenticatedWith verify task
where
	verify {Credentials|username,password} = authenticateUser username password False

doAuthenticatedWith :: !(Credentials -> Task (?User)) (Task a) -> Task a | iTask a
doAuthenticatedWith verifyCredentials task
	=	Title "Log in" @>> Hint "Please enter your credentials" @>> enterInformation []
	>>!	verifyCredentials
	>>- \mbUser -> case mbUser of
		?None      = throw "Authentication failed"
		?Just user = workAs user task

createUser :: !UserAccount -> Task StoredUserAccount
createUser account = createUserIn userAccounts account

createUserIn :: !(sds () [StoredUserAccount] [StoredUserAccount]) !UserAccount -> Task StoredUserAccount | RWShared sds
createUserIn userAccountSds account
	=	createStoredAccount >>~ \storedAccount ->
	    get (sdsFocus (identifyUserAccount storedAccount) (userAccountIn userAccountSds))
	>>- \mbExisting -> case mbExisting of
		?None
			= upd (\accounts -> accounts ++ [storedAccount]) userAccountSds @ const storedAccount
		_
			= throw ("A user with username '" +++ toString account.UserAccount.credentials.Credentials.username +++ "' already exists.")
where
	createStoredAccount :: Task StoredUserAccount
	createStoredAccount
		= createStoredCredentials account.UserAccount.credentials.Credentials.username
			account.UserAccount.credentials.Credentials.password
		@ \credentials ->
			{ StoredUserAccount | credentials = credentials , displayName = account.UserAccount.displayName
			, roles = account.UserAccount.roles, token = ?None }

deleteUser :: !UserId -> Task ()
deleteUser userId = upd (filter (\acc -> identifyUserAccount acc <> userId)) userAccounts @! ()

deleteUserIn :: !(sds () [StoredUserAccount] [StoredUserAccount]) !UserId -> Task () | RWShared sds
deleteUserIn userAccountSds userId = upd (filter (\acc -> identifyUserAccount acc <> userId)) userAccountSds @! ()

manageUsers :: Task ()
manageUsers =: manageUsersWithConfig defaultConfig
where
	defaultConfig :: ManageUserConfig
	defaultConfig =
		{ ManageUserConfig
		| demoUserImport = True
		, exportCSVFile = True
		, createUserConfig = defaultCreateUserConfig
		}

//* This type is only used internally for representation in the UI.
:: ManageUsersFields =
	{ username :: !Username
	, displayName :: !?DisplayName
	, roles :: ![Role]
	}

derive class iTask ManageUsersFields

manageUsersWithConfig :: !ManageUserConfig -> Task ()
manageUsersWithConfig config = manageUsersWithConfigAndCustomStore config userAccounts

manageUsersWithConfigAndCustomStore ::
	!ManageUserConfig !(sds () [StoredUserAccount] [StoredUserAccount]) -> Task () | RWShared sds
manageUsersWithConfigAndCustomStore config sdsForUserAccounts =
	(		Title "Users" @>> Hint "The following users are available" @>> ApplyLayout scrollContent @>>
			getCurrentAuthenticatedUserId >>- \currentAuthenticatedUserId ->
			(enterChoiceWithSharedAs
				[ ChooseFromGrid
					( \{StoredUserAccount| credentials, displayName, roles} ->
						{ ManageUsersFields
						| username = let (Username userId) = credentials.StoredCredentials.username in
							Username $ if (?Just userId == currentAuthenticatedUserId)
								(userId +++ " (current user)")
								userId
						, displayName = displayName
						, roles = roles
						}
					)
				]
				sdsForUserAccounts identifyUserAccount
			) <<@ ScrollContent
		>>* (
			[ OnAction
				(Action "New")
				(always $
					createUserFlowWithConfigAndCustomStore config.createUserConfig sdsForUserAccounts @ const False
				)
			, OnAction ActionEdit
				(hasValue
					(\u = updateUserFlowWithConfigAndCustomStore u config.createUserConfig sdsForUserAccounts
						@ const False
					)
				)
		    , OnAction
				(Action "Change Password")
				(hasValue (\u -> changePasswordFlowWith u (userAccountIn sdsForUserAccounts) @ const False))
			// The currently logged-on user cannot be deleted.
			, OnAction ActionDelete
				(ifValue
					(\u -> ?Just u <> currentAuthenticatedUserId)
					(\u -> deleteUserFlowWith u sdsForUserAccounts @ const False)
				)
			]
			++
			(if config.exportCSVFile
				[OnAction (Action "Export CSV file...")
				(always (exportUserFileFlowWith sdsForUserAccounts @ const False))] []
			)
			++
			(if config.demoUserImport
				[OnAction (Action "Import demo users")
					(always (importDemoUsersFlowWith sdsForUserAccounts @ const False))] []
			)
		)
	) <! id @! ()
where
	getCurrentAuthenticatedUserId :: Task (?UserId)
	getCurrentAuthenticatedUserId = get currentUser >>~ \user ->
		case user of
			(AuthenticatedUser userId _ _) = return $ ?Just userId
			_ = return ?None

createUserFlowWithConfig :: !CreateUserConfig -> Task ()
createUserFlowWithConfig config = createUserFlowWithConfigAndCustomStore config userAccounts

createUserFlowWithConfigAndCustomStore ::
	!CreateUserConfig !(sds () [StoredUserAccount] [StoredUserAccount]) -> Task () | RWShared sds
createUserFlowWithConfigAndCustomStore config sdsForUserAccounts =
		(Title "Create user" @>> Hint "Enter user information" @>> enterInformation [EnterUsing id $ userEditor config])
	>>*	[ OnAction ActionCancel (always (return ()))
		, OnAction
			ActionOk
			(hasValue (\(userAccount, repeatPassword) ->
				if (userAccount.UserAccount.credentials.Credentials.password <> repeatPassword)
					(	viewInformation [] "Passwords do not match. Please try again." >!|
						createUserFlowWithConfigAndCustomStore config sdsForUserAccounts
					)
					(	catchAll
							(	createUserIn sdsForUserAccounts userAccount
								>-| Title "User created" @>> viewInformation [] "Successfully added new user"
							)
							(\error -> Title "Error creating user" @>> viewInformation [] error)
						>!| return ()
					)
				)
			)
		]
where
	userEditor :: !CreateUserConfig -> Editor (UserAccount, Password) (EditorReport (UserAccount, Password))
	userEditor {CreateUserConfig| usernamePredicate, roles, usernameLabel} =
		mapEditorRead
			(\({UserAccount| credentials = {Credentials| username, password}, displayName, roles}, repeatPassword) ->
				(username, password, repeatPassword, displayName, roles)
			)
		$ mapEditorWriteError
			((\editorReport = case editorReport of
				ValidEditor (username, password, repeatPassword, displayName, roles)
					| password <> repeatPassword = Error "The passwords do not match."
					= Ok
						(	{ UserAccount
							| credentials = {Credentials| username = username, password = password}
							, displayName = displayName
							, roles = roles}
						, repeatPassword
						)
				EmptyEditor = Error "The user editor was empty."
				InvalidEditor _ = Error "Some fields are empty or contain erroneous values.") o editorReportTuple5)
		$ panel5
			(row usernameLabel $
				mapValidEditorWriteError usernamePredicate (gEditor{|*|} EditValue))
			(row "Password:" $ gEditor{|*|} EditValue)
			(row "Repeat password:" $ gEditor{|*|} EditValue)
			(row "Display name:" $ gEditor{|*|} EditValue)
			(row "Roles:" $ if (isNone roles) (gEditor{|*|} EditValue) (rolesEditor $ fromJust roles))
		<<@ classAttr ["itasks-wrap-height"]

rolesEditor :: [String] -> Editor [String] (EditorReport [String])
rolesEditor roles = choiceEditor EditValue id ValidEditor True roles

row :: !String !(Editor a (EditorReport a)) -> Editor a (EditorReport a)
row l e
	= mapEditorWrite snd
	$ mapEditorRead (\x -> ((),x))
	$ (container2 (viewConstantValue l label) e) <<@ classAttr ["itasks-horizontal"]

createUserFlow :: Task ()
createUserFlow =: createUserFlowWithConfig defaultCreateUserConfig

defaultCreateUserConfig :: CreateUserConfig
defaultCreateUserConfig =
	{ CreateUserConfig
	| usernamePredicate = \u -> Ok u
	, roles = ?None
	, usernameLabel = "Username"
	}

updateUserFlow :: !UserId -> Task StoredUserAccount
updateUserFlow userId = updateUserFlowWithConfig userId defaultCreateUserConfig

updateUserFlowWithConfig :: !UserId !CreateUserConfig -> Task StoredUserAccount
updateUserFlowWithConfig userId createUserConfig =
	updateUserFlowWithConfigAndCustomStore userId createUserConfig userAccounts

updateUserFlowWithConfigAndCustomStore ::
	!UserId !CreateUserConfig !(sds () [StoredUserAccount] [StoredUserAccount]) -> Task StoredUserAccount | RWShared sds
updateUserFlowWithConfigAndCustomStore userId createUserConfig sdsForUserAccounts
	=	get (sdsFocus userId (userAccountIn sdsForUserAccounts))
	>>- \mbAccount -> case mbAccount of
		?Just account
			=	(Title ("Editing " +++ accountDisplayName account) @>> Hint "Please make your changes" @>>
				updateInformation [UpdateUsing id (\_ v -> v) (customUserEditor account createUserConfig)] account
			>>*	[ OnAction ActionCancel (always (return account))
				, OnAction ActionOk (hasValue (\newAccount ->
					set (?Just newAccount) (sdsFocus userId (userAccountIn sdsForUserAccounts))
					>>- \storedAccount -> Title "User updated"
					    @>> viewInformation
							[ViewAs
								(\(?Just {StoredUserAccount|displayName})
									= "Successfully updated " +++ accountDisplayName account
								)
							]
							storedAccount
					>!| return newAccount
					))
				])
		?None
			=	(throw "Could not find user details")
where
	customUserEditor ::
		!StoredUserAccount !CreateUserConfig -> Editor StoredUserAccount (EditorReport StoredUserAccount)
	customUserEditor account config =
		mapEditorRead readf
		$ mapEditorWrite (fmap writef o editorReportTuple2)
		$ panel2
			(row "Display name:" $ gEditor{|*|} EditValue)
			(row "Roles:" $ if (isNone config.CreateUserConfig.roles) (gEditor{|*|} EditValue) (rolesEditor $ fromJust config.CreateUserConfig.roles))
			<<@ classAttr ["itasks-wrap-height"]
	where
		readf {StoredUserAccount | displayName, roles} = (displayName, roles)
		writef (displayName, roles) =
			{ StoredUserAccount
			| credentials = account.StoredUserAccount.credentials
			, displayName = displayName
			, roles = roles
			, token = account.StoredUserAccount.token
			}

/**
 * Using this record results in a generic editor that asks for a password twice.
 */
:: PasswordTwice = {password :: !Password, repeatPassword :: !Password}
derive class iTask \ gEditor PasswordTwice

gEditor{|PasswordTwice|} ViewValue = mapEditorRead (\{password, repeatPassword} = (password, repeatPassword)) $
	mapEditorWrite
		(fmap (\(password, repeatPassword) = {password = password, repeatPassword = repeatPassword})
			o editorReportTuple2)
		$ container2 (gEditor{|*|} ViewValue) (gEditor{|*|} ViewValue)
gEditor{|PasswordTwice|} EditValue = mapEditorRead (\{password, repeatPassword} = (password, repeatPassword)) $
	withDynamicHintAttributes "password" $
	mapEditorWrite
		((\editorReport = case editorReport of
			ValidEditor (password, repeatPassword)
				| password <> repeatPassword = InvalidEditor ["The passwords do not match."]
				= ValidEditor {password = password, repeatPassword = repeatPassword}
			v = liftedNonValidEditorReport v)
			o editorReportTuple2)
		$ (container2 (gEditor{|*|} EditValue) (gEditor{|*|} EditValue)
			// Overwrite the flex attributes to make the container containing both password fields fit the content
			// (otherwise the icon indicating whether the passwords match is placed all the way to the right)
			<<@ styleAttr "flex-basis: fit-content; flex-grow: 0; flex-shrink: 0;")

changePasswordFlow :: !UserId -> Task StoredUserAccount
changePasswordFlow userId = changePasswordFlowWith userId (userAccountIn userAccounts)

changePasswordFlowWith ::
	!UserId (sds UserId (?StoredUserAccount) (?StoredUserAccount)) -> Task StoredUserAccount | RWShared sds
changePasswordFlowWith userId accountStore =
	get (sdsFocus userId accountStore) >>~ \mbAccount ->
	case mbAccount of
		?Just account =
			Title ("Change Password for " +++ accountDisplayName account) @>>
			Hint "Please enter a new password" @>>
			enterInformation []
			>>* [ OnAction ActionCancel (always   $ return account)
			    , OnAction ActionOk     (hasValue $ updatePassword account)
			    ]
		?None = throw "Could not find user details"
where
	updatePassword :: !StoredUserAccount !PasswordTwice -> Task StoredUserAccount
	updatePassword account {password, repeatPassword}
		| password <> repeatPassword =
			viewInformation [] "Passwords do not match. Please try again." >!|
			changePasswordFlowWith userId accountStore
		= createStoredCredentials account.StoredUserAccount.credentials.StoredCredentials.username password >>- \creds ->
		let account` = {StoredUserAccount| account & credentials = creds} in
		set (?Just account`) (sdsFocus userId accountStore) >>- \account`` ->
		Hint "Password updated" @>> viewInformation
		                [ ViewAs \(?Just {StoredUserAccount|displayName}) ->
		                         "Successfully changed password for " +++ accountDisplayName account
		                ] account`` >!|
		return account`

deleteUserFlow :: !UserId -> Task StoredUserAccount
deleteUserFlow userId = deleteUserFlowWith userId userAccounts

deleteUserFlowWith :: !UserId !(sds () [StoredUserAccount] [StoredUserAccount]) -> Task StoredUserAccount | RWShared sds
deleteUserFlowWith userId sdsForUserAccounts
	=	get (sdsFocus userId (userAccountIn sdsForUserAccounts))
	>>- \mbAccount -> case mbAccount of
		?Just account
			=	Title "Delete user" @>>
				viewInformation []
					("Are you sure you want to delete " +++ accountDisplayName account +++ "? This cannot be undone.")
			>>*	[ OnAction ActionNo	(always (return account))
				, OnAction ActionYes
					(always $
						deleteUserIn sdsForUserAccounts userId >-|
						viewInformation [ViewAs (\account -> "Successfully deleted " +++ accountDisplayName account +++ ".")] account
							<<@ Hint "User deleted"
						>!| return account
					)
				]

changeOwnPasswordFlow :: !UserId -> Task ()
changeOwnPasswordFlow userId = changeOwnPasswordFlowWith userId (userAccountIn userAccounts)

changeOwnPasswordFlowWith :: !UserId (sds UserId (?StoredUserAccount) (?StoredUserAccount)) -> Task () | RWShared sds
changeOwnPasswordFlowWith userId accountStore =
		allTasks [Label label @>> enterInformation [] \\ label <- ["Current password","New password","New password (again)"]]
			>>* [ OnAction ActionCancel (always $ return ())
			    , OnAction ActionOk     (ifValue passwordOk $ updatePassword)
			    ]
where
	passwordOk [Password cur,Password new1,Password new2] = new1 == new2
	passwordOk _ = False

	updatePassword [cur,password,_]
		= createStoredCredentials (Username userId) password
		>>- \creds ->
		upd (fmap (\a -> if (isValidPassword cur a.StoredUserAccount.credentials) {StoredUserAccount|a & credentials = creds, token = ?None} a))
			(sdsFocus userId accountStore)
		>>- \mbAccount -> if (maybe False (\a -> isValidPassword password a.StoredUserAccount.credentials) mbAccount)
			(Hint "Password updated" @>> viewInformation [] "Successfully updated password." )
			(Hint "Password not updated" @>> viewInformation [] "The password was not updated." )
		>!| return ()

exportUserFileFlow :: Task Document
exportUserFileFlow =: exportUserFileFlowWith userAccounts

exportUserFileFlowWith :: !(sds () [StoredUserAccount] [StoredUserAccount]) -> Task Document | RWShared sds
exportUserFileFlowWith sdsForUserAccounts =
	get sdsForUserAccounts -&&- get applicationName
	>>- \(list,app) ->
		createCSVFile (app +++ "-users.csv") (map toRow list)
	>>-	\file ->
		Title "Export users file" @>>
		Hint "A CSV file containing the users of this application has been created for you to download." @>>
			 viewInformation [] file
where
	toRow {StoredUserAccount| credentials = {StoredCredentials|username = (Username username), saltedPasswordHash, salt}, displayName, roles}
		= [fromMaybe "" displayName,username,saltedPasswordHash,salt:roles]

importDemoUsersFlow :: Task [UserAccount]
importDemoUsersFlow =: importDemoUsersFlowWith userAccounts

importDemoUsersFlowWith :: !(sds () [StoredUserAccount] [StoredUserAccount]) -> Task [UserAccount] | RWShared sds
importDemoUsersFlowWith sdsForUserAccounts =
	allTasks [catchAll (createUserIn sdsForUserAccounts u @ const u) (\_ -> return u) \\ u <- demoUser <$> names]
where
	demoUser name
		= {UserAccount
		  | credentials = {Credentials| username = Username (toLowerCase name), password = Password (toLowerCase name)}
		  , displayName = ?Just name
		  , roles = ["manager"]
		  }
	names = ["Alice","Bob","Carol","Dave","Eve","Fred"]

minimalMultiUserTask :: ((Task (?User)) -> Task (?User))
	(User -> Task ()) (User -> Task ()) (User -> [(String,Task ())])
	(sds UserId (?StoredUserAccount) (?StoredUserAccount))
	(sds String (?StoredUserAccount) ()) -> Task () | RWShared sds
minimalMultiUserTask loginModifier headerTask sessionTask userTasks accountAccess tokenAccess = forever
	(anyTask
		[loginModifier ((enterInformation [] -&&- (Label "Remember login" @>> enterInformation []))
		 >>* [OnAction (Action "Login") (hasValue authenticate)])
		,authenticateUsingTokenWith tokenAccess
		] <<@ ApplyLayout layout
	>>- main headerTask sessionTask
	>-| clearAuthenticationToken
	)
where
	authenticate ({Credentials|username,password},persistent) = authenticateUserWith username password persistent accountAccess

	main h t (?Just user) = workAs user ((manageSession user h userTasks  -|| (t user)) <<@ ArrangeWithHeader 0)
	main h t ?None = (Title "Login failed" @>> viewInformation [] "Your username or password is incorrect" >!| return ()) <<@ ApplyLayout frameCompact
	layout = sequenceLayouts [layoutSubUIs (SelectByType UIAction) (setActionIcon ('DM'.fromList [("Login","login")])), frameCompact]

manageSession user headerTask userTasks
    = ((headerTask user >^* [OnAction (Action a) (always (t <<@ InWindow)) \\ (a,t) <- userTasks user])
		 >>* [OnAction (Action "Sign out") (always (return ()))] )
     <<@ ApplyLayout (sequenceLayouts
        [removeCSSClass "step-actions"
        ,addCSSClass "main-header"
		,moveSubUIs (SelectOR (SelectByType UIAction) (SelectByType UIWindow)) [] 1
		,layoutSubUIs (SelectByPath [0]) unwrapUI
        ,layoutSubUIs (SelectByType UIAction) (setActionIcon ('DM'.fromList [("Sign out","logout")]))
        ] )

createStoredCredentials :: !Username !Password -> Task StoredCredentials
createStoredCredentials username password =
	get (sdsFocus 32 randomString) @ \salt ->
	{ username           = username
	, saltedPasswordHash = sha1 $ toString password +++ salt
	, salt               = salt
	}

instance == StoredUserAccount derive gEq
instance == StoredCredentials derive gEq
