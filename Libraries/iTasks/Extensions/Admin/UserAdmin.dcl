definition module iTasks.Extensions.Admin.UserAdmin
/**
* This extension provides workflows for managing the users of an iTask system.
*/
import iTasks
import iTasks.Extensions.User
from iTasks.Extensions.Document import :: Document

/**
 * A config record for the manageUsersWithConfig task.
 */
:: ManageUserConfig =
	{ demoUserImport :: !Bool
	//* Should the button for importing demo users be visible
    , exportCSVFile :: !Bool
	//* Should the button for exporting all userdata to a CSV file be visible
	, createUserConfig :: !CreateUserConfig
	//* Additional configuration for creating a new user
	}

/**
 * A config record for the createUserFlowWithConfig task.
 * This record may be subject to changes, use `defaultCreateUserConfig` and update the fields specific your
 * application to avoid having to change anything when upgrading to a new major version.
 */
:: CreateUserConfig =
	{ usernamePredicate :: !Username -> MaybeErrorString Username
	//* Predicate that should apply to the username.
	//* If the predicate holds it should return Ok Username. Otherwise it should return the message to be displayed.
	, roles :: !?[String]
	//* A list of roles from which zero or more roles can be selected. When `?None`, roles can be entered at will.
	, usernameLabel :: !String
	//* A custom label for entering the username.
	}

//* The default `:: CreateUserConfig`.
defaultCreateUserConfig :: CreateUserConfig

/**
 * A user account which contains the password in cleartext.
 */
:: UserAccount =
	{ credentials   :: !Credentials
	//* The credentials in plaintext.
	, displayName   :: !?DisplayName
	//* A descriptive name (not used for identification).
	, roles         :: ![Role]
	//* The roles for this user.
	}

/**
 * A user account which can safely be stored, as it does not contains the password in cleartext.
 */
:: StoredUserAccount =
	{ credentials :: !StoredCredentials
	, displayName :: !?DisplayName
	, roles       :: ![Role]
	, token       :: !?StoredCredentials
	}

/**
 * Stored user credentials not containing the password in cleartext.
 */
:: StoredCredentials = { username           :: !Username //* The username.
                       , saltedPasswordHash :: !String   //* The salted SHA1 password hash.
                       , salt               :: !String   //* The 32-byte random salt.
                       }

instance == StoredUserAccount
derive class iTask UserAccount, StoredUserAccount, StoredCredentials

// User accounts in the iTasks store

//* All user accounts
userAccounts			::				SDSLens () [StoredUserAccount] [StoredUserAccount]

//* All users
users					:: 				SDSLens () [User] ()
//* Users with a specific role
usersWithRole			:: !Role ->		SDSLens () [User] ()

//* Alternative user account file, in a user defined location
userAccountsFile :: SDSLens FilePath [StoredUserAccount] [StoredUserAccount]

/**
 * Transforms an SDS which stores the user accounts to a lens which can be used to modify the
 * `StoredUserAccount` associated with a particular `UserId`.
 *
 * @param The SDS which stores the user accounts.
 * @result A lens which can be used to get/set the stored user account of a user given their id.
 */
userAccountIn ::
	!(sds () [StoredUserAccount] [StoredUserAccount]) -> SDSLens UserId (?StoredUserAccount) (?StoredUserAccount)
	| RWShared sds

/**
 * Transforms an SDS which stores the user accounts to a lens which can be used to retrieve the
 * `StoredUserAccount` associated with a particular token.
 *
 * @param The SDS which stores the user accounts.
 * @result A lens which can be used to get the stored user account of a user given their token if there is one present.
 */
userTokenIn ::
	!(sds () [StoredUserAccount] [StoredUserAccount]) -> SDSLens String (?StoredUserAccount) () | RWShared sds

/**
 * Transforms an SDS which stores the user accounts to a lens which takes
 * takes a parameter containing a set user tokens, and then returns the tokens which are still active (in use)
 * as a read value.
 *
 * @param The SDS which stores the user account.
 * @param A lens which returns the tokens which are active given a set of tokens through the parameter.
 */
activeTokensIn ::
	!(sds () [StoredUserAccount] [StoredUserAccount]) -> SDSLens (Set String) (Set String) () | RWShared sds

/**
* Authenticates a user by username and password
*
* @param Username: The username
* @param Password: The password
* @param Persistent: Set a persistent authentication token in browser cookies
*
* @result A single user who matches the given credentials, or `?None` of none or more than one exists.
*/
authenticateUser :: !Username !Password !Bool -> Task (?User)
authenticateUserWith :: !Username !Password !Bool (sds UserId (?StoredUserAccount) (?StoredUserAccount)) -> Task (?User) | RWShared sds

/**
* Check the browser cookies for a stored persistent login token.
* - If no token is set, the task blocks indefinitely.
* - If a valid token is set, it returns the user.
* - If an invalid token is set, it returns `?None`.
*/
authenticateUsingToken :: Task (?User)
authenticateUsingTokenWith :: (sds String (?StoredUserAccount) ()) -> Task (?User) | RWShared sds

/**
* Clear any persistent authentication cookies
*/
clearAuthenticationToken :: Task ()

/**
* Wraps a task with an authentication task
*
* @param	the task to wrap
*/
doAuthenticated :: (Task a) -> Task a | iTask a
doAuthenticatedWith :: !(Credentials -> Task (?User)) (Task a) -> Task a | iTask a

/**
* Add a new user.
*
* @param User details: The user-information which needs to be stored.
* @return The stored user.
*/
createUser :: !UserAccount -> Task StoredUserAccount

/**
* Add a new user to the given custom user account store.
*
* @param User details: The user-information which needs to be stored.
* @return The stored user.
*/
createUserIn :: !(sds () [StoredUserAccount] [StoredUserAccount]) !UserAccount -> Task StoredUserAccount | RWShared sds

/**
* Delete an existing user.
*
* @param User: The user who needs to be deleted.
* @return The deleted user.
*/
deleteUser :: !UserId -> Task ()

/**
* Delete an existing user in the given custom user account store.
*
* @param User: The user who needs to be deleted.
* @return The deleted user.
*/
deleteUserIn :: !(sds () [StoredUserAccount] [StoredUserAccount]) !UserId -> Task () | RWShared sds

/**
* Browse and manage the existing users
*/
manageUsers :: Task ()

/**
* Same as `manageUsers`, but with additional parameters
* @param ManageUserConfig: the configuration options
*/
manageUsersWithConfig :: !ManageUserConfig -> Task ()

/**
 * Same as `manageUsersWithConfig`, but allows to store the user accounts in a custom SDS.
 *
 * @param The configuration options.
 * @param The SDS in which the user accounts are stored.
 * @result Task.
 */
manageUsersWithConfigAndCustomStore ::
	!ManageUserConfig !(sds () [StoredUserAccount] [StoredUserAccount]) -> Task () | RWShared sds

createUserFlow :: Task ()

/**
* Same as `createUserFlow`, but with additional parameters
* @param CreateUserConfig: the configuration options
*/
createUserFlowWithConfig :: !CreateUserConfig -> Task ()

/**
 * Workflow to update the information related to a user
 *
 * @param The id of the user.
 * @result A task which allows to update the information related to a user.
 */
updateUserFlow :: !UserId -> Task StoredUserAccount

/**
 * Workflow to change the password of a user.
 *
 * @param The id of the user.
 * @param Task which makes it possible to alter the password of the given user.
 */
changePasswordFlow :: !UserId -> Task StoredUserAccount

/**
 * Same as `changePasswordFlow` but it allows specifying a custom data source in which the user accounts are stored.
 *
 * @param The id of the user.
 * @param The data source which stores the user accounts.
 */
changePasswordFlowWith ::
	!UserId (sds UserId (?StoredUserAccount) (?StoredUserAccount)) -> Task StoredUserAccount | RWShared sds

/**
 * Workflow to delete a user.
 *
 * @param The id of the user.
 * @result A task which allows to delete a user (contains confirmation).
 */
deleteUserFlow :: !UserId -> Task StoredUserAccount

/**
 * Like `deleteUserFlow`, but allows specifying a custom data source which stores the accounts.
 *
 * @param The id of the user.
 * @param The data source which is used to store the user accounts.
 * @result A task which allows to delete a user (contains confirmation).
 */
deleteUserFlowWith :: !UserId !(sds () [StoredUserAccount] [StoredUserAccount]) -> Task StoredUserAccount | RWShared sds

exportUserFileFlow :: Task Document

changeOwnPasswordFlow :: !UserId -> Task ()
changeOwnPasswordFlowWith :: !UserId (sds UserId (?StoredUserAccount) (?StoredUserAccount)) -> Task () | RWShared sds

/**
* Do a task in a basic multi-user setting, users need to log in before they can do a task
*/
minimalMultiUserTask :: ((Task (?User)) -> Task (?User))
	(User -> Task ()) (User -> Task ())
	(User -> [(String,Task ())])
	(sds UserId (?StoredUserAccount) (?StoredUserAccount))
	(sds String (?StoredUserAccount) ()) -> Task () | RWShared sds

/**
* Create set of user names handy for giving demo's: alice, bob, carol, ...
*/
importDemoUsersFlow :: Task [UserAccount]

/**
 * An Editor for selecting user roles.
 * @param The list of roles from which zero or more roles can be selected.
 * @result An Editor using multiple choice checkboxes.
 */
rolesEditor :: [String] -> Editor [String] (EditorReport [String])
