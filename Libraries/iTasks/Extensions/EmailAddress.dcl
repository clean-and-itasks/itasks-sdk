definition module iTasks.Extensions.EmailAddress

from StdOverloaded import class toString

from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Text.GenJSON import :: JSONNode, generic JSONEncode, generic JSONDecode
from Text.HTML import class html

from iTasks.Internal.Generic.Visualization import :: TextFormat, generic gText
from iTasks.UI.Editor import :: Editor, :: EditorReport
from iTasks.UI.Editor.Generic import :: EditorPurpose, generic gEditor
from iTasks.WF.Definition import class iTask

:: EmailAddress	=: EmailAddress String

instance toString EmailAddress
instance html EmailAddress
instance == EmailAddress

derive class iTask \ gText, gEditor EmailAddress
derive gText EmailAddress
derive gDefault EmailAddress
derive gEditor EmailAddress
