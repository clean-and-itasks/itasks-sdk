implementation module iTasks.SDS.Combinators.Common

import iTasks.SDS.Definition, iTasks.SDS.Combinators.Core, iTasks.SDS.Sources.Core
import iTasks.SDS.Sources.System
import iTasks.WF.Combinators.Core
from iTasks.Internal.Task import exception, :: TaskException
import qualified Data.Map as DM
import Data.Func
import qualified Data.IntMap.Strict as DIS
from Data.IntMap.Strict import :: IntMap
from Data.Map import :: Map
import Data.Error, Data.Either, Data.GenHash
import Data.Maybe
import StdEnv
import Text.GenJSON
import System.FilePath

import iTasks.Internal.SDS
import iTasks.Internal.TaskState
import iTasks.WF.Derives

sdsFocus :: !p !(sds p r w) -> (SDSLens p` r w) | TC, gHash{|*|} p & gHash{|*|} p` & TC r & TC w & RWShared sds
sdsFocus p sds = sdsTranslate ("(" +++ toString (gHash{|*|} p) +++ ")/") (const p) sds

sdsProject ::
	!(SDSReadProjection rs r) !(SDSWriteProjection rs ws w)
	!(?(SDSReducer p ws w))
	!(sds p rs ws)
	-> SDSLens p r w
	| TC, gHash{|*|} p & TC rs & TC ws & RWShared sds
sdsProject read write reducer sds = sdsLens "()" param` read` write` (SDSNotifyConst True) reducer sds
where
    param` p = p
    read` = case read of
        SDSLensRead f = SDSRead (\p rs -> f rs)
        SDSConstRead f = SDSReadConst (\_ -> f)

    write` = case write of
        SDSLensWrite f = SDSWrite (\p rs w -> f rs w)
        SDSBlindWrite f = SDSWriteConst (\p w -> f w)

sdsTranslate :: String !(p -> ps) !(sds ps r w) -> SDSLens p r w | gHash{|*|} p & TC ps & TC r & TC w & RWShared sds
sdsTranslate name param sds = sdsLens name param
	(SDSRead (\_ rs -> Ok rs))
	(SDSWriteConst (\_ w -> Ok (?Just w)))
	(SDSNotifyConst True)
	(?Just \p ws. Ok (ws))
	sds

sdsSplit ::
	String
	!(p -> (ps,pn)) !(pn rs -> r) !(pn rs w -> (ws,SDSNotifyPred pn))
	!(?(SDSReducer p ws w))
	!(sds ps rs ws)
	-> SDSLens p r w
	| gHash{|*|} p & TC ps & TC pn & TC rs  & TC ws & RWShared sds
sdsSplit name param read write reducer sds = sdsLens name param` (SDSRead read`) (SDSWrite write`) (SDSNotify notify`) reducer sds
where
	param` p             = fst (param p)
	read` p rs           = Ok (read (snd (param p)) rs)
	write` p rs w        = Ok (?Just (fst (write (snd (param p)) rs w)))
	notify` p rs w ts pq = (snd (write (snd (param p)) rs w)) ts (snd (param pq))

sdsStamp :: !(sds1 p1 r1 w1) !(sds2 p2 r2 w2) (r2 w -> w1) -> SDSLens (p1, p2) r1 w | TC, gHash{|*|} p1 & TC, gHash{|*|} p2 & TC r1 & TC r2 & TC w1 & TC w2 & RWShared sds2 & RWShared sds1
sdsStamp source helper writefun = sdsLens "*<" id
	(SDSRead  \_ (r1, r2)->Ok r1)
	(SDSWrite \_ (r1, r2) w->Ok $ ?Just $ (writefun r2 w, ()))
	(SDSNotifyConst True)
	?None
	$ sdsParallel ">**<" id id
		(SDSWriteConst $ const $ Ok o ?Just o fst)
		(SDSWriteConst $ const $ Ok o ?Just o snd)
		source
		(mapWrite (\_ _ -> ?None) ?None $ SDSNoNotify helper)

timespecStampedShare :: !(sds p b (Timespec,c)) -> SDSLens p b c | TC, gHash{|*|} p & TC b & TC c & RWShared sds
timespecStampedShare sds
	= sdsTranslate "timespecStampedShare" (\p->(p, ()))
	$ sdsStamp sds currentTimespec \x y->(x, y)

removeMaybe :: !(?a) !(sds p (?a) (?a)) -> SDSLens p a a | TC, gHash{|*|}, gText{|*|} p & TC a & RWShared sds
removeMaybe defaultValue sds =
	sdsLens "removeMaybe" id (SDSRead read) (SDSWriteConst write) (SDSNotifyConst True) (?Just reducer) sds
where
	read p (?Just r) = Ok r
	read p ?None = maybe (Error (exception ("Required value not available in shared data source: " <+++ p))) Ok defaultValue

	write p w = Ok (?Just (?Just w))

	reducer _ (?Just a) = Ok a
	reducer p ?None     = maybe (Error (exception ("Required value not available in shared data source: " <+++ p))) Ok defaultValue

mapRead :: !(r -> r`) !(sds p r w) -> SDSLens p r` w | TC, gHash{|*|} p & TC r & TC w & RWShared sds
mapRead read sds = mapReadError (\r -> Ok (read r)) sds

mapWrite ::
	!(w` r -> ?w)
	!(?(SDSReducer p w w`))
	!(sds p r w)
	-> SDSLens p r w`
	| TC, gHash{|*|} p & TC r & TC w & RWShared sds
mapWrite write reducer sds = mapWriteError (\r w -> Ok (write r w)) reducer sds

mapReadWrite ::
	!(!r -> r`,!w` r -> ?w)
	!(?(SDSReducer p w w`))
	!(sds p r w)
	-> SDSLens p r` w`
	| TC, gHash{|*|} p & TC r & TC w & RWShared sds
mapReadWrite (read,write) reducer sds = mapReadWriteError (\r -> Ok (read r), (\r w -> Ok (write r w))) reducer sds

mapReadError ::
	!(r -> MaybeError TaskException r`)
	!(sds p r w)
	-> SDSLens p r` w
	| TC, gHash{|*|} p & TC r & TC w & RWShared sds
mapReadError read sds = sdsProject (SDSLensRead read) (SDSBlindWrite (Ok o ?Just)) (?Just \_ ws. Ok ws) sds

mapWriteError ::
	!(w` r -> MaybeError TaskException (?w))
	!(?(SDSReducer p w w`))
	!(sds p r w)
	-> SDSLens p r w`
	| TC, gHash{|*|} p & TC r & TC w & RWShared sds
mapWriteError write reducer sds = sdsProject (SDSLensRead Ok) (SDSLensWrite (flip write)) reducer sds

mapReadWriteError ::
	!(!r -> MaybeError TaskException r`,!w` r -> MaybeError TaskException (?w))
	!(?(SDSReducer p w w`))
	!(sds p r w)
	-> SDSLens p r` w`
	| TC, gHash{|*|} p & TC r & TC w & RWShared sds
mapReadWriteError (read,write) reducer sds = sdsProject (SDSLensRead read) (SDSLensWrite (flip write)) reducer sds

mapSingle :: !(sds p [r] [w]) -> (SDSLens p r w) | TC, gHash{|*|} p & TC r & TC w & RWShared sds
mapSingle sds = sdsProject (SDSLensRead read) (SDSBlindWrite write) (?Just reducer) sds
where
	read [x]    = Ok x
	read []     = Error (exception "List element not found")
	read _      = Error (exception "Multiple list elements found, expected only one")

	write x     = Ok (?Just [x])

	reducer p ws = read ws

toReadOnly :: !(sds p r w) -> SDSLens p r () | TC, gHash{|*|} p & TC r & TC w & RWShared sds
toReadOnly sds = sdsProject (SDSLensRead Ok) (SDSBlindWrite \_. Ok ?None) ?None sds

toDynamic :: !(sds p r w) -> (SDSLens p Dynamic Dynamic) | TC, gHash{|*|} p & TC r & TC w & RWShared sds //FIXME: Use 1 lens directly
toDynamic sds = mapRead (\r -> (dynamic r :: r^)) (mapWrite (\(w :: w^) _ -> ?Just w) (?Just reducer) sds)
where
	reducer _ w = Ok (dynamic w)

(>*<) infixl 6 :: !(sds1 p rx wx) !(sds2 p ry wy) -> SDSParallel p (rx,ry) (wx,wy) | TC, gHash{|*|} p & TC rx & TC ry & TC wx & TC wy & RWShared sds1 & RWShared sds2
(>*<) l r = sdsParallel ">*<" (\p -> (p,p)) id (SDSWriteConst write1) (SDSWriteConst write2) l r
where
	write1 _ w = Ok (?Just (fst w))
	write2 _ w = Ok (?Just (snd w))

(>*|) infixl 6 :: !(sds1 p rx wx) !(sds2 p ry wy) -> SDSParallel p (rx,ry) wx | TC, gHash{|*|} p & TC rx & TC ry & TC wx & TC wy & RWShared sds1 & Registrable sds2
(>*|) l r = SDSParallelWriteLeft l r opts
where
	opts =
		{ SDSParallelOptions
		| id     = createSDSIdentity ">*|" (?Just (sdsIdentity l)) (?Just (sdsIdentity r))
		, name   = ">*|"
		, param  = \p -> (p,p)
		, read   = id
		, writel = SDSWriteConst (\_ w -> Ok (?Just w))
		, writer = SDSWriteConst (\_ _ -> Ok ?None)
		}

(|*<) infixl 6 :: !(sds1 p rx wx) !(sds2 p ry wy) -> SDSParallel p (rx,ry) wy | TC, gHash{|*|} p & TC rx & TC ry & TC wx & TC wy & Registrable sds1 & RWShared sds2
(|*<) l r = SDSParallelWriteRight l r opts
where
	opts =
		{ SDSParallelOptions
		| id     = createSDSIdentity "|*<" (?Just (sdsIdentity l)) (?Just (sdsIdentity r))
		, name   = "|*<"
		, param  = \p -> (p,p)
		, read   = id
		, writel = SDSWriteConst (\_ _ -> Ok ?None)
		, writer = SDSWriteConst (\_ w -> Ok (?Just w))
		}

(|*|) infixl 6 :: !(sds1 p rx wx) !(sds2 p ry wy) -> SDSParallel p (rx,ry) () | TC, gHash{|*|} p & TC rx & TC ry & TC wx & TC wy & Registrable sds1 & Registrable sds2
(|*|) l r = SDSParallelWriteNone l r opts
where
	opts =
		{ SDSParallelOptions
		| id     = createSDSIdentity "|*|" (?Just (sdsIdentity l)) (?Just (sdsIdentity r))
		, name   = "|*|"
		, param  = \p -> (p,p)
		, read   = id
		, writel = SDSWriteConst (\_ _ -> Ok ?None)
		, writer = SDSWriteConst (\_ _ -> Ok ?None)
		}

symmetricLens ::
	!(a b -> b) !(b a -> a)
	!(sds1 p a a) !(sds2 p b b)
	-> (!SDSLens p a a, !SDSLens p b b)
	| TC, gHash{|*|} p & TC a & TC b & RWShared sds1 & RWShared sds2
symmetricLens putr putl sharedA sharedB = (newSharedA,newSharedB)
where
	sharedAll = sharedA >*< sharedB
	newSharedA = mapReadWrite (fst,\a (_,b) -> ?Just (a,putr a b)) (?Just \_ (l,_). Ok l) sharedAll
	newSharedB = mapReadWrite (snd,\b (a,_) -> ?Just (putl b a,b)) (?Just \_ (_,r). Ok r) sharedAll

//Derived shares of tasklists
taskListState :: !(SharedTaskList a) -> SDSLens () [TaskValue a] () | TC a
taskListState tasklist = mapRead (\(_,items) -> [value \\ {TaskListItem|value} <- items]) (toReadOnly (sdsFocus listFilter tasklist))
where
    listFilter = {TaskListFilter|fullTaskListFilter & includeValue=True}

taskListMeta :: !(SharedTaskList a) -> SDSLens () [TaskListItem a] [(TaskId,TaskAttributes)] | TC a
taskListMeta tasklist = mapRead (\(_,items) -> items) (sdsFocus listFilter tasklist)
where
    listFilter = {TaskListFilter|fullTaskListFilter & includeTaskAttributes=True,includeManagementAttributes=True,includeProgress=True}

taskListIds :: !(SharedTaskList a) -> SDSLens () [TaskId] () | TC a
taskListIds tasklist = mapRead prj (toReadOnly (sdsFocus fullTaskListFilter tasklist))
where
    prj (_,items) = [taskId \\ {TaskListItem|taskId} <- items]

taskListEntryMeta :: !(SharedTaskList a) -> SDSLens TaskId (TaskListItem a) TaskAttributes | TC a
taskListEntryMeta tasklist = mapSingle (sdsSplit "taskListEntryMeta" param read write (?Just reducer) tasklist)
where
	param p = ({fullTaskListFilter & onlyTaskId= ?Just [p],includeTaskAttributes=True,includeManagementAttributes=True,includeProgress=True},p)
	read p (_,items) = [i \\ i=:{TaskListItem|taskId} <- items | taskId == p]
	write p _ attributes    = ([(p,a) \\ a <- attributes], const ((==) p))
	reducer _ l = Ok (snd (unzip l))

taskListSelfId :: !(SharedTaskList a) -> SDSLens () TaskId () | TC a
taskListSelfId tasklist = mapRead (\(_,items) -> hd [taskId \\ {TaskListItem|taskId,self} <- items | self]) (toReadOnly (sdsFocus listFilter tasklist))
where
    listFilter = {TaskListFilter|fullTaskListFilter & onlySelf=True}

taskListSelfManagement :: !(SharedTaskList a) -> SimpleSDSLens TaskAttributes | TC a
taskListSelfManagement tasklist = mapReadWriteError (toPrj,fromPrj) (?Just reducer) (sdsFocus listFilter tasklist)
where
	toPrj (_,items) = case [m \\ m=:{TaskListItem|taskId,self} <- items | self] of
		[] = Error (exception "Task id not found in self management share")
		[{TaskListItem|managementAttributes}:_] = Ok managementAttributes

	fromPrj attributes (_,[{TaskListItem|taskId}]) = Ok (?Just [(taskId,attributes)])

	listFilter = {TaskListFilter|fullTaskListFilter & onlySelf=True}

	reducer _ [(_,attr)] = Ok attr

taskListItemValue :: !(SharedTaskList a) -> SDSLens (Either Int TaskId) (TaskValue a) () | TC a
taskListItemValue tasklist = mapReadError read (toReadOnly (sdsTranslate "taskListItemValue" listFilter tasklist))
where
	listFilter (Left index) = {TaskListFilter| fullTaskListFilter & onlyIndex= ?Just [index],includeValue=True}
	listFilter (Right taskId) = {TaskListFilter| fullTaskListFilter & onlyTaskId= ?Just [taskId],includeValue=True}

	read (_,items) = case [value \\ {TaskListItem|value} <- items] of
		[v:_] = (Ok v)
		_     = Error (exception "taskListItemValue: item not found")

mapMaybeLens :: !String !(Shared sds (Map a b)) -> SDSLens a (?b) b | TC, <, ==, gHash{|*|} a & TC b & RWShared sds
mapMaybeLens name origShare = sdsLens name (const ()) (SDSRead read) (SDSWrite write) (SDSNotify notify) (?Just reducer) origShare
where
	read :: !a !(Map a b) -> MaybeError TaskException (?b) | < a & == a
	read idx m = Ok ('DM'.get idx m)

	write :: !a !(Map a b) !b -> MaybeError TaskException (?(Map a b)) | < a & == a
	write idx oldmap newval = Ok (?Just ('DM'.put idx newval oldmap))

	notify :: !a !(Map a b) !b -> SDSNotifyPred a | < a & == a
	notify idx oldmap newval = \_ idx` -> idx == idx`

	reducer p map = Ok (fromJust ('DM'.get p map))

mapLens :: !String !(Shared sds (Map a b)) !(?b) -> SDSLens a b b | TC, <, ==, gHash{|*|} a & TC b & RWShared sds
mapLens name origShare mdef = sdsLens name (const ()) (SDSRead (read mdef)) (SDSWrite write) (SDSNotify notify) (?Just reducer) origShare
where
	read :: !(?b) !a !(Map a b) -> MaybeError TaskException b | < a & == a
	read mdef idx m = case 'DM'.get idx m of
		?Just x -> Ok x
		_       -> case mdef of
			?Just def -> Ok def
			_         -> Error (exception (name +++ " (mapLens): Index not found"))

	write :: !a !(Map a b) !b -> MaybeError TaskException (?(Map a b)) | < a & == a
	write idx oldmap newval = Ok (?Just ('DM'.put idx newval oldmap))

	notify :: !a !(Map a b) !b -> SDSNotifyPred a | < a & == a
	notify idx oldmap newval = \_ idx` -> idx == idx`

	reducer :: a (Map a b) -> MaybeError TaskException b | < a & == a
	reducer a map = case 'DM'.get a map of
		?Just b = Ok b
		?None = Error (exception (name +++ " (mapLens): Index not found"))

intMapLens :: !String !(Shared sds (IntMap a)) !(?a) -> SDSLens Int a a | TC a & RWShared sds
intMapLens name origShare mdef = sdsLens name (const ()) (SDSRead (read mdef)) (SDSWrite write) (SDSNotify notify) (?Just (reducer mdef)) origShare
where
	read :: !(?a) !Int !(IntMap a) -> MaybeError TaskException a
	read mdef idx intmap = case 'DIS'.get idx intmap of
		?Just x -> Ok x
		_       -> case mdef of
			?Just def -> Ok def
			_         -> Error (exception (name +++ " (intMapLens): Index " +++ toString idx +++ " not found"))

	write :: !Int !(IntMap a) !a -> MaybeError TaskException (?(IntMap a))
	write idx oldmap newval = Ok (?Just ('DIS'.put idx newval oldmap))

	notify :: !Int !(IntMap a) !a -> SDSNotifyPred Int
	notify idx oldmap newval = \_ idx` -> idx == idx`

	reducer mdef i map = read mdef i map
