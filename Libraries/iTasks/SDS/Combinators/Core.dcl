definition module iTasks.SDS.Combinators.Core
/**
* This module provides the core builtin combinators for composing shared data sources.
*/
import iTasks.SDS.Definition
from iTasks.Internal.IWorld import :: IWorld
from Data.Either import :: Either
from Data.GenHash import generic gHash
from Text.GenJSON import :: JSONNode
from System.Time import :: Timespec

from iTasks.WF.Definition import class iTask
from iTasks.WF.Definition import generic gEditor, generic gEq, generic gText, generic JSONEncode, generic JSONDecode

from iTasks.UI.Editor import :: Editor
from iTasks.Internal.Generic.Visualization import :: TextFormat

/** Apply a parametric lens to a SDS.
 * @param A name used to uniquely identify the SDS in combination with the child's identity. (Lazy as the name should
 *        only be evaluated when required for performance reasons.)
 * @param Translaction function for the parameter spaces.
 * @param The write part of the lens.
 * @param The read part of the lens.
 * @param The notification part of the lens.
 * @param An optional reducer required for atomically updating the SDS.
 * @param The original SDS.
 * @result The derived SDS.
 */
sdsLens ::
	String
	!(p -> ps)
	!(SDSLensRead p r rs) !(SDSLensWrite p w rs ws) !(SDSLensNotify p w rs)
	!(?(SDSReducer p ws w))
	!(sds ps rs ws)
	-> SDSLens p r w
	| gHash{|*|} p & TC ps & TC rs & TC ws & RWShared sds

/**
 * An SDS which select one of its two child SDS based on the parameter.
 *
 * @param A name used to uniquely identify the SDS in combination with the children's identity. (Lazy as the name
 *        should only be evaluated when required for performance reasons.)
 * @param The selection function for choosing a child and determining its parameter.
 * @param The first child.
 * @param The second child.
 * @result The select SDS.
 */
sdsSelect ::
	String !(p -> Either p1 p2) !(sds1 p1 r w) !(sds2 p2 r w) -> SDSSelect p r w
	| TC p1 & TC p2 & TC r & TC w & RWShared sds1 & RWShared sds2

/**
 * Create a new SDS by simultaneous access to two independent SDSs.
 * @param A name used to uniquely identify the SDS in combination with the children's identity. (Lazy as the name
 *        should only be evaluated when required for performance reasons.)
 * @param Translation function for the parameter space.
 * @param Translation function for the read value.
 * @oaram Write lens for the first child.
 * @oaram Write lens for the second child.
 * @param The first child.
 * @param The second child.
 * @result The derived SDS.
 */
sdsParallel ::
	String
	!(p -> (p1,p2)) !((r1,r2) -> r)
	!(SDSLensWrite p w r1 w1) !(SDSLensWrite p w r2 w2)
	!(sds1 p1 r1 w1) !(sds2 p2 r2 w2)
	-> SDSParallel p r w
	| TC p1 & TC p2 & TC r1 & TC r2 & TC w1 & TC w2 & RWShared sds1 & RWShared sds2

/**
 * Create a new SDS by sequential access to two dependent SDSs.
 * @param A name used to uniquely identify the SDS in combination with the children's identity. (Lazy as the name
 *        should only be evaluated when required for performance reasons.)
 * @param Translation function for the parameter space of the first child.
 * @param Translation function for reading with the choice to either produce a values based on the first child or a
 *        parameter and function to produce a value based on the second child as well. The first option provides better
 *        performance as the value of the second child is not required.
 * @oaram Write lens for the first child.
 * @oaram Write lens for the second child.
 * @param The first child.
 * @param The second child.
 * @result The derived SDS.
 */
sdsSequence ::
	String !(p -> p1) !(p r1 -> Either r (p2, r2 -> r)) !(SDSLensWrite p w r1 w1) !(SDSSequenceWrite p r1 w p2 r2 w2)
	!(sds1 p1 r1 w1) !(sds2 p2 r2 w2)
	-> SDSSequence p r w | TC p1 & TC p2 & TC r1 & TC r2 & TC w1 & TC w2 & RWShared sds1 & RWShared sds2

// Create a cached version of an SDS
sdsCache ::
	!(p (?r) (?w) w -> (?r, SDSCacheWrite))
	!(SDSSource p r w)
	-> SDSCache p r w
	| iTask, gHash{|*|} p & TC r & TC w
