implementation module iTasks.SDS.Combinators.Core

import Data.Either, Data.GenHash
import iTasks.SDS.Definition, iTasks.Internal.SDS

import iTasks.Internal.Generic.Visualization
import iTasks.Internal.Generic.Defaults
import iTasks.UI.Editor.Generic
import iTasks.WF.Definition
import Data.GenEq, StdString

sdsLens ::
	String
	!(p -> ps)
	!(SDSLensRead p r rs) !(SDSLensWrite p w rs ws) !(SDSLensNotify p w rs)
	!(?(SDSReducer p ws w))
	!(sds ps rs ws)
	-> SDSLens p r w
	| gHash{|*|} p & TC ps & TC rs & TC ws & RWShared sds
sdsLens name param read write notify reducer sds = SDSLens sds
	{ SDSLensOptions
	| id      = createSDSIdentity name (?Just (sdsIdentity sds)) ?None
	, name    = name
	, param   = param
	, read    = read
	, write   = write
	, notify  = notify
	, reducer = reducer
	}

sdsSelect ::
	String !(p -> Either p1 p2) !(sds1 p1 r w) !(sds2 p2 r w) -> SDSSelect p r w
	| TC p1 & TC p2 & TC r & TC w & RWShared sds1 & RWShared sds2
sdsSelect name select sds1 sds2 = SDSSelect sds1 sds2
	{ SDSSelectOptions
	| id      = createSDSIdentity name (?Just (sdsIdentity sds1)) (?Just (sdsIdentity sds2))
	, name    = name
	, select  = select
	}

sdsParallel ::
	String
	!(p -> (p1,p2)) !((r1,r2) -> r)
	!(SDSLensWrite p w r1 w1) !(SDSLensWrite p w r2 w2)
	!(sds1 p1 r1 w1) !(sds2 p2 r2 w2)
	-> SDSParallel p r w
	| TC p1 & TC p2 & TC r1 & TC r2 & TC w1 & TC w2 & RWShared sds1 & RWShared sds2
sdsParallel name param read write1 write2 sds1 sds2 = SDSParallel sds1 sds2
	{ SDSParallelOptions
	| id     = createSDSIdentity ("|"+++name) (?Just (sdsIdentity sds1)) (?Just (sdsIdentity sds2))
	, name   = name
	, param  = param
	, read   = read
	, writel = write1
	, writer = write2
	}

sdsSequence ::
	String !(p -> p1) !(p r1 -> Either r (p2, r2 -> r)) !(SDSLensWrite p w r1 w1) !(SDSSequenceWrite p r1 w p2 r2 w2)
	!(sds1 p1 r1 w1) !(sds2 p2 r2 w2)
	-> SDSSequence p r w | TC p1 & TC p2 & TC r1 & TC r2 & TC w1 & TC w2 & RWShared sds1
	& RWShared sds2
sdsSequence name paraml read write1 write2 sds1 sds2 = SDSSequence sds1 sds2
	{ SDSSequenceOptions
	| id = createSDSIdentity (">"+++name) (?Just (sdsIdentity sds1)) (?Just (sdsIdentity sds2)), name   = name
	, paraml = paraml, read = read, writel = write1, writer = write2 }

sdsCache ::
	!(p (?r) (?w) w -> (?r, SDSCacheWrite))
	!(SDSSource p r w)
	-> SDSCache p r w
	| iTask, gHash{|*|} p & TC r & TC w
sdsCache write sds = SDSCache sds
	{ SDSCacheOptions
	| id    = createSDSIdentity "%" (?Just (sdsIdentity sds)) ?None
	, write = write
	}
