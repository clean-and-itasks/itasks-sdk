implementation module iTasks.SDS.Sources.System

import iTasks.SDS.Definition
import iTasks.SDS.Combinators.Core
import iTasks.SDS.Combinators.Common
import iTasks.Extensions.DateTime //FIXME: Extensions should not be part of core
import System.Time
import Data.Func, Data.Either, Data.GenHash
import qualified Data.Map as Map
import Data.Maybe
from Text import class Text(concat), instance Text String

import iTasks.Engine
import iTasks.Internal.SDS
import iTasks.Internal.IWorld
import iTasks.Internal.AsyncIO
import iTasks.Internal.Util
import iTasks.WF.Definition
import iTasks.WF.Derives

import iTasks.Internal.TaskState
import iTasks.WF.Combinators.Core

import StdTuple, StdList, StdString
from StdFunc import id, o, const

import qualified Data.Map as DM

derive gDefault TaskListFilter, TaskId

NS_SYSTEM_DATA :== "SystemData"

currentDateTime :: SDSParallel () DateTime ()
currentDateTime = iworldLocalDateTime

currentTime :: SDSLens () Time ()
currentTime =: mapRead toTime iworldLocalDateTime

currentDate :: SDSLens () Date ()
currentDate =: mapRead toDate iworldLocalDateTime

currentUTCDateTime :: SDSLens () DateTime ()
currentUTCDateTime =: mapRead timestampToGmDateTime currentTimestamp

currentUTCTime :: SDSLens () Time ()
currentUTCTime =: mapRead (toTime o timestampToGmDateTime) currentTimestamp

currentUTCDate :: SDSLens () Date ()
currentUTCDate =: mapRead (toDate o timestampToGmDateTime) currentTimestamp

currentTimestamp :: SDSLens () Timestamp ()
currentTimestamp =: toReadOnly (sdsFocus {start=Timestamp 0,interval=Timestamp 1} iworldTimestamp)

currentTimespec :: SDSLens () Timespec ()
currentTimespec =: toReadOnly (sdsFocus {start=zero,interval=zero} iworldTimespec)

// Workflow processes
topLevelTasks :: SharedTaskList ()
topLevelTasks = topLevelTaskList

currentSessions :: SDSLens () [TaskListItem ()] ()
currentSessions =:
	mapRead (map (toTaskListItem self) o snd) (toReadOnly (sdsFocus param taskListMetaData))
where
	self = TaskId 0 0
	param = (TaskId 0 0,self,defaultValue,efilter)
	efilter = {ExtendedTaskListFilter|defaultValue & includeSessions = True, includeDetached = False, includeStartup = False}

currentProcesses :: SDSLens () [TaskListItem ()] ()
currentProcesses =:
	mapRead (map (toTaskListItem self) o snd) (toReadOnly (sdsFocus param taskListMetaData))
where
	self = TaskId 0 0
	param = (TaskId 0 0,self,defaultValue,efilter)
	efilter = {ExtendedTaskListFilter|defaultValue & includeSessions = False, includeDetached = True, includeStartup = False}

taskInstanceFromMetaData :: TaskMeta -> TaskInstance
taskInstanceFromMetaData {TaskMeta|taskId=taskId=:(TaskId instanceNo _),instanceType,build,createdAt,detachedFrom,status
	,instanceKey,firstEvent,lastEvent,taskAttributes,managementAttributes}
    = {TaskInstance|instanceNo = instanceNo, instanceKey = instanceKey, session = session, listId = listId, build = build
      ,taskAttributes = taskAttributes, managementAttributes = managementAttributes, value = value
	  ,issuedAt = createdAt, firstEvent = firstEvent, lastEvent = lastEvent}
where
	session = (instanceType =: SessionInstance )
	listId = fromMaybe (TaskId 0 0) detachedFrom
	value = either Exception (\stable -> if stable Stable Unstable) status

currentTaskInstanceNo :: SDSSource () InstanceNo ()
currentTaskInstanceNo =:
	createInternalReadOnlySDS False
		"iTasks.SDS.Sources.System.currentTaskInstanceNo"
		(\() iworld=:{currentInstance} = (Ok currentInstance, iworld))

currentTaskInstanceAttributes :: SDSSequence () TaskAttributes TaskAttributes
currentTaskInstanceAttributes =:
	sdsSequence
		"currentTaskInstanceAttributes" (\_ = ()) read (SDSWriteConst \_ _ = Ok ?None)
		(SDSSequenceWrite (\_ selfNo = paramFor selfNo) write2) currentTaskInstanceNo taskListMetaData
where
	read ::
		x !Int
		-> Either y ((TaskId, TaskId, TaskListFilter, ExtendedTaskListFilter), (z, [TaskMeta]) -> Map String JSONNode)
	read  _ selfNo = Right
		( paramFor selfNo
		, \(_,[{TaskMeta|taskAttributes,managementAttributes}]) = 'DM'.union managementAttributes taskAttributes )

	write2 :: x !Int !(y, ![TaskMeta]) !(Map String JSONNode) -> MaybeError TaskException (?[TaskMeta])
	write2 _ selfNo (_,[meta]) update = Ok $ ?Just
		[{TaskMeta|meta & managementAttributes = 'DM'.union update meta.TaskMeta.managementAttributes}]

	paramFor :: !InstanceNo -> (!TaskId, !TaskId, !TaskListFilter, !ExtendedTaskListFilter)
	paramFor selfNo = (TaskId 0 0, TaskId selfNo 0, tfilter selfNo, defaultValue)
	where
		tfilter no = {TaskListFilter|defaultValue & onlyTaskId = ?Just [TaskId no 0]}

currentTaskInstanceCookies :: SDSSequence () Cookies (String,String,?Int)
currentTaskInstanceCookies =: sdsSequence
		"currentTaskInstanceCookies" (\_ = ()) read (SDSWriteConst \_ _ = Ok ?None)
		(SDSSequenceWrite (\_ = param2) write2) currentTaskInstanceNo taskListMetaData
where
	param2 :: !Int -> (!TaskId, !TaskId, !TaskListFilter, !ExtendedTaskListFilter)
	param2 selfNo = (TaskId 0 0, TaskId selfNo 0, tfilter selfNo, defaultValue)
	where
		tfilter no = {TaskListFilter|defaultValue & onlyTaskId = ?Just [TaskId no 0]}

	read ::
		x !Int
		-> Either y ((TaskId, TaskId, TaskListFilter, ExtendedTaskListFilter), (TaskId, [TaskMeta]) -> Cookies)
	read _ selfNo = Right (param2 selfNo, read`)
	where
		read` :: !(!TaskId, ![TaskMeta]) -> Map String String
		read` (_,[{TaskMeta|cookies}]) = cookies
		read` _ = 'Map'.newMap

	write2 :: !x !Int !(y, ![TaskMeta]) !(String, String, ?Int) -> MaybeError TaskException (?[TaskMeta])
	write2 _ selfNo (_, [meta]) setcookie = Ok $ ?Just
		[ {TaskMeta|meta & cookies = set setcookie meta.TaskMeta.cookies
		, unsyncedCookies = [setcookie:meta.TaskMeta.unsyncedCookies]} ]
	where
		set (key,value,?Just 0) cookies = 'DM'.del key cookies
		set (key,value,_) cookies = 'DM'.put key value cookies
	write2 _ _ _ _ = Error $ exception "currentTaskInstanceCookies: meta info not present"

allTaskInstances :: SDSSequence () [TaskInstance] ()
allTaskInstances =: sdsSequence
	"allTaskInstances" (\_ = ()) read (SDSWriteConst \_ _ = Ok ?None) (SDSSequenceWriteConst \_ _ _ = Ok ?None)
	currentTaskInstanceNo taskListMetaData
where
	param2 :: !Int -> (!TaskId, !TaskId, !TaskListFilter, !ExtendedTaskListFilter)
	param2 selfNo = (TaskId 0 0,TaskId selfNo 0, fullTaskListFilter,fullExtendedTaskListFilter)

	read ::
		x !Int
		->
		Either y ((!TaskId, !TaskId, !TaskListFilter, !ExtendedTaskListFilter), (TaskId, [TaskMeta]) -> [TaskInstance])
	read _ selfNo = Right (param2 selfNo, \(_,meta) = map taskInstanceFromMetaData meta)

detachedTaskInstances :: SDSSequence () [TaskInstance] ()
detachedTaskInstances =: sdsSequence
	"detachedTaskInstances" (\_ = ()) read (SDSWriteConst \_ _ = Ok ?None) (SDSSequenceWriteConst \_ _ _ = Ok ?None)
	currentTaskInstanceNo taskListMetaData
where
	param2 :: !Int -> (!TaskId, !TaskId, !TaskListFilter, !ExtendedTaskListFilter)
	param2 selfNo = (TaskId 0 0,TaskId selfNo 0,tfilter,efilter)
	where
		tfilter = {TaskListFilter|fullTaskListFilter & includeProgress = True, includeManagementAttributes = True}
		efilter =
			{ ExtendedTaskListFilter|fullExtendedTaskListFilter & includeSessions = False, includeDetached = True
			, includeStartup = False }

	read ::
		x !Int
		-> Either y ((TaskId, TaskId, TaskListFilter, ExtendedTaskListFilter), (TaskId, [TaskMeta]) -> [TaskInstance])
	read _ selfNo = Right (param2 selfNo, \(_,meta) = map taskInstanceFromMetaData meta)

taskInstanceByNo :: SDSSequence InstanceNo TaskInstance TaskAttributes
taskInstanceByNo =: sdsSequence
	"taskInstanceByNo" (\_ = ()) read (SDSWriteConst \_ _ = Ok ?None) (SDSSequenceWrite param2 write2)
	currentTaskInstanceNo taskListMetaData
where
	param2 :: !Int !Int -> (!TaskId, !TaskId, !TaskListFilter, !ExtendedTaskListFilter)
	param2 no selfNo = (TaskId 0 0, TaskId selfNo 0, tfilter no, defaultValue)
	where
		tfilter no = {TaskListFilter|defaultValue & onlyTaskId = ?Just [TaskId no 0]}

	read ::
		!Int !Int
		-> Either x ((TaskId, TaskId, TaskListFilter, ExtendedTaskListFilter), (TaskId, [TaskMeta]) -> TaskInstance)
	read no selfNo = Right (param2 no selfNo, \(_,[meta]) = taskInstanceFromMetaData meta)

	write2 :: !Int !Int !(x, ![TaskMeta]) !TaskAttributes -> MaybeError e (?[TaskMeta])
	write2 no selfNo (_,[meta]) update =
		Ok $ ?Just [{TaskMeta|meta & managementAttributes = 'DM'.union update meta.TaskMeta.managementAttributes}]

taskInstanceAttributesByNo :: SDSSequence InstanceNo TaskAttributes TaskAttributes
taskInstanceAttributesByNo =: sdsSequence
	"taskInstanceAttributesByNo" (\_ = ()) read (SDSWriteConst \_ _ = Ok ?None) (SDSSequenceWrite param2 write2)
	currentTaskInstanceNo taskListMetaData
where
	param2 :: !Int !Int -> (!TaskId, !TaskId, !TaskListFilter, !ExtendedTaskListFilter)
	param2 no selfNo = (TaskId 0 0, TaskId selfNo 0, tfilter no, defaultValue)
	where
		tfilter no = {TaskListFilter|defaultValue & onlyTaskId = ?Just [TaskId no 0]}

	read ::
		!Int !Int
		->Either
			x ((TaskId, TaskId, TaskListFilter, ExtendedTaskListFilter), (TaskId, [TaskMeta]) -> Map String JSONNode)
	read no selfNo =
		Right
			( param2 no selfNo
			, \(_,[{TaskMeta|taskAttributes,managementAttributes}]) =
				'DM'.union managementAttributes taskAttributes )

	write2 :: !Int !Int !(x, ![TaskMeta]) !TaskAttributes -> MaybeError e (?[TaskMeta])
	write2 no selfNo (_, [meta]) update =
		Ok $ ?Just [{TaskMeta|meta & managementAttributes = 'DM'.union update meta.TaskMeta.managementAttributes}]

taskInstancesByAttribute :: SDSLens (!String,!JSONNode) [TaskInstance] ()
taskInstancesByAttribute
    =
      (sdsProject (SDSLensRead readInstances) (SDSBlindWrite \_. Ok ?None) ?None
       (sdsTranslate "taskInstancesByAttribute" param taskListMetaData))
where
	self = TaskId 0 0
	param p = (TaskId 0 0,self, tfilter p, defaultValue)
	tfilter p = {TaskListFilter|defaultValue & onlyAttribute = ?Just p}

    readInstances (_,is) = Ok (map taskInstanceFromMetaData is)

currentTopTask :: SDSLens () TaskId ()
currentTopTask =: mapRead (\currentInstance -> TaskId currentInstance 0) currentTaskInstanceNo

applicationName :: SDSSource () String ()
applicationName =: createInternalReadOnlySDS False "iTasks.SDS.Sources.System.applicationName" appName
where
	appName () iworld=:{IWorld|options={EngineOptions|appName}} = (Ok appName,iworld)

applicationURL :: SDSSource () String ()
applicationURL =: createInternalReadOnlySDS False "iTasks.SDS.Sources.System.applicationURL" appUrl
where
	appUrl _ iworld=:{IWorld | options={serverProtocol,serverDomain,serverPort,serverDirectory}}
		= (Ok $ concat parts, iworld)
	where
		parts = [serverProtocol,"://",serverDomain,mbPort,serverDirectory]
		mbPort = if (serverPort == Port 80) "" (":" +++ toString serverPort)

applicationVersion :: SDSSource () String ()
applicationVersion =: createInternalReadOnlySDS False "iTasks.SDS.Sources.System.applicationVersion" appBuild
where
	appBuild () iworld=:{IWorld|options={EngineOptions|appVersion}} = (Ok appVersion,iworld)

applicationDirectory :: SDSSource () FilePath ()
applicationDirectory =: createInternalReadOnlySDS False "iTasks.SDS.Sources.System.applicationDirectory" appDir
where
	appDir () iworld=:{IWorld|options={EngineOptions|appPath}} = (Ok $ takeDirectory appPath,iworld)

applicationOptions :: SDSSource () EngineOptions ()
applicationOptions =: createInternalReadOnlySDS False "iTasks.SDS.Sources.System.applicationOptions" options
where
	options () iworld=:{IWorld|options} = (Ok options,iworld)
