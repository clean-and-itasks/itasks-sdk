definition module iTasks.SDS.Sources.Core
/*
* This module provides the builtin shared sources
*/
import iTasks.SDS.Definition
from System.FilePath import :: FilePath
from Data.Error import :: MaybeError, :: MaybeErrorString
from Data.GenHash import generic gHash

// constant share from which you always read the same value
constShare :: !a -> SDSSource p a () | gHash{|*|} p & gHash {|*|} a

/**
 * constant share from which you always read the same value, with a given SDS identifier
 * (no `gHash` constraint on the value).
 *
 * @param The unique identifier
 * @param The value
 * @result SDS from which the value may be read.
 */
constShareNamed :: !String !a -> SDSSource p a () | gHash{|*|} p

// null share to which you can write anything
nullShare :: SDSSource p () a | gHash{|*|} p

// Useful placeholder when you need a share don't intent to use it
unitShare :: SimpleSDSSource ()

// Random source
randomInt :: SDSSource () Int ()

// Random string (the parameters determines its length)
randomString :: SDSSource Int String ()

/**
 * An SDS which reads/writes using world functions.
 *
 * @param Whether reads should be cached (`True`) or not (`False`).
 * @param The read function, which reads a value using `World`.
 * @param The write function, which writes a value using `World`.
 * @param Function which determines if the SDS should be notified (`True`) or not (`False`).
 *
 */
worldShare ::
	!Bool
	!(p *World -> *(MaybeErrorString r,*World))
	!(p w *World -> *(MaybeErrorString (),*World))
	!(p Timespec p -> Bool)
	-> SDSSource p r w
	| gHash{|*|} p

// memory share (essentially a global variable)
memoryShare :: SDSSource String (?a) (?a) | TC a

/**
 * Share that maps to the plain contents of a file on disk.
 * When the file does not exist on reading it returns `?None`. By writing
 * `?None` you can remove the file.
 */
fileShare :: SDSSource FilePath (?String) (?String)

//* Share that maps to a file encoded as JSON.
jsonFileShare :: SDSSource FilePath (?a) (?a) | JSONEncode{|*|}, JSONDecode{|*|} a

// Share that maps to a file that holds a serialized graph representation of the value
graphFileShare :: SDSSource FilePath (?a) (?a)

// Directory
directoryListing :: SDSSource FilePath [String] ()
