implementation module BasicAPIExamples.Extensions.JavaScript

import Data.Functor
import Text

import iTasks
import iTasks.Extensions.JavaScript

import ABC.Interpreter.JavaScript
import ABC.Interpreter.JavaScript.Monad

wf :: String -> Workflow
wf a = workflow a "Run JavaScript code on the frontend" main

main :: Task ()
main =
	allTasks
		[ Title "Check the browser console" @>>
			appJSWorld (jsTrace "Hello world from WebAssembly!") @! ""
		, Title "This is your userAgent:" @>>
			(accJSWorld userAgent >>~ viewInformation [])
		, Title "This is your doNotTrack setting:" @>>
			(runJSMonad () doNotTrack >>~ viewInformation [])
		]
	@! ()
where
	userAgent :: *JSWorld -> (String, *JSWorld)
	userAgent world = jsGlobal "navigator" .# "userAgent" .?? ("", world)

	doNotTrack :: JS () String
	doNotTrack =
		fromJS "undefined" <$>
		accJS ((.?) (jsWindow .# "navigator.doNotTrack"))
