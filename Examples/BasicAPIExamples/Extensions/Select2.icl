implementation module BasicAPIExamples.Extensions.Select2

import Text

import iTasks
import iTasks.UI.Editor.Common
import iTasks.Extensions.Editors.Select2

wf :: String -> Workflow
wf a = workflow a "Try out the Select2 extension for searchable select boxes" main

main :: Task ()
main = ArrangeWithTabs False @>>
	allTasks
		[ select2AndNormal "Enter expression (withSelect2)" enterExpression
		, select2AndNormal "Dropdown" enterWithDropdown
		, select2AndNormal "Dropdown with groups" enterWithDropdownWithGroups
		, withShared fruits \shared ->
			Title "Dropdown with dynamic options" @>> (
				(Hint "Enter options:" @>> updateSharedInformation [] shared) ||-
				select2AndNormal "" (dropdownWithDynamicOptions shared)
			)
		]
	@! ()
where
	select2AndNormal title task = Title title @>> ArrangeHorizontal @>>
		(
			forever (task True "with Select2 input fields")
		-&&-
			forever (task False "with standard HTML select boxes")
		)
		@! ()

:: Expr
	= Literal Int
	| Add Expr Expr
	| Sum [Expr]

derive class iTask Expr

enterExpression :: !Bool !String -> Task Expr
enterExpression select2 hint =
	Hint (concat ["Enter an expression ",hint,":"]) @>>
	enterInformation [EnterUsing id editor] >>! \expr ->
	Hint "You entered:" @>>
	viewInformation [] expr >>! return
where
	editor = if select2 (withSelect2 True (gEditor{|*|} EditValue)) (gEditor{|*|} EditValue)

fruits =: ["Apple", "Banana", "Cherry"]
veggies =: ["Arugula", "Broccoli", "Chickpea"]

enterWithDropdown :: !Bool !String -> Task String
enterWithDropdown select2 hint =
	Hint (concat ["Enter a value ",hint,":"]) @>>
	enterInformation [EnterUsing id (choose fruits)] @ ((!!) fruits) >>! \choice ->
	Hint "You entered:" @>>
	viewInformation [] choice >>! return
where
	choose = if select2 chooseWithSelect2Dropdown chooseWithDropdown

enterWithDropdownWithGroups :: !Bool !String -> Task String
enterWithDropdownWithGroups select2 hint =
	Hint (concat ["Enter a value ",hint,":"]) @>>
	enterInformation [EnterUsing id editor] @ (fst o (!!) options) >>! \choice ->
	Hint "You entered:" @>>
	viewInformation [] choice >>! return
where
	editor = mapEditorRead (\i -> [i])
		(mapEditorWrite (\sel -> case sel of [x] -> ValidEditor x; _ -> EmptyEditor)
		(withConstantChoices numberedOptions
			(if select2 select2DropdownWithGroups dropdownWithGroups)))
	numberedOptions =
		[({ChoiceText | id=i, text=t}, group) \\ (t,group) <- options & i <- [0..]]
	options =
		[(opt, ?Just "Fruits") \\ opt <- fruits] ++
		[(opt, ?Just "Veggies") \\ opt <- veggies]

derive class iTask ChoiceText

dropdownWithDynamicOptions :: !(SimpleSDSLens [String]) !Bool !String -> Task [String]
dropdownWithDynamicOptions options select2 hint =
	ArrangeVertical @>>
	Hint (concat ["Make a choice ",hint,":"]) @>>
	editSelectionWithShared
		[ SelectUsing
			(\opts -> [{ChoiceText | id=i,text=o} \\ o <- opts & i <- [0..]])
			(\opts is -> [opts !! i \\ i <- is])
			editor
		]
		options
		(const []) >>! \choices ->
	Hint "You entered:" @>>
	viewInformation [] choices >>! return
where
	editor = if select2 select2Dropdown dropdown <<@ multipleAttr True
