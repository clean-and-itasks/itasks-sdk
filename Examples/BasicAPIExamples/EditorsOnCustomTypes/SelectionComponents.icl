implementation module BasicAPIExamples.EditorsOnCustomTypes.SelectionComponents

import Data.Func
import Text.HTML
from Data.List import concatMap

import iTasks

wf :: String -> Workflow
wf a = workflow a "Play around with selection components" main

singleSelection :: SimpleSDSLens [Int]
singleSelection =: sharedStore "singleSelection" [0]

multipleSelection :: SimpleSDSLens [Int]
multipleSelection =: sharedStore "multipleSelection" [0]

main :: Task ()
main =
	(
		viewInformation [] "These tasks each contain six selection components connected to a single share."
	-&&-
		(ArrangeWithTabs False @>>
		allTasks
		[ selectionTasks False
		, selectionTasks True
		])
	)
	@! ()
where
	selectionTasks multiple =
		Title (if multiple "Multiple selection" "Single selection") @>>
		ArrangeHorizontal @>>
		(
			allTasks
				[ edit "Dropdown"   SelectInDropdown   toChoiceTexts
				, edit "CheckGroup" SelectInCheckGroup toChoiceTexts
				, edit "List"       SelectInList       toChoiceTexts
				]
		-&&-
			allTasks
				[ edit "Grid"       SelectInGrid       toChoiceGrid
				, edit "Tree"       SelectInTree       toChoiceNodes
				, edit "Tabs"       SelectInTabs       toChoiceTexts
				]
		)
	where
		edit title type mod =
			Title title @>>
			editSharedSelection
				[type mod toChoices, SelectMultiple multiple]
				options
				(if multiple multipleSelection singleSelection)

	options =
		[ ("Fruits",     ["Apple", "Banana", "Cherry"])
		, ("Vegetables", ["Asparagus", "Broccoli", "Cauliflower"])
		]

	toChoices options keys =
		[ opt
		\\ opt <- concatMap snd options & id <- [0..]
		| isMember id keys
		]

withCounters :: ![(a,[b])] -> [(a,[b],Int)]
withCounters xs = fst $ mapSt (\(group,xs) n -> ((group, xs, n), n+length xs)) xs 0

toChoiceTexts options =
	[ {ChoiceText | id=id, text=opt}
	\\ opt <- concatMap snd options & id <- [0..]
	]

toChoiceGrid options =
	{ header = ["Group", "Option"]
	, rows =
		[ {id=id, cells=[Text group, Text opt]}
		\\ (group,opts,start) <- withCounters options
		, opt <- opts & id <- [start..]
		]
	}

toChoiceNodes options =
	[
		{ id       = id
		, label    = group
		, icon     = ?None
		, expanded = True
		, children =
			[
				{ id       = id
				, label    = opt
				, icon     = ?None
				, expanded = False
				, children = []
				}
			\\ opt <- opts & id <- [start..]
			]
		}
	\\ (group,opts,start) <- withCounters options
	& id <- [-1,-2..]
	]
