implementation module BasicAPIExamples.InteractionUsingShares.CurrentDateAndTime

// Just show the current date and time which are offered in a share

import iTasks
import iTasks.Extensions.DateTime

wf :: String -> Workflow
wf a = workflow a "View the current Date and Time" showDateAndTime

main :: Task ()
main = showDateAndTime @! ()

showDateAndTime :: Task DateTime
showDateAndTime
	= 	Hint "The current Date and Time is:" @>> viewSharedInformation [] currentDateTime
