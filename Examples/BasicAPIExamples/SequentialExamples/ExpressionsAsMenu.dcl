definition module BasicAPIExamples.SequentialExamples.ExpressionsAsMenu

/**
 * This Basic API examples allows entering simple boolean/arithmatic expressions.
 * The choice for what kind of expression to enter/evaluate can be made through a menu.
 * The main purpose of this example is to demonstrate how ArrangeAsMenu can be used to generate a menu for
 * selecting a task continuation.
 */

from iTasks.WF.Definition import :: Task
from iTasks.Extensions.Admin.WorkflowAdmin import :: Workflow

wf :: String -> Workflow

main :: Task ()

menuExample :: Task ()
