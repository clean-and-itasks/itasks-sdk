implementation module BasicAPIExamples.SequentialExamples.PositiveNumber

// Enter a postive Integer number

import iTasks

wf :: String -> Workflow
wf a = workflow a "Enter a positive number" positiveNumber

main :: Task ()
main = positiveNumber @! ()

positiveNumber :: Task Int
positiveNumber =
	Title "Please enter a positive number" @>> enterInformation []
	>>* 	[ OnAction  ActionOk (ifValue (\n -> n > 0) return)
	        ]
	>>- \enteredNumber -> Title "Entered number is:" @>> viewInformation [] enteredNumber
