implementation module BasicAPIExamples.SequentialExamples.ExpressionsAsMenu

import iTasks
import iTasks.UI.Layout.Common
import Data.Func

wf :: String -> Workflow
wf a = workflow a "Expressions as menu" menuExample

main :: Task ()
main = menuExample

menuExample :: Task ()
menuExample =
 	(viewInformation [] "Pick a menu option"
 		>>*
		[ OnAction (Action "/Boolean Expressions/And") (always $ andExpr @! ())
		, OnAction (Action "/Boolean Expressions/Or") (always $ orExpr @! ())
		, OnAction (Action "/Arithmetic Expressions/Add") (always $ addExpr @! ())
		, OnAction (Action "/Arithmetic Expressions/Subtract") (always $ subExpr @! ())
		]
	)
	//* The top level menus have to be specified to arrange as menu and the Action names have to match the top
	//* level menu. For more documentation, see arrangeAsMenu in iTasks.UI.Layout.Common.
	<<@ ArrangeAsMenu [!Menu "/Boolean Expressions", Menu "/Arithmetic Expressions"]
where
	expr :: !(a a -> a) -> Task a | iTask a
	expr op = enterInformation [] >>! viewInformation [] o uncurry op
	andExpr :: Task Bool
	andExpr = expr (&&)
	orExpr :: Task Bool
	orExpr  = expr (||)
	addExpr :: Task Int
	addExpr = expr (+)
	subExpr :: Task Int
	subExpr = expr (-)
