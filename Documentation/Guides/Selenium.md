# Testing with Selenium

We are experimenting with tests using the [Selenium][] framework. This page
gives some background.

## Overview

- The iTasks server under test is compiled with a **specification** of the
  tests.
- This specification is sent as bytecode to a **test server** in JavaScript
  ([node.js][]), which uses the [ABC interpreter][] to run the specification in
  WebAssembly.
- Using the JavaScript interface in `ABC.Interpreter.JavaScript`, and for
  Selenium specifically `iTasks.Testing.Selenium.Interface`, these tests can
  interact with the [JavaScript bindings of Selenium][selenium-js].
- The library can work with [`cleantest`][cleantest] to generate test reports,
  and can make screenshots that can be uploaded as GitLab CI artifacts. See our
  [CI config](/.gitlab-ci.yml) and [pipelines][] for more details.

## Installation

Prerequisites: [node.js][] and [npm][] (normally included with node.js).

- Run `npm install` in `./Tests/Selenium/driver`.
- On Linux or Mac, run `./postinstall.sh` from `driver` to fetch the
  dependencies.
- On Windows (or when `postinstall.sh` fails), fetch the dependencies manually:
  - Download the Selenium driver for your browser to `driver`. See
	[the docs][selenium-js] for links for each browser; the tool has been
	tested with Firefox and Chrome.
  - Copy the contents of `$CLEAN_HOME/lib/ABCInterpreter/WebPublic/js` to
	`driver` as well.

You should now be able to run `node driver.js` from `driver`.

Note that to use the driver for some browser, you need to have that browser
installed as well: you cannot use the Chrome driver without installing Chrome.

## Running tests

With the driver still running, run in another terminal in
`Tests/Selenium/Test`:

```sh
cp BasicAPIExamples.prj{.default,}
cpm BasicAPIExamples.prj
./BasicAPIExamples.exe
```

(Or, on Windows, rename `BasicAPIExamples.prj.default` to
`BasicAPIExamples.prj` and build it using the Clean IDE.)

A browser window will open briefly to run the tests, and close automatically.

See `./BasicAPIExamples.exe --help` for options, and see the implementation to
see how you can write tests for your own application.

[ABC interpreter]: https://gitlab.com/clean-and-itasks/abc-interpreter
[cleantest]: https://gitlab.com/clean-and-itasks/clean-test
[node.js]: https://nodejs.org/en/
[npm]: https://www.npmjs.com/
[pipelines]: https://gitlab.com/clean-and-itasks/itasks-sdk/-/pipelines
[Selenium]: https://www.selenium.dev/
[selenium-js]: https://seleniumhq.github.io/selenium/docs/api/javascript/index.html
