# Creating Custom Editors #

NOTE: The editors in iTasks are in a transition to a new API. Some of the examples in this guide may seem unnecessarily complex because of this. They will become simpler again in future versions.

## Introduction ##

In this guide we assume you are already familiar with writing programs with iTasks and have used the common task definitions for user interaction such as `updateInformation` and `viewInformation`, but find that the automagically generated GUI's are not working for your application.

In this guide we'll walk you through the process of creating an `Editor`. This is the construct that is used by tasks like `updateInformation` to render the GUI and handle user events. Editors are typed GUI building blocks that can be composed to create any GUI you like. Editors abstract away all UI concerns into a single opaque value. These building blocks define how the GUI is rendered, how events are handled and how the GUI is synchronized with the (task) value it represents.

The remainder of this document is structured as follows: First we'll look at how a custom editor can be applied in an interaction task. We'll then look at how we can configure the builtin editors of the iTask framework for basic types. We end by looking at how we can use editor combinators to build editors for more complex data structures such as records and Algebraic data types.

## Using custom editors in tasks ##

The first thing you need to know to work with custom editors is how you can use them in a task. Let's look at a very simple task we'll use as example:

```Clean
myTask :: Task Int
myTask = Hint "Change the magic number" @>> updateInformation [] 42
```

When rendered, this will give you a simple input box that allows you to update the value. But what if we wanted to use a slider component to edit the integer? We can do this with the following code:

```Clean
myTask :: Task Int
myTask = Hint "Change the magic number" @>> updateInformation
           [UpdateUsing (\x -> x) (\_ x -> x) (mapEditorWrite ValidEditor slider)] 42
```

The GUI will now be rendered as a nice slider. Let's look a little closer at what's going on here.

1. Adding an editor to the task. The second parameter of `updateInformation` is a list of `UpdateOption` values that allow us to change its default behavior. In this case we are using the `UpdateUsing` constructor that is defined like this:

   ```Clean
   ...
   | E.v: UpdateUsing (a -> v) (a v -> a) (Editor v (EditorReport v)) & iTask v
   ...
   ```

   The option consists of three parts. The last is the most important one. We need to specify some value of type `Editor v (EditorReport v)`. The other two functions define how to map between the task value and the value that is being edited (of type ` v`). When we look at editor combinators later on we'll see that the mapping to the  `v` domain is not strictly necessary. By passing the identity mapping `(\x -> x) (\x _ -> x)` we stay in the same domain as the task value, so the editor we are using needs to be of type `Editor Int (EditorReport Int)`.

2. So when using  `UpdateUsing` (or `EnterUsing`,`ViewUsing` etc.) we pass a value of type `Editor v (?v)`. If we are lazy we can simply call `gEditor{|*|}` to create a the generic version that is used by default, but the goal in this guide is to specify a custom GUI. In this example we therefore use the `slider` function, which is a builtin function that returns a value of type `Editor Int Int`.

3. The final piece of the puzzle is `mapEditorWrite`. Editors have two type parameters. The first defines the type of data that is passed into the editor. The second type parameter defines the type of values the editor produces. In most interactions these types are `a` together with `EditorReport a`. The `EditorReport a` allows editors to specify that the editor in its current state can produce an empty value or a temporary invalid value. An editor like `slider` is guaranteed to always have a valid value. Therefore it has the type `Editor Int Int` instead of `Editor Int (EditorReport Int)`. By applying `mapEditorWrite ValidEditor` to a `slider` we make it fit the expected type.

So to use a custom task UI we need to do two things: We need to specify an editor of the right type, and then pass it in the option list of the interaction task we are using. In the next section we'll look at the builtin editors and how we can use them to replace the generic editor function.

## Using the builtin editors ##

The iTask framework provides a number of builtin UI components that it can render in a browser. These are the lowest level building blocks with which all iTasks GUI's are constructed.

In the example of the previous section we have seen the `slider` editor. This editor is one of the builtin componentens in the `iTasks.UI.Editor.Controls` module. All builtin editors have no arguments, but can dynamically be configured by setting attributes. For example if we wanted to set the maximum value of the slider, we would write `slider <<@ maxAttr 42`. The tuning combinators `<<@` or `@>>` are used to set attributes on editors. This pattern is used to make it easy to create editors without the need to specify all attributes in advance. In many cases, it is not necessary to deviate from the default values of the configurable attributes. Forcing a programmer to specify them all makes our GUI code too verbose. The price we pay for this convenience is that we lose some type safety. We dynamically set arbitrary attributes on editors, whether the UI rendering code uses them or not. Two noteworthy attributes are `classAttr` and `styleAttr` which let you attach CSS classes and inline styles to editors.

## Composing editors ##

Creating editors for basic values is useful, but more often we want to construct editors for composite datastructures such as records. Let's expand the slider example to show how you can compose editors.

```Clean
myTask :: Task Int
myTask = Hint "Change the magic number" @>> updateInformation
    [UpdateUsing (\x -> x) (\_ x -> x) editor] 42
where
    editor :: Editor Int (EditorReport Int)
    editor
        = mapEditorRead (\x -> ("MyLabel",x))
        $ mapEditorWrite snd
        $ container2 label slider <<@ classAttr ["itasks-horizontal"]
```

When you run this example, you'll see the same slider as before, but this time with a label "Mylabel" to the left of it. There are a few things going on here. The first new thing is the `label` builtin editor that we are using. This is an editor of type `String a` and simply displays its value. The next new thing is the `container2` combinator. This is where the composition happens. This combinator takes two editors and puts them together in a container. The values are combined into a tuple, so the type of the combined editor in this case is `(String,Int) (EditorReport a,EditorReport Int)`.  There are combinators for different kinds of containers, such as `panel`, `window` or `tabset`. The 'write' type parameter is a tuple of `?` values because, when editing, one of the editors or both can be changed. Because grouping editors with these combinators creates tuples, we need different versions of each depending on how many items we group together. In this case we are using `container2` to group two editors. The last thing we are doing in this example is providing the actual label. We are using the `mapEditorRead`  combinator to add the label as the first element of the tuple. We use `mapEditorWrite` to select only the output of the second element in the tuple. Because `container2` has a pair of `EditorReport` as its write type, we don't have to wrap it in a `ValidEditor` this time.

We now have a composed editor with a nice label that is still of type `Editor Int (EditorReport Int)`, so we can use it as drop-in replacement for our original editor.

In the next example, we'll take it one step further and create a nice little form for a custom record:

```Clean
:: MyRecord =
    { foo :: String 
    , bar :: Int
    }
derive class iTask MyRecord

myTask :: Task MyRecord
myTask = Hint "Enter your data" @>> enterInformation [EnterUsing id editor]
where
    editor
        = mapEditorRead (\{foo,bar} -> (foo,bar))
        $ mapEditorWrite
           (\(mbfoo,mbbar) -> maybe ?None
             (\foo -> (maybe ?None (\bar -> ?Just {foo=foo,bar=bar}) mbbar)) mbfoo)
        $ panel2
            (row "Footastic:" passwordField)
            (row "Barmagic:" (mapEditorWrite ?Just slider))
        <<@ classAttr ["itasks-wrap-height"]
    row l e
        = mapEditorWrite snd
        $ mapEditorRead (\x -> ((),x))
        $ (container2 (viewConstantValue l label) e) <<@ classAttr ["itasks-horizontal"]
```

This example is a little more complex, but is similar to things we have already seen. The `row` function generalizes the addition of labels to an arbitrary editor. 

By constructing editors from the basic building blocks and transforming the value domain of the editors, we can construct any kind of GUI we like.

## Creating dynamic editors ##

One of the nice features of the generic editors is that they work for any type of data structure. You can easily create editors for recursive data types that have values of arbitrary size, not just static forms.

When you choose between different constructors of an ADT, the editor for the fields of the ADT depends on the selected constructor. You can create similar behaviour in your custom editors with the `containerc`/`panelc`/… combinators. The following example shows how this is done for a custom list type `MyList a`.

```
:: MyList a = MyNil | MyCons a (MyList a)
derive class iTask MyList

myTask :: Task (MyList String)
myTask = Hint "Enter the list" @>> enterInformation [EnterUsing id editor]
where
	editor
		= containerc
			(chooseWithDropdown ["Nil","Cons"])
			read
			[(const ?None, nilEditor)
			,(const ?None, consEditor)
			]
		<<@ heightAttr WrapSize

	read MyNil = 0
	read (MyCons _ _) = 1

	nilEditor
 		= mapEditorWrite (const (ValidEditor MyNil)) emptyEditor

	consEditor
		= mapEditorWrite (fmap (\(x,xs) -> MyCons x xs) o editorReportTuple2)
		$ mapEditorRead (\(MyCons x xs) -> (x,xs))
		$ container2 (gEditor{|*|} EditValue) editor
```

There is quite a lot going on in this example, but the central combinator making this work is `containerc`. This combinator groups an editor for making a selection (of type `Editor Int (EditorReport Int)`) with a list of editors. Based on the value of the selection editor an editor from the list is selected to edit the value.

The first argument of `containerc` is the editor for choosing which editor to use. In this case a simple `chooseWithDropdown` with two options. This gives us an editor of type `Editor Int (EditorReport Int)` that produces the values `ValidEditor 0`, `ValidEditor 1` or `EmptyEditor`.
The second argument, the `read` function tells `containerc` which editor in the list to use when updating an existing value. We use the first editor for `MyNil` values and the second for any `MyCons` value.
The last argument is the list of editors to use. We have an editor for creating `MyNil` values and an editor for `MyCons` values.
The type of the list of editors is not `[Editor r (EditorReport w)]`, but `[((EditorReport w) -> ?r, Editor r (EditorReport w)]`.
The function in the tuple is applied whenever the selection changes.
This allows us to initialize the newly chosen editor using the value of the last chosen selection.
In this example we simply use `const ?None` in both cases to start with an empty editor when whe change a `MyCons` to a `MyNil` or the other way around.

The two editors we use are both of type `Editor (MyList String) (EditorReport (MyList String))`.
The `nilEditor` is very simple. It is an empty editor which maps the constant value `ValidEditor MyNil` over its output. This results in an invisible editor which always has a valid `MyNil` value.
The `consEditor` is a grouping of a generic editor, to enter the head of our list, and a recursive application of `editor` for the tail of our list.
The mapping functions in `mapEditorRead` and `mapEditorWrite` map the `MyCons` constructor to a tuple and back.
The `application of `editorReportTuple2` maps the two separate `EditorReport` values to a single `EditorReport` which is only valid if both parts are available.
Note that using a partial function `(MyCons x xs) -> (x,xs)` is safe in this case, because this editor will never be initialised with a `MyNil` value.

What this example shows is that with these combinators you can also make dynamic editors for recursive types.
You can even plug in in the generic editors to create editors for higher order-types.

## Conclusion ##

In this guide we have shown how you can fully customize the GUI of your iTask tasks by creating editors. We have not covered all builtin editors and combinators, but just enough to get you started. You can look at the  documentation of `iTasks.UI.Editor.Controls`, `iTasks.UI.Editor.Containers` and `iTasks.UI.Editor.Modifiers` to find out all possibilities.

Although editors give you full control over the GUI of interaction tasks, they don't define full GUI's. Tasks can be composed, so the GUIs of task compositions are created from their parts. As with everything in iTasks this happens automatically by default, but if you also want to customize this process, the "Layouting Task Compositions" guide will get you started.
