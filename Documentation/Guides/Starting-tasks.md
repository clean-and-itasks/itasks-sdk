# Starting Tasks #

## Introduction ##
For many cases the "standard" way of starting tasks using `doTasks` is all you need.
For example, when you create an iTask program with a single simple task such as this:
```Clean
Start world = doTasks (viewInformation () [] "Hello, world") world
```
your task will make your task available to be started at the URL path `/` of the built-in web-server.
For each client that requests this URL, the server creates an instance of that task for that client.

While this is the common way of starting tasks, it is not the only way.
You can specify different tasks to be started through different URL paths, or you can specify that certain tasks should be started when the iTask program is (re)started.
In this guide we will show you how to do this.

## Starting the engine ##
The engine runs according to the specified `EngineOptions`.
When starting the engine with `doTasks` the default options are used and a command line interface (CLI) is offered to customise the options at runtime:

```Clean
doTasks :: a !*World -> *World | Startable a
```

To customise the engine options, or to start the engine with a different `Startable`, `doTasksWithOptions` is used.
Given the provided command line argument handling function, the engine is initialised with the `Startable`.

```Clean
doTasksWithOptions :: ([String] EngineOptions -> MaybeError [String] (a,EngineOptions)) !*World -> *World | Startable a
```

Extending the default engine options and CLI you get when running `doTasks` is possible using  `withDefaultEngineCLIOptions`.
The first argument contains the list of extra `OptDescr` items (See platform's `System.GetOpt`) and the second argument is a function processing the positional arguments.
Finally the third argument is a function that, given the final engine options, transforms these options (possibly enriched with user options in the `userOpts` field) produces a startable.

With the first three arguments curried in, this function can be passed to `doTasksWithOptions`.

```Clean
withDefaultEngineCLIOptions ::
	[OptDescr ((?EngineOptions) -> ?EngineOptions)]
	([String] -> MaybeError [String] ((?EngineOptions) -> ?EngineOptions))
	(EngineOptions -> MaybeError [String] a)
	[String] EngineOptions -> MaybeError [String] (a, EngineOptions)
```

For example, let's say we want to start the engine so that it has an extra flag for setting the url for the management interface task and a positional argument that specifies whether it should operate as a client or as a server:

```Clean
Start w = doTasksWithOptions (withDefaultEngineCLIOptions extraOpts positionals startable usage) w
where
	extraOpts =
		[Option ['u'] ["url"]
			(ReqArg (\a->fmap \o->{o & userOpts='Data.Map'.put "url" [a] o.userOpts}) "URL")
			"Set the entrypoint url for the management interface (default: /manage)"
		]
	positionals [] = Ok id
	positionals [x] = case x of
		"client" = Ok (fmap \o->{o & userOpts='Data.Map'.put "client" [] o.userOpts})
		"server" = Ok id
		x = Error ["Unrecognised option: ", x]
	positionals _ = Error ["Only one positional argument allowed"]

	startable o
		# url = maybe "/" hd $ 'Data.Map'.get "url" o.userOpts
		= case 'Data.Map'.get "client" o.userOpts of
			// If client is not specified, default to server
			?None = Ok $ onRequest url serverTask
			?Just _ = Ok $ onRequest url clientTask

	usage argv0 = "Usage: " +++ argv0 +++ " [OPTIONS] {client|server}"

	serverTask :: Task ()
	serverTask = ...

	clientTask :: Task ()
	clientTask = ...
```

## Starting different tasks in one application ##
If we want to start different tasks by accessing different URLs we need to specify which URLs correspond to which tasks. This also possible with `doTasks`.
The `doTasks` function is overloaded. It can be used with everything that is `Startable`.
There is an instance of `Startable` for `Task a` which is used in the simple case.
If we want to use `doTasks` with multiple tasks that can have different types, we have to create a value that
wraps those tasks and groups them together.
We do this as follows:
```Clean
taska = viewInformation () [] "Hello, world"
taskb = viewInformation () [] 42

Start world = doTasks
	[onRequest "/ta" taska
    ,onRequest "/tb" taskb
	] world
```

The `onRequest` function takes a task and wraps it together with the URL path as a `StartableTask` value.
If you need the information from the HTTP request to compute the task, you can use `onRequestFromRequest`, which takes a function of type `HTTPRequest -> Task a` instead.
There is an instance of `Startable` for `[StartableTask]`.
This makes it possible to specify a heterogeneous list of tasks with different types that can be started by requesting different URLs. In most cases you don't need the HTTP request, but it can be useful.

## Starting tasks when the application starts ##
Some applications contain tasks that don't need any user interaction and should be started immediately.
For those cases there is the `onStartup` function which also wraps a task as `StartableTask` value.
Tasks that are wrapped this way are created when the iTask program starts and are not directly bound to any user or client.
The following example shows a combination of an initialization task that is started when the application is started and another that can be started through the web.

```Clean
Start world = doTasks
	[onStartup initDatabase 
	,onRequest "/" browseDatabase
	] world
```
These tasks are not directly attached to a client when they are created. For a database initialization this may not be necessary, but sometimes you may want to attach them later.
In those cases you can use `onStartupWithAttributes`. This version takes another argument of type `TaskAttributes`. It lets you set the initial meta-data of the tasks, so you can identify the task instance.
This makes it possible to find and attach the task later.

## Advanced patterns ##
If you create frameworks for similar types of applications you can also create an instance of `Startable` for a type of your own. This lets you abstract from tasks that are always available in a certain type of application.
For example, the `iTasks.Extensions.Admin.WorkflowAdmin` extension defines the type `WorkflowCollection` which is `Startable`. Values of this type contain a set of tasks with names and descriptions.
This way, when you create an application using `WorkflowCollection` it initializes a database with those wrapped tasks, called workflows, on startup. It also adds a generic task that lets you login and choose which of those workflows to work on.

