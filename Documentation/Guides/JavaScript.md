# Custom JavaScript in iTasks

It is possible to embed custom JavaScript in iTasks applications. This is done
through custom editors, which use `JavaScriptInit` to run a JavaScript
function on the client when they are initialised.

In the most basic form, however, there are tasks which run JavaScript code on
the client and optionally return a value from the client as the value of the
task. These tasks can be found in iTasks.Extensions.JavaScript. Their
implementation gives a very basic example of how to use `JavaScriptInit` as
well.

The client-side function is written in Clean and executed in an interpreter in
WebAssembly. It can use a foreign function interface to access the JavaScript
world. The foreign function interface is not specific to iTasks. It is
contained in the [abc-interpreter][] library. **That repository contains more
detailed information about using the FFI, optimising client-side code, and
debugging client-side issues.** The documentation here only discusses
iTasks-specific aspects.

## Arguments to the client-side function

The client-side function (i.e., the argument of `JavaScriptInit`) receives
three arguments:

- A `FrontendEngineOptions` record, which contains information about the server
  that may be necessary (for instance to generate correct URLs).
- A reference to the JavaScript iTasks component of the editor. This is an
  instance of the `itasks.Component` class defined in itasks-core.js.
- The `*JSWorld`, similar to `*World` but specifically for JavaScript.

(FFI functions that require a JS component with finalizer capabilities (such as
`jsWrapFun`, `jsWrapFunWithResult`, and `jsMakeCleanReference`) can use the
iTasks component for this. ITasks components are garbage-collected by the
system, which allows Clean values shared by these functions to be
garbage-collected from the WebAssembly world as well.)

## Event handlers

A single initialisation function can be enough in some cases (where an input
field needs to be enhanced with some kind of client-side code, for example). In
other cases, it may be necessary to run client-side code also after
initialisation of the editor. The iTasks framework does not provide a general
method for this. Instead, the initialisation function should set up listeners
to run more code when specific events occur.

These events may be regular JavaScript events, such as key presses and the
like. However, they may also be iTasks events. For instance, the Pikaday editor
(iTasks.Extensions.Form.Pikaday) overwrites the `onAttributeChange` property of
the iTasks component to handle changes of the `value` attribute.

Another important event is the initialisation of the DOM element, which happens
only once. The initialisation function given to `JavaScriptInit` is
executed before an element for the iTasks component exists in the DOM. It can
set an `initDOMEl` listener on the component to initialise the DOM element.
Note that at the time the handler is called, the child components are not
rendered yet. To perform a function after rendering, including children, is
complete, use `afterRender`.

One typically has to string together the initialisation function, the
`initDOMEl` listener, and possibly some usages of `addJSFromUrl` and
`addCSSFromUrl` (see below):

```clean
// initUI is given to `JavaScriptInit` and executed when the component is initialised
initUI frontendEngineOptions me w
	# (cb,w) = jsWrapFun (initDOMEl serverDirectory me) me w
	# w      = (me .# "initDOMEl" .= cb) w
	= w

// initDOMEl is executed when the DOM element exists
initDOMEl serverDirectory me _ w // the wildcard is for arguments, {!JSVal}, but is here always empty
	# (cb,w) = jsWrapFun (initDOMEl` me) me w
	# w      = addJSFromUrl (serverDirectory+++"js/my-plugin.js") (?Just cb) w
	= w

// initDOMEl` is executed after my-plugin.js has been loaded
initDOMEl` me _ w
	// further initialisation can now use both the DOM element and my-plugin.js
	= w
```

## Communication with the server

To communicate with the server, the same connection is used as is used by
normal editors. For an overview, see
[the documentation on the client runtime](/Documentation/Design/client-runtime.md).

Communication from the server to the client happens through attribute changes.
You can set the `onAttributeChange` property of the iTasks component (as
described above) to execute a function when an attribute of the UI changed.
Attributes can be changed on the server side in the `onEdit` and `onRefresh`
functions of editors by returning a `UIChange` that changes attributes.

For example, the Pikaday editor (iTasks.Extensions.Form.Pikaday) changes the
`value` attribute when the value of the editor is changed in `onRefresh`:

```clean
onRefresh dp new st vst
	| st == ?Just new
		= (Ok (NoChange, st, ?None), vst)
		= (Ok (ChangeUI [SetAttribute "value" (JSONString new)] [], ?Just new, ?None), vst)
```

In the `onAttributeChange` handler, the changed value is passed to the Pikaday
JavaScript object:

```clean
onAttributeChange picker me {[0]=name,[1]=value} w
	| jsValToString name <> ?Just "value"
		= w
		= (picker .# "setDate" .$! value) w
```

In the other direction, the client must send edit events that are JSON values
that are passed to the `onEdit` function of the editor. When the editor is a
`LeafEditor`, these values are passed through `JSONDecode` to get a Clean value
of type `edit`; the values created on the client must therefore conform to the
JSON encoding for that type.

For example, the Pikaday editor (iTasks.Extensions.Form.Pikaday) does the
following in an event handler running when a new date is selected:

```clean
# (val,w) = (me .# "picker.toString" .$ "YYYY-MM-DD") w
# val     = jsValToString val
# w       = (me .# "doEditEvent" .$ (me .# "attributes.taskId", me .# "attributes.editorId", toJSON val)) w
```

(The `onEdit` is such that it expects a `String` representation of a date in
`YYYY-MM-DD` format.)

## Error reporting

All client-side JavaScript errors are caught generically, and besides being
logged in the client's console, are sent back to the server. This is done using
the standard web socket, using the message `frontend-error`. Its arguments
contain the name of the JavaScript exception, its stack trace, and an
additional stack trace corresponding to the client-side execution of Clean
code. This error is logged on the server's standard error channel.

## Loading external CSS and JavaScript

In some cases it is needed to load CSS or JavaScript from an external source or
another file. This can be useful to load third-party plugins. It can also be
used to write some supporting JavaScript yourself, when it becomes so much that
using the FFI would be cumbersome or too slow.

The FFI contains the functions `addJSFromUrl` and `addCSSFromUrl` for this.
They take an optional callback that is executed when the source is loaded, to
avoid dependency problems.

To have the iTasks resource collector copy local CSS and JavaScript files to
the `-www` directory so that iTasks can serve them, you must create a WebPublic
directory in any of the directories that also contain code for the application
(there may be more than one WebPublic directory, so you typically keep
WebPublic directories close to the Clean code using them). For example, your
directory tree may look like this:

```
iTasks
└─ Extensions
   ├─ MyPlugin-WebPublic
   │  ├─ css/my-plugin.css
   │  └─ js/my-plugin.js
   ├─ MyPlugin.dcl
   └─ MyPlugin.icl
```

It is customary to use `css` and `js` directories to separate CSS and
JavaScript code.

It is also possible to use the [`npm`][npm] package manager to manage your
frontend dependencies. Add a `package.json` with some dependencies to your
project and make sure that the dependencies are installed in `node_modules` by
running `npm install`. Then add a file `MyPlugin-WebPublic-node_modules.json`
to your directory tree:

```
iTasks
└─ Extensions
   ├─ MyPlugin-WebPublic-node_modules.json
   ├─ MyPlugin.dcl
   └─ MyPlugin.icl
```

This file should contain a JSON object with, in the key `copy`, a list of file
pairs, e.g.:

```json
{
	"copy": [
		["moment/min/moment.min.js", "momentjs/moment.min.js"]
	]
}
```

With such a file, the iTasks resource collector will copy
`node_modules/moment/min/moment.min.js` to `-www/momentjs/moment.min.js`. The
path separator should be `/`, also on Windows.

## Getting extra heap and stack size

Clean code on the client is run in an interpreter with a fixed amount of
memory. This is set in `itasks-core.js` (search for `heap_size` and
`stack_size`). When the default is not enough, you will get a `heap full` error
in the console, or, in case of a stack overflow, a crash (fortunately, stack
overflows are much less common, and almost always indicate an error's on the
programmer's side). In such a case you can use a custom index.html in which you
set the value of `itasks.settings.heap_size` (or `stack_size`) to a higher
value. This index.html should be added to the code in the same way as the
custom CSS and JavaScript above, in a WebPublic directory; you can use the
default index.html as a starting point.

By default garbage collection is done on idle times. This could cause high CPU
usage in case a large Clean heap is used on the client. In this case disabling
the garbage collections could solve the problem by setting
`itasks.settings.perform_gc_on_idle` to `false`.

[abc-interpreter]: https://gitlab.com/clean-and-itasks/abc-interpreter/
[npm]: https://www.npmjs.com/
