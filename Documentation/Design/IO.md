# IO in iTasks

All TCP I/O is multiplexed by the `AsyncIO` library.
`AsyncIO` identifies connections with their `ConnectionId`.
This is a unique ID only while the connection is active (in practice, the `ConnectionId` represents the underlying file descriptor)
The glue between iTasks and `AsyncIO` is defined in `iTasks.Internal.AsyncIO`.
The `IWorld` contains several IO states that are maintained of which only one is supposed to be read or modified by a core task implementor (e.g tcplisten or tcpconnect).

- `aioState` contains the state maintained by the `AsyncIO` library (e.g. handlers).
  This field is left alone by iTasks, it is only passed on to the `AsyncIO` functions.
- `ioStates` is a map from `IOHandle`s to an `IOState`.
  Besides the handlers (that may be executed while the `AsyncIO` connection is already closed) it also contains:
  - `taskId`: the Task id of the task that manages the I/O
  - `ioStatus`: The state of the I/O, if the I/O is active, this contains the `ConnectionId`
  - `listenerMap`: A set of clients, this is only non-empty when the I/O operation is a listener.
  - `clientInstances`: A set of web clients, this is only non-empty when the I/O operation is a listener and the task a web service task.
- `ioHandleMap` is a map from `ConnectionId`s to `IOHandle`s.
- `nextIOHandle` is the next `IOHandle`, this is an integer that is incremented each time a connection is started.

## IOStatus

```
:: IOStatus
	= IOConnecting          !ConnectionId
	| IOListener            !ConnectionId
	| IOActive     !Dynamic !ConnectionId
	| IODestroyed  !Dynamic !ConnectionId
	| IOClosed     !Dynamic
	| IOException  !String
```

- Client:
	- `IOConnecting`: the state while a connection is being established (connecting is non blocking).
	- `IOActive`: the state when the task is active and the connection is active.
	- `IODestroyed`: the state when the task is destroyed but the connection is still active.
	- `IOClosed`: the state when the task is active but the connection is closed.
	- `IOException`: an exception occurred.
- Listener:
	- `IOListener`: the state while accepting connections.

## Handlers


## Setting up TCP connections

There are two ways of performing TCP connections.

### Core level

On the core level, there is `addConnection` and `addListener`.

When using these, it is the responsibility of the context (i.e. the custom task) to mark the IOState as IODestroyed once the task is destroyed.
iTasks will clean up I/O tasks that are set to `IODestroyed`

Furthermore, when using `addListener` it is the responsibility of the context (i.e. the custom task) to close the listener again (using `closeListener`).

### Task level

On the task level, there is `tcpconnect` and `tcplisten`.
They are simple wrappers around the above.
