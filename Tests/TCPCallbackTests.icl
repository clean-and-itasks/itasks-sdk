module TCPCallbackTests

import StdDebug
import qualified Data.Map as DM
import Data.Functor
import iTasks
import System.GetOpt

Start w = doTasksWithOptions (
	withDefaultEngineCLIOptions (map opt types) positionals (Ok o onStartup o startable) (\a->a +++ " [OPTIONS]")
	) w
where
	opt type = Option [] [type] (NoArg (fmap \s->{s & userOpts='DM'.put type [] s.userOpts}))
		("Let the " +++ type +++ " handler return an error")

	types = ["onConnect", "onData", "onShareChange", "onDisconnect", "onDestroy"]

	positionals [] = Ok id
	positionals _ = Error ["Positional arguments not supported"]

	startable eo = task [maybe False (\_->True) ('DM'.get ty eo.userOpts) \\ ty <- types]

task :: ![Bool] -> Task ()
task [onCon,onDat,onSha,onDis,onDes] = withShared () \sds->tcpconnect "localhost" (Port 8123) sds
	{ ConnectionHandlers
	| onConnect = \cId ipAddr ()
		| onCon = (Error ("oops, wrong number: " +++ ipAddr), ?None, [], False)
		= (Ok (), ?None, [], False)
	, onData = \cId data () ()
		| onDat = (Error ("oops, wrong data"), ?None, [], False)
		= (Ok ?None, ?None, [], False)
	, onShareChange = \() ()
		| onSha = (Error ("oops, wrong sharechange: "), ?None, [], False)
		= (Ok ?None, ?None, [], False)
	, onDisconnect = \cId () ()
		| onDis = (Error ("oops, wrong disconnect: "), ?None)
		= (Ok ?None, ?None)
	, onDestroy = \()
		| onDes = (Error ("oops, wrong destroy: "), [])
		= (Ok ?None, [])
	}
task _ = abort "malformed arguments\n"
