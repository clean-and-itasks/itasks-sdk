module MemoryLeaks

from AsyncIO import :: AIOState {aioFd}
import Data.Func, Data.Either, Data.Functor
from Data.Foldable import class Foldable, instance Foldable []
import qualified Data.Foldable as DF
import qualified Data.Set as Set
import qualified Data.Map as Map
import graph_copy
import StdEnv
import System._Finalized
import Text
import Testing.TestEvents

import iTasks, iTasks.Testing.Unit
import iTasks.Internal.IWorld, iTasks.Internal.Task, iTasks.Internal.TaskIO

Start world = runUnitTests tests world

repetitions :: Int
repetitions = 1000

tests :: [UnitTest]
tests =:
	[ memoryConsumptionOf "recursive step"          getIWorldSize
	, memoryConsumptionOf "local share"             (withShared 3 \_ -> getIWorldSize)
	, memoryConsumptionOf "parallel task"           (viewInformation [] 3 ||- getIWorldSize)
	, memoryConsumptionOf "shared interaction task" (withShared 3 \sds -> viewSharedInformation [] sds ||- getIWorldSize)
	]

memoryConsumptionOf :: !String !(Task Int) -> UnitTest
memoryConsumptionOf label task =
	testTaskResult
		("memory consumption of " +++ label) (seqTask 0 (Left [])) events (0,0) iWorldSizeIsNotIncreasing
where
	events = [Left ResetEvent]

	// Because sizes are not entirely stable, we take the maximum of the initial 10% IWorld sizes as the initial value.
	// When the value is Left, we are still collecting the first 10% of values to determine the initial IWorld size.
	// When the value is Right, we have the initial IWorld size and are updating only the final IWorld size.
	seqTask i (Left sizes) =
		task >>- \sz
			| i >= repetitions / 10
				-> seqTask (i+1) (Right (maxList [sz:sizes], sz))
				-> seqTask (i+1) (Left [sz:sizes])
	seqTask i (Right result=:(initSize,endSize))
		| i >= repetitions
			= return result
			= task >>- \sz -> seqTask (i+1) (Right (initSize, sz))

	iWorldSizeIsNotIncreasing :: a !(!Int,!Int) -> EndEventType
	iWorldSizeIsNotIncreasing _ (initSize,endSize)
		| endSize <= initSize = Passed
		| otherwise =
			Failed $
				?Just $
					CustomFailReason $
						concat ["Memory consumption increased by ", toString $ endSize - initSize, " bytes."]

/* NB: We subtract the size of the names of shares. Because share names include
 * TaskIds in string representation, and these IDs are incremented, including
 * share names may cause the IWorld size to increase; we want to ignore these
 * small changes.
 */
getIWorldSize :: Task Int
getIWorldSize = mkInstantTask \_ iworld=:{memoryShares}
	# asyncIoFinalizer = iworld.aioState.aioFd
	# iworldString = copy_to_string iworld
	# (iworldSize,iworldString) = usize iworldString
	# (iworld,_) = copy_from_string iworldString
	// Need to make sure the AsyncIO file descriptor is not finalized when the `IWorld` is serialized as a string.
	// as otherwise, when retrieving IO events later it will no longer exist, causing AsyncIO to crash for the test.
	# iworld & aioState.aioFd = asyncIoFinalizer
	# shareNamesSize = 'Map'.foldrWithKey` (\k _ n -> n + size (copy_to_string k)) 0 memoryShares
		// We cannot simply fold with `size k` due to padding in `copy_to_string`.
	-> (Ok (iworldSize - shareNamesSize), iworld)
