module TestAsyncTask

import Data.Func
import Data.Functor
import Data.Error.GenJSON
import StdEnv
import System.Process
import System.Time, System.Time.GenJSON
import Text

import iTasks
import iTasks.Extensions.DateTime

Start world
	# (start, world) = nsTime world
	= flip doTasksWithOptions world $ withDefaultEngineCLIOptions
		[]
		(\l->case l of
			[] = Ok $ fmap \eo->{eo & verboseOperation=False, distributed= ?Just 9090}
			["child"] = Ok $ fmap \eo->{eo & verboseOperation=True}
			[x] = Error ["Unrecognised CLI option: " +++ x]
			_ = Error ["Multiple CLI options given"]
		)
		(\_ -> Ok task)
		(\argv0->"Usage: " +++ argv0 +++ " [child]")
where
	task = onStartup (
		    accWorld nsTime
		>>- \start->(allTasks (map test $ map (\x -> Port x) [8000..8005])
			-|| (waitForTimer False 30 >-| traceValue "TestAsyncTask killed after 30 seconds" >-| shutDown 1))
		>-| accWorld nsTime
		>>- \done->if (done - start > {tv_sec=15,tv_nsec=0})
				(traceValue "TestAsyncTask took too long" >-| shutDown 1)
				(traceValue "Passed" >-| shutDown 0)
		)
	test port = get applicationOptions >>- \{appPath}->
		withShared [] \stdin->
		withShared ([], []) \stdout->
			    (externalProcess
					{tv_sec=1,tv_nsec=0} appPath
					[ Arguments ["--distributedChild", toString port, "child"]
					, ExitCodeOrSignal 9, ProcessPtyOptions defaultPtyOptions] stdin stdout
				-|| viewSharedInformation [] stdout)
			||-  (
				    traceValue ("wait for " +++ toString port +++ " to become ready")
				>-| wait (any (startsWith "SDS server listening on ") o split "\n" o concat o fst) stdout
				>-| traceValue ("sending task to " +++ toString port)
				>-| asyncTask "localhost" port (accWorld (timespecSleep {tv_sec=5, tv_nsec=0}))
				>-| traceValue ("task " +++ toString port +++ " finished")
				)
