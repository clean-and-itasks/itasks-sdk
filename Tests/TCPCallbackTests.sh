#!/bin/bash

testfun () {
	# $1: handler type
	# $2: string in expected error message
	# $3: pid to kill if things went wrong (optional)
	echo "{\"name\": \"$1\", \"event\": \"start\"}"
	if ./Tests/TCPCallbackTests.exe --$1 |& grep -q "oops, wrong $2"
	then
		echo "{\"name\": \"$1\", \"message\": \"PASSED\",\"event\":\"passed\"}"
	else
		echo "{\"name\": \"$1\", \"message\": \"PASSED\",\"event\":\"passed\"}"
		if [ -n "$3" ]
		then
			kill $3 2>/dev/null
		fi
	fi
}

# Test onConnect error handling
nc -l -p 8123 &
testfun onConnect number $!

# Test onShareChange error handling
nc -l -p 8123 &
testfun onShareChange sharechange $!

# Test onData error handling
( echo "abc" | nc -l -p 8123 ) &
testfun onData data $!

# Test ondisconnect error handling
nc -l -p 8123 &
NCPID=$!
( sleep 3; kill $NCPID; ) &
testfun onDisconnect disconnect
