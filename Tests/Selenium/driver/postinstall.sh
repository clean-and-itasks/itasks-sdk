#!/bin/sh

set -e

CHROME_PLATFORM=linux64
GECKO_PLATFORM=linux64
CLEAN_PLATFORM="linux-x64"
if [ "$(uname -s)" = "Darwin" ]; then
	CHROME_PLATFORM=mac64
	GECKO_PLATFORM=macos
	CLEAN_PLATFORM="macos-x64"
fi

echo "Fetching ABC interpreter dependencies..."
# Copy files from topmost `nitrile-packages` directory.
(
	curDir=$PWD
	while [ ! -d "nitrile-packages" ]; do
		if [ $PWD = "/" ]; then echo "Could not find nitrile-packages path."; exit 1; fi
		cd ..
	done
	cp nitrile-packages/"$CLEAN_PLATFORM"/abc-interpreter/lib/WebPublic/js/* $curDir
)

if [ ! -f chromedriver ]; then
	echo "Fetching chromedriver..."
	RELEASE="$(curl -Ls http://chromedriver.storage.googleapis.com/LATEST_RELEASE)"
	curl -Ls http://chromedriver.storage.googleapis.com/$RELEASE/chromedriver_$CHROME_PLATFORM.zip | funzip > chromedriver
fi

if [ ! -f geckodriver ]; then
	echo "Fetching geckodriver..."
	URL="$(curl -Ls https://api.github.com/repos/mozilla/geckodriver/releases/latest \
		| jq -r '.assets | .[] | .browser_download_url' \
		| grep "$GECKO_PLATFORM.tar.gz$")"
	curl -Ls "$URL" | tar xz
fi

chmod +x chromedriver geckodriver
