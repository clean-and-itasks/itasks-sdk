const HOST='127.0.0.1';
const PORT=8081;

const fs=require ('fs');
const path=require ('path');
const net=require ('net');
const process=require ('process');

const Selenium=require ('selenium-webdriver');
global.Builder=Selenium.Builder;
global.Button=Selenium.Button;
global.By=Selenium.By;
global.Condition=Selenium.Condition;
global.Key=Selenium.Key;
global.Level=Selenium.Level;
global.WebElement=Selenium.WebElement;
global.until=Selenium.until;
global.firefox=require ('selenium-webdriver/firefox');
global.chrome=require ('selenium-webdriver/chrome');

const {ABC_DEBUG, ABCError, SharedCleanValue, ABCInterpreter}=require ('./abc-interpreter.js');

process.env['PATH']=process.env['PATH']+':'+__dirname;

var ABC; // Points to the currently running interpreter

class TestRunner {
	constructor (socket) {
		this.abc=null;
		this.abc_size=0;
		this.socket=socket;
		this.shared_clean_values=null;
		this.tests=[];
		this.wait_for_idle_resolvers={};
	}

	destroy () {
		if (typeof this.driver!='undefined')
			this.driver.quit();
		this.driver=undefined;
	}

	send (data) {
		this.socket.write (JSON.stringify (data) + '\n');
	}

	handle (data) {
		const me=this;
		ABC=this.abc;

		if (this.abc===null) {
			if (this.abc_size==0) {
				this.abc_size=parseInt (data.toString ('ascii'));
				this.abc_buffer=new Uint8Array (this.abc_size);
				this.abc_ptr=0;
				this.send (['OK']);
			} else {
				for (var i=0; i<data.length; i++)
					this.abc_buffer[this.abc_ptr++]=data[i];

				if (this.abc_ptr>=this.abc_size) {
					ABCInterpreter.instantiate({
						bytecode_path: 'bytecode',
						util_path: 'abc-interpreter-util.wasm',
						interpreter_path: 'abc-interpreter.wasm',
						fetch: function (filepath) {
							if (filepath=='bytecode')
								return Promise.resolve ({
									ok: true,
									arrayBuffer: () => me.abc_buffer.buffer
								});

							filepath=path.join (path.dirname (require.main.filename),filepath);
							return fs.promises.readFile (filepath).then (function (contents) {
								return {arrayBuffer: () => contents};
							});
						},
						encoding: 'utf-8',
						with_js_ffi: true,
					}).then (function (abc) {
						me.abc=abc;
						me.abc.initialized=false;
						me.send (['OK']);
					});
				}
			}
		} else {
			const command=JSON.parse (data.toString ('ascii'));
			if (!command || !command.length)
				throw new Error ('Illegal command: ' + data.toString ('ascii'));

			switch (command[0]) {
				case 'ExecuteFunction':
					const fun=this.abc.deserialize (Buffer.from (command[1],'base64'));
					const ref=this.abc.share_clean_value (fun,me);
					this.abc.interpret (new SharedCleanValue (ref), [me, me.abc.initialized ? 0 : 1]);
					break;
				case 'TaskIsIdle':
					this.wait_for_idle_resolvers[command[1]]();
					delete this.wait_for_idle_resolvers[command[1]];
					break;
				default:
					throw new Error ('Illegal command: ' + command);
			}

			this.send (['OK']);
		}
	}

	close () {
		this.send (['Close']);
	}

	waitForIdle (instance_no) {
		const me=this;
		this.send (['WaitForIdle', instance_no]);
		return new Promise(function (resolve) {
			me.wait_for_idle_resolvers[instance_no]=resolve;
		});
	}

	startTest (desc) {
		this.send (['StartTest', desc]);
		return this.tests.push({name: desc}) - 1;
	}

	finishTest (id,success,fail_stage) {
		if (id<0 || id>=this.tests.length)
			throw new Error ('test id out of range');
		if (!(id in this.tests))
			throw new Error ('test has already been finished');

		this.send (['FinishTest', this.tests[id].name, success, fail_stage || '']);

		delete this.tests[id];
	}

	testIsActive (id) {
		return id in this.tests;
	}
}

const server=net.createServer (onConnect);
server.listen (PORT,HOST,() => console.info ('listening on port %d...',PORT));

function onConnect (socket) {
	const identifier=socket.remoteAddress+':'+socket.remotePort;
	console.info ('new client: %s',identifier);

	const handler=new TestRunner(socket);

	socket.on ('data',(data) => handler.handle (data));
	socket.on ('close',function(){
		console.info ('%s disconnected',identifier)
		handler.destroy();
	});
	socket.on ('error',(err) => console.info ('connection error: %s (%s)',err.message,identifier));
}
