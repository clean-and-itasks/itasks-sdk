implementation module Test.Examples.BasicAPIExamples.SequentialExamples.CalculateSumInShare

import Control.Monad => qualified return, forever, sequence
import Data.Func
import Data.Functor
from Text import class Text(concat), instance Text String

import Gast.Gen

import iTasks.Testing.Selenium
import iTasks.Testing.Selenium.Gast
import iTasks.Testing.Selenium.Interface

import BasicAPIExamples.SequentialExamples.CalculateSumInShare

TEST_NAME :== "test"
N_TESTS :== 100

tests :: [TestedTask]
tests =
	[ TestedTask (main <<@ Name TEST_NAME) sum_is_correct
	]

sum_is_correct :: TestSpec
sum_is_correct = \url ->
	seleniumTest "sum in share" $
	getURL url `then` \_ ->
	findElement (ByTestName TEST_NAME) NoElement `then` \task ->
	waitForIdle task $
	findElements (ByCSS "input") (Element task) `then` \inputs ->
	accJS (jsValToList` inputs id) >>= \inputs ->
	case inputs of
		[f1, f2, fsum] ->
			withGast
				N_TESTS {genState & mode=BentGeneration} safe_integers
				\(i1,i2) -> runTest i1 i2 f1 f2 fsum
		_ ->
			fail "could not find three input fields."
where
	runTest :: !Int !Int !JSVal !JSVal !JSVal -> JS TestState JSVal
	runTest i1 i2 f1 f2 fsum =
		doAndWaitForUIEvent (
			clear f1 `then` \_ ->
			clear f2 `then` \_ ->
			sendKeys (TextKeys (toString i1)) f1 `then` \_ ->
			sendKeys (TextKeys (toString i2)) f2
		) `then` \_ ->
		waitForIdle fsum $
		getAttribute "value" fsum \v ->
		let sum = toInt v in
		expect
			(concat
				[ "expected ",toString (i1+i2)
				, " for ",toString i1," + ",toString i2
				, "; got ",toString sum])
			(sum == i1+i2)

	safe_integers :: !(!Int, !Int) -> Bool
	safe_integers (i1,i2) = safe i1 && safe i2
	where
		// These are Number.{MIN,MAX}_SAFE_INTEGER; because JavaScript has no
		// native integers, not all 64-bit integers can appear.
		safe i = -9007199254740991 <= i && i <= 9007199254740991
