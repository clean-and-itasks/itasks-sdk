module Test.Examples.BasicAPIExamples

import StdEnv
import iTasks.Testing.Selenium

import qualified Test.Examples.BasicAPIExamples.SequentialExamples.CalculateSumInShare as SeqExs.CalculateSumInShare

Start w = runTestSuiteWithCLI (flatten suites) w
where
	suites =
		[ 'SeqExs.CalculateSumInShare'.tests
		]
