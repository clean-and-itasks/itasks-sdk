module Test.iTasks.Extensions.JavaScript

import StdEnv
import Data.Func
import Text
import iTasks, iTasks.Testing.Selenium, iTasks.Testing.Selenium.Interface
import iTasks.Extensions.JavaScript

Start w = runTestSuiteWithCLI tests w

/*
 * {app,acc}JSWorld and runJSMonad use an editor to run the code.
 * During the run, an itasks-component is shown for the value (or for ())
 * So when the task stabilises and the step is made, the ui for return () will
 * show, which is itasks-raw-empty
 */

tests :: [TestedTask]
tests =
	[ TestedTask appJSWorldTask $ testStability "appJSWorld"
	, TestedTask accJSWorldTask $ testStability "accJSWorld"
	, TestedTask runJSMonadTask $ testStability "runJSMonad"
	]
where
	appJSWorldTask = appJSWorld (jsGlobal "console" .# "log" .$! toJS "appJSWorld") >>- \_->return ()
	accJSWorldTask = accJSWorld (\w->accFun "accJSWorld" (42, w)) >>- \_->return ()
	runJSMonadTask = runJSMonad () (accJS (\w->accFun "runJSMonad" (42, w))) >>- \_->return ()
	
	accFun msg = jsGlobal "console" .# "log" .$? toJS msg

	testStability name url =
		seleniumTest testname $
		getURL url `then` \_ ->
		findElement (ByClassName "itasks-raw-empty") NoElement `then` \task ->
		resolvePromise ()
	where testname = name +++ " produces a stable value when the javascript is done"
