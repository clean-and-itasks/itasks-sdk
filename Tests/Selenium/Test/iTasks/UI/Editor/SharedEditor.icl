module Test.iTasks.UI.Editor.SharedEditor

import Control.Monad => qualified forever, sequence
import Data.Func

import iTasks
import iTasks.Extensions.DateTime
import iTasks.Testing.Selenium
import iTasks.Testing.Selenium.Interface

Start w = runTestSuiteWithCLI tests w

tests :: [TestedTask]
tests =
	// Temporarily commented out as it is unstable, see #520.
	[ //TestedTask task rapid_updates
	]

task =
	withShared [0] \s ->
		forever (set [0] s >-| waitForTimer False 1)
	-&&-
		updateSharedInformation [] s

rapid_updates url =
	seleniumTest "Rapid updates to shared editor do not cause race conditions" $
	getURL url `then` \_ ->
	tryPromise (
		focusAndIncrement 20
	) \_ -> fail "the input field has disappeared"
where
	focusAndIncrement 0 =
		resolvePromise ()
	focusAndIncrement n =
		findElement (ByCSS "input") NoElement `then` \field ->
		click field `then` \_ ->
		doAndWaitForUIEvent (
			tryPromise (increment 500) (\_ -> resolvePromise ())
		) `then` \_ ->
		focusAndIncrement (n-1)
	where
		increment 0 = resolvePromise ()
		increment n = pressKey ArrowUpKey `then` \_ -> increment (n-1)
