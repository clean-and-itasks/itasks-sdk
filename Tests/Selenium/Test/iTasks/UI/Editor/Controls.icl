module Test.iTasks.UI.Editor.Controls

import StdEnv
import Data.Func
import Text
import iTasks, iTasks.Testing.Selenium, iTasks.Testing.Selenium.Interface

Start w = runTestSuiteWithCLI tests w

TEST_1_NAME :== "The editor obeys minlength attribute"
TEST_2_NAME :== "ArrangeAsMenu opening/collapsing menus and task continuations of menu items function as intended"
TEST_4_NAME :== "ChooseFromGrid in combination with updateChoice does not lead to an exception"
TEST_5_NAME :== "ChooseFromGrid in a sidebar has a scrollbar"

tests :: [TestedTask]
tests =
	[ editorObeysMinlength n editorName editor editorCssSelector taskName task
	\\ (editorName, editor, editorCssSelector) <- testedControls, (taskName, task) <- testedTasks, n <- [0..2]
	] ++
	[ TestedTask (arrangeAsMenuTask <<@ Name TEST_2_NAME) arrangeAsMenuTest
	, TestedTask chooseFromGridTask gridExists
	, TestedTask chooseFromGridWithSidebarTask gridHasScrollbar
	]
where
	//* (label for the editor, tested editor, editor CSS selector)
	testedControls :: [(String, Editor String (EditorReport String), String)]
	testedControls =
		[ ("textField",     textField,     "input")
		, ("textArea",      textArea,      "textArea")
		, ("passwordField", passwordField, "input")
		]

	//* (label for the task, task to test editor with)
	testedTasks :: [(String, (Editor String (EditorReport String)) -> Task String)]
	testedTasks =
		[ ("entering", \editor -> enterInformation [EnterUsing id editor])
		, ("updating", \editor -> updateInformation [UpdateUsing id const editor] "")
		]

/**
 * Tests whether the given editor obyed the `minlength` attribute.
 *
 * @param The value of the `minlength` attribute to test.
 * @param The name of the editor for naming the test.
 * @param The editor to test (the `minlength` attribute is attached by this function).
 * @param The CSS selector for finding the editor's DOM element.
 * @param The name of the task for which the editor is tested for naming the test.
 * @param The task for which the editor is tested.
 */
editorObeysMinlength ::
	!Int
	!String
	!(Editor String (EditorReport String))
	!String
	!String
	!((Editor String (EditorReport String)) -> Task String)
	-> TestedTask
editorObeysMinlength n editorName editor editorCssSelector taskName task =
	TestedTask (enterWithMinLength <<@ Name TEST_1_NAME) $ validityObeysAttribute
where
	enterWithMinLength = task (if (n == defaultMinlength) editor (editor <<@ minlengthAttr n)) >>! return

	validityObeysAttribute url =
		seleniumTest
			(concat
				[ editorName, " editor obeys minlength attribute set to ", toString n
				, " when ", taskName, " information"
				]
			) $
		getURL url `then` \_ ->
		findElement (ByTestName TEST_1_NAME) NoElement `then` \task ->
		waitForIdle task $
		findElement (ByCSS editorCssSelector) NoElement `then` \input ->
		findElement (ButtonByLabel "Continue") NoElement `then` \button ->
		if (n == 0) (expectValidValue button) (expectInvalidValue button) `cont`
		sendKeys (TextKeys $ toString $ repeatn n 'x') input `cont`
		expectValidValue button
	where
		expectValidValue   = expectElementToEnable  "the editor did not provide a valid value as expected"
		expectInvalidValue = expectElementToDisable "the editor did provide a valid value, but it should not have"

	// if the minlength value is the default one, the value is not applied explicitly as this should not be required
	defaultMinlength = 0

arrangeAsMenuTask :: Task ()
arrangeAsMenuTask =
	(viewInformation [] "some info" >>*
		[ OnAction (Action "/File/New")   $ always $ (viewInformation [] "test" @! ())
		]
	)
	 <<@ ArrangeAsMenu [!Menu "/File"]

arrangeAsMenuTest url =
	seleniumTest TEST_2_NAME $
	getURL url `then` \_ ->
	findElement (ByTestName TEST_2_NAME) NoElement `then` \task ->
	waitForIdle task $
	findElement (ByClassName "itasks-menu-content") NoElement `then` \menuContent ->
	// Initially the content of the menu's should be hidden.
	expectCssValue "display" "none" menuContent `cont`
	// But if a menu is clicked, the contents are shown.
	findElement (ByClassName "itasks-menu") NoElement `then` \menu ->
	click menu `cont`
	expectCssValue "display" "block" menuContent `cont`
	// Then if the menu is clicked again, it collapses.
	click menu `cont`
	expectCssValue "display" "none" menuContent `cont`
	// Then if the menu is clicked again and a menu item is clicked on, the corresponding task is executed.
	click menu `cont`
	findElement (ByClassName "itasks-button-label") NoElement `then` \menuButton ->
	click menuButton `cont`
	findElement (ByClassName "itasks-textview") NoElement `then` \textView ->
	// Verify that the task continuation (viewInformation [] "test") is shown.
	getText textView $ \text -> if (text == "test") (resolvePromise ()) (fail "The task continuation was not shown.")

:: Expr = Add Expr Expr | Mul Expr Expr | Val Int
derive class iTask Expr

chooseFromGridTask :: Task Int
chooseFromGridTask = updateChoice [ChooseFromGrid id] [1,2,3] 1

gridExists url =
	seleniumTest TEST_4_NAME $
	getURL url `then` \_ ->
	findElement (ByClassName "itasks-choicegrid-body") NoElement `then` \chooseFromGrid ->
	if (jsIsNull chooseFromGrid) (fail "chooseFromGrid could not be found") (resolvePromise ())

chooseFromGridWithSidebarTask :: Task Int
chooseFromGridWithSidebarTask =: ArrangeWithSideBar 1 RightSide True @>> (
		enterChoice [ChooseFromGrid id] [1..1000] >&^
		viewSharedInformation []
	)

gridHasScrollbar :: TestSpec
gridHasScrollbar =: \url ->
	seleniumTest TEST_5_NAME $
	getURL url `then` \_ ->
	findElement (ByClassName "itasks-choicegrid-body-inner") NoElement `then` \body ->
	executeScript "return arguments[0].scrollHeight > arguments[0].offsetHeight;" body `then` \hasScrollbar ->
	if (fromJS False hasScrollbar) (resolvePromise ()) (fail "choicegrid body does not have a scrollbar")
