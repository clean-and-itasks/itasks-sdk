module Test.iTasks.UI.Events.UiEvents

import iTasks
import iTasks.Testing.Selenium
import iTasks.Testing.Selenium.Interface
import iTasks.UI.Editor.Common

import Data.Func

Start w = runTestSuiteWithCLI tests w

TEST_NAME_0 :== "Sending ui event to non-attached task instance results in exception."

tests :: [TestedTask]
tests =
	[ TestedTask (sendUIEventToNonAttachedInstanceTask <<@ Name TEST_NAME_0) sendUIEventToNonAttachedInstanceTest]

sendUIEventToNonAttachedInstanceTask :: Task ()
sendUIEventToNonAttachedInstanceTask = appendTopLevelTask defaultValue True enterInt >>- \taskId ->
	viewInformation [] taskId @! ()
where
	enterInt :: Task Int
	enterInt = enterInformation [] >>! \i -> viewInformation [] i

sendUIEventToNonAttachedInstanceTest url =
	seleniumTest TEST_NAME_0 $
	getURL url `then` \_ ->
	executeScript "itasks.components.get(document.body).connection.sendEditEvent(6, '0', '0', Just(10))" () `cont`
	findElement (ByXPath "//*[contains(text(), 'Client not authorized to access instance')]") NoElement
