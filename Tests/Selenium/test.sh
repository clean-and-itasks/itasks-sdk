#!/bin/bash

set -e

NODE_PID=""
cleanup () {
	kill $NODE_PID 2>/dev/null
}

trap 'ps -p $! >/dev/null || exit 1' CHLD
trap cleanup INT TERM

node --unhandled-rejections=strict Tests/Selenium/driver/driver.js &
NODE_PID="$!"
sleep 5

test-runner -r ./Tests/Selenium/Test/iTasks.UI.Editor.SharedEditor --options '--headless;--window-size;1920x1080;--screenshots-for-failed-tests' --junit junit-selenium-shared-editor.xml
test-runner -r ./Tests/Selenium/Test/iTasks.UI.Editor.Controls --options '--headless;--window-size;1920x1080;--screenshots-for-failed-tests' --junit junit-selenium-controls.xml
test-runner -r ./Tests/Selenium/Test/iTasks.UI.Events.UiEvents --options '--headless;--window-size;1920x1080;--screenshots-for-failed-tests' --junit junit-selenium-uievents.xml
test-runner -r ./Tests/Selenium/Test/iTasks.Extensions.JavaScript --options '--headless;--window-size;1920x1080;--screenshots-for-failed-tests' --junit junit-selenium-javascript.xml

cleanup
