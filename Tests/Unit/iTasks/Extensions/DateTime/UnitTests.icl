module iTasks.Extensions.DateTime.UnitTests

import StdEnv
import Data.Func, Data.Either
import iTasks, iTasks.Extensions.DateTime
import iTasks.Testing.Unit
import Text.GenPrint

Start :: !*World -> *World
Start world = runUnitTests [testWaitForTimerTerminates] world

// Under heavy load (which is provoked by starting a large number of tasks in parallel)
// discrepancies caused by non synchronised clocks for SDS and clock dependency management (as fixed in !619)
// can cause the iTasks server to sleep infinitely before the task is stable.
testWaitForTimerTerminates :: UnitTest
testWaitForTimerTerminates =:
	testTaskResult "Test whether waitForTimer terminates under heavy load" task [Left ResetEvent] () checkEqual
where
	task = allTasks (repeatn 2000 $ waitForTimer True 1)  @! ()