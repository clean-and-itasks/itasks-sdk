module iTasks.Extensions.FileCollection.UnitTests

import iTasks.Extensions.FileCollection
import iTasks, iTasks.Testing.Unit
import Testing.TestEvents


import Text.GenPrint

derive gEq FileFilterDecision
derive gPrint FileFilterDecision

Start world = runUnitTests
	[testCatchAll
	,testIntermediateDir
 	] world

testCatchAll = assertEqual "Catch all" IncludeFile (matchFileFilter [("**",IncludeFile)] "foo/bar/baz.txt" True)
testIntermediateDir = assertEqual "Catch all" IncludeFile (matchFileFilter [("**/foo.txt",IncludeFile)] "foo/" True)
