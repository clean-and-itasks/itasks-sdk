module iTasks.Extensions.Process.UnitTests

import StdEnv

import iTasks.Extensions.Process
import iTasks
import iTasks.Internal.TaskIO
import iTasks.Testing
import iTasks.Testing.Unit
import iTasks.UI.Definition
import Testing.TestEvents

import System.OS, Data.Either, Data.Functor
import qualified Data.Set as DS
import qualified Data.Map as DM

import Text.GenPrint

derive JSONEncode TaskOutputMessage
derive gPrint ProcessStatus

Start world = runUnitTests
	[testCallFastProcess
	,testCallSlowProcess
 	] world

testCallFastProcess = testTaskResult "Test call for fast process" sut events undef terminatesProperly
where
	(cmd, args) = IF_WINDOWS
		("C:\\Windows\\System32\\cmd.exe", ["/c", "exit"])
		("/bin/date", [])

	sut = callProcess [] cmd [Arguments args]
	events = [Left ResetEvent,Right 1,Left (RefreshEvent 'DS'.newSet)]

testCallSlowProcess = testTaskResult "Test call for slow process" sut events undef terminatesProperly
where
	(cmd, args) = IF_WINDOWS
		("C:\\Windows\\System32\\ping.exe", ["127.0.0.1"])
		("/bin/sleep", ["4"])

	sut = callProcess [] cmd [Arguments args]
	events = [Left ResetEvent ,Right 1 ,Left (RefreshEvent 'DS'.newSet),Right 2,Left (RefreshEvent 'DS'.newSet) ,Left (RefreshEvent 'DS'.newSet)]

terminatesProperly :: a !ProcessInformation -> EndEventType
terminatesProperly _ {status} = checkEqual (CompletedProcess 0) status
