module iTasks.UI.Editor.Generic.UnitTests

import graph_copy

import Data.Either
import Data.Func
import Data.Functor
import Text, Text.GenPrint
import iTasks
import iTasks.Extensions.DateTime
import iTasks.Testing
import iTasks.Testing.Unit
import iTasks.UI.Editor.Generic
import iTasks.UI.Layout
import qualified Data.Map as DM

//Unfortunately the javascript linker introduces
//the need to carry the iworld around in editors
import iTasks.Internal.IWorld
from iTasks.Engine import defaultEngineOptions

derive gPrint LUI, LUIChanges, LUIEffects, LUIEffectStage, LUINo, Set, UI, UIType, JSONNode, Map, LUINode
derive gPrint MaybeError, UIChange, UIChildChange, UIAttributeChange
derive gPrint EditState, LeafState
gPrint{|EditorId|} x st = gPrint{|*|} (toString x) st

gPrint{|Dynamic|} d st =
	case d of
		(d :: Bool) = gPrint{|*|} ("Dynamic of type Bool: " +++ toString d) st
		_           = gPrint{|*|} ("Dynamic of type "+++toString (typeCodeOfDynamic d)) st

/* NB: this is not a proper implementation of gEq, since x and y may differ in
 * shared pointers in the graph, which would be reflected in the string
 * representation. However, in editor states sharing in the graph probably does
 * not occur, so hopefully we're safe here. */
gEq{|EditState|} x y = sx == sy
where
	sx = copy_to_string (hyperstrict x)
	sy = copy_to_string (hyperstrict y)

tests = flatten
	[primitiveEditorTests
	,compositeEditorTests
	]

primitiveEditorTests = flatten
	[intEditorTests
	,realEditorTests
	,charEditorTests
	,boolEditorTests
	,stringEditorTests
	,dateTimeEditorTests
	,dateEditorTests
	,timeEditorTests
	]

compositeEditorTests = flatten
	[adtEditorTests
	,recordEditorTests
	,tupleEditorTests
	]

//For different types consider:
//- Generate the initial UI (mandatory and optional editors)
//- Process different types of events (mandatory and optional editors)
//- Refresh with different values  (mandatory and optional editors)
//- Consider different edit modes

intEditorTests =
	[ genRequiredIntUI
	,skip genOptionalIntUI
	]

genRequiredIntUI = assertEqualWorld "Generate UI for Editor of type Int"
	(Ok (UI UIIntegerField
				('DM'.fromList
					[("tooltip-type",JSONString "info")
					,("editorId",JSONString "0")
					,("tooltip",JSONString "Please enter a whole number (this value is required)")
					,("optional",JSONBool False)
					,("taskId",JSONString "4-2")
					,("value",JSONNull)
					]
				)
			[]
	    , ?Just EmptyEditor))
	(onResetWrapper ?None intEditor)
where
	intEditor :: Editor Int (EditorReport Int)
	intEditor = gEditor{|*|} EditValue

genOptionalIntUI = assertEqualWorld "Generate UI for Editor of type ?Int"
	(Ok (UI UIIntegerField
				('DM'.fromList
					[("hint-type",JSONString "info")
					,("editorId",JSONString "1")
					,("hint",JSONString "Please enter a whole number")
					,("optional",JSONBool True)
					,("mode",JSONString "enter")
					,("taskId",JSONString "4-2")
					,("value",JSONNull)
					]
				)
			[]
	    , ?None))
	(onResetWrapper ?None intEditor)
where
	intEditor :: Editor (?Int) (EditorReport (?Int))
	intEditor = gEditor{|*|} EditValue

realEditorTests = []
charEditorTests = []
boolEditorTests = []
stringEditorTests = []
dateTimeEditorTests =:
	[testEditorDefaultValue "update ?DateTime editor has ?None task value by default" updateMaybeDateTime ?None]
where
	updateMaybeDateTime :: Task (?DateTime)
	updateMaybeDateTime = updateInformation [] ?None >>~ return
dateEditorTests =:
	[testEditorDefaultValue "update ?Date editor has ?None task value by default" updateMaybeDate ?None]
where
	updateMaybeDate :: Task (?Date)
	updateMaybeDate = updateInformation [] ?None >>~ return
timeEditorTests =:
	[testEditorDefaultValue "update ?Time editor has ?None task value by default" updateMaybeTime ?None]
where
	updateMaybeTime :: Task (?Time)
	updateMaybeTime = updateInformation [] ?None >>~ return

testEditorDefaultValue :: !String !(Task a) !a -> UnitTest | gPrint{|*|}, iTask a
testEditorDefaultValue testDesc task expectedValue
	= testTaskResult testDesc task [Left ResetEvent] expectedValue checkEqual

:: EnumADT = ACons | BCons | CCons

derive class iTask EnumADT
derive gPrint EnumADT

:: GroupADT = GroupCons Int Int

:: RecursiveADT = NilCons | RecCons RecursiveADT

:: MixedADT = MixedGroupA Int String | MixedSimpleA | MixedGroupB Bool

derive gEditor GroupADT, RecursiveADT, MixedADT
derive gDefault RecursiveADT

adtEditorTests =
	[ refreshAdtEditorWithViewPurpose $ ?Just False
	, refreshAdtEditorWithViewPurpose $ ?Just True
	, refreshAdtEditorWithViewPurpose ?None
	, writeValueForAdtEditor consAState (ValidEditor ACons)
	, writeValueForAdtEditor consBState (ValidEditor BCons)
	, writeValueForAdtEditor noValueState EmptyEditor
	]

/*
 * The flag indicates whether the new value is the same as the initial one.
 * `?None` indicates that the editor was created without value.
 */
refreshAdtEditorWithViewPurpose :: !(?Bool) -> UnitTest
refreshAdtEditorWithViewPurpose initValue =
	assertEqualWorld
		(concat3 "Refresh UI for Editor of type EnumADT with View Purpose (initialised with " initValueStr ")")
		(Ok (change, consBState, ?None))
		(onRefreshWrapper (?Just BCons) initState editor)
where
	(initValueStr, initState) = case initValue of
		?Just False = ("value different from change", consAState)
		?Just True  = ("same value as change",        consBState)
		?None       = ("no value",                    noValueState)

	editor :: Editor EnumADT (EditorReport EnumADT)
	editor = gEditor{|*|} ViewValue

	change
		| initValue =: (?Just True) =
			ChangeUI [] [(1, ChangeChild NoChange)]
		| otherwise =
			ReplaceUI
				(UI
					UIContainer
					('DM'.singleton
						"class"
						(JSONArray $
							if (isNone initValue) [JSONString "var-cons", JSONString "cons"] [JSONString "cons"]
						)
					)
					[ UI UITextView ('DM'.singleton "value" $ JSONString "BCons") []
					, UI UIEmpty ('DM'.fromList [("editorId", JSONString "0"), ("taskId", JSONString "4-2")]) []
					]
				)

writeValueForAdtEditor :: !EditState !(EditorReport EnumADT) -> UnitTest
writeValueForAdtEditor st expectedValue =
	assertEqualWorld
		(concat3 "Write Value for Editor of type EnumADT (for value " (printToString expectedValue) ")")
		(Ok expectedValue)
		(writeValueWrapper st editor)
where
	editor :: Editor EnumADT (EditorReport EnumADT)
	editor = gEditor{|*|} EditValue

consAState :: EditState
consAState =:
	CompoundState
		(?Just (dynamic ()))
		[ LeafState {editorId = ?None, touched = False, state = ?None}
		, CompoundState
			(?Just (dynamic False))
			[ CompoundState
				(?Just (dynamic ()))
				[LeafState {editorId = ?Just (fromString "0"), touched = False, state = ?Just (dynamic ())}]
			]
		]

consBState :: EditState
consBState =:
	CompoundState
		(?Just (dynamic ()))
		[ LeafState {editorId = ?None, touched = False, state = ?None}
		, CompoundState
			(?Just (dynamic True))
			[CompoundState
				(?Just (dynamic False))
				[ CompoundState
					(?Just (dynamic ()))
					[LeafState {editorId = ?Just (fromString "0"), touched = False, state = ?Just (dynamic ())}]
				]
			]
		]

//* This represents an ADT editor's "no value" state for the "view puporse" (without state for the cons viewer).
noValueState :: EditState
noValueState =: CompoundState (?Just (dynamic ())) []

:: TwoFieldRecord =
	{ fieldA :: String
	, fieldB :: Int
	}

:: OptionalRecord =
	{ fieldC :: ?TwoFieldRecord
	, fieldD :: Int
	}

derive gEditor TwoFieldRecord, OptionalRecord
derive gPrint TwoFieldRecord, OptionalRecord
derive gEq TwoFieldRecord, OptionalRecord
derive gText TwoFieldRecord, OptionalRecord

recordEditorTests =
	[skip genRequiredTwoFieldRecordUI
	,skip editRequiredTwoFieldRecord
	,genViewOptionalRecordUI
	]

genRequiredTwoFieldRecordUI = assertEqualWorld "Generate UI for Editor of type TwoFieldRecord"
	(Ok (UI UIContainer
			('DM'.singleton "class" (JSONArray [JSONString "record"]))
			[ UI UITextField
				('DM'.fromList
					[("hint-type",JSONString "info")
					,("editorId",JSONString "1")
					,("hint",JSONString "Please enter a single line of text (this value is required)")
					,("optional",JSONBool False)
					,("mode",JSONString "enter")
					,("taskId",JSONString "4-2")
					,("value",JSONNull)
					,("label",JSONString "fieldA")
					,("minlength",JSONInt 1)
					]
				)
				[]
			, UI UIIntegerField
				('DM'.fromList
					[("hint-type",JSONString "info")
					,("editorId",JSONString "2")
					,("hint",JSONString "Please enter a whole number (this value is required)")
					,("optional",JSONBool False)
					,("mode",JSONString "enter")
					,("taskId",JSONString "4-2")
					,("value",JSONNull)
					,("label",JSONString "fieldB")
					]
				)
				[]
			]
		, ?None
		)
	)
	(onResetWrapper ?None editor)
where
	editor :: Editor TwoFieldRecord (EditorReport TwoFieldRecord)
	editor = gEditor{|*|} EditValue


editRequiredTwoFieldRecord = assertEqualWorld "Edit UI for Editor of type TwoFieldRecord"
	(Ok (?Just (postChange,postState,?None)))
	(onEditWrapper edit preState editor)
where
	editor :: Editor TwoFieldRecord (EditorReport TwoFieldRecord)
	editor = gEditor{|*|} EditValue

	edit = (fromString "1",JSONString "x")
	preState = CompoundState ?None
			[LeafState {editorId= ?Just (fromString "1"),touched=False,state= ?None}
			,LeafState {editorId= ?Just (fromString "2"),touched=False,state= ?None}
			]

	postChange =
		ChangeUI
		[]
		[
			(0,ChangeChild
				(ChangeUI
					[SetAttribute "value" (JSONString "x")
					,SetAttribute "hint" (JSONString "You have correctly entered a single line of text")
					,SetAttribute "hint-type" (JSONString "valid")
					]
					[]
				)
			)
		]
	postState = CompoundState ?None
				[LeafState {editorId= ?Just (fromString "1"),touched=True,state= ?Just (dynamic "x")}
				,LeafState {editorId= ?Just (fromString "2"),touched=False,state= ?None}
				]


//HERE!
genViewOptionalRecordUI = assertEqualWorld "Generate UI for viewing of type OptionalRecord "
	(Ok (
		UI UIContainer
			('DM'.fromList [("class",JSONArray [JSONString "record"])])
			[UI UIContainer ('DM'.fromList [("label",JSONString "Field c"),("class",JSONArray [JSONString "record"]),("optional",JSONBool True)])
				[UI UICheckbox ('DM'.fromList [("taskId",JSONString "4-2"),("editorId",JSONString "0"),("value",JSONBool True)]) []
				,UI UITextField ('DM'.fromList
					[("taskId",JSONString "4-2")
					,("tooltip-type",JSONString "valid")
					,("editorId",JSONString "1")
					,("tooltip",JSONString "You have correctly entered a single line of text")
					,("optional",JSONBool False)
					,("minlength",JSONInt 1)
					,("label",JSONString "Field a")
					,("value",JSONString "1")
					]) []
				,UI UIIntegerField ('DM'.fromList
					[("tooltip",JSONString "You have correctly entered a whole number")
					,("optional",JSONBool False)
					,("tooltip-type",JSONString "valid")
					,("value",JSONInt 2)
					,("editorId",JSONString "2")
					,("taskId",JSONString "4-2")
					,("label",JSONString "Field b")
					]) []
				]
			,UI UIIntegerField ('DM'.fromList
				[("tooltip",JSONString "You have correctly entered a whole number")
				,("optional",JSONBool False)
				,("tooltip-type",JSONString "valid")
				,("value",JSONInt 3)
				,("editorId",JSONString "3")
				,("taskId",JSONString "4-2")
				,("label",JSONString "Field d")
				]) []
			]

	, ?Just (ValidEditor {fieldC= ?Just {fieldA="1", fieldB =2 }, fieldD=3})))
	(onResetWrapper (?Just {fieldC = ?Just {fieldA = "1", fieldB = 2}, fieldD = 3}) recordEditor)
where
	recordEditor :: Editor OptionalRecord (EditorReport OptionalRecord)
	recordEditor = gEditor{|*|} EditValue

tupleEditorTests = []

onResetWrapper :: !(?r) !(Editor r w) !*World -> (!MaybeError String (UI, ?w), !*World)
onResetWrapper mbval editor world
	# (options,world) = defaultEngineOptions world
	# mbIworld = createIWorld options world
	| mbIworld =: Left _ = let (Left (err, world)) = mbIworld in (Error err, world)
	# iworld = let (Right iworld) = mbIworld in iworld
	# vst =
		{ taskId            = "4-2"
		, nextEditorId      = zero
		, optional          = False
		, selectedConsIndex = 0
		, pathInEditMode    = []
		, abcInterpreterEnv = iworld.IWorld.abcInterpreterEnv
		, engineOptions     = iworld.options
		}
	# (res, _) = editor.Editor.onReset emptyAttr mbval vst
	= ((\(ui, _, mbw) -> (ui, mbw)) <$> res,iworld.world)

onEditWrapper ::
	!(!EditorId, !JSONNode) !EditState !(Editor r w) !*World
	-> (!MaybeError String (?(UIChange, EditState, ?w)), !*World)
onEditWrapper edit state editor world
	# (options,world) = defaultEngineOptions world
	# mbIworld = createIWorld options world
	| mbIworld =: Left _ = let (Left (err, world)) = mbIworld in (Error err, world)
	# iworld = let (Right iworld) = mbIworld in iworld
	# vst =
		{ taskId            = "4-2"
		, nextEditorId      = zero
		, optional          = False
		, selectedConsIndex = 0
		, pathInEditMode    = []
		, abcInterpreterEnv = iworld.IWorld.abcInterpreterEnv
		, engineOptions     = iworld.options
		}
	# (res, _) = editor.Editor.onEdit edit state vst
	= (res,iworld.world)

onRefreshWrapper :: !(?r) !EditState !(Editor r w) !*World -> (!MaybeErrorString (UIChange, EditState, ?w), !*World)
onRefreshWrapper value state editor world
	# (options,world) = defaultEngineOptions world
	# mbIworld = createIWorld options world
	| mbIworld =: Left _ = let (Left (err, world)) = mbIworld in (Error err, world)
	# iworld = let (Right iworld) = mbIworld in iworld
	# vst =
		{ taskId            = "4-2"
		, nextEditorId      = zero
		, optional          = False
		, selectedConsIndex = 0
		, pathInEditMode    = []
		, abcInterpreterEnv = iworld.IWorld.abcInterpreterEnv
		, engineOptions     = iworld.options
		}
	# (res, _) = editor.Editor.onRefresh value state vst
	= (res,iworld.world)

writeValueWrapper :: !EditState !(Editor r w) !*World -> (!MaybeErrorString w, !*World)
writeValueWrapper state editor world
	# (options,world) = defaultEngineOptions world
	# mbIworld = createIWorld options world
	| mbIworld =: Left _ = let (Left (err, world)) = mbIworld in (Error err, world)
	# iworld = let (Right iworld) = mbIworld in iworld
	# vst =
		{ taskId            = "4-2"
		, nextEditorId      = zero
		, optional          = False
		, selectedConsIndex = 0
		, pathInEditMode    = []
		, abcInterpreterEnv = iworld.IWorld.abcInterpreterEnv
		, engineOptions     = iworld.options
		}
	# res = editor.Editor.writeValue state
	= (res,iworld.world)

Start w = runUnitTests tests w
