module iTasks.Engine.UnitTests

import Data.Maybe
import StdEnv
import Text.GenPrint

import iTasks
import iTasks.Testing.Unit
import iTasks.Internal.Test.Stubs
import iTasks.Internal.IWorld

Start world = runUnitTests tests world

tests = [determineTimeoutTest]

/**
 * This test makes sure that determineTimeout returns ?Just 0 when iworld.shutdown has been set.
 * If this is not the case, retrieving I/O events may block forever even though the program is supposed to halt.
 */
determineTimeoutTest = assertEqualWorld "determineTimeoutTest" (?Just 0) testDetermineTimeout
where
    testDetermineTimeout :: !*World -> (!?Int, !*World)
    testDetermineTimeout world
        # iworld = toStubIWorld world
        # iworld & shutdown = ?Just 0
        # (timeout, iworld) = determineTimeout iworld
        = (timeout, destroyIWorld iworld)
