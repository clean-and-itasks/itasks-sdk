module iTasks.WF.Tasks.IO.NetworkIOClient

/**
 * The executable obtained by building this module is used by the networkIODist test found in UnitTests.icl.
 */

import Data.GenHash

import iTasks
import iTasks.Extensions.DateTime

Start w = doTasks (onStartup connect) w

connect :: Task ()
connect = tcpconnect "localhost" (Port 1234) nullShare handlersClientDist @! ()

handlersClientDist :: ConnectionHandlers () () ()
handlersClientDist =
	{ConnectionHandlers
	| onConnect = \_ _ _ -> (Ok (), ?None, ["Sending request"], False)
	, onData = \_ _ _ _ -> (Ok ?None, ?None, [], False)
	, onShareChange = \_ _ -> (Ok ?None, ?None, [], False)
	, onDisconnect = \_ _ _ -> (Ok ?None, ?None)
	, onDestroy = \_ -> (Ok ?None, [])
	}
