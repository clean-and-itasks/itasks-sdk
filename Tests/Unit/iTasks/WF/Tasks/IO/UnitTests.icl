module iTasks.WF.Tasks.IO.UnitTests

import StdEnv
import Data.Either
import Data.GenHash
import iTasks
import iTasks.Extensions.Process
import iTasks.Testing
import iTasks.Testing.Unit
import iTasks.WF.Tasks.IO
import iTasks.Internal.AsyncIO
import System.Time
import Data.Func
import Data.Tuple
import Testing.TestEvents
import Text, Text.GenPrint
import System.Directory
import iTasks.Extensions.DateTime

Start world
	# (curDir, world) = getCurrentDirectory world
	= runUnitTests
	// Fast command means: command does not take long to terminate
	[ testExternalProcess "externalProcessLowPollTimeFastCommand" {tv_sec=0,tv_nsec=1}
	                      fast_command fast_command_args
	, testExternalProcess "externalProcessLowPollTimeSlowCommand" {tv_sec=0,tv_nsec=1}
	                      slow_command slow_command_args
	, testExternalProcess "externalProcessHighPollTimeFastCommand" {tv_sec=0,tv_nsec=1000000}
	                      fast_command fast_command_args
	, testExternalProcess "externalProcessHighPollTimeSlowCommand" {tv_sec=0,tv_nsec=1000000}
	                      slow_command slow_command_args
	, testTCP "tcplisten onConnect/onData/onDisconnect callbacks work."
		( withShared (0, Fail, "")  \sdsListen ->
		  withShared (0, Fail, "") \sdsClient ->
		  networkIO (Port 1234) sdsListen sdsClient handlersListen handlersClient
		)
	, testTCP "Network I/O works when using multiple clients that are started through externalProcess."
		(networkIODist (Port 1234) (fromOk curDir))
	, testTCP "tcpconnect: connecting to a host for which there is no listener results in a task exception."
		(connectToHostWithoutListenerReturnsException (Port 1234))
	, testTCP "tcplisten: listening to a port that is already being listened on returns task exception."
		(listenToPortThatIsAlreadyListenedOnReturnsException (Port 1234))
	, testTCP "Task value is updated when onData callback is evaluated."
		(testTaskValue (Port 1234))
	, externalProcessClosesPipes
	, externalProcessValueRemainsStableAfterProcessTerminated
	] world
where
	(slow_command, slow_command_args) = IF_WINDOWS
		("C:\\Windows\\System32\\cmd.exe", ["/c", "exit"])
		("/bin/sleep", ["5"])
	(fast_command, fast_command_args) = IF_WINDOWS
		("C:\\Windows\\System32\\cmd.exe", ["/c", "echo"])
		(IF_MAC "/usr/bin/true" "/bin/true", [])

	/**
	 * This test starts an event loop after queuing a Left RefreshEvent.
	 * which causes the externalProcess task (task argument) to be evaluated.
	 * This results in starting the external process that is specified by cmd.
	 * The externalProcess task returns the return code which was returned by the external process
	 * as a stable task value after the external process terminates.
	 * testTaskResult verifies that the return code of the external process is 0
	 * which means that the program was successfully executed.
	 */
	testExternalProcess :: !String !Timespec !String ![String] -> UnitTest
	testExternalProcess name ts cmd args = testTaskResult name task [Left ResetEvent] 0 checkEqual
	where
		task =
			withShared [] \stdin ->
			withShared ([], []) \stdout ->
			externalProcess ts cmd [Arguments args] stdin stdout

	// By executing the external process many times, the limit for the number of open pipes/file handles
	// would be reached if the pipes were not closed, causing the program to crash.
	externalProcessClosesPipes :: UnitTest
	externalProcessClosesPipes =
		testTaskResult "External process closes pipes" (runExternalProcessNTimes 1200) [Left ResetEvent] 0 checkEqual
	where
		runExternalProcessNTimes :: !Int -> Task Int
		runExternalProcessNTimes 0 = return 0
		runExternalProcessNTimes n =
			withShared [] \stdin =
			withShared ([], []) \stdout =
			externalProcess {tv_sec=0,tv_nsec=1} fast_command [Arguments fast_command_args] stdin stdout >>- \exit =
			if (exit <> 0)
				(throw $ "Error: externalProcessClosesPipes, exit code is " +++ toString exit)
				(runExternalProcessNTimes (n - 1))

	// A bug in `externalProcess` caused the task value to become `NoValue`
	// if the task was evaluated with a refresh event which was not for the external process task.
	// As a result, if `externalProcess` was ran in parallel with some other task which would generate a refresh event,
	// the value of the `externalProcess` task could become `NoValue` even though the process had terminated with
	// an exit code. This test mimics this behavior and asserts that the task always remains stable
	// if the process has terminated when a refresh event is triggered for another task.
	externalProcessValueRemainsStableAfterProcessTerminated :: UnitTest
	externalProcessValueRemainsStableAfterProcessTerminated =
		testTaskResult
			"External process value remains stable after process terminated"
			(((runProcess @? toStableIndicator) -&&- (waitForTimer False 5)) @ fst) [Left ResetEvent] True checkEqual
	where
		// Transforms the stability of the value to always be stable even though it is not.
		// The value itself is changed to a `Bool` indicating the actual stability.
		toStableIndicator :: !(TaskValue a) -> TaskValue Bool
		toStableIndicator NoValue = Value False True
		toStableIndicator (Value _ s) = Value s True

		runProcess =
			withShared [] \stdin =
			withShared ([], []) \stdout =
			externalProcess {tv_sec=0,tv_nsec=1} fast_command [Arguments fast_command_args] stdin stdout

	/**
	 * This test starts an event loop after queuing a Left RefreshEvent
	 * This causes `task` to be evaluated.
	 * testTaskResult checks that the value is True when the task value of the task becomes stable.
	 */
	testTCP :: !String !(Task Bool) -> UnitTest
	testTCP name task
		= testTaskResult name task [Left ResetEvent] True checkEqual

/*
 * This task causes the tcplisten task to be evaluated, creating a TCP listener.
 * This is followed by tcpconnect being evaluated, establishing a connection to the TCP listener.
 * As a result the TCP listener and client communicate, which causes the SDS containing the state to change.
 * This state is watched, when the state is correct, a stable value with value True is returned.
 */
networkIO ::
	!Port
	!(sds () (Int,Callback,String) w)
	!(sds () (Int,Callback,String) w)
	!(ConnectionHandlers l (Int,Callback,String) w)
	!(ConnectionHandlers l (Int,Callback,String) w)
	-> Task Bool | iTask l  & iTask w & RWShared sds
networkIO port sdsListen sdsClient handlersListen handlersClient =
	tcplisten port True sdsListen handlersListen ||-
	tcpconnect "localhost" port sdsClient handlersClient
	||-
	watch sdsListen @? checkCallback >>- return

testTaskValue :: !Port -> Task Bool
testTaskValue port =
	(	tcplisten port True nullShare handlersTestTaskValue
		||-
		tcpconnect "localhost" port nullShare handlersTestTaskValue
	) >>* [OnValue $ ifValue (\i -> i > 5) (return o const True)]

networkIODist :: !Port !FilePath -> Task Bool
networkIODist port curDir
	// The test assumes windows.sh/posix.sh is executed from iTasks-SDK/
	= withShared 0 \sdsListen =
			(tcplisten port True sdsListen handlersListenDist @! True)
		||-
			(startClientExternalProcesses @! True)
		>-| get sdsListen >>- \v = return (v == 4)
where
	startClientExternalProcesses :: Task (Int, (Int, (Int, Int)))
	startClientExternalProcesses
		# path = IF_WINDOWS "Tests\\Unit\\iTasks.WF.Tasks.IO.NetworkIOClient.exe"
	                        "Tests/Unit/iTasks.WF.Tasks.IO.NetworkIOClient.exe"
		= withShared [] \stdinShare -> withShared ([],[]) \stdoutShare ->
			externalProcess {tv_sec=1, tv_nsec=0} path [StartupDirectory curDir] stdinShare stdoutShare
				-&&-
			externalProcess {tv_sec=1, tv_nsec=0} path [StartupDirectory curDir] stdinShare stdoutShare
				-&&-
			externalProcess {tv_sec=1, tv_nsec=0} path [StartupDirectory curDir] stdinShare stdoutShare
				-&&-
			externalProcess {tv_sec=1, tv_nsec=0} path [StartupDirectory curDir] stdinShare stdoutShare

connectToHostWithoutListenerReturnsException :: !Port -> Task Bool
connectToHostWithoutListenerReturnsException port =
	tcpconnect "localhost" port defaultSds handlersClient
	//* If there is an inconsistency in the IO state the error is: "No IO state yet, status is IOConnecting" (see issue 548)
	>>* [OnAllExceptions (return o not o startsWith "tcpconnect: No IO state yet")]

listenToPortThatIsAlreadyListenedOnReturnsException :: !Port -> Task Bool
listenToPortThatIsAlreadyListenedOnReturnsException port =
	(	tcplisten port True defaultSds handlersListen
			-||-
		tcplisten port True defaultSds handlersListen
	)
	>>* [OnAllExceptions (const $ return True)]

defaultSds :: SimpleSDSLens (Int,Callback, String)
defaultSds = sharedStore "sdsClient" (0,Fail, "")

checkCallback :: !(TaskValue (Int,Callback,String)) -> TaskValue Bool
checkCallback (Value (2, OnDisconnect,"Send") s) = (Value True True)
checkCallback (Value (_,_,_) s) = (Value False False)
checkCallback _ = NoValue

/* This type is introduced for maintaining an administration of which callback was last executed
 * through using it as a connection state. This is used for testing that the callbacks
 * are evaluated in the order they are supposed to be evaluated in.
 */
:: Callback = OnConnect | OnData | OnDisconnect | Fail

derive class iTask Callback

instance == Callback where
	(==) OnConnect OnConnect = True
	(==) OnData OnData = True
	(==) OnDisconnect OnDisconnect = True
	(==) Fail Fail = True
	(==) _ _ = False

handlersClient :: ConnectionHandlers Callback (Int,Callback,String) (Int,Callback,String)
handlersClient =
	{ConnectionHandlers
	| onConnect = \cid s r -> (Ok OnConnect, ?Just (0,OnConnect,""), [], False)
	, onData = \_ data l _ ->
		(Ok (?Just OnData), if (l == OnConnect) (?Just (1,OnData,data)) (?Just (0,Fail,"")), ["Send"], True)
	, onShareChange = \l _ -> (Ok ?None, ?None, [], False)
	, onDisconnect = \_ l (_,_,data) ->
		(Ok (?Just OnDisconnect), if (l == OnData) (?Just (2,OnDisconnect,data)) (?Just (0,Fail,"")))
	,onDestroy = \_ -> (Ok ?None, [])
	}

handlersListen :: ConnectionHandlers Callback (Int,Callback,String) (Int,Callback,String)
handlersListen =
	{ConnectionHandlers
	| onConnect = \cid _ _ -> (Ok OnConnect, ?Just (0,OnConnect,""), ["Send"], False)
	, onData = \_ data l _ -> (Ok (?Just OnData), if (l == OnConnect) (?Just (1,OnData,data)) (?Just (0,Fail,"")), [], False)
	, onShareChange = \l _ -> (Ok ?None, ?None, [], False)
	, onDisconnect = \_ l (_,_,data) ->
		(Ok (?Just OnDisconnect), if (l == OnData) (?Just (2,OnDisconnect,data)) (?Just (0,Fail,"")))
	, onDestroy = \_ -> (Ok ?None, [])
	}

handlersListenDist :: ConnectionHandlers Callback Int Int
handlersListenDist =
	{ConnectionHandlers
	| onConnect = \cid _ _ -> (Ok OnConnect, ?None, [], False)
	, onData = \_ data _ r -> (Ok (?Just OnData), ?Just (r+1), ["Sending response"], True)
	, onShareChange = \_ _ -> (Ok ?None, ?None, [], False)
	, onDisconnect = \_ l _ -> (Ok (?Just OnDisconnect), ?None)
	, onDestroy = \_ -> (Ok ?None, [])
	}

handlersTestTaskValue :: ConnectionHandlers Int () ()
handlersTestTaskValue =
	{ConnectionHandlers
	| onConnect = \cid _ _ -> (Ok 0, ?None, ["echo"], False)
	, onData = \_ data l _ -> (Ok (?Just (l+1)), ?None, ["Sending response"], False)
	, onShareChange = \_ _ -> (Ok ?None, ?None, [], False)
	, onDisconnect = \_ _ _ -> (Ok ?None, ?None)
	, onDestroy = \_ -> (Ok ?None, [])
	}
