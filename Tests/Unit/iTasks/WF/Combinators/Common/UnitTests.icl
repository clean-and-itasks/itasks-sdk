module iTasks.WF.Combinators.Common.UnitTests

import Data.Func, Data.Either
import qualified Data.Set as Set
import qualified Data.Map as Map
import Text.GenPrint

import iTasks
import iTasks.Internal.TaskIO
import iTasks.Testing
import iTasks.Testing.Unit

Start world = runUnitTests tests world

tests = [foreverStStateChange]

foreverStStateChange = testTaskResult "foreverSt state change" task events 100 checkEqual
where
	task   = foreverStIf (\st -> st < n) 0 (\st -> return $ inc st)
	events = [Left ResetEvent]
	where
		attrs =
			'Map'.fromList
				[("value", JSONString $ toString n), ("class", JSONArray [JSONString "step", JSONString "interact"])]
	n      = 100

derive gPrint TaskOutputMessage, UIChange, UIChildChange, UIAttributeChange, UI, UIType, Map, JSONNode
