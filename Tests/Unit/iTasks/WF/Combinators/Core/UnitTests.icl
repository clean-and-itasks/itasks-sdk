module iTasks.WF.Combinators.Core.UnitTests

import Data.Either
import Text.GenPrint

import iTasks
import iTasks.Testing.Unit

Start world = runUnitTests
	[ testTaskResult "parallelStability" parallelStable [Left ResetEvent] [(0, Value 42 True)] (checkEqualWith parallelStablePred)
	] world
where
	parallelStable = parallel [(Embedded, \_->return 42)] []
	parallelStablePred [(_, Value i True)] [(_, Value j True)] = i == j
	parallelStablePred _ _ = False

derive gPrint TaskValue
