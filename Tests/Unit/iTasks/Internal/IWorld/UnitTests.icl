module iTasks.Internal.IWorld.UnitTests

import StdEnv
import Data.Either
import Data.Func
import iTasks
import iTasks.Engine
import iTasks.Internal.IWorld
import iTasks.Testing
import iTasks.Testing.Unit
import System.Time
import Text.GenPrint

derive gPrint Timespec

testInitIWorld = assertWorld "Init IWorld" id sut
where
	sut world
		# (options,world) = defaultEngineOptions world
		# mbIworld = createIWorld options world
		| mbIworld =: Left _ = let (Left (_, world)) = mbIworld in (False, world)
		# iworld = let (Right iworld) = mbIworld in iworld
		//Check some properties
		//# res = server.paths.dataDirectory == appDir </> "TEST-data"//Is the data directory path correctly initialized
		# world = destroyIWorld iworld
		= (True,world)

testComputeNextFire =
	//Interval == 0
	[ assertEqual "computeNextFire start=regTime interval=0" timeReg
		$ computeNextFire timeReg {start=timeReg, interval=zero}
	, assertEqual "computeNextFire start<regTime interval=0" (timeReg-timeSome)
		$ computeNextFire timeReg {start=timeReg-timeSome, interval=zero}
	, assertEqual "computeNextFire start>regTime interval=0" (timeReg+timeSome)
		$ computeNextFire timeReg {start=timeReg+timeSome, interval=zero}
	//Interval <> 0
	, assertEqual "computeNextFire start<regTime interval<>0" (timeReg-timeEps+timeSome)
		$ computeNextFire timeReg {start=timeReg-timeEps, interval=timeSome}
	, assertEqual "computeNextFire start=regTime interval<>0" (timeReg+timeSome)
		$ computeNextFire timeReg {start=timeReg, interval=timeSome}
	: [ assertEqual
			("computeNextFire start>regTime interval<>0, " +++ toString i +++ " intervals passed")
			(timeReg + scale (i+1) timeSome)
			$ computeNextFire (timeReg + timeEps + scale i timeSome) {start=timeReg, interval=timeSome}
	\\i<-[0..5]]]
where
	scale i ts = sum [ts\\_<-[0..i-1]]
	timeReg = {tv_sec=23467,tv_nsec=67890}
	timeNow = {tv_sec=12356,tv_nsec=56789}

	timeSome = {tv_sec=123,tv_nsec=456}
	timeEps  = {tv_sec=0,tv_nsec=1}

tests = [testInitIWorld:testComputeNextFire]

Start world = runUnitTests tests world
