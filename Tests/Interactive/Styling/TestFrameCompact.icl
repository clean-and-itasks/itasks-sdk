module TestFrameCompact

import StdEnv
import Text
import iTasks

test :: Task String
test = viewInformation [] (join "\n" (repeatn 1000 "This is a line of text.")) <<@ ApplyLayout frameCompact

Start world = doTasks test world
