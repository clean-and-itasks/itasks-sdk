module TestADTSingleConsOne
import iTasks
import iTasks.Testing
import iTasks.Testing.Interactive

:: ADTSingleCons = ADTSingleCons Int
derive class iTask ADTSingleCons
derive gDefault ADTSingleCons

test :: Task ADTSingleCons
test = testCommonInteractions "ADTSingleCons"

Start world = doTasks test world

