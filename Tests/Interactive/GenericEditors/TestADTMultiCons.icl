module TestADTMultiCons

import iTasks
import iTasks.Testing
import iTasks.Testing.Interactive

:: ADTMultiCons
    = ADTMultiConsNone
    | ADTMultiConsSingle Int
    | ADTMultiConsMulti Int String

derive class iTask ADTMultiCons
derive gDefault ADTMultiCons

test :: Task ADTMultiCons
test = testCommonInteractions "ADTMultiCons"

Start world = doTasks test world

