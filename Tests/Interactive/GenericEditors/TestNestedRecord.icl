module TestNestedRecord
import iTasks
import iTasks.Testing
import iTasks.Testing.Interactive

:: TwoFieldRecord =
    { first     :: Int
    , second    :: String
    }

:: NestedRecord =
    { firstTwo  :: TwoFieldRecord
    , third     :: Bool
    }
derive class iTask TwoFieldRecord, NestedRecord
derive gDefault TwoFieldRecord, NestedRecord

test :: Task NestedRecord
test = testCommonInteractions "NestedRecord"

Start world = doTasks test world

