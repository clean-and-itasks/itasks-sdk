module TestSingleRecord
import iTasks
import iTasks.Testing
import iTasks.Testing.Interactive

:: TwoFieldRecord =
    { first     :: Int
    , second    :: String
    }
derive class iTask TwoFieldRecord
derive gDefault TwoFieldRecord

test :: Task TwoFieldRecord
test = testCommonInteractions "TwoFieldRecord"

Start world = doTasks test world

