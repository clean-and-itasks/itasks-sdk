module TestADTSingleConsMulti
import iTasks
import iTasks.Testing
import iTasks.Testing.Interactive

:: ADTSingleConsMulti = ADTSingleConsMulti Int String
derive class iTask ADTSingleConsMulti
derive gDefault ADTSingleConsMulti

test :: Task ADTSingleConsMulti
test = testCommonInteractions "ADTSingleConsMulti"

Start world = doTasks test world

