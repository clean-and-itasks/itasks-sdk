module TestADTSingleConsMany
import iTasks
import iTasks.Testing
import iTasks.Testing.Interactive

:: ADTSingleConsMany = ADTSingleConsMany String Int String Int String
derive class iTask ADTSingleConsMany
derive gDefault ADTSingleConsMany

test :: Task ADTSingleConsMany
test = testCommonInteractions "ADTSingleConsMany"

Start world = doTasks test world

