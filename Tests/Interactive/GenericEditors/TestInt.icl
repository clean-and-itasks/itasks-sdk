module TestInt
import iTasks
import iTasks.Testing
import iTasks.Testing.Interactive

test :: Task Int
test = testCommonInteractions "Int"

Start world = doTasks test world
