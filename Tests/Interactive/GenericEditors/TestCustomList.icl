module TestCustomList
import iTasks
import iTasks.Testing
import iTasks.Testing.Interactive

:: List = Nil | Cons Int List
derive class iTask List
derive gDefault List

test :: Task List
test = testCommonInteractions "Custom list"

Start world = doTasks test world
