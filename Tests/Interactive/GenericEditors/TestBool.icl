module TestBool
import iTasks
import iTasks.Testing
import iTasks.Testing.Interactive

test :: Task Bool
test = testCommonInteractions "Bool"

Start world = doTasks test world
