module TestMultipleViewsOnSDS
import iTasks
import Text

test :: Task (?String)
test = withShared "" editTextWithDifferentViews

editTextWithDifferentViews model
	=           editInTextArea model
				-||-
				editAsListOfLines model
		>>*     [OnAction ActionQuit (?Just o return o toMaybe)]

editInTextArea model
	=           updateSharedInformation [noteEditor] model
	>^*         [ OnAction (Action "Trim") (\txt -> ?Just (upd trim model))  
				]

editAsListOfLines model
	=   Title "Lines" @>> Hint "Edit lines" @>> updateSharedInformation [listEditor] model

noteEditor = UpdateSharedUsing id (const id) textArea
listEditor = UpdateSharedAs (split "\n") (\_ l -> join "\n" l)

toMaybe (Value v _) =  (?Just v)
toMaybe _   =  ?None

Start world = doTasks test world
