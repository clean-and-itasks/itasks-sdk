module TestMenu

import Data.Func
import qualified Data.Map as DM
import iTasks

test :: Task String
test =
	enterInformation [EnterUsing id editor]
		//Remove prompt
		<<@ ApplyLayout (sequenceLayouts [/*removeSubUIs (SelectByPath [0]),unwrapUI,*/setUIAttributes (textAttr "Sub menu")])
	>>! test
where
	editor =
		mapEditorWrite ValidEditor $
			menu2 (button <<@ 'DM'.unions[textAttr "Button a",iconClsAttr "icon-ok"]) (button <<@ (textAttr "Button b"))
	test (b1, b2) =
		viewInformation [] ("This is the content of the container, button a was clicked: "
			+++ toString b1 +++ " button b was clicked: " +++ toString b2)
			<<@ ApplyLayout (wrapUI UIMenu)
			<<@ ApplyLayout (setUIAttributes (textAttr "Open menu"))

Start world = doTasks test world
