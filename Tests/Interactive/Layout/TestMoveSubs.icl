module TestMoveSubs

import Data.Func
import iTasks
import Text

test :: Task ()
test = (viewInformation [] "Test for moving a sub ui (button to toolbar)" >>*
		[ OnAction (Action "button")   $ always $ (viewInformation [] "Test task" @! ())
		]) <<@ ApplyLayout (sequenceLayouts layout)
where
	layout =
		[ wrapUI UIPanel
			, insertChildUI 0
				(uic UIToolBar
					[ uia UIMenu $ textAttr $ "The toolbar"]
				)
			, moveSubUIs (continueButtonSelector "button") [0, 0] 0
		]

	continueButtonSelector :: !String -> UISelection
	continueButtonSelector buttonName =
		(SelectAND
			(SelectByDepth 2)
			(SelectAND
				(SelectByType UIAction)
				(SelectByAttribute "actionId" (matchName buttonName)
			)
		))
	where
		matchName name (JSONString s) = startsWith name s
		matchName _ _ = False

Start world = doTasks test world
