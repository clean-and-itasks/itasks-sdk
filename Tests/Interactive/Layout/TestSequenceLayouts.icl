module TestSequenceLayouts

import Data.Func

import iTasks

test :: Task String
test = viewInformation [] "Test for sequencing layouts" <<@ ApplyLayout layout
where
    layout = sequenceLayouts [setUIAttributes $ labelAttr "label text", setUIAttributes $ styleAttr "background: #ff0"]

Start world = doTasks test world
