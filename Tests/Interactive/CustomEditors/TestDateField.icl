module TestDateField

import iTasks
import iTasks.Extensions.DateTime
import iTasks.Testing.Interactive

test :: Task Date
test = testEditor (gEditor{|*|} EditValue) (?Just {Date|year=2003,mon=1,day=13}) False

Start world = doTasks test world
