module TestMixedCombinedTextFields
import iTasks, iTasks.Testing.Interactive

test :: Task (String,String)
test = testEditor (mapEditorWrite editorReportTuple2 editor) (?Just ("Hello","world")) False
where
    editor = container2 username password
    username = pink (labelAttr "Username" @>> textField)
    password = pink (labelAttr "Password" @>> passwordField)
    pink :: (Editor String (EditorReport String)) -> Editor String (EditorReport String)
    pink e = (styleAttr "background-color: pink") @>> e

Start world = doTasks test world
