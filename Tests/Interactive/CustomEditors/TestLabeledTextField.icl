module TestLabeledTextField
import iTasks, iTasks.Testing.Interactive

test :: Task String
test = testEditor (Label "Foo" @>> textField) (?Just "Hello world") False

Start world = doTasks test world
