module TestCombinedTextFields

import Data.Func
import iTasks, iTasks.Testing.Interactive

test :: Task (String,String)
test = testEditor (mapEditorWrite editorReportTuple2 $ container2 textField textField) (?Just ("Hello","world")) False

Start world = doTasks test world
