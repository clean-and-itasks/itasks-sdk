module TestColoredTextField

import iTasks, iTasks.Testing.Interactive

test :: Task String
test = testEditor (styleAttr "background-color: pink" @>> textField) (?Just "Hello world") False

Start world = doTasks test world
