module TestPikadayEditlet

import iTasks.Extensions.Form.Pikaday, iTasks.Testing.Interactive

derive gDefault Date

test :: Task Date
test = testEditor pikadayDateField defaultValue False

Start world = doTasks test world
