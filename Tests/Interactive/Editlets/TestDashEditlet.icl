module TestDashEditlet
import iTasks
import iTasks.Extensions.Dashboard
import iTasks.Extensions.DateTime

test = withShared LightOff \v ->
	switch v
	||-
	viewSharedInformation [] v
where
	switch v = forever (
		set LightOff      v >-| waitForTimer False 1 >-|
		set LightOnRed    v >-| waitForTimer False 1 >-|
		set LightOnOrange v >-| waitForTimer False 1 >-|
		set LightOnGreen  v >-| waitForTimer False 1)

Start world = doTasks test world
