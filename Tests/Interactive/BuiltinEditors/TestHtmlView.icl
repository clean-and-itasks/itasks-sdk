module TestHtmlView
import iTasks, iTasks.Testing.Interactive, Text.HTML

test :: Task HtmlTag
test = testEditor (mapEditorWrite (const EmptyEditor) htmlView) (?Just (H2Tag [] [Text "Hello World"])) True

Start world = doTasks test world
