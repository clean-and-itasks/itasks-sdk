module TestList
import StdOverloadedList
import iTasks, iTasks.Testing.Interactive
import iTasks.UI.Editor.Common

test :: Task [String]
test = testEditor (mapEditorWrite (\l -> ValidEditor [x \\ ValidEditor x <- l])
	(listEditor False (?Just (const (?Just "New item"))) True True (?Just (\items -> length items +++> " items")) ValidEditor textField)) (?Just []) False

Start world = doTasks test world
