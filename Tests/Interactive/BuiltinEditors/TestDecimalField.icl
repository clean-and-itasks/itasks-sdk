module TestDecimalField
import iTasks, iTasks.Testing.Interactive

test :: Task Real
test = testEditor decimalField (?Just 3.14) False

Start world = doTasks test world
