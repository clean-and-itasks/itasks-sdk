module TestProgressBar
import iTasks, iTasks.Testing.Interactive

test :: Task (?Int,?String)
test = testEditor (mapEditorWrite (const EmptyEditor) progressBar) (?Just (?Just 90,?Just "Almost done")) False

Start world = doTasks test world
