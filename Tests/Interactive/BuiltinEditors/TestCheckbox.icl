module TestCheckbox

import iTasks, iTasks.Testing.Interactive

test :: Task Bool
test = testEditor (mapEditorWrite ValidEditor checkBox) (?Just False) False

Start world = doTasks test world

