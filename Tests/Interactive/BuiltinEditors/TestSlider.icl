module TestSlider
import iTasks, iTasks.Testing.Interactive
import qualified Data.Map as DM

test :: Task Int
test = testEditor (mapEditorWrite ValidEditor slider <<@ ('DM'.union (minAttr 1) (maxAttr 5))) (?Just 3) False

Start world = doTasks test world
