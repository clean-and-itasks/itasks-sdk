module TestGridMulti
import iTasks, iTasks.Testing.Interactive, Text.HTML

derive class iTask ChoiceGrid, ChoiceRow

test :: Task (ChoiceGrid,[Int])
test = testEditor (mapEditorWrite (\sel -> ValidEditor (cgrid [],sel)) grid <<@ multipleAttr True)
	(?Just (cgrid rows, [])) False
where
	cgrid rows = {ChoiceGrid|header=["Key","Value"],rows=rows}
    rows = [{ChoiceRow|id=1,cells=[Text "A",Text "1"]},{ChoiceRow|id=2,cells=[Text "B",Text "2"]},{ChoiceRow|id=3,cells=[Text "C",Text "3"]}]

Start world = doTasks test world
