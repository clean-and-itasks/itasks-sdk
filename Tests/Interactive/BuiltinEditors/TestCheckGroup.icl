module TestCheckGroup
import iTasks, iTasks.Testing.Interactive
derive class iTask ChoiceText 

test :: Task ([ChoiceText],[Int])
test = testEditor (mapEditorWrite (\sel -> ValidEditor ([],sel)) checkGroup)
	(?Just ([{ChoiceText|id=0,text="A"},{ChoiceText|id=1,text="B"},{ChoiceText|id=2,text="C"}],[])) False

Start world = doTasks test world

