module TestButton
import iTasks, iTasks.Testing.Interactive

test :: Task Bool
test = testEditor ((mapEditorWrite ValidEditor button) <<@ (textAttr "Click")) (?Just False) False

Start world = doTasks test world
