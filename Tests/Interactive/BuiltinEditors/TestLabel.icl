module TestLabel
import iTasks, iTasks.Testing.Interactive

test :: Task String
test = testEditor (mapEditorWrite (const EmptyEditor) label) (?Just "Hello world") False

Start world = doTasks test world
