module TestTextView
import iTasks, iTasks.Testing.Interactive

test :: Task String
test = testEditor (mapEditorWrite (const EmptyEditor) textView) (?Just "Hello World") False

Start world = doTasks test world
