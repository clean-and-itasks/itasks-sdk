module TestTreeMulti
import iTasks, iTasks.Testing.Interactive
derive class iTask ChoiceNode

test :: Task ([ChoiceNode],[Int])
test = testEditor (mapEditorWrite (\sel -> ValidEditor ([],sel)) tree <<@ multipleAttr True)
	(?Just
		([{ChoiceNode|id=1,label="A",icon= ?None,expanded=False,children=[]}
        ,{ChoiceNode|id=2,label="B",icon= ?None,expanded=False,children=[]}
        ,{ChoiceNode|id=3,label="C",icon= ?None,expanded=False,children=[]}
        ],[])) False

Start world = doTasks test world
