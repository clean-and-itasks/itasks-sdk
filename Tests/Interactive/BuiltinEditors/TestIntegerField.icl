module TestIntegerField
import iTasks, iTasks.Testing.Interactive

test :: Task Int
test = testEditor integerField (?Just 42) False

Start world = doTasks test world

