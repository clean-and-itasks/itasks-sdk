module TestIcon
import iTasks, iTasks.Testing.Interactive

test :: Task (String,?String)
test = testEditor (mapEditorWrite (const EmptyEditor) icon) (?Just ("icon-valid",?Just "Icon with a tooltip!")) False

Start world = doTasks test world
