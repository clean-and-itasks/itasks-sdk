module TestCallProcess

import iTasks
import System.Time

test =
    Hint "Press the button to run an OS process (/bin/date)" @>> viewInformation [] () >!|
    withShared [] \stdinShare ->
    withShared ([],[]) \stdOutAndStdErrShare ->
        externalProcess  {tv_sec=1, tv_nsec=0} "/bin/date" [] stdinShare stdOutAndStdErrShare
    -||
        (Hint "Output: " @>> viewSharedInformation [] stdOutAndStdErrShare)

Start world = doTasks test world
