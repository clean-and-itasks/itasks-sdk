#! /bin/bash
# This tests whether we can retrieve the index page from the HTTP server. A low keepalive time is used and we wait for
# longer than this time, before we retrieve the page. This is to check whether a buggy implementation does not
# terminate the connection due to not correctly updated timestamps in the internal administration.
set -e
Examples/BasicAPIExamples.exe --keepalive=1 & (sleep 3s; kill $!;) & (sleep 2s; curl http://localhost:8080 > /dev/null)
