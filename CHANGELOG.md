#### 0.16.2

- Fix: restrict line line length of email body content to avoid SMTP errors in
       case lines are too long.

#### 0.16.1

- Fix: do not call `afterRender` on children for which the function does not exist.

## 0.16.0

- Enhancement: provide immediate feedback if passwords do not match when
          creating a user through the user management extension.
- Enhancement: show immediate feedback to the user if the `userNamePredicate`
               does not match.
- Enhancement: provide immediate feedback if passwords do not match when changing passwords.
- Change: allow to customize the user label when entering a user through the
          user management extension.
- Change: Remove redundant timeout argument from `tcpconnect`.
- Change: Remove redundant timeout argument from `addConnection`.
- Change: Remove redundant `EmailOptTimeout` option.
- Fix: typo in javascript that results in a crash when exceptions are thrown.
- Fix: prioritization bug in `withExtraAttributes`, to make sure when using
       `withExtraAttributes w x $ withExtraAttribues y z $ editor`, the attributes
       provided by `w` and `x` take precedence in case of a conflict.
- Fix: avoid possible SDS updates using outdated values in lens functions in `updateSharedInformation` and
       `updateInformationWithShared` in rare race conditions by not using local copies in the editor states.

#### 0.15.8

- Fix: prevent error when creating document from file with empty document store.

#### 0.15.7

- Enhancement: optimise `importDocument`, avoid file content to be copied to Clean heap.

#### 0.15.6

- Feature: add `deleteUserIn` and `createUserIn` to `iTasks.Extensions.User`.
           This allows to add users to/delete users from a custom data source containing user accounts.

#### 0.15.5

- Fix: explicitely export generic instances for `Task`, `->` and `Dynamic`, which are exported implicitely when using
       `base` `2.0`.

#### 0.15.4

- Fix: export `JSON` instances of `Timestamp`, `Timespec`, `Tm` and `Clock`, accidentally removed in `0.15.3`.

#### 0.15.3

- Chore: accept `base` `3.0`.

#### 0.15.2

- Feature: add `iTasks.UI.Definition.placeholderAttr` to define placeholder texts for
           `iTasks.UI.Editor.Controls.textField`s.
- Feature: add `VIIA.DataSources.ItemStore.workOnChosenTaskWithSharedChoiceWithSharedLabels` which extends
           `workOnChosenTaskWithSharedChoice` with the possibility to determine labels dynamically based on a shared
           value.

#### 0.15.1

- Fix: error tooltip label width.

### 0.15.0

- Change: do not include `drivers.tar` for Selenium anymore, as it contained part of the ABC interpreter dependency,
          which could cause incompatibility issues, when used with other packages.
- Fix: make Selenium post-installation script usable for Selenium tests in other projects.
- Chore: update node packages.

#### 0.14.5

- Fix: prevent JS errors when using `UI.Editor.Controls.toHtmlEventCall` with events of which the JSON representation
       contains single quotes.

#### 0.14.4

- Fix: record update syntax (accepted by current compiler due to bug).

#### 0.14.3

- Fix: `ChoiceTree` not showing/collapsing directory contents when the +/- button was clicked.

#### 0.14.2

- Feature: add `idAttr` to `UI.Definition`, allowing to set the id attribute of a UI container.
- Fix: situation where an exception was thrown when a tcp connection was closed
  by the client.

#### 0.14.1

- Enhancement: remove HTML ids of choice tree nodes, as they are unused.
- Enhancement: weaken some constraints in WF.Combinators.Core to prevent requiring derivation of generic instances,
               which are never used actually.

## 0.14.0

- Change: switch to `base-compiler 3`; do not require `base-compiler-itasks` anymore. Project have to use `cocl` instead
  of `cocl-itasks` as compiler from now on.
- Change: uniqueness attribute of `Editor`/`LeafEditor` has changes. Extensions defining custom editors may need
  adaption.

#### 0.13.9

- Fix: place 'Date' header before first part of multipart mails.

#### 0.13.8

- Feature: add `==`, `gPrint` and Gast instances for `iTasks.Extensions.Email.Attachment`.
- Fix: `gEditor` implementation for `Bool` when used with e.g. `enterInformation`; use `False` as initial value if
       the editor is non-optional.

#### 0.13.7

- Feature: add `iTask` instances for `Extensions.Email.Attachment`.
- Fix: bug in choiceEditor causing it to break when editing a value using a choice editor.
- Fix: error handling in tcp handlers.
- Fix: issues with sending mails with attachments.

#### 0.13.6

- Fix: `sendEmail` did not work.

#### 0.13.5

- Feature: add `choiceEditorWithCustomLabels`, `multipleChoiceEditor` and `multipleChoiceEditorWithCustomLabels`.

#### 0.13.4

- Fix: definition of `group6`, `buttonbar6` and `buttonbar7`.

#### 0.13.3

- Chore: support gPrint `^3`.

#### 0.13.2

- Feature: add date header to emails sent using `sendEmail`.

#### 0.13.1

- Chore: adapt based on `MaybeError` now being located in `Data.Error.GenJSON`.
- Chore: adapt based on `Map` now being located in `Data.Map.GenJSON`.
- Chore: adapt based on `Set` now being located in `Data.Set.GenJSON`.

## 0.13

- Removed: JSON derives of `MaybeError`, `Set` and `Map` (they are now derived in `Text.GenJSON` instead).
- Chore: allow generic-print 2 alongside generic-print 1.

## 0.12

- Chore: support base `v2.0`.
- Change: removed several extensions, they will be published separately.

#### 0.11.3

- Fix: use the correct instance number in `attach` and `appendTask`.

#### 0.11.2

- Fix: do not delete the IO State when the connection is closed due to an `onShareChange`.
- Fix: gather values for the task result of `tcplisten` before deleting the IO states when the task is destroyed.

#### 0.11.1

- Fix: enabled/visible attributes not being taken into account when using `withExtraAttributes`.
- Fix: fix CSS syntax error in default style.
- Enhancement: use style colors for dropdown controls.

## 0.11.0

- Change: Change some I/O handlers to return a `?l` instead of an `l`.
  This info can be used to determine whether or not to send a refresh.
- Change: rename `createReadOnlySDSError` to `createReadOnlySDS` and remove the original
          `createReadOnlySDSError` function, Add a parameter which indicates if read operations on such an SDS
          should be cached.
- Feature: add `constShareNamed` for making a const share with a provided SDS identity.
- Fix: Prevent crashes when using `select2Dropdown`.
- Fix: Call `afterRender` of client components after their DOM element is inserted.
- Enhancement: The `TaskId` in `IOState` is now optional. If it is `?None`, no
  refresh event is sent.
- Enhancement: reorganise the `TaskEvalInfo`, `TaskEvalState` and `TaskEvalOpts`
  types so that they contains fields that are suitable (e.g. `TaskEvalOpts` now
  only contains read-only fields).
- Enhancement: use brighter colors in the default color scheme.

### 0.10.2

- Fix: `addConnection` now stores the connectionId immediately in the handlemap
  to avoid `` onError` `` being called while there is no ioHandle available in
  said map.

### 0.10.1

- Chore: Update graph-copy dependency

## 0.10.0

- Change (Fix): Redo I/O layer between iTasks and AsyncIO
	- Connections are now indexed by a unique `IOHandle` instead of by `ConnectionId`.
	  that is guaranteed to be unique.
	- I/O state is simplified in IWorld (see [Documentation/Design/IO.md](Documentation/Design/IO.md))
	- `addConnection` now only returns an `IOHandle` (the initial value wasn't
	  used anyway).
	- Move some functionality that required AsyncIO functions from
	  `iTasks.Internal.TaskServer` to `iTasks.Internal.AsyncIO`:
	  `processOnShareChangeOnDestroy` and `emptyAsyncIOWriteQueues`.
- Change: `readRegister` now receives a `TaskContext` as well as a `TaskId`.
- Change: Async SDS interface is made safer (fixed some types) and more
  localised (IO is occuring now in the instance instead of via a chain of
  helper functions).
- Change: `evalTaskInstance` now does not return the last task value (was not
  used anyway and very often wrong [e.g. On a destroy, it yielded NoValue])
- Enhancement: `connect` that handles the initial listeners of the engine now
  uses `addListener` instead of copying its behaviour locally.
- Enhancement: `processOnShareChangeOnDestroy` in the engine now skips calling
  many handlers for certain I/O states.
- Feature: Add debugging functions to `iTasks.Internal.Util` to trace the
  `IOStates` and print `Dynamic` values.
- Enhancement: Make the async task test more robust by letting it stop itself
  if some time is reached and trace the status.

### 0.9.1

- Feature: UserAdmin extension:
           add `activeTokens` lens which given a set of authentication tokens (through the SDS parameter),
           returns the authentication tokens that are currently active as a read value.

### 0.9.0

- Change the SDS semantics. `modifySDS` now returns an auxiliary value and can
  write to the SDS.

  User-facing, the following functions have been added:

  `access`, the task to read, possibly write and produce an auxiliary result an SDS.

  Internally, the following functions have been added/changed:

  `amend`, the internal function to read, possibly write, and produce an auxiliary result an SDS.
  `modifySDS`, now includes an auxiliary result and makes writing optional.
- Adapt `SDSSource` so that it includes an amend function as well, allowing you
  to create SDSs that atomically update and yield a value (see
  `Examples/AtomicInsertSQLite.icl`)

### 0.8.12

- Export `register` and `checkRequests` functions from iTasks.Internal.SDS.
  These are required for implementing your own SDS types (i.e. datatypes that
  implement the Readable, Writeable, etc. classes).

### 0.8.11

- Feature: add `iTasks.Extensions.CSVFile.createExcelCompatibleCsvFile`.

### 0.8.10

- Enhancement: add error information if iTasks unit test crashed when applying events/failing to create the `IWorld`.

### 0.8.9

- Feature: add `UI.Editor.Containers.group6`/`group7` and variants.
- Chore: allow `abc-interpreter` version `^1.2.2`, which fixes the issue of the previous version.

### 0.8.8

- Fix: HTTP server closing connection directly if being idle for longer than the keep-alive time.

### 0.8.7

- Fix: set abc-interpreter dependency to 1.2.0.

### 0.8.6 (BROKEN)

- Fix: prevent infinite stream of generated events when destroying sessions tasks to which was never attached, i.e.
       the first event the task was evaluated with was a `DestroyedEvent`.
- Chore: update node dependencies.

### 0.8.5

- Enhancement: remove redundant `onRefresh` queue from `tcpconnect` task.

### 0.8.4

- Fix: `WF.Tasks.IO`: adapt externalProcess for a bug causing an external
       process to have `NoValue` after the process had terminated.

### 0.8.3

- Chore: adapt iTasks to use AsyncIO package.

### 0.8.2

- Feature: add the `afterRender` event to iTasks components, which is required for JavaScript code run after the
          children of a component are rendered.

### 0.8.1

- Enhancement: the list `specialkeys` in `iTasks.Extensions.SVG.SVGEditor` extended to cover the special keys;
    This is relevant for the `OnKeyDownAttr` attribute function of `Graphics.Scalable` (clean-platform)

## 0.8.0

- Change: replace `withClientSideInit` and `withClientSideInitOnLeafEditor` with a type `JavaScriptInitFunction` and
  corresponding `tune` instances, in a new module `iTasks.UI.JavaScript`.
- Change: Providing `Int` for the port number of the tcpconnect/tcplisten task
          has been replaced for a `Port` newtype.
- Change: The `onData` and `onDisconnect` callbacks of the
          `tcplisten`/`tcpconnect` tasks now accept a connection identifier as
          it's first argument.
- Removed: `serializeForClient` from `iTasks.Internal.Serialization`; use `jsDeserializeGraph` instead.
- Feature: make it possible to tune tasks with client-side initialization code.
- Feature: add `Collapsible` which can make panels collapsible by clicking on their header.
- Enhancement: add `TitleIcon` to be able to add icons to titles. These are added to the left of panel and window
  headers in the default layout, but ignored in tab buttons.
- Enhancement: add `SecondaryTitle` and `SecondaryTitleIcon`, which are like `Title` and `TitleIcon` but of secondary
  importance. The default aligns these on the right in panel and window headers, and ignores them in tab buttons.
- Enhancement: Use System.AsyncIO for network I/O.
    This means that the `select` mechanism of the underlying event loop
    has been replaced for epoll (Linux)/IOCP (Windows) and network I/O is
    provided through these mechanisms instead.

### 0.7.3

- Fix: make sure `externalProcess` task closes pipes.

### 0.7.2

- Feature: added support for handling keyboard events & controlling keyboard focus to SVGEditor.

### 0.7.1

- Feature: enable ABC interpreter GC on idle by default and add option to disable it.
- Fix: Update `WF.Tasks.IO`, fix bug in `externalProcess` causing pipes to not be closed.

## 0.7.0

- Change: adapt `sdsSequence` such that it is only required to specify the parameter for the right SDS if we actually
          perform a read or write operation on that SDS.
- Feature: provide `genShow`, `gPrint` and `gHash` instance for `DynamicEditorValue`.
- Enhancement: remove superfluous `gHash` constraints from SDS related functions.

### 0.6.2

- Fix: add scrollbar to `ChooseFromGrid` selection tasks in some environments
  where it did not appear.
- Fix: Add overflow-y: scroll to `itasks-scroll-content` class
	to make sure a vertical scroll bar is added when the content does not fit
	height-wise.

### 0.6.1

- Feature: add `DisableIoRedirectionToShares` option to `ProcessOption` to disable redirection of stdout/stderr when
           using external process tasks.
- Chore: remove leaflet related packages from package.json.

## 0.6.0

- Change: Removed the iTasks leaflet extension from the SDK repository.
	It is now located in the iTasks-leaflet repository
	(https://gitlab.com/clean-and-itasks/itasks-leaflet).
	The leaflet extension is now published as a nitrile package
	named "itasks-leaflet".
	The `iTasks.Extensions.GIS.Leaflet` module has been renamed to
	`iTasks.Leaflet`.
	The `iTasks.Extensions.GIS.LeafletNavalIcons` module has been renamed to
	`iTasks.LeafletNavalIcons`.

### 0.5.10

- Fix: Make tasks in `iTasks.Extensions.JavaScript` yield stable results.

### 0.5.9

- Feature: Update the user admin extension to allow specifying a custom data source for storing user accounts.

### 0.5.8

- Fix: Leaflet "on map click" events on mobile devices.

### 0.5.7
 - Chore: Make leaflet map functions strict and add type annotations
 - Enhancement: Use CSS variable for color of leaflet tooltip instead of always coloring it orange.
                The default color matches the color of the itasks tooltip.

### 0.5.6

- Enhancement: Add `instance ==` and `instance <` for `URL`.

### 0.5.5

- Enhancement: Adapt leaflet-window CSS to be consistent with the style of iTasks windows.

### 0.5.4

- Enhancement: iTasks.UI.Layout: add `selectActionsWithName UISelection helper function.

### 0.5.3

 - Fix: check if the enabled attribute of input component is null on the client,
        which may happen if the attribute is deleted on the server.

### 0.5.2

- Fix: enabledAttr UIAttribute is now taken into account for html input elements.

### 0.5.1

- Enhancement: Change html representation of dynamic editor lists based on changes to the editor for such lists.

## 0.5.0
- Change: Adapt API for `SDSLensNotify` type to allow to specify that a `SDSLens` should never or always be notified
          when the child of the `SDSLens` is notified.
          to avoid filtering notification registrations when it is not necessary to do so and avoid registering
		  the SDSLens itself.

### 0.4.1
- Enhancement: cache values read from SDS source during a single task evaluation to improve performance and provide a
               consistent view.

## 0.4.0
 - Change: Adapt API for `WF.Tasks.IO.externalProcess` `Extensions.Process.callProcess` and
           `Extensions.Process.callProcessAndCheckReturnCode`.
           Allows providing a custom exit code/signal for terminating the external process when the task is destroyed
           for `callProcess` and `callProcessAndCheckReturnCode`.

### 0.3.1
- Fix: `workOnChosenTaskWithSharedChoice` no longer hides tasks which should be displayed when used in a nested manner.

## 0.3.0

- Fix: Make write type of `htmlViewWithCustomEditEvents` return an optional write value.
       `htmlViewWithCustomEditEvents` would break when used within a composition of editors.

### 0.2.12
- Feature: Add `htmlViewWithCustomEditEvents` editor alongside `toHtmlEventCall` function.
           This provides an interface for sending edit events to the server based on interacting
		   with the HTML that is being shown using the editor (E.g clicking a HTML button).

### 0.2.11
- Enhancement: Leaflet extension: remove workaround which is not required anymore.

### 0.2.10
- Chore: Update Leaflet and pixi.js libraries to include various bugfixes.

### 0.2.9
- Fix: Leaflet extension: prevent warnings about listeners of type `undefined` and possible follow-up issues.
- Fix: Leaflet extension: update marker connectors when collapsing Leaflet windows.
- Fix: Leaflet extension: prevent tooltips not being removed when corresponding marker is.

### 0.2.8
- Feature: add `gEq` instance of `EditorReport`.

### 0.2.7
- Feature: add `callProcessAndCheckReturnCode` checking for return code of called processes.

### 0.2.6
- Chore: update pixi, including some bugfixes.

### 0.2.5
- Fix: `workOnChosenTask` altering attributes of `:: LeafEditor` which affect size do no longer lead to map being drawn incorrectly.

### 0.2.4
- Chore: update `ace-builds` and `moment`.

### 0.2.3
- Fix: use ellipsis truncation for leaflet window titles, fixing a bug where windows couldn't be closed when the title was too long.

### 0.2.2
- Feature: package the itasks selenium driver.

### 0.2.1
- Chore: upgrade base-compiler-itasks version constraint from =1.0.0 to ^1.0.0 || ^2.0.0.

## 0.2.0
- Fix: a list editor without values is valid again.
- Feature: add `maybeToEditorReport :: !(?a) -> EditorReport a`.
- Fix: improve generic `Bool` editor: when the value is required, do not show
  a dropdown with a `Select...` option but use a checkbox.
- Switch to nitrile package manager using semantic versioning.

## 22.5

No changes.

## 22.4

No changes.

## 22.3

### New features

- `Extensions.Editors.DynamicEditor`: Added functionConsDynWithPredOnChildConses :: !DynamicConsId !String !(Int DynamicConsId -> Bool) !Dynamic -> DynamicCons, Gijs Alberts (gijs@gijsalberts.nl), !683
- `UI.Editor.Controls`: Added `textFieldNoValueByDefault :: Editor String (EditorReport String)`, Gijs Alberts (gijs@gijsalberts.nl), !686
- Re-added Leaflet options to hide/show the attribution/zoom control, Steffen Michels (@smichels), !690

### Bug fixes

- `iTasks.Extensions.Admin.UserAdmin`: It's no longer possible to change username of existing users, since this leads to unexpected behavior, Elroy Jumpertz (@ElroyTop), !691
- Strings provided through `Label`, such as `Label "This Is My Text"` are generated as-is, instead of changing the capitalization to `This is my text`, Elroy Jumpertz (@ElroyTop) !694

## 22.2

### New features

- Upgraded Leaflet. Improved performance using Pixi.
  Deleted (unused) fields in Leaflet's `MapOptions` - `attributionControl`, `zoomControl`, `editable`.
  New fields in Leaflet's `MapOptions` - `viewMousePosition`, `viewMeasureDistance`, `viewLegend`.
  New fields in Leaflet's `LeafletMarker` - `rotation`, `tint`.
  Changes in Leaflet's `LeafletMarker` - `title` is now of type `?HtmlTag`.
  Additional dependencies added, which are managed through npm.
  Elroy Jumpertz (@ElroyTop), !677
- Added `choiceEditor` to `UI.Editor.Common`, Elroy Jumpertz (@ElroyTop), !680

## 22.1

### New features
- Added `listConsDynWithOptions :: !Bool !Bool !DynamicConsId !String !Dynamic -> DynamicCons`
  to `Extensions.Editors.DynamicEditor` which allows to specify whether dynamic editor list elements may be removed
  or reordered, Gijs Alberts (gijs@gijsalberts.nl), !658
- Added `gDefault` instance for `:: EmailAddress` to `Extensions.EmailAddress`, Gijs Alberts (gijs@gijsalberts.nl), !661
- Added waitForTimerNSec, Gijs Alberts (gijs@gijsalberts.nl), !667
- Added `defaultEngineCLIOptionsWithExtraOpts :: [OptDescr ((?EngineOptions) -> ?EngineOptions)] a [String] EngineOptions -> MaybeError [String] (a, EngineOptions)` to allow specifying custom command line parameters to iTasks programs in a convenient manner, Gijs Alberts (gijs@gijsalberts.nl), !668
- iTasks.Extensions.DateTime: added `waitForUtcDate :: !Bool !Date -> Task Date` which waits for a date based on UTC time, Gijs Alberts (gijs@gijsalberts.nl), !671

### API changes
- 32-bit support is removed; from now on iTasks supports 64-bit platforms only, Steffen Michels (@smichels), !669

## 22.0

### New features
- Added `dynamicEditorValueContainsConsId :: !(DynamicEditorValue a) !DynamicConsId -> Bool` which returns whether a dynamic editor value contains the specified contructor id to `iTasks.Extensions.Editors.DynamicEditor`, Gijs Alberts (gijs@gijsalberts.nl), !653

## 21.7

### New features
- `iTasks.Testing.Selenium.Interface`: export `expectCSSValue` function, Gijs Alberts (gijs@gijsalberts.nl), !635
- Added `Eastwood.yml` for Clean language server support for the entire iTasks codebase, Elroy Jumpertz (@elroyTop), !639

### API changes
- `iTasks.UI.Layout.Common`: change type of `arrangeAsMenu` from `:: [[Int]] -> LayoutRule` to `:: ![!Menu] -> LayoutRule`, Gijs Alberts (gijs@gijsalberts.nl), !635

### Bug fixes
- fix directory WebResourceCollector places the web content in; this was not consistent with the task server engine for executables with extensions different from .exe, Steffen Michels (@smichels), !645
- fix behaviour of textField/passwordField when entering information with minlength = 0 & fix documentation, Steffen Michels (@smichels), !646

### Other
- 32-bit platforms are not actively supported anymore, Steffen Michels (@smichels), !634
- Tests are now all compiled in the CI pipeline, with some exceptions, Elroy Jumpertz (@elroyTop), !644

## 21.6

### New features
- A user can no longer delete himself using `manageUsers`, Elroy Jumpertz (@elroyTop), !624
- Export `==` instance for `:: EmailAddress`, Gijs Alberts (gijs@gijsalberts.nl), !629
- Export `rolesEditor` in `UserAdmin.dcl`, Gijs Alberts (gijs@gijsalberts.nl), !630
- Export `userRoles` in `WorkflowAdmin` module, Gijs Alberts (gijs@gijsalberts.nl), !631

### Fixed bugs
- Fixed issue where `currentTaskInstanceCookies` aborts in very rare cases due to no matching pattern, leading to
a server crash, Elroy Jumpertz (@elroyTop), !623
- Prevent 100% CPU load by sleeping or shutting down when there are no `IOTask` instances, Elroy Jumpertz (@elroyTop), !626
- Fixed internal exception in `UILoader`, Camil Staps (info@camilstaps.nl), !628

## 21.5

### New features
- Introduced a distinction between invalid and empty editors.
  For this editors have been changed in two ways:
  1. changed the editor to accept optional values in `onRefresh`, just as in `onReset` enabling a soft-reset.
  2. changed the top-level write type of editors used in tasks to a three valued ADT to indicate valid/invalid/empty
  This eliminates several problems with using optional values in editors and setting useful hint and error messages.
  Bas Lijnse (@baslijns), !549
- Changed default generic editor for booleans from checkbox to dropdown, Bas Lijnse (@baslijns), !609

## 21.4

### New features
- Added frontend setting `itasks.settings.error_handler` to customize frontend error reporting (see `WebPublic/index.html`), Camil Staps (info@camilstaps.nl), !601
- Minor additions to Selenium interface: `TabKey`; `getFocusedElement`; `focusElement`; `elementsAreEqual`, Camil Staps (info@camilstaps.nl), !603

## 21.3

### New features
- The frontend function `doEditEvent` now has an alternative with only a value argument. This simplifies the API for custom editors, Camil Staps (info@camilstaps.nl), !591

## 21.2

### New features
- Errors in the WebAssembly ABC interpreter are now sent to the server and logged there, Camil Staps (info@camilstaps.nl), !579

### API changes
- Remove the notify lenses from iTasks.SDS.Combinators.Core.sdsSelect to simplify the implementation and API, Steffen Michels (@smichels), !590
